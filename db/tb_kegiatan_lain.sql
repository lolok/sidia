-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 03:26 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan_lain`
--

CREATE TABLE `tb_kegiatan_lain` (
  `id_kegiatan_lain` int(15) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `institusi` varchar(255) NOT NULL,
  `tempat_waktu` varchar(255) NOT NULL,
  `id_user` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kegiatan_lain`
--

INSERT INTO `tb_kegiatan_lain` (`id_kegiatan_lain`, `kegiatan`, `institusi`, `tempat_waktu`, `id_user`) VALUES
(1, 'Penyuluhan KB', 'Instalasi Farmasi Semarang', 'Yogyakarta, 14 Juli 1783', 2),
(2, 'Kegiatan Pramuka Dasar', 'SMA 3 Semarang', 'Medan, 20 Agustus 1386', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kegiatan_lain`
--
ALTER TABLE `tb_kegiatan_lain`
  ADD PRIMARY KEY (`id_kegiatan_lain`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kegiatan_lain`
--
ALTER TABLE `tb_kegiatan_lain`
  MODIFY `id_kegiatan_lain` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
