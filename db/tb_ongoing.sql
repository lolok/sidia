-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2018 at 03:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_ongoing`
--

CREATE TABLE `tb_ongoing` (
  `id_ongoing` int(15) NOT NULL,
  `nama_penelitian` varchar(255) NOT NULL,
  `desc_ongoing` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ongoing`
--

INSERT INTO `tb_ongoing` (`id_ongoing`, `nama_penelitian`, `desc_ongoing`) VALUES
(11, 'Mantap', 'beevish aperient oxazine feneration encoronet Junco stick subgenerical omniprevalent Polyplectron demand tamarind mobbism Passiflora tagilite Emeline subsequency'),
(12, 'Mantap Juga', 'beevish aperient oxazine feneration encoronet Junco stick subgenerical omniprevalent Polyplectron demand tamarind mobbism Passiflora tagilite Emeline subsequency'),
(17, 'Istimewah Juga', 'Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit AmetLorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet Lorem Ipsum Dolor Sit Amet'),
(21, 'Istimewah Juga', 'beevish aperient oxazine feneration encoronet Junco stick subgenerical omniprevalent Polyplectron demand tamarind mobbism Passiflora tagilite Emeline subsequency');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_ongoing`
--
ALTER TABLE `tb_ongoing`
  ADD PRIMARY KEY (`id_ongoing`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_ongoing`
--
ALTER TABLE `tb_ongoing`
  MODIFY `id_ongoing` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
