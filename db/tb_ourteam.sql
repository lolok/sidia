-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2018 at 03:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_ourteam`
--

CREATE TABLE `tb_ourteam` (
  `id_ourteam` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` enum('Aktif','Tidak Aktif','','') NOT NULL,
  `nip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ourteam`
--

INSERT INTO `tb_ourteam` (`id_ourteam`, `nama`, `status`, `nip`) VALUES
(28, 'Reza Ilham Maulana', 'Aktif', '24010313120028'),
(29, 'Reza Maulana Ilham', 'Aktif', '24010313120038'),
(30, 'Maulana Ilham Ibam', 'Aktif', '24010313120088');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_ourteam`
--
ALTER TABLE `tb_ourteam`
  ADD PRIMARY KEY (`id_ourteam`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_ourteam`
--
ALTER TABLE `tb_ourteam`
  MODIFY `id_ourteam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
