-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 02, 2018 at 09:59 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_pembimbingan`
--

CREATE TABLE `detail_pembimbingan` (
  `id_detail` int(11) NOT NULL,
  `id_pembimbingan` int(11) NOT NULL,
  `nama_mhs` varchar(225) NOT NULL,
  `nim_mhs` varchar(225) NOT NULL,
  `id_user` int(225) NOT NULL,
  `jenis` int(11) NOT NULL,
  `kategori` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `detail_pembimbingan`
--

INSERT INTO `detail_pembimbingan` (`id_detail`, `id_pembimbingan`, `nama_mhs`, `nim_mhs`, `id_user`, `jenis`, `kategori`) VALUES
(3, 0, 'Reza Ilham Maulana', '24010313120028', 0, 0, 0),
(4, 0, 'Ajis Riyananta', '24010313120027', 0, 0, 0),
(5, 0, '', '', 0, 0, 0),
(6, 0, '', '', 0, 0, 0),
(7, 0, '', '', 0, 0, 0),
(8, 0, 'Reza Ilham Maulana', '24010313120028', 0, 0, 0),
(9, 0, 'Reza Ilham Maulana', '24010313120028', 0, 0, 0),
(22, 19, 'Reza Ilham Maulana', '24010313120028', 0, 0, 0),
(23, 19, 'Ajis', '24010313120028', 0, 0, 0),
(24, 19, '', '', 0, 0, 0),
(25, 19, '', '', 0, 0, 0),
(26, 19, '', '', 0, 0, 0),
(27, 19, '', '', 0, 0, 0),
(28, 20, 'Reza Ilham Maulana', '24010313120028', 2, 1, 1),
(29, 20, '', '', 2, 1, 1),
(30, 20, '', '', 2, 1, 1),
(31, 20, '', '', 2, 1, 1),
(32, 20, '', '', 2, 1, 1),
(33, 20, '', '', 2, 1, 1),
(52, 21, 'Ibam', '24010313120028', 2, 2, 1),
(53, 21, 'Ajis', '24010313120028', 2, 2, 1),
(54, 21, 'ibam', '24010313120028', 2, 2, 1),
(55, 21, '', '', 2, 2, 1),
(56, 21, '', '', 2, 2, 1),
(57, 21, '', '', 2, 2, 1),
(64, 22, 'Reza Ilham Maulana', '24010313120028', 2, 3, 1),
(65, 22, 'Ajis Riyananta', '24010414120027', 2, 3, 1),
(66, 22, '', '', 2, 3, 1),
(67, 22, '', '', 2, 3, 1),
(68, 22, '', '', 2, 3, 1),
(69, 22, '', '', 2, 3, 1),
(70, 23, 'Reza Ilham Maulana', '24010313120028', 2, 1, 2),
(71, 23, 'Ajis Riyananta', '24010414120027', 2, 1, 2),
(72, 23, '', '', 2, 1, 2),
(73, 23, '', '', 2, 1, 2),
(74, 23, '', '', 2, 1, 2),
(75, 23, '', '', 2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `file_berkas`
--

CREATE TABLE `file_berkas` (
  `id_berkas` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL,
  `tahun` int(4) NOT NULL,
  `semester` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_cv`
--

CREATE TABLE `file_cv` (
  `id_file_cv` int(15) NOT NULL,
  `id_cv` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_kegiatan`
--

CREATE TABLE `file_kegiatan` (
  `id_file_kegiatan` int(15) NOT NULL,
  `id_kegiatan` int(15) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `path_file` varchar(255) NOT NULL,
  `kategori_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_kerjasama`
--

CREATE TABLE `file_kerjasama` (
  `id_file_kerjasama` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `id_kerjasama` int(11) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_pembimbingan`
--

CREATE TABLE `file_pembimbingan` (
  `id_file_pembimbingan` int(15) NOT NULL,
  `id_pembimbingan` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_penelitian`
--

CREATE TABLE `file_penelitian` (
  `id_file_penelitian` int(15) NOT NULL,
  `id_penelitian` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_pengajaran`
--

CREATE TABLE `file_pengajaran` (
  `id_file_pengajaran` int(15) NOT NULL,
  `id_pengajaran` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `file_pengajaran`
--

INSERT INTO `file_pengajaran` (`id_file_pengajaran`, `id_pengajaran`, `nama_file`, `path_file`, `kategori_file`) VALUES
(5, 13, 'kontrak-pengajaran-210518112547-3', 'assets/file/tridarma/pengajaran/kontrak-pengajaran-210518112547-3.jpeg', 'kontrak');

-- --------------------------------------------------------

--
-- Table structure for table `file_penunjang`
--

CREATE TABLE `file_penunjang` (
  `id_file_penunjang` int(15) NOT NULL,
  `id_penunjang` int(11) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `path_file` varchar(255) NOT NULL,
  `kategori_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_personal`
--

CREATE TABLE `file_personal` (
  `id_file_personal` int(15) NOT NULL,
  `id_personal` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_pkm`
--

CREATE TABLE `file_pkm` (
  `id_file_pkm` int(15) NOT NULL,
  `id_pkm` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `file_publikasi`
--

CREATE TABLE `file_publikasi` (
  `id_file_publikasi` int(15) NOT NULL,
  `id_publikasi` int(15) NOT NULL,
  `nama_file` varchar(225) NOT NULL,
  `path_file` varchar(225) NOT NULL,
  `kategori_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bimbingan`
--

CREATE TABLE `tb_bimbingan` (
  `id_bimbingan` int(15) NOT NULL,
  `nama_mhs` varchar(225) NOT NULL,
  `nim_mhs` int(25) NOT NULL,
  `file_bimbingan` varchar(225) DEFAULT NULL,
  `file_pengujian` varchar(225) DEFAULT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_cv`
--

CREATE TABLE `tb_cv` (
  `id_cv` int(15) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_dosen_matkul`
--

CREATE TABLE `tb_dosen_matkul` (
  `id_dosen_matkul` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_dosen_matkul`
--

INSERT INTO `tb_dosen_matkul` (`id_dosen_matkul`, `id_user`, `id_matkul`, `status`) VALUES
(3, 2, 5, 1),
(10, 2, 11, 1),
(11, 3, 11, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kegiatan`
--

CREATE TABLE `tb_kegiatan` (
  `id_kegiatan` int(15) NOT NULL,
  `namkeg` varchar(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_ebkd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kegiatan`
--

INSERT INTO `tb_kegiatan` (`id_kegiatan`, `namkeg`, `tahun`, `semester`, `id_user`, `status_ebkd`) VALUES
(1, 'A2', 2018, 1, 5, 0),
(2, 'A1', 2018, 1, 5, 0),
(3, 'A1', 2018, 1, 5, 0),
(4, 'A1', 2018, 1, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kerjasama`
--

CREATE TABLE `tb_kerjasama` (
  `id_kerjasama` int(15) NOT NULL,
  `nama_kerjasama` varchar(225) NOT NULL,
  `semester` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_matkul`
--

CREATE TABLE `tb_matkul` (
  `id_matkul` int(15) NOT NULL,
  `kode_matkul` varchar(225) NOT NULL,
  `nama_matkul` varchar(255) NOT NULL,
  `sks_matkul` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(4) NOT NULL,
  `kelas` varchar(2) NOT NULL,
  `jenjang` varchar(3) NOT NULL,
  `mhs` int(11) NOT NULL,
  `dosen` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_matkul`
--

INSERT INTO `tb_matkul` (`id_matkul`, `kode_matkul`, `nama_matkul`, `sks_matkul`, `semester`, `tahun`, `kelas`, `jenjang`, `mhs`, `dosen`) VALUES
(11, '1234', 'Matakuliah Tigaaaa', 3, 1, 2018, 'A', 'D3', 20, 2),
(12, '1234', 'Matakuliah Tiga', 3, 1, 2018, 'A1', 'S1', 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pembimbingan`
--

CREATE TABLE `tb_pembimbingan` (
  `id_pembimbingan` int(15) NOT NULL,
  `kategori` int(15) NOT NULL,
  `jenis` int(11) NOT NULL,
  `jml_mhs` int(11) NOT NULL,
  `tahun` int(15) NOT NULL,
  `semester` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `masa_penugasan` varchar(225) NOT NULL,
  `status_ebkd` int(11) NOT NULL,
  `bukti_penugasan` varchar(225) NOT NULL,
  `bukti_dokumen` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pembimbingan`
--

INSERT INTO `tb_pembimbingan` (`id_pembimbingan`, `kategori`, `jenis`, `jml_mhs`, `tahun`, `semester`, `id_user`, `id_dosen`, `masa_penugasan`, `status_ebkd`, `bukti_penugasan`, `bukti_dokumen`) VALUES
(22, 1, 3, 0, 2018, 1, 2, NULL, '21 May 2018 s.d 01 June 2018', 0, 'Bukti Satu', 'Bukti Dua'),
(23, 2, 1, 0, 2018, 1, 2, NULL, '21 May 2018 s.d 01 June 2018', 0, 'Bukti Satu', 'Bukti Dua');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penelitian`
--

CREATE TABLE `tb_penelitian` (
  `id_penelitian` int(15) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `pencapaian_id` int(11) NOT NULL,
  `nama_penelitian` varchar(225) NOT NULL,
  `status_anggota` varchar(225) NOT NULL,
  `nilai_status_anggota` int(11) DEFAULT NULL,
  `sks_status_anggota` int(11) DEFAULT NULL,
  `kategori` varchar(225) NOT NULL,
  `jenis` varchar(225) NOT NULL,
  `sks` float NOT NULL,
  `status_pencapaian` varchar(225) NOT NULL,
  `nilai_status_pencapaian` int(11) NOT NULL,
  `masa_penugasan` varchar(225) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `status_ebkd` int(11) NOT NULL,
  `bukti_penugasan` varchar(225) NOT NULL,
  `bukti_dokumen` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_penelitian`
--

INSERT INTO `tb_penelitian` (`id_penelitian`, `kategori_id`, `jenis_id`, `anggota_id`, `pencapaian_id`, `nama_penelitian`, `status_anggota`, `nilai_status_anggota`, `sks_status_anggota`, `kategori`, `jenis`, `sks`, `status_pencapaian`, `nilai_status_pencapaian`, `masa_penugasan`, `semester`, `tahun`, `id_user`, `id_dosen`, `status_ebkd`, `bukti_penugasan`, `bukti_dokumen`) VALUES
(8, 0, 0, 0, 0, 'asdaaaaa', 'Ketua', 100, 2, 'Penelitian', 'Penelitian Kelompok', 3, 'Proposal Penelitian', 20, '16 May 2018 s.d 16 May 2018', 1, 2018, 0, NULL, 0, 'asd', 'ads'),
(9, 0, 0, 0, 0, 'penelitian satu', 'Ketua', 100, 2, 'Penelitian', 'Penelitian Kelompok', 3, 'Proposal Penelitian', 20, '16 May 2018 s.d 16 May 2018', 1, 2018, 0, NULL, 0, 'uud 45', 'pancasila'),
(11, 1, 1, 0, 0, 'ibam', 'Ketua', 100, 2, 'Penelitian', 'Penelitian Kelompok', 3, 'Proposal Penelitian', 20, '16 May 2018 s.d 16 May 2018', 1, 2018, 0, NULL, 0, 'lul', 'lul'),
(21, 1, 1, 1, 1, 'penelitian satu', 'Ketua', 100, 2, 'Penelitian', 'Penelitian Kelompok', 3, 'Proposal Penelitian', 20, '21 May 2018 s.d 21 May 2018', 1, 2018, 2, NULL, 0, 'Bukti', 'Bukti satu'),
(22, 2, 2, 1, 6, 'penelitian satu', 'Tidak ada pilihan', 100, 0, 'Menulis Buku', 'Menulis buku yang akan diterbitkan', 3, 'Penulisan Artikel Ilmiah', 100, '21 May 2018 s.d 21 May 2018', 1, 2018, 2, NULL, 0, 'Bukti', 'Bukti satu');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pengajaran`
--

CREATE TABLE `tb_pengajaran` (
  `id_pengajaran` int(15) NOT NULL,
  `id_matkul` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `jml_tatapmuka` int(15) NOT NULL,
  `total_tatapmuka` int(15) NOT NULL,
  `masa_penugasan` varchar(225) NOT NULL,
  `status_ebkd` int(11) NOT NULL,
  `bukti_penugasan` varchar(225) NOT NULL,
  `bukti_dokumen` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pengajaran`
--

INSERT INTO `tb_pengajaran` (`id_pengajaran`, `id_matkul`, `tahun`, `semester`, `id_user`, `id_dosen`, `jml_tatapmuka`, `total_tatapmuka`, `masa_penugasan`, `status_ebkd`, `bukti_penugasan`, `bukti_dokumen`) VALUES
(12, 1, 2018, 1, 2, NULL, 20, 10, '19 May 2018 s.d 19 May 2018', 2, 'Bukti satu', 'Bukti Dua'),
(13, 4, 2018, 1, 2, NULL, 7, 14, '21 May 2018 s.d 21 May 2018', 2, 'Absensi', 'Surat Tugas SK');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penunjang`
--

CREATE TABLE `tb_penunjang` (
  `id_penunjang` int(15) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `jenis_id` int(11) NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `kategori` varchar(225) NOT NULL,
  `jenis` varchar(225) NOT NULL,
  `status_anggota` varchar(225) NOT NULL,
  `sks` float NOT NULL,
  `nampen` varchar(255) NOT NULL,
  `tahun` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_ebkd` int(11) NOT NULL,
  `masa_penugasan` varchar(225) NOT NULL,
  `bukti_dokumen` varchar(225) NOT NULL,
  `bukti_penugasan` varchar(225) NOT NULL,
  `jml_mhs` int(11) DEFAULT NULL,
  `jml_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penunjang`
--

INSERT INTO `tb_penunjang` (`id_penunjang`, `kategori_id`, `jenis_id`, `anggota_id`, `kategori`, `jenis`, `status_anggota`, `sks`, `nampen`, `tahun`, `semester`, `id_user`, `status_ebkd`, `masa_penugasan`, `bukti_dokumen`, `bukti_penugasan`, `jml_mhs`, `jml_dosen`) VALUES
(5, 1, 1, 0, 'Pembinaan Sivitas Akademika', 'Bimbingan akademik', 'undefined', 0, 'Membimbing mahasiswa', 2018, 1, 0, 0, '18 May 2018 s.d 18 May 2018', 'Bukti dua', 'bukti satu', 1, 0),
(22, 1, 1, 1, 'Pembinaan Sivitas Akademika', 'Bimbingan akademik', 'Tidak ada pilihan', 1, 'bimbingan', 2018, 1, 2, 0, '21 May 2018 s.d 21 May 2018', 'bukti dua', 'bukti satu', 8, 0),
(23, 2, 2, 4, 'Administrasi dan Manajemen', 'Jabatan non struktural', 'Kepala Laboratorium', 2, 'ketua', 2018, 1, 2, 0, '21 May 2018 s.d 21 May 2018', 'bukti dua', 'bukti satu', 0, 0),
(24, 2, 2, 1, 'Administrasi dan Manajemen', 'Jabatan non struktural', 'Sekretaris Jurusan/Prodi', 2, 'ketua', 2018, 1, 2, 0, '21 May 2018 s.d 21 May 2018', 'bukti dua', 'bukti satu', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_personal`
--

CREATE TABLE `tb_personal` (
  `id_personal` int(15) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `nosertif` varchar(225) NOT NULL,
  `perting` varchar(225) NOT NULL,
  `status` varchar(5) NOT NULL,
  `alamat` varchar(225) NOT NULL,
  `jurusan` varchar(225) NOT NULL,
  `prodi` varchar(225) NOT NULL,
  `jabatan` varchar(225) NOT NULL,
  `tempat` varchar(225) NOT NULL,
  `s1` varchar(225) NOT NULL,
  `s2` varchar(225) NOT NULL,
  `s3` varchar(225) NOT NULL,
  `ilmu` varchar(225) NOT NULL,
  `nohape` varchar(225) NOT NULL,
  `tanggal` varchar(225) NOT NULL,
  `tahun` int(4) NOT NULL,
  `semester` varchar(15) NOT NULL,
  `id_user` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_personal`
--

INSERT INTO `tb_personal` (`id_personal`, `nama`, `nosertif`, `perting`, `status`, `alamat`, `jurusan`, `prodi`, `jabatan`, `tempat`, `s1`, `s2`, `s3`, `ilmu`, `nohape`, `tanggal`, `tahun`, `semester`, `id_user`) VALUES
(1, 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', '05/29/2018', 2018, '1', 2),
(2, 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', 'as', '05/29/2018', 2018, '1', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pkm`
--

CREATE TABLE `tb_pkm` (
  `id_pkm` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `kategori` varchar(225) NOT NULL,
  `sks` float NOT NULL,
  `anggota_id` int(11) NOT NULL,
  `status_anggota` varchar(225) NOT NULL,
  `nilai_status_anggota` int(11) NOT NULL,
  `pencapaian_id` int(11) NOT NULL,
  `status_pencapaian` varchar(225) NOT NULL,
  `nilai_status_pencapaian` int(11) NOT NULL,
  `nama_pkm` varchar(255) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `masa_penugasan` varchar(225) NOT NULL,
  `bukti_penugasan` varchar(225) NOT NULL,
  `bukti_dokumen` varchar(225) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL,
  `status_ebkd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_pkm`
--

INSERT INTO `tb_pkm` (`id_pkm`, `kategori_id`, `kategori`, `sks`, `anggota_id`, `status_anggota`, `nilai_status_anggota`, `pencapaian_id`, `status_pencapaian`, `nilai_status_pencapaian`, `nama_pkm`, `semester`, `tahun`, `masa_penugasan`, `bukti_penugasan`, `bukti_dokumen`, `id_user`, `id_dosen`, `status_ebkd`) VALUES
(5, 4, 'Memberikan jasa konsultan yang relevan dengan kepakarannya', 3, 1, 'Konsultan Tunggal', 100, 1, 'Tidak ada pilihan', 100, 'Pengabdian satu', 1, 2018, '21 May 2018 s.d 21 May 2018', 'bukti satu', 'bukti dua', 2, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `id_profile` int(15) NOT NULL,
  `nama_web` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `link_fb` varchar(255) NOT NULL,
  `link_twitter` varchar(255) NOT NULL,
  `link_ig` varchar(255) NOT NULL,
  `link_linkedin` varchar(255) NOT NULL,
  `desc_laradem` varchar(999) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`id_profile`, `nama_web`, `email`, `no_hp`, `alamat`, `link_fb`, `link_twitter`, `link_ig`, `link_linkedin`, `desc_laradem`) VALUES
(1, 'Kontoru', 'laradem@info.com', '0299-9000', 'Jalan Prof. Soedarto SH', 'https://loric.com/decider/isazoxy?a=vindicativeness&b=superpersonal#proctotrypoidea', 'https://loric.com/decider/isazoxy?a=vindicativeness&b=superpersonal#proctotrypoidea', 'https://loric.com/decider/isazoxy?a=vindicativeness&b=superpersonal#proctotrypoidea', 'https://loric.com/decider/isazoxy?a=vindicativeness&b=superpersonal#proctotrypoidea', 'converging spondylic bidactyl redhibitory discommendable forelooper coffee succor congregative triserial seabeard unsurveyable deappetizing pepperish drapery osteophone bacteriotropic Sintoism recool indemonstrable loggia ainhum thecaphore unconclusiveconverging spondylic bidactyl redhibitory discommendable forelooper coffee succor congregative triserial seabeard unsurveyable deappetizing pepperish drapery osteophone bacteriotropic Sintoism recool indemonstrable loggia ainhum thecaphore unconclusive');

-- --------------------------------------------------------

--
-- Table structure for table `tb_publikasi`
--

CREATE TABLE `tb_publikasi` (
  `id_publikasi` int(11) NOT NULL,
  `nama_publikasi` varchar(255) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_dosen` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(15) NOT NULL,
  `id_dosen` int(11) NOT NULL,
  `nama` varchar(225) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL,
  `email` varchar(225) NOT NULL,
  `level` int(2) NOT NULL,
  `nip` varchar(225) NOT NULL,
  `foto_user` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `id_dosen`, `nama`, `username`, `password`, `email`, `level`, `nip`, `foto_user`) VALUES
(1, 0, 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@laredem.org', 1, '', ''),
(2, 0, 'Dr. Reza Ilham Maulana', 'ibamaulana', '8104afda63c883a7f12c385b06bbf242', '', 2, '23412341', ''),
(3, 0, 'Prof. Reza Ilham Maulana', '', '', '', 2, '23412341', ''),
(4, 0, 'Reza Ilham Maulana', '', '', '', 2, '24010313120029', ''),
(5, 0, 'dosen', 'dosen', 'ce28eed1511f631af6b2a7bb0a85d636', '', 2, '24010313120028', ''),
(6, 0, 'Reza Ganteng Maulana', 'reza', '81dc9bdb52d04dc20036dbd8313ed055', '', 2, '24010313120028', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_pembimbingan`
--
ALTER TABLE `detail_pembimbingan`
  ADD PRIMARY KEY (`id_detail`);

--
-- Indexes for table `file_berkas`
--
ALTER TABLE `file_berkas`
  ADD PRIMARY KEY (`id_berkas`);

--
-- Indexes for table `file_cv`
--
ALTER TABLE `file_cv`
  ADD PRIMARY KEY (`id_file_cv`);

--
-- Indexes for table `file_kegiatan`
--
ALTER TABLE `file_kegiatan`
  ADD PRIMARY KEY (`id_file_kegiatan`);

--
-- Indexes for table `file_kerjasama`
--
ALTER TABLE `file_kerjasama`
  ADD PRIMARY KEY (`id_file_kerjasama`);

--
-- Indexes for table `file_pembimbingan`
--
ALTER TABLE `file_pembimbingan`
  ADD PRIMARY KEY (`id_file_pembimbingan`);

--
-- Indexes for table `file_penelitian`
--
ALTER TABLE `file_penelitian`
  ADD PRIMARY KEY (`id_file_penelitian`);

--
-- Indexes for table `file_pengajaran`
--
ALTER TABLE `file_pengajaran`
  ADD PRIMARY KEY (`id_file_pengajaran`);

--
-- Indexes for table `file_penunjang`
--
ALTER TABLE `file_penunjang`
  ADD PRIMARY KEY (`id_file_penunjang`);

--
-- Indexes for table `file_personal`
--
ALTER TABLE `file_personal`
  ADD PRIMARY KEY (`id_file_personal`);

--
-- Indexes for table `file_pkm`
--
ALTER TABLE `file_pkm`
  ADD PRIMARY KEY (`id_file_pkm`);

--
-- Indexes for table `file_publikasi`
--
ALTER TABLE `file_publikasi`
  ADD PRIMARY KEY (`id_file_publikasi`);

--
-- Indexes for table `tb_bimbingan`
--
ALTER TABLE `tb_bimbingan`
  ADD PRIMARY KEY (`id_bimbingan`);

--
-- Indexes for table `tb_cv`
--
ALTER TABLE `tb_cv`
  ADD PRIMARY KEY (`id_cv`);

--
-- Indexes for table `tb_dosen_matkul`
--
ALTER TABLE `tb_dosen_matkul`
  ADD PRIMARY KEY (`id_dosen_matkul`);

--
-- Indexes for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  ADD PRIMARY KEY (`id_kegiatan`);

--
-- Indexes for table `tb_kerjasama`
--
ALTER TABLE `tb_kerjasama`
  ADD PRIMARY KEY (`id_kerjasama`);

--
-- Indexes for table `tb_matkul`
--
ALTER TABLE `tb_matkul`
  ADD PRIMARY KEY (`id_matkul`);

--
-- Indexes for table `tb_pembimbingan`
--
ALTER TABLE `tb_pembimbingan`
  ADD PRIMARY KEY (`id_pembimbingan`);

--
-- Indexes for table `tb_penelitian`
--
ALTER TABLE `tb_penelitian`
  ADD PRIMARY KEY (`id_penelitian`);

--
-- Indexes for table `tb_pengajaran`
--
ALTER TABLE `tb_pengajaran`
  ADD PRIMARY KEY (`id_pengajaran`);

--
-- Indexes for table `tb_penunjang`
--
ALTER TABLE `tb_penunjang`
  ADD PRIMARY KEY (`id_penunjang`);

--
-- Indexes for table `tb_personal`
--
ALTER TABLE `tb_personal`
  ADD PRIMARY KEY (`id_personal`);

--
-- Indexes for table `tb_pkm`
--
ALTER TABLE `tb_pkm`
  ADD PRIMARY KEY (`id_pkm`);

--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indexes for table `tb_publikasi`
--
ALTER TABLE `tb_publikasi`
  ADD PRIMARY KEY (`id_publikasi`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_pembimbingan`
--
ALTER TABLE `detail_pembimbingan`
  MODIFY `id_detail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `file_berkas`
--
ALTER TABLE `file_berkas`
  MODIFY `id_berkas` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `file_cv`
--
ALTER TABLE `file_cv`
  MODIFY `id_file_cv` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_kegiatan`
--
ALTER TABLE `file_kegiatan`
  MODIFY `id_file_kegiatan` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_kerjasama`
--
ALTER TABLE `file_kerjasama`
  MODIFY `id_file_kerjasama` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_pembimbingan`
--
ALTER TABLE `file_pembimbingan`
  MODIFY `id_file_pembimbingan` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_penelitian`
--
ALTER TABLE `file_penelitian`
  MODIFY `id_file_penelitian` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_pengajaran`
--
ALTER TABLE `file_pengajaran`
  MODIFY `id_file_pengajaran` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `file_penunjang`
--
ALTER TABLE `file_penunjang`
  MODIFY `id_file_penunjang` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_personal`
--
ALTER TABLE `file_personal`
  MODIFY `id_file_personal` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_pkm`
--
ALTER TABLE `file_pkm`
  MODIFY `id_file_pkm` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_publikasi`
--
ALTER TABLE `file_publikasi`
  MODIFY `id_file_publikasi` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_bimbingan`
--
ALTER TABLE `tb_bimbingan`
  MODIFY `id_bimbingan` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_cv`
--
ALTER TABLE `tb_cv`
  MODIFY `id_cv` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_dosen_matkul`
--
ALTER TABLE `tb_dosen_matkul`
  MODIFY `id_dosen_matkul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_kegiatan`
--
ALTER TABLE `tb_kegiatan`
  MODIFY `id_kegiatan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_kerjasama`
--
ALTER TABLE `tb_kerjasama`
  MODIFY `id_kerjasama` int(15) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_matkul`
--
ALTER TABLE `tb_matkul`
  MODIFY `id_matkul` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_pembimbingan`
--
ALTER TABLE `tb_pembimbingan`
  MODIFY `id_pembimbingan` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tb_penelitian`
--
ALTER TABLE `tb_penelitian`
  MODIFY `id_penelitian` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tb_pengajaran`
--
ALTER TABLE `tb_pengajaran`
  MODIFY `id_pengajaran` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `tb_penunjang`
--
ALTER TABLE `tb_penunjang`
  MODIFY `id_penunjang` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_personal`
--
ALTER TABLE `tb_personal`
  MODIFY `id_personal` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pkm`
--
ALTER TABLE `tb_pkm`
  MODIFY `id_pkm` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_profile`
--
ALTER TABLE `tb_profile`
  MODIFY `id_profile` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_publikasi`
--
ALTER TABLE `tb_publikasi`
  MODIFY `id_publikasi` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
