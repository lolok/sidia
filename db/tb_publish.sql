-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2018 at 09:41 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_publish`
--

CREATE TABLE `tb_publish` (
  `id_publish` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `jenis` varchar(255) NOT NULL,
  `tanggal` date NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_publish`
--

INSERT INTO `tb_publish` (`id_publish`, `judul`, `jenis`, `tanggal`, `publisher`, `link`) VALUES
(13, 'Mantap Juga', 'Buat Baca', '2018-06-05', 'Dr.Reza Ilham', 'http://localhost/sidia/temp/asset/form-tinymce-wysihtml5.html'),
(14, 'Mantap Juga Bor', 'Buat Baca', '2018-06-05', 'Dr.Reza Ilham', 'http://localhost/sidia/temp/asset/form-tinymce-wysihtml5.html'),
(15, 'SAD', 'Renungan', '2018-06-02', 'Dr.Maulana Ilham', 'http://localhost/sidia/temp/asset/form-tinymce-wysihtml5.html'),
(16, 'CHANGE''s', 'Renungan', '2018-06-26', 'Dr.Ilham Reza', 'http://localhost/sidia/temp/asset/form-tinymce-wysihtml5.html'),
(17, 'PRIGEL', 'BERAT', '2018-06-14', 'Dr.Reza Ilham', 'http://localhost/sidia/temp/asset/form-tinymce-wysihtml5.html');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_publish`
--
ALTER TABLE `tb_publish`
  ADD PRIMARY KEY (`id_publish`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_publish`
--
ALTER TABLE `tb_publish`
  MODIFY `id_publish` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
