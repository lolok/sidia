-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2018 at 03:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `file_galery`
--

CREATE TABLE `file_galery` (
  `id_galery` int(15) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `path_file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `file_galery`
--

INSERT INTO `file_galery` (`id_galery`, `nama_file`, `path_file`) VALUES
(4, 'Portfolio Item 1', 'assets/file/berkas/-100618201420-1.jpg'),
(5, 'Portfolio Item 2', 'assets/file/berkas/-100618201426-1.jpg'),
(6, 'Portfolio Item 3', 'assets/file/berkas/-100618201431-1.jpg'),
(7, 'Portfolio Item 4', 'assets/file/berkas/-100618201435-1.jpg'),
(8, 'Portfolio Item 5', 'assets/file/berkas/-100618201446-1.jpg'),
(9, 'Portfolio Item 6', 'assets/file/berkas/-100618201450-1.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `file_galery`
--
ALTER TABLE `file_galery`
  ADD PRIMARY KEY (`id_galery`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file_galery`
--
ALTER TABLE `file_galery`
  MODIFY `id_galery` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
