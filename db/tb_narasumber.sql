-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 02:58 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_narasumber`
--

CREATE TABLE `tb_narasumber` (
  `id_narasumber` int(15) NOT NULL,
  `kegiatan` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `penyelenggara` varchar(255) NOT NULL,
  `tempat_waktu` varchar(255) NOT NULL,
  `id_user` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_narasumber`
--

INSERT INTO `tb_narasumber` (`id_narasumber`, `kegiatan`, `status`, `penyelenggara`, `tempat_waktu`, `id_user`) VALUES
(1, 'Kegiatan Kerja Bakti Masyarakat', 'Volunteer', 'Perum Jati Raya Indah', 'Semarang, 19 Februari 1964', 2),
(3, 'Kegiatan', 'Panitia', 'SD Negeri Keramas', 'Wonosobo, 5 - 18 Mei 1833', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_narasumber`
--
ALTER TABLE `tb_narasumber`
  ADD PRIMARY KEY (`id_narasumber`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_narasumber`
--
ALTER TABLE `tb_narasumber`
  MODIFY `id_narasumber` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
