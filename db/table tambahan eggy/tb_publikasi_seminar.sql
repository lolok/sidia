-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 05:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_publikasi_seminar`
--

CREATE TABLE IF NOT EXISTS `tb_publikasi_seminar` (
`id_publikasi_seminar` int(11) NOT NULL,
  `nama_pertemuan` varchar(225) NOT NULL,
  `judul_artikel` varchar(225) NOT NULL,
  `waktu` varchar(225) NOT NULL,
  `tempat` varchar(225) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_publikasi_seminar`
--

INSERT INTO `tb_publikasi_seminar` (`id_publikasi_seminar`, `nama_pertemuan`, `judul_artikel`, `waktu`, `tempat`, `id_user`) VALUES
(1, 'Rapat Tahunan', 'Sistem Pendukung Keputusan', '2 Juni 2018', 'Universitas Diponegoro Semarang', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_publikasi_seminar`
--
ALTER TABLE `tb_publikasi_seminar`
 ADD PRIMARY KEY (`id_publikasi_seminar`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_publikasi_seminar`
--
ALTER TABLE `tb_publikasi_seminar`
MODIFY `id_publikasi_seminar` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
