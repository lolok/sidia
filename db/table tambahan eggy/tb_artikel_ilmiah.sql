-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 05:11 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel_ilmiah`
--

CREATE TABLE IF NOT EXISTS `tb_artikel_ilmiah` (
`id_artikel_ilmiah` int(11) NOT NULL,
  `judul_artikel` varchar(225) NOT NULL,
  `nama_jurnal` varchar(225) NOT NULL,
  `volume_nomor_tahun` varchar(225) NOT NULL,
  `semester` int(11) NOT NULL,
  `tahun` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_artikel_ilmiah`
--

INSERT INTO `tb_artikel_ilmiah` (`id_artikel_ilmiah`, `judul_artikel`, `nama_jurnal`, `volume_nomor_tahun`, `semester`, `tahun`, `id_user`) VALUES
(1, 'Sistem Pendukung Keputusan', 'Jurnal Thesis 1', 'XII/10/2018', 1, 2017, 0),
(2, 'Sistem Pendukung Keputusan', 'Jurnal Thesis', 'XII/10/2018', 2, 2016, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_artikel_ilmiah`
--
ALTER TABLE `tb_artikel_ilmiah`
 ADD PRIMARY KEY (`id_artikel_ilmiah`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_artikel_ilmiah`
--
ALTER TABLE `tb_artikel_ilmiah`
MODIFY `id_artikel_ilmiah` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
