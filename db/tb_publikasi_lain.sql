-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 04, 2018 at 02:01 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sidia`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_publikasi_lain`
--

CREATE TABLE `tb_publikasi_lain` (
  `id_publikasi_lain` int(15) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `media` varchar(30) NOT NULL,
  `waktu` varchar(50) NOT NULL,
  `id_user` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_publikasi_lain`
--

INSERT INTO `tb_publikasi_lain` (`id_publikasi_lain`, `judul`, `media`, `waktu`, `id_user`) VALUES
(1, 'Seminar Nasional', 'Youtubes', '11 Mei - 26 Februari 2061', 2),
(2, 'Seminar Di Psikologi', 'Koran', '26 januari 995', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_publikasi_lain`
--
ALTER TABLE `tb_publikasi_lain`
  ADD PRIMARY KEY (`id_publikasi_lain`),
  ADD KEY `id_user` (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_publikasi_lain`
--
ALTER TABLE `tb_publikasi_lain`
  MODIFY `id_publikasi_lain` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tb_publikasi_lain`
--
ALTER TABLE `tb_publikasi_lain`
  ADD CONSTRAINT `tb_publikasi_lain_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `tb_user` (`id_user`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
