<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">KODE</th>
                <th class="text-center">MATA KULIAH</th>
                <th class="text-center">JENJANG</th>
                <th class="text-center">KELAS</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->semester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->kode_matkul ?></td>
                <td class="text-center"><?= $value->nama_matkul ?></td>
                <td class="text-center"><?= $value->jenjang ?></td>
                <td class="text-center">Kelas <?= $value->kelas ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_matkul ?>" kategori="kontrak"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_matkul ?>" kategori="absendosen"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Absen Dosen"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_matkul ?>" kategori="absenmhs"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Absen Mahasiswa"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="dosenMatkul" data-toggle="modal" data-target="#dosenMatkulModal" id="<?= $value->id_matkul ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dosen Matakuliah"><i class="ti-user"></i></button></a>
                </td>
                <td class="text-center"><?= $value->tahun ?> - <?= $semester ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id_matkul ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Matakuliah"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_matkul ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Matakuliah"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('MatkulCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editkode_matkul").val(data['kode_matkul']);
                form.find("#editnama_matkul").val(data['nama_matkul']);
                form.find("#editsks").val(data['sks_matkul']);
                form.find("#editjenjang").val(data['jenjang']);
                form.find("#editkelas").val(data['kelas']);
                form.find("#editmhs").val(data['mhs']);
                form.find("#editdosen").val(data['dosen']);
                form.find("#edittahun_matkul").val(data['tahun_matkul']);
                form.find("#editsemester_matkul").val(data['semester_matkul']);
                form.find("#idmatkul").val(data['id_matkul']);
            }
        });
    });
    $('.dosenMatkul').on('click',function(){
        id = $(this).attr('id');
        form = $('#form-dosen');
        $.ajax({
            url:'<?= site_url('MatkulCntrl/getDosen') ?>',
            data:{
                id:id,
            },
            success:function(data){
                $('#data-dosen').html(data);
                form.find('#id_matkul').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('MatkulCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('MatkulCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infokode_matkul").val(data['kode_matkul']);
                form.find("#infonama_matkul").val(data['nama_matkul']);
                form.find("#infosks").val(data['sks_matkul']);
                form.find("#infojenjang").val(data['jenjang']);
                form.find("#infokelas").val(data['kelas']);
                form.find("#infomhs").val(data['mhs']);
                form.find("#infodosen").val(data['dosen']);
                form.find("#infotahun_matkul").val(data['tahun_matkul']);
                form.find("#infosemester_matkul").val(data['semester_matkul']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>