<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">KATEGORI BERKAS:</label>
                            <select type="text" class="form-control" id="kategori" name="kategori" required>
                                <option value="1">General</option>
                                <option value="2">Pengajaran</option>
                                <option value="3">Pembimbingan</option>
                                <option value="4">Penelitian</option>
                                <option value="5">PKM</option>
                                <option value="6">Penunjang</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">NAMA BERKAS:</label>
                            <input type="text" class="form-control" id="nama" name="nama" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="tahun" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="semester" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control select2" id="editmatkul" name="editmatkul" required>
                                <?php foreach($matkul->result() as $val){ ?>
                                    <option value="<?= $val->id_matkul ?>"><?= $val->nama_matkul ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="editdosen" name="editdosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="editdosen" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="editdatepicker1" name="editmasa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">JUMLAH TATAP MUKA:</label>
                            <input type="number" class="form-control" id="editjml_tatapmuka" name="editjml_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TOTAL TATAP MUKA:</label>
                            <input type="number" class="form-control" id="edittotal_tatapmuka" name="edittotal_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edittahun" name="edittahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="editsemester" name="editsemester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control" id="editbukti_penugasan" name="editbukti_penugasan" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control" id="editbukti_dokumen" name="editbukti_dokumen" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idpengajaran">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control select2" id="infomatkul" name="infomatkul" disabled>
                                <?php foreach($matkul->result() as $val){ ?>
                                    <option value="<?= $val->id_matkul ?>"><?= $val->nama_matkul ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="infodosen" name="infodosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infodosen" name="infodosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="editdatepicker1" name="infomasa_penugasan" disabled> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">JUMLAH TATAP MUKA:</label>
                            <input type="number" class="form-control" id="infojml_tatapmuka" name="infojml_tatapmuka" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TOTAL TATAP MUKA:</label>
                            <input type="number" class="form-control" id="infototal_tatapmuka" name="infototal_tatapmuka" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun" name="infotahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester" name="infosemester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control" id="infobukti_penugasan" name="infobukti_penugasan" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control" id="infobukti_dokumen" name="infobukti_dokumen" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-pengajaran">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>