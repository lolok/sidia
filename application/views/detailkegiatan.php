<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('temp/plugins/images/undip.png') ?>">
    <title><?= $title ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('temp/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') ?>" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('temp/plugins/bower_components/custom-select/custom-select.css'); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('temp/plugins/bower_components/switchery/dist/switchery.min.css'); ?>" rel="stylesheet" />
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-select/bootstrap-select.min.css'); ?>" rel="stylesheet" />
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
    <link href="<?= base_url('temp/plugins/bower_components/multiselect/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="<?= base_url('temp/asset/css/animate.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url('temp/asset/css/style.css') ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url('temp/asset/css/colors/blue.css') ?>" id="theme" rel="stylesheet">
    <style type="text/css">
        .clockpicker-popover {
            z-index: 999999 !important;
        };
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg" > 
                                <div class="overlay-box" style="background-color: #2087e5; opacity: 1;">
                                    <div class="user-content">
                                        <i class="mdi mdi-calendar-text" style="color: white;font-size: 90px"></i>
                                        <h4 class="text-white">Detail Kegiatan</h4></div>
                                </div>
                            </div>
                            <div class="user-btm-box" style="margin-bottom: -10px ">
                            <?php foreach ($data->result() as $key) {?>
                            <form>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Nama:</label>
                                        <input type="text" class="form-control" id="nama" name="nama" value="<?= $key->nama ?>" disabled> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nama" class="control-label">Jenis Kegiatan:</label>
                                        <input type="text" class="form-control" id="nama" name="nama" disabled value="<?= $key->kategori ?>">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="tanggal" class="control-label">Tanggal:</label>
                                        <input type="text" class="form-control" id="golongan" name="golongan" disabled value="<?= $key->tanggal ?>"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="editjam" class="control-label">Jam:</label>
                                        <input type="text" class="form-control" id="npwp" name="npwp" disabled value="<?= $key->jam ?>"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nohp" class="control-label">Lokasi:</label>
                                        <input type="text" class="form-control" id="satuan" name="satuan" disabled value="<?= $key->lokasi ?>"> 
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="nohp" class="control-label">Dasar Hukum:</label>
                                        <input type="text" class="form-control" id="jml_brutto" name="jml_brutto" disabled value="<?= $key->dasar_hukum ?>"> 
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Isian Kegiatan </h3>
                                </div>
                                <div class="col-md-8">
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addIsianModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Isian" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a>
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Isian" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a>
                                </div>
                            </div>
                            <hr>
                            <div id="isianbody">
                                
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->             
                <?php $this->load->view('modal-isian') ?>          
                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('temp/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url('temp/asset/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('temp/asset/js/waves.js') ?>"></script>
    <!--Counter js -->
    <script src="<?= base_url('temp/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/counterup/jquery.counterup.min.js') ?>"></script>
    <!--Morris JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/raphael/raphael-min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/morrisjs/morris.js') ?>"></script>
    <!-- chartist chart -->
    <script src="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>
    <!-- Calendar JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <script src='<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>'></script>
    <script src="<?= base_url('temp/plugins/bower_components/calendar/dist/cal-init.js') ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('temp/asset/js/custom.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.js')?>"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- Select Script -->
    <script src="<?= base_url('temp/plugins/bower_components/switchery/dist/switchery.min.js'); ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/custom-select/custom-select.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-select/bootstrap-select.min.js'); ?>" type="text/javascript"></script>
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'); ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js'); ?>" type="text/javascript"></script>
    <script type="text/javascript" src="<?= base_url('temp/plugins/bower_components/multiselect/js/jquery.multi-select.js'); ?>"></script>
    <!-- Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/toast-master/js/jquery.toast.js') ?>"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/clockpicker/dist/bootstrap-clockpicker.js') ?>"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') ?>"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') ?>"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
    <script src="<?= base_url('temp/asset/js/cbpFWTabs.js') ?>"></script>
    <script type="text/javascript">
    (function() {
        [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
            new CBPFWTabs(el);
        });
    })();
    </script>
    <!-- Datatable -->
    <script type="text/javascript">
    var notification = {
        _toast : function(heading, text, icon){
            $.toast({
                heading: heading,
                text: text,
                position: 'top-right',
                loaderBg: '#fff',
                icon: icon,
                hideAfter: 3500
            });
        }
    };
    function getIsian() {
        var id = <?= $id ?>;
        $.ajax({
            url:'<?= site_url('KegCntrl/getIsian') ?>',
            data:{
                id : id  
            },
            success:function(data){
                $('#isianbody').html(data);
            }
        });
    };
    $(document).ready(function() {
        var id = <?= $id ?>;
        getIsian();
        $('#add-isian').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '<?= site_url('KegCntrl/tambahIsian') ?>',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(){
                    getIsian();
                    $('#addIsianModal').modal('hide');
                    notification._toast('Success','Input Data Isian','success');
                }
            });
        });
        $('#edit-isian').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '<?= site_url('KegCntrl/editIsian/') ?>',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(){
                    getIsian();
                    $('#editIsianModal').modal('hide');
                    notification._toast('Success','Update Data Isian','success');
                }
            });
        });
        $('#hapus-isian').click(function(){
            $.ajax({
                url:'<?= site_url('KegCntrl/deleteIsian/') ?>',
                data:{
                    id:idisian
                },
                success:function(data){
                    getIsian();
                    $('.hapusIsianModal').modal('hide');
                    notification._toast('Success','Delete Data Isian','success');
                }
            });
        });
        $('.golongan').change(function(){
            val = $('.add-brutto').val();
            gol = $(this).val();
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.add-pajak').val(paj);
            $('.add-netto').val(net);
        });
        $('.add-uraian').keyup(function(){
            qty = $(this).val();
            sat = $('.add-satuan').val();
            gol = $('.golongan').val();
            val = qty * sat;
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.add-brutto').val(val);
            $('.add-pajak').val(paj);
            $('.add-netto').val(net);
        });
        $('.add-satuan').keyup(function(){
            qty = $('.add-uraian').val();
            sat = $(this).val();
            gol = $('.golongan').val();
            val = qty * sat;
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.add-brutto').val(val);
            $('.add-pajak').val(paj);
            $('.add-netto').val(net);
        });
        $('.edit-golongan').change(function(){
            val = $('.edit-brutto').val();
            gol = $(this).val();
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.edit-pajak').val(paj);
            $('.edit-netto').val(net);
        });
        $('.edit-uraian').keyup(function(){
            qty = $(this).val();
            sat = $('.edit-satuan').val();
            gol = $('.edit-golongan').val();
            val = qty * sat;
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.edit-brutto').val(val);
            $('.edit-pajak').val(paj);
            $('.edit-netto').val(net);
        });
        $('.edit-satuan').keyup(function(){
            qty = $('.edit-uraian').val();
            sat = $(this).val();
            gol = $('.edit-golongan').val();
            val = qty * sat;
            if (gol === '1'){
                paj = val * 0 / 100;
            }else if (gol === '2'){
                paj = val * 0 / 100;
            }else if (gol === '3'){
                paj = val * 5 / 100;
            }else if (gol === '4'){
                paj = val * 15 / 100;
            }else if (gol === '5'){
                paj = val * 5 / 100;
            }
            net = val+paj;
            $('.edit-brutto').val(val);
            $('.edit-pajak').val(paj);
            $('.edit-netto').val(net);
        });
        jQuery('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.atkpicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.prlngkpnpicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.konsumsipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.mulaipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.selesaipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        $(".select2").select2();
        $('.clockpicker').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $('#editjam').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'KegCntrl/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editnama").val(data['nama']);
                    $("#edittanggal").val(data['tanggal']);
                    $("#editjam").val(data['jam']);
                    $("#editlokasi").val(data['lokasi']);
                    $("#editdashuk").val(data['dashuk']);
                    $("#editkegiatan").val(data['katkeg']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'KegCntrl/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
        });

        $(".hapus").click(function(){
            id = $(this).attr('id');
        });
                                        

        $("#hapusKeg").click(function(){
            $.ajax({
                url:'KegCntrl/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            });
        });
        //honor script
        // 
        //end honor
    });
    </script>
</body>

</html>
