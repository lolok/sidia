<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('temp/plugins/images/undip.png') ?>">
<title>SIDIA | <?= $title ?></title>
<!-- Bootstrap Core CSS -->
<link href="<?= base_url('temp/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
<!-- Menu CSS -->
<link href="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
<!-- toast CSS -->
<link href="<?= base_url('temp/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
<!-- morris CSS -->
<link href="<?= base_url('temp/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
<!-- chartist CSS -->
<link href="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
<!-- Color picker plugins css -->
<link href="<?= base_url('temp/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') ?>" rel="stylesheet">
<!-- Date picker plugins css -->
<link href="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('temp/plugins/bower_components/custom-select/custom-select.css'); ?>" rel="stylesheet" type="text/css" />
<link href="<?= base_url('temp/plugins/bower_components/switchery/dist/switchery.min.css'); ?>" rel="stylesheet" />
<link href="<?= base_url('temp/plugins/bower_components/bootstrap-select/bootstrap-select.min.css'); ?>" rel="stylesheet" />
<link href="<?= base_url('temp/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
<link href="<?= base_url('temp/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
<link href="<?= base_url('temp/plugins/bower_components/multiselect/css/multi-select.css'); ?>" rel="stylesheet" type="text/css" />
<!-- Daterange picker plugins css -->
<link href="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
<link href="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
<!-- Calendar CSS -->
<link href="<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
<!-- animation CSS -->
<link href="<?= base_url('temp/asset/css/animate.css') ?>" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?= base_url('temp/asset/css/style.css') ?>" rel="stylesheet">
<!-- color CSS -->
<link href="<?= base_url('temp/asset/css/colors/green-dark.css') ?>" id="theme" rel="stylesheet">
<style type="text/css">
    .clockpicker-popover {
        z-index: 999999 !important;
    };
</style>