<div class="table-responsive" id="div-atk">
    <table class="display nowrap" cellspacing="0" width="100%" id="table-data">
        <thead>
            <tr>
                <th class="text-center">NO.</th>
                <th class="text-center">NAMA FILE</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i = 0;
            foreach($tabel->result() as $value){
            $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?></td>
                <td class="text-center"><?= $value->nama_file ?></td>
                <td class="text-center">
                    <a href="<?= site_url('')?><?= $value->path_file?> " class="editData" id="<?= $value->id_galery ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                    <a href="#" class="hapusFileData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_galery ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus File"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.hapusFileData').on('click',function(){
        id = $(this).attr('id');
    });
    //atur data table buat tabel
    $('#table-data').DataTable({   
    });
</script>