<div class="table-responsive" id="div-atk">
    <table id="tabel-pengabdian" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th colspan="2" class="text-center">KINERJA</th>
                <th rowspan="2" class="text-center">STATUS</th>
                <th rowspan="2" class="text-center">BERKAS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
                <th class="text-center">BUKTI KINERJA</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <!-- Pengajaran -->
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                $total = $value->nilai_status_anggota/100*$value->nilai_status_pencapaian/100*$value->sks;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left"><?= $value->kategori ?> "<?= $value->nama_pkm ?>" <b>( <?= $value->nilai_status_anggota?>% x <?=$value->nilai_status_pencapaian?>% x <?=$value->sks;?> )</b></td>
                <td class="text-center"><?= $value->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $value->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengabdian" status="0" tabel-name="tb_pkm" id-name="id_pkm" data-title="Status E-BKD" data-id="<?= $value->id_pkm ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($value->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengabdian" status="1" tabel-name="tb_pkm" id-name="id_pkm" data-title="Status E-BKD" data-id="<?= $value->id_pkm ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($value->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengabdian" status="2" tabel-name="tb_pkm" id-name="id_pkm" data-title="Status E-BKD" data-id="<?= $value->id_pkm ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($value->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengabdian" status="3" tabel-name="tb_pkm" id-name="id_pkm" data-title="Status E-BKD" data-id="<?= $value->id_pkm ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($value->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengabdian" status="4" tabel-name="tb_pkm" id-name="id_pkm" data-title="Status E-BKD" data-id="<?= $value->id_pkm ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkasDokumen" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_pkm ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumen Berkas"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editmatkul").select2('val',data['id_matkul']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editdatepicker1").val(data['masa_penugasan1']);
                form.find("#editdatepicker2").val(data['masa_penugasan2']);
                form.find("#editjml_tatapmuka").val(data['jml_tatapmuka']);
                form.find("#edittotal_tatapmuka").val(data['total_tatapmuka']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#idpengajaran").val(data['id_pengajaran']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_pengabdian').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
        tabel = $(this).attr('tabel-name');
        idname = $(this).attr('id-name');
        kategori = 3;
        $('#status').val(status);
    });
    $('.fileBerkasDokumen').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PkmCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['id_matkul']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-pengabdian').DataTable({
        ordering: false,   
        columns: [
            null,
            { "width": "30%" },
            { "width": "15%" },
            { "width": "5%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "5%" },
            null,
            null
        ]
    });
    $(".select2").select2();
    
</script>