<div class="table-responsive" id="div-honor">
    <table id="tabel-isian" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Golongan</th>
                <th>Kepanitiaan</th>
                <th>Netto</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($isian->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->nama_isian ?></td>
                <td><?= $value->nama_golongan ?></td>
                <td><?= $value->kategori ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editIsian" data-toggle="modal" data-target="#editIsianModal" id="<?=htmlspecialchars($value->id_isian); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Isian"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusIsian" data-toggle="modal" data-target=".hapusIsianModal" id="<?=htmlspecialchars($value->id_isian); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Isian"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoIsian" data-toggle="modal" data-target="#infoIsianModal" id="<?=htmlspecialchars($value->id_isian); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Isian"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editIsian').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-isian');
        $.ajax({
            url:'<?= site_url('KegCntrl/getDataIsian') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama").val(data['nama_isian']);
                form.find("#editkategori").val(data['id_jabatan']);
                form.find("#editgolongan").val(data['golongan']);
                form.find("#editnpwp").val(data['npwp']);
                form.find("#editsatuan").val(data['satuan']);
                form.find("#edituraian").val(data['uraian']);
                form.find("#editbrutto").val(data['jml_brutto']);
                form.find("#editpajak").val(data['pajak']);
                form.find("#editnetto").val(data['jml_netto']);
                form.find("#idisian").val(data['id_isian']);
            }
        });
    });
    $('.hapusIsian').on('click',function(){
        idisian = $(this).attr('id');
    });
    $('.infoIsian').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-isian');
        $.ajax({
            url:'<?= site_url('KegCntrl/getDataIsian') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama").val(data['nama_isian']);
                form.find("#infojabatan").val(data['id_jabatan']);
                form.find("#infogolongan").val(data['golongan']);
                form.find("#infonpwp").val(data['npwp']);
                form.find("#infosatuan").val(data['satuan']);
                form.find("#infobrutto").val(data['jml_brutto']);
                form.find("#infopajak").val(data['pajak']);
                form.find("#infonetto").val(data['jml_netto']);
                form.find("#idisian").val(data['id_isian']);
            }
        });
    });
    $('#tabel-isian').DataTable({  
    });
</script>