<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">JUDUL ARTIKEL</th>
                <th class="text-center">NAMA JURNAL</th>
                <th class="text-center">VOLUME/NOMOR/TAHUN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama_user ?></td>
                <td class="text-center"><?= $value->judul_artikel ?></td>
                <td class="text-center"><?= $value->nama_jurnal ?></td>
                <td class="text-center"><?= $value->volume_nomor_tahun ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->tahun ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editArtikelIlmiahModal" id="<?= $value->id ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Publikasi Artikel Ilmiah"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Publikasi Artikel Ilmiah"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('ArtikelIlmiahCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#edit_judul_artikel").val(data['judul_artikel']);
                form.find("#edit_nama_jurnal").val(data['nama_jurnal']);
                form.find("#edit_volume_nomor_tahun").val(data['volume_nomor_tahun']);
                form.find("#edit_semester").val(data['semester']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#id_artikel_ilmiah").val(data['id_artikel_ilmiah']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('ArtikelIlmiahCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonip").val(data['nip']);
                form.find("#infonama").val(data['nama']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>