<table class="table" id="table-file">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Dosen</th>
            <th>Status</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if(!empty($data_dosen->result())){
        $i = 0;
        foreach($data_dosen->result() as $value){
        $i++;
        if ($value->status == 1) {
            $status = 'Koordinator';
        }else{
            $status = 'Anggota';
        }
        ?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->nama ?></td>
            <td><?= $status ?></td>
            <td>
                <a href="#" class="hapusFileData" data-toggle="modal" data-target=".hapusDosenModal" id="<?= $value->id_dosen_matkul ?>" idmatkul="<?= $value->id_matkul ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus File"><i class="icon-trash"></i></button></a>
            </td>
        </tr>
        <?php }
        }else{ ?>
        <tr>
            <td colspan="4" class="text-center">No Data Available</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script type="text/javascript">
    $('.hapusFileData').on('click',function(){
        id = $(this).attr('id');
        idmatkul = $(this).attr('idmatkul');
    });
</script>
