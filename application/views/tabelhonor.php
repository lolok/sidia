<div class="table-responsive" id="div-honor">
    <table id="tabel-kepanitiaan" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Golongan</th>
                <th>Kepanitiaan</th>
                <th>Netto</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($honor->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->nama_honor ?></td>
                <td><?= $value->nama_golongan ?></td>
                <td><?= $value->kategori ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editHonor" data-toggle="modal" data-target="#editHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Honor"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusHonor" data-toggle="modal" data-target=".hapusHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Honor"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoHonor" data-toggle="modal" data-target="#infoHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Honor"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editHonor').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-honor');
        $.ajax({
            url:'<?= site_url('HonorCntrl/getDataHonor') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama").val(data['nama_honor']);
                form.find("#editkategori").val(data['id_kepanitiaan']);
                form.find("#editgolongan").val(data['golongan']);
                form.find("#editnpwp").val(data['npwp']);
                form.find("#edituraian").val(data['uraian']);
                form.find("#editsatuan").val(data['satuan']);
                form.find("#editbrutto").val(data['jml_brutto']);
                form.find("#editpajak").val(data['pajak']);
                form.find("#editnetto").val(data['jml_netto']);
                form.find("#idhonor").val(data['id_honor']);
            }
        });
    });
    $('.hapusHonor').on('click',function(){
        idhonor = $(this).attr('id');
    });
    $('.infoHonor').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-honor');
        $.ajax({
            url:'<?= site_url('HonorCntrl/getDataHonor') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama").val(data['nama_honor']);
                form.find("#infokategori").val(data['id_kepanitiaan']);
                form.find("#infogolongan").val(data['golongan']);
                form.find("#infonpwp").val(data['npwp']);
                form.find("#infouraian").val(data['uraian']);
                form.find("#infosatuan").val(data['satuan']);
                form.find("#infobrutto").val(data['jml_brutto']);
                form.find("#infopajak").val(data['pajak']);
                form.find("#infonetto").val(data['jml_netto']);
                form.find("#idhonor").val(data['id_honor']);
            }
        });
    });
    $('#tabel-kepanitiaan').DataTable({  
    });
</script>