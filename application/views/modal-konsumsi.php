<div id="addKonsumsiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Konsumsi</h4> </div>
            <div class="modal-body">
                <form id="add-konsumsi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="nama_konsumsi" name="nama_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control konsumsipicker" id="tanggal_konsumsi" name="tanggal_konsumsi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_konsumsi" name="satuan_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_konsumsi" name="jumlah_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control numeric add-total" id="total_konsumsi" name="jumlahakhir_konsumsi" onkeydown="return false"> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusKonsumsiModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Perlengkapan</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus perlengkapan ?</h4>
                <small style="color: red">semua data perlengkapan akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-konsumsi">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editKonsumsiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Konsumsi</h4> </div>
            <div class="modal-body">
                <form id="edit-konsumsi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="editnama_konsumsi" name="editnama_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control konsumsipicker" id="edittanggal_konsumsi" name="edittanggal_konsumsi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric edit-satuan" id="editsatuan_konsumsi" name="editsatuan_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric edit-jumlah" id="editjumlah_konsumsi" name="editjumlah_konsumsi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control numeric edit-total" id="editjumlahakhir_konsumsi" name="editjumlahakhir_konsumsi" onkeydown="return false">
                        </div>
                    </div>
                    <input type="hidden" name="idkonsumsi" id="idkonsumsi">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoKonsumsiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info Konsumsi</h4> </div>
            <div class="modal-body">
                <form id="info-konsumsi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="infonama_konsumsi" name="infonama_konsumsi" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control konsumsipicker" id="infotanggal_konsumsi" name="infotanggal_konsumsi" onkeydown="return false" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control" id="infosatuan_konsumsi" name="infosatuan_konsumsi" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="infojam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control" id="infojumlah_konsumsi" name="infojumlah_konsumsi" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control" id="infojumlahakhir_konsumsi" name="infojumlahakhir_konsumsi" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>