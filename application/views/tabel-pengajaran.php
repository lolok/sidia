<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">MATA KULIAH</th>
                <th class="text-center">SKS</th>
                <th class="text-center">MASA PENUGASAN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $value->matkul ?></td>
                <td class="text-center"><?= $value->sks ?> SKS</td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->datatahun ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->idmatkul ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Pengajaran"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Pengajaran"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find(".editselect_dosen").select2('val', data['id_user']);
                form.find("#editmatkul").select2('val', data['id_matkul']);
                form.find("#editdatepicker1").val(data['masa_penugasan']);
                form.find("#editjml_tatapmuka").val(data['jml_tatapmuka']);
                form.find("#edittotal_tatapmuka").val(data['total_tatapmuka']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#editpenugasan").select2('val', data['bukti_penugasan']);
                form.find("#editdokumen").select2('val', data['bukti_dokumen']);
                form.find("#idpengajaran").val(data['id_pengajaran']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getFileTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['id_matkul']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#editdatepicker1").val(data['masa_penugasan']);
                form.find("#infojml_tatapmuka").val(data['jml_tatapmuka']);
                form.find("#infototal_tatapmuka").val(data['total_tatapmuka']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
                form.find("#infobukti_penugasan").val(data['bukti_penugasan']);
                form.find("#infobukti_dokumen").val(data['bukti_dokumen']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>