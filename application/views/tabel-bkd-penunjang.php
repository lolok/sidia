<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA PENUNJANG</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nampen ?></td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->datatahun ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_penunjang" status="1" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".activeModal" data-toggle="modal"><span class="label label-danger label-rouded" style="">Tidak Aktif</span></a>
                    <?php }else{?>
                        <a href="" class="dotip aktif_penunjang" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Aktif</span></a>
                    <?php }?>   
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnampen").val(data['nampen']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#id_penunjang").val(data['id_penunjang']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_penunjang').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['nampen']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>