<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th colspan="2" class="text-center">KINERJA</th>
                <th rowspan="2" class="text-center">STATUS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
                <th class="text-center">BUKTI DOKUMEN</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->jenjang == 'D3') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S1') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S2'){
                    if ($value->mhs <= 25) {
                        $persen = 100;
                    }else if($value->mhs > 25 && $value->mhs <= 50){
                        $persen = 150;
                    }else if($value->mhs > 50 && $value->mhs <= 75){
                        $persen = 200;
                    }
                }
                $total = $value->jml_tatapmuka/$value->total_tatapmuka*$value->sks*$persen/100;

            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Memberikan kuliah pada jenjang <?= $value->jenjang ?> mata kuliah <?= $value->matkul ?>, kelas <?= $value->kelas ?>, <?= $value->sks ?> SKS, <?= $value->dosen ?> dosen, <?= $value->jml_tatapmuka ?> tatap muka dari <?= $value->total_tatapmuka ?>, jumlah mahasiswa <?= $value->mhs ?>. <b>( <?= $value->jml_tatapmuka ?>/<?= $value->total_tatapmuka ?> x <?= $value->sks ?> sks x <?= $persen ?>% )</b></td>
                <td class="text-center"><?= $value->bukti_penugasan ?></td>
                <td class="text-center"><?= $total ?></td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $value->bukti_dokumen ?></td>
                <td class="text-center"><?= $total ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".activeModal" data-toggle="modal"><span class="label label-danger label-rouded" style="">Selesai</span></a>
                    <?php }else if($value->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($value->status_ebkd == 2){?>
                    <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($value->status_ebkd == 3){?>   
                    <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($value->status_ebkd == 4){?>
                    <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editmatkul").select2('val',data['id_matkul']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editdatepicker1").val(data['masa_penugasan1']);
                form.find("#editdatepicker2").val(data['masa_penugasan2']);
                form.find("#editjml_tatapmuka").val(data['jml_tatapmuka']);
                form.find("#edittotal_tatapmuka").val(data['total_tatapmuka']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#idpengajaran").val(data['id_pengajaran']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_pengajaran').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
        kategori = 1;
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['id_matkul']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({
        ordering: false,   
        columns: [
            null,
            { "width": "25%" },
            { "width": "15%" },
            { "width": "5%" },
            { "width": "15%" },
            { "width": "10%" },
            { "width": "10%" },
            null
        ]
    });
    $(".select2").select2();
    
</script>