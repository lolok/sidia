<div class="modal fade hapusModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">HAPUS <?= $title ?></h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus data ?</h4>
                <small style="color: red">semua data akan terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-data">Hapus</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade hapusModalnf" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">HAPUS <?= $title ?></h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus data ?</h4>
                <small style="color: red">semua data akan terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-datanf">Hapus</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade hapusFileModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">HAPUS <?= $title ?></h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus data ?</h4>
                <small style="color: red">semua data akan terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-datafile">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusDosenModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">HAPUS DOSEN</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus dosen ?</h4>
                <small style="color: red">semua data akan terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-dosen">Hapus</a>
            </div>
        </div>
    </div>
</div>


<div class="modal fade activeModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">Input Data ke BKD</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk input data ke BKD?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-info waves-effect waves-light" id="activeButton">Aktif</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade deactiveModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">Hapus Data dari BKD</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus data dari BKD?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="deactiveButton">Non Aktifkan</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade statusModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h5 class="modal-title">Ubah status BKD</h5> </div>
            <div class="modal-body">
                <div class="form-group">
                    <label>Status</label>
                    <select type="text" class="form-control" id="status" name="">
                        <option value="0">Selesai</option>
                        <option value="1">Lanjutkan</option>
                        <option value="2">Gagal</option>
                        <option value="3">Lainnya</option>
                        <option value="4">Beban Lebih</option>
                    </select> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-success waves-effect waves-light" id="statusButton">Ubah</a>
            </div>
        </div>
    </div>
</div>