<table id="tabel-kepanitiaan-after" class="display nowrap" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Kategori</th>
            <th>Golongan</th>
            <th>NPWP</th>
            <th>TTD</th>
            <th>Aksi</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $no=0; 
        foreach ($honor->result() as $value) {
        $no++;    ?>
        <tr>
            <td><?= $no ?></td>
            <td><?= $value->nama ?></td>
            <td><?= $value->kategori ?></td>
            <td><?= $value->golongan ?></td>
            <td><?= $value->npwp ?></td>
            <td><a href="#" class="hapus" data-toggle="modal" data-target=".infoHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="TTD"><i class="ti-more" ></i></button></a></td>
            <td>
                <a href="#" class="edit" data-toggle="modal" data-target="#editHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Honorer"><i class="ti-pencil-alt"></i></button></a>
                <a href="#" class="hapus" data-toggle="modal" data-target=".hapusHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Honorer"><i class="icon-trash"></i></button></a>
                 <a href="#" class="hapus" data-toggle="modal" data-target=".infoHonorModal" id="<?=htmlspecialchars($value->id_honor); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Honorer"><i class="ti-more" ></i></button></a>
            </td>
        </tr>
        <?php } ?>
    </tbody>
</table>

