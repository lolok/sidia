<table class="table" id="table-file">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama File</th>
            <th>File</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        if(!empty($tabel_file->result())){
        $i = 0;
        foreach($tabel_file->result() as $value){
        $i++;
        ?>
        <tr>
            <td><?= $i ?></td>
            <td><?= $value->nama_file ?></td>
            <td>
                <a href="<?= site_url('')?><?= $value->path_file?> " class="editData" id="<?= $value->id_file_matkul ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                <a href="#" class="hapusFileData" data-toggle="modal" data-target=".hapusFileModal" id="<?= $value->id_file_matkul ?>" kategori="<?= $value->kategori_file ?>" id-data="<?= $value->id_matkul ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus File"><i class="icon-trash"></i></button></a>
            </td>
        </tr>
        <?php }
        }else{ ?>
        <tr>
            <td colspan="3" class="text-center">No Data Available</td>
        </tr>
        <?php } ?>
    </tbody>
</table>
<script type="text/javascript">
    $('.hapusFileData').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        idpengajaran = $(this).attr('id-data');
    });
</script>
