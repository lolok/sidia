<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">JUDUL PUBLIKASI</th>
                <th class="text-center">MEDIA PUBLIKASI</th>
                <th class="text-center">WAKTU</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $value->datajudul ?></td>
                <td class="text-center"><?= $value->datamedia ?></td>
                <td class="text-center"><?= $value->datawaktu ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editPublikasiLainModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Data Publikasi Lain"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Data Publikasi Lain"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PublikasiLainCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editjudul").val(data['judul']);
                form.find("#editmedia").val(data['media']);
                form.find("#editwaktu").val(data['waktu']);
                form.find("#idpublikasilain").val(data['id_publikasi_lain']);
            }
        });
    });

    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });

    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>