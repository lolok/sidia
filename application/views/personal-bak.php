<div class="col-md-5">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="col-md-2">
                                        <i class="fa fa-user" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center">Data Personal </h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addPersonalModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Data Personal" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Nama</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> John </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">No Hape</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> John </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Alamat</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Doe </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Jurusan</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Male </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Prodi</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1687 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Jabatan</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1687 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Tempat</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Category1 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Ilmu</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Free </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">S1</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Male </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">S2</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1687 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">S3</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1687 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">No Sertifikat</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> Male </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Perting</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1687 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-4" style="text-align:left;padding-left: 30px">Status</label>
                                                        <label class="control-label col-md-1">:</label>
                                                        <div class="col-md-6">
                                                            <p class="form-control-static"> 11/06/1987 </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="col-md-2">
                                        <i class="fa fa-archive" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center">berkas Personal </h3>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addPersonalModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Berkas Personal" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div id="databody">
                                    
                                </div>
                            </div>
                        </div>
                    </div>