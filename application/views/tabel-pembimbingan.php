<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <?php if($this->session->userdata('level') == 1){?>
                    <th class="text-center">DOSEN</th>
                <?php } ?>
                <th class="text-center">KATEGORI</th>
                <th class="text-center">MAHASISWA</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->semester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
                if ($value->kategori == 1){
                    $kategori = 'Membimbing';
                }else{
                    $kategori = 'Menguji';
                }
                if ($value->jenis == 1) {
                    $jenis = 'S1';
                }else if($value->jenis == 2){
                    $jenis = 'S2';
                }else if($value->jenis == 3){
                    $jenis = 'S3';
                }else if($value->jenis == 4){
                    $jenis = 'KP';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <?php if($this->session->userdata('level') == 1){?>
                    <td class="text-center"><?= $value->nama ?></td>
                <?php }?>
                <td class="text-center"><?= $kategori ?> <?= $jenis ?></td>
                <td class="text-center"> 
                    <a href="#" class="detailMhs" data-toggle="modal" data-target="#daftarDosenModal" id="<?= $value->id_pembimbingan ?>" kategori="<?= $value->kategori ?>" jenis="<?= $value->jenis ?>" dosen="<?= $value->id_user ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Mahasiswa"><i class="ti-user"></i></button></a>
                </td>
                <td class="text-center"><?= $semester ?>-<?= $value->tahun ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_pembimbingan ?>" kategori="membimbing"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Halaman Pengesahan Membimbing"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_pembimbingan ?>" kategori="menguji"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Halaman Pengesahan Menguji"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkasDokumen" data-toggle="modal" data-target="#fileModalDokumen" id="<?= $value->id_pembimbingan ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumen Berkas"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id_pembimbingan ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Pembimbingan"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_pembimbingan ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Pembimbingan"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editkategori").select2('val',data['kategori']);
                form.find("#editjenis").select2('val',data['jenis']);
                form.find("#editmasa_penugasan").val(data['masa_penugasan']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#idpembimbingan").val(data['id_pembimbingan']);
                form.find(".editselect_penugasan").select2('val',data['bukti_penugasan']);
                form.find(".editselect_dokumen").select2('val',data['bukti_dokumen']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.fileBerkasDokumen').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    //daftar dosen
    $('.detailMhs').on('click',function(){
        id = $(this).attr('id');
        form = $('#form-dosen');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getMhs') ?>',
            data:{
                id:id,
            },
            success:function(data){
                $('#data-dosen').html(data);
                form.find('#id_data').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infokategori").select2('val',data['kategori']);
                form.find("#infojenis").select2('val',data['jenis']);
                form.find("#infomasa_penugasan").val(data['masa_penugasan']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
                form.find("#infobukti_penugasan").val(data['bukti_penugasan']);
                form.find("#infobukti_dokumen").val(data['bukti_dokumen']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>