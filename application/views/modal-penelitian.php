<div id="addPenelitianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4>
            </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('PenelitianCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select_dosen" id="dosen" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="dosen" name="dosen" value="<?= $this->session->userdata('id_user') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">KATEGORI PENELITIAN:</label>
                            <div type="text" class="form-control select_kategori" id="kategori" name="kategori" required>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">JENIS PENELITIAN:</label>
                            <div type="text" class="form-control select_jenis" id="jenis" name="jenis">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS ANGGOTA:</label>
                            <div type="text" class="form-control select_anggota" id="status_anggota" name="status_anggota" required>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS PENCAPAIAN:</label>
                            <div type="text" class="form-control select_pencapain"  id="status_pencapaian" name="status_pencapaian" required >
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_penelitian" class="control-label">NAMA PENELITIAN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="nama_penelitian" name="nama_penelitian" required> 
                        </div>
                    </div>
                    
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker" name="masa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tahun" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="tahun" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="semester" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="semester" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control select_penugasan" id="bukti_penugasan" name="bukti_penugasan" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control select_dokumen" id="bukti_dokumen" name="bukti_dokumen" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editPenelitianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="edit_dosen" name="edit_dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="edit_dosen" name="edit_dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">KATEGORI PENELITIAN:</label>
                            <div type="text" class="form-control editselect_kategori" id="editkategori" name="kategori" required>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">JENIS PENELITIAN:</label>
                            <div type="text" class="form-control editselect_jenis" id="editjenis" name="jenis">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS ANGGOTA:</label>
                            <div type="text" class="form-control editselect_anggota" id="editstatus_anggota" name="status_anggota" required>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS PENCAPAIAN:</label>
                            <div type="text" class="form-control editselect_pencapaian"  id="editstatus_pencapaian" name="status_pencapaian" required >
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_nama_penelitian" class="control-label">NAMA PENELITIAN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_nama_penelitian" name="edit_nama_penelitian" required> 
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker1" name="masa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edit_tahun" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edit_tahun" name="edit_tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edit_semester" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="edit_semester" name="edit_semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control editselect_penugasan" id="editbukti_penugasan" name="editbukti_penugasan" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control editselect_dokumen" id="editbukti_dokumen" name="editbukti_dokumen" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idpenelitian">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoPenelitianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">KATEGORI PENELITIAN:</label>
                            <div type="text" class="form-control editselect_kategori" id="infokategori" name="kategori" disabled>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">JENIS PENELITIAN:</label>
                            <div type="text" class="form-control editselect_jenis" id="infojenis" name="jenis" disabled>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS ANGGOTA:</label>
                            <div type="text" class="form-control editselect_anggota" id="infostatus_anggota" name="status_anggota" disabled>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS PENCAPAIAN:</label>
                            <div type="text" class="form-control editselect_pencapaian"  id="infostatus_pencapaian" name="status_pencapaian" disabled >
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="info_nama_penelitian" class="control-label">NAMA PENELITIAN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_nama_penelitian" name="info_nama_penelitian" disabled>
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="info_dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="info_dosen" name="info_dosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="info_dosen" name="info_dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker1" name="masa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="info_tahun" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="info_tahun" name="info_tahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="info_semester" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="info_semester" name="info_semester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control" id="infobukti_penugasan" name="infobukti_penugasan" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control" id="infobukti_dokumen" name="infobukti_dokumen" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-penelitian">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModalDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div id="file-berkas">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>