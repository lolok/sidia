<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">PENGUNDUH</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->datatahun ?></td>
                <td class="text-center"><?= $pengunduh ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="cv_simlitabmas"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="CV Simlitabmas"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="cv_web"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="CV Web"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editCvModal" id="<?= $value->id ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit CV"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus CV"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoData" data-toggle="modal" data-target="#infoCvModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail CV"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('CvCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#edit_semester").val(data['semester']);
                form.find("#idcv").val(data['id_cv']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('CvCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-cv').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('CvCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#info_dosen").select2('val',data['id_user']);
                form.find("#info_tahun").val(data['tahun']);
                form.find("#info_semester").val(data['semester']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>