<div id="addIsianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Kepanitiaan</h4> </div>
            <div class="modal-body">
                <form id="add-isian" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="nama" name="nama" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Kategori Jabatan:</label>
                            <select type="text" class="form-control" id="jabatan" name="jabatan" required>
                                <option value="">Pilih Kategori Kepanitiaan</option>
                                <?php foreach ($kat_jabatan->result() as $value) {?>
                                <option value="<?= $value->id_jabatan ?>"><?= $value->kategori ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="golongan" class="control-label">Golongan:</label>
                            <select type="text" class="form-control golongan" id="golongan" name="golongan" required>
                                <option value="">Pilih jabatan</option>
                                <?php foreach ($golongan->result() as $value) {?>
                                <option value="<?= $value->id_golongan ?>" data="<?= $value->pajak_golongan ?>"><?= $value->nama_golongan ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="editjam" class="control-label">NPWP:</label>
                            <input type="text" class="form-control" id="npwp" name="npwp" required> 
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Uraian:</label>
                            <input type="text" class="form-control numeric add-uraian" id="uraian" name="uraian" required> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan" name="satuan" required> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Brutto:</label>
                            <input type="text" class="form-control numeric add-brutto" id="jml_brutto" name="jml_brutto" onkeydown="return false"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Pajak:</label>
                            <input type="text" class="form-control numeric add-pajak" id="pajak" name="pajak" onkeydown="return false"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Netto:</label>
                            <input type="text" class="form-control numeric add-netto" id="jml_netto" name="jml_netto" onkeydown="return false"> 
                        </div>
                    </div>
                    <input type="hidden" class="form-control" id="id_kegiatan" name="id_kegiatan" value="<?= $this->input->get('id'); ?>"> 
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusIsianModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Kepanitiaan</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus kepanitiaan ?</h4>
                <small style="color: red">semua data kepanitiaan akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-honor">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editIsianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Kepanitiaan</h4> </div>
            <div class="modal-body">
                <form id="edit-isian" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="editnama" name="editnama" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Kategori Kepanitiaan:</label>
                            <select type="text" class="form-control" id="editkategori" name="editkategori" required>
                                <option value="">Pilih Kategori Kepanitiaan</option>
                                <?php foreach ($kat_jabatan->result() as $value) {?>
                                <option value="<?= $value->id_jabatan ?>"><?= $value->kategori ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Golongan:</label>
                            <select type="text" class="form-control edit-golongan" id="editgolongan" name="editgolongan" required>
                                <option value="">Pilih Golongan</option>
                                <?php foreach ($golongan->result() as $value) {?>
                                <option value="<?= $value->id_golongan ?>"><?= $value->nama_golongan ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="editjam" class="control-label">NPWP:</label>
                            <input type="text" class="form-control" id="editnpwp" name="editnpwp" required> 
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Uraian:</label>
                            <input type="text" class="form-control numeric edit-uraian" id="edituraian" name="edituraian" required> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control edit-satuan numeric" id="editsatuan" name="editsatuan" required> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Brutto:</label>
                            <input type="text" class="form-control numeric edit-brutto" id="editbrutto" name="editbrutto" onkeydown="return false"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Pajak:</label>
                            <input type="text" class="form-control numeric edit-pajak" id="editpajak" name="editpajak" onkeydown="return false"> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Netto:</label>
                            <input type="text" class="form-control numeric edit-netto" id="editnetto" name="editnetto" onkeydown="return false"> 
                        </div>
                    </div>
                    <input type="hidden" name="idhonor" id="idhonor">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoIsianModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info Kepanitiaan Kegiatans</h4> </div>
            <div class="modal-body">
                <form id="info-honor" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="infonama" name="infonama" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Kategori Kepanitiaan:</label>
                            <select type="text" class="form-control" id="infokategori" name="infokategori" disabled>
                                <option value="">Pilih Kategori Kepanitiaan</option>
                                <?php foreach ($kat_jabatan->result() as $value) {?>
                                <option value="<?= $value->id_jabatan ?>"><?= $value->kategori ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Golongan:</label>
                            <select type="text" class="form-control info-jabatan" id="infojabatan" name="infojabatan" disabled>
                                <option value="">Pilih Golongan</option>
                                <?php foreach ($golongan->result() as $value) {?>
                                <option value="<?= $value->id_golongan ?>"><?= $value->nama_golongan ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="infojam" class="control-label">NPWP:</label>
                            <input type="text" class="form-control" id="infonpwp" name="infonpwp" disabled> 
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Uraian:</label>
                            <input type="text" class="form-control numeric info-uraian" id="infouraian" name="infouraian" disabled=""> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control" id="infosatuan" name="infosatuan" disabled> 
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Brutto:</label>
                            <input type="text" class="form-control" id="infobrutto" name="infobrutto" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Pajak:</label>
                            <input type="text" class="form-control" id="infopajak" name="infopajak" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Jumlah Netto:</label>
                            <input type="text" class="form-control" id="infonetto" name="infonetto" disabled> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>