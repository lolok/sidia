<div id="addRiwayatModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> 
            </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('RiwayatCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="dosen" class="control-label">KATEGORI:</label>
							<select type="text" class="form-control" name="kategori" required>
									<option value="" disabled>Jenjang</option>
									<option value="1">S1</option>
									<option value="2">S2</option>
									<option value="3">S3</option>
							</select>
						</div>
					</div>
					<div class="col-md-9">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERGURUAN TINGGI:</label>
                            <input type="text" class="form-control" name="nama_perguruan_tinggi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">BIDANG ILMU:</label>
                            <input type="text" class="form-control" name="bidang" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TAHUN MASUK - LULUS:</label>
                            <input type="text" class="form-control" name="tahun" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL:</label>
                            <input type="text" class="form-control" name="judul" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PEMBIMBING:</label>
                            <input type="text" class="form-control" name="nama_pembimbing" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editRiwayatModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
					<div class="col-md-3">
						<div class="form-group">
							<label for="dosen" class="control-label">KATEGORI:</label>
							<select type="text" class="form-control" name="edit_kategori" id="edit_kategori" required>
									<option value="" disabled>Jenjang</option>
									<option value="1">S1</option>
									<option value="2">S2</option>
									<option value="3">S3</option>
							</select>
						</div>
					</div>
					<div class="col-md-9">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERGURUAN TINGGI:</label>
                            <input type="text" class="form-control" id="edit_perguruan_tinggi" name="edit_perguruan_tinggi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">BIDANG ILMU:</label>
                            <input type="text" class="form-control" id="edit_bidang" name="edit_bidang" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TAHUN MASUK - LULUS:</label>
                            <input type="text" class="form-control" id="edit_tahun" name="edit_tahun" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL:</label>
                            <input type="text" class="form-control" id="edit_judul" name="edit_judul" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PEMBIMBING:</label>
                            <input type="text" class="form-control" id="edit_nama_pembimbing" name="edit_nama_pembimbing" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_riwayat">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>



