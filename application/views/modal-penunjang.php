<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('PenunjangCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select_dosen" id="kepanitiaan" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="satuan_atk" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">KATEGORI PENUNJANG:</label>
                            <div type="text" class="form-control select_kategori" id="kategori" name="kategori" required>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">JENIS PENUNJANG:</label>
                            <div type="text" class="form-control select_jenis" id="jenis" name="jenis">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS:</label>
                            <div type="text" class="form-control select_anggota" id="status_anggota" name="status_anggota" required>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6" id="jml_mhs" style="display: none">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">JUMLAH MAHASISWA:</label>
                            <input type="number" id="nilai_mhs" name="jml_mhs" class="form-control" min="0" max="12" onkeydown="return false" value="1">
                        </div>
                    </div>
                    <div class="col-md-6" id="jml_dosen" style="display: none">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">JUMLAH DOSEN:</label>
                            <input type="number" id="nilai_dosen" name="jml_dosen" class="form-control" min="0" max="8" onkeydown="return false" value="1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PENUNJANG:</label>
                            <input type="text" class="form-control" id="kepanitiaan" name="nampen" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker" name="masa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control select_penugasan" id="bukti_penugasan" name="bukti_penugasan" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control select_dokumen" id="bukti_dokumen" name="bukti_dokumen" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="editdosen" name="editdosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="editdosen" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">KATEGORI PENUNJANG:</label>
                            <div type="text" class="form-control editselect_kategori" id="editkategori" name="kategori" required>
                                
                            </div>  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="dosen" class="control-label">JENIS PENUNJANG:</label>
                            <div type="text" class="form-control editselect_jenis" id="editjenis" name="jenis">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">STATUS:</label>
                            <div type="text" class="form-control editselect_anggota" id="editstatus_anggota" name="status_anggota" required>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6" id="editjml_mhs" style="display: none">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">JUMLAH MAHASISWA:</label>
                            <input type="number" id="editnilai_mhs" name="editjml_mhs" class="form-control" min="0" max="8" onkeydown="return false" value="1">
                        </div>
                    </div>
                    <div class="col-md-6" id="editjml_dosen" style="display: none">
                        <div class="form-group">
                            <label for="status_anggota" class="control-label">JUMLAH DOSEN:</label>
                            <input type="number" id="editnilai_dosen" name="editjml_dosen" class="form-control" min="0" max="8" onkeydown="return false" value="1">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PENUNJANG:</label>
                            <input type="text" class="form-control" id="editnampen" name="editnampen" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker1" name="editmasa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edittahun" name="edittahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="editsemester" name="editsemester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control editselect_penugasan" id="editbukti_penugasan" name="editbukti_penugasan" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control editselect_dokumen" id="editbukti_dokumen" name="editbukti_dokumen" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_penunjang">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PENUNJANG:</label>
                            <input type="text" class="form-control select2" id="infonampen" name="infonampen" disabled>
                            </input>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="infodosen" name="infodosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infodosen" name="infodosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun" name="infotahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester" name="infosemester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-pengajaran">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModalDokumen" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div id="file-berkas">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>