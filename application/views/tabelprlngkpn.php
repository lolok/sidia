<div class="table-responsive" id="div-atk">
    <table id="tabel-prlngkpn" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Uraian Barang</th>
                <th>Tanggal</th>
                <th>Satuan</th>
                <th>Jumlah Akhir</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($prlngkpn->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->uraian_barang ?></td>
                <td><?= $value->tanggal_prlngkpn ?></td>
                <td><?= number_format($value->satuan) ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editPrlngkpn" data-toggle="modal" data-target="#editPrlngkpnModal" id="<?=htmlspecialchars($value->id_perlengkapan); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Prlngkpn"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusPrlngkpn" data-toggle="modal" data-target=".hapusPrlngkpnModal" id="<?=htmlspecialchars($value->id_perlengkapan); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Prlngkpn"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoPrlngkpn" data-toggle="modal" data-target="#infoPrlngkpnModal" id="<?=htmlspecialchars($value->id_perlengkapan); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Prlngkpn"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editPrlngkpn').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-prlngkpn');
        $.ajax({
            url:'<?= site_url('PrlngkpnCntrl/getDataPrlngkpn') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_prlngkpn").val(data['uraian_barang']);
                form.find("#edittanggal_prlngkpn").val(data['tanggal_prlngkpn']);
                form.find("#editsatuan_prlngkpn").val(data['satuan']);
                form.find("#editjumlah_prlngkpn").val(data['jumlah']);
                form.find("#editjumlahakhir_prlngkpn").val(data['jumlah_akhir']);
                form.find("#idprlngkpn").val(data['id_perlengkapan']);
            }
        });
    });
    $('.hapusPrlngkpn').on('click',function(){
        idprlngkpn = $(this).attr('id');
    });
    $('.infoPrlngkpn').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-prlngkpn');
        $.ajax({
            url:'<?= site_url('PrlngkpnCntrl/getDataPrlngkpn') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_prlngkpn").val(data['uraian_barang']);
                form.find("#infotanggal_prlngkpn").val(data['tanggal_prlngkpn']);
                form.find("#infosatuan_prlngkpn").val(data['satuan']);
                form.find("#infojumlah_prlngkpn").val(data['jumlah']);
                form.find("#infojumlahakhir_prlngkpn").val(data['jumlah_akhir']);
            }
        });
    });
    $('#tabel-prlngkpn').DataTable({  
    });
</script>