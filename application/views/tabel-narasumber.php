<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">KEGIATAN ILMIAH</th>
                <th class="text-center">STATUS</th>
                <th class="text-center">PENYELENGGARA</th>
                <th class="text-center">TEMPAT & WAKTU</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $value->datakegiatan ?></td>
                <td class="text-center"><?= $value->datastatus ?></td>
                <td class="text-center"><?= $value->datapenyelenggara ?></td>
                <td class="text-center"><?= $value->datatempatwaktu ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editNarasumberModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Data Kegiatan Lain"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Data Kegiatan Lain"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('NarasumberCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editkegiatan").val(data['kegiatan']);
                form.find("#editstatus").val(data['status']);
                form.find("#editpenyelenggara").val(data['penyelenggara']);
                form.find("#edittempatwaktu").val(data['tempat_waktu']);
                form.find("#idnarasumber").val(data['id_narasumber']);
            }
        });
    });

    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });

    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>