<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="col-md-2">
                                        <i class="fa fa-graduation-cap" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center"><?= $title ?> </h3>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sorttahun" name="kepanitiaan" required>
                                                    <option value="all">Semua Tahun</option>
                                                    <?php foreach($tahun as $val){ ?>
                                                        <option value="<?= $val->year ?>">Tahun <?= $val->year ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortsemester" name="kepanitiaan" required>
                                                    <option value="all">Semua Semester</option>
                                                    <option value="1">Semester Ganjil</option>
                                                    <option value="2">Semester Genap</option>
                                                </select> 
                                            </div>
                                        </div>
                                    </form>
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addPenelitianModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Data Penelitian" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div id="databody">
                                
                            </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-penelitian'); ?>              
                <?php $this->load->view('modal-hapus'); ?>
                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            getData('PenelitianCntrl/getTabel');
            //add
            $(".select_dosen").select2();
            $(".select_kategori").select2({
                data:[
                    {
                      id: 1,
                      text: "Penelitian"
                    },
                    {
                      id: 2,
                      text: "Menulis Buku"
                    },
                    {
                      id: 3,
                      text: "Menulis Karya Ilmiah yang dipublish di jurnal"
                    },
                    {
                      id: 4,
                      text: "Menulis Karya Ilmiah yang dipublish di seminar"
                    },
                    {
                      id: 5,
                      text: "Memperoleh hak paten"
                    },
                    {
                      id: 6,
                      text: "Menjadi narasumber"
                    },
                    {
                      id: 7,
                      text: "Menulis di media massa"
                    }
                ]
            });
            $(".select_jenis").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".select_anggota").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".select_pencapain").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $("#kategori").on('change', function(){
                kategori = $(this).val();
                if (kategori == 1) {
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penelitian Kelompok",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Penelitian Mandiri",
                              sks: 4
                            }
                        ]
                    });
                }else if(kategori == 2){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Menulis buku internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Menulis buku yang akan diterbitkan",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Menyunting buku internasional",
                              sks: 4
                            },
                            {
                              id: 4,
                              text: "Menyunting buku yang akan diterbitkan",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Menerjemahkan buku",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Menulis buku yang tidak diterbitkan",
                              sks: 2
                            },
                            {
                              id: 7,
                              text: "Menyunting buku yang tidak diterbitkan",
                              sks: 1
                            },
                        ]
                    });
                }else if(kategori == 3){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Diterbitkan oleh jurnal international bereputasi",
                              sks: 7
                            },
                            {
                              id: 2,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional terakreditasi",
                              sks: 5
                            },
                            {
                              id: 3,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional tidak terakreditasi",
                              sks: 3
                            }
                        ]
                    });
                }else if(kategori == 4){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dimuat dalam prosiding seminar internasional",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Dimuat dalam prosiding seminar nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar internasional",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar nasional",
                              sks: 1
                            }
                        ]
                    });
                }else if(kategori == 5){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Paten internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Paten biasa",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Paten sederhana",
                              sks: 3
                            },
                        ]
                    });
                }else if(kategori == 6){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat internasional",
                              sks: 4
                            },
                            {
                              id: 2,
                              text: "Tingkat nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Tingkat daerah",
                              sks: 2
                            },
                        ]
                    });
                }else if(kategori == 7){ 
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true,
                              disabled: true,                           
                            }
                        ]
                    });
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected: true
                            },
                        ]
                    });
                }
            });
            $("#jenis").on('change', function(){
                kategori = $("#kategori").val();
                jenis = $(this).val();
                if (kategori == 1 && jenis == 1) {
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks:2,
                              nilai:100
                            },
                            {
                              id: 2,
                              text: "Anggota",
                              sks:1,
                              nilai:100
                            }
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai:20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if (kategori == 1 && jenis == 2) {
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected:true
                            }
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai: 20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 1 || jenis == 2){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            }
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 3 || jenis == 4){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (tiap bab)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (tiap anggota dewan)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (tiap bab)",
                              nilai: 40
                            }
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 5){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penerjemah Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penerjemah Kelompok (ketua)",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penerjemah Kelompok (anggota)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Naskah",
                              nilai: 65
                            },
                            {
                              id: 2,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 3,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 6){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Anggota",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 2 && jenis == 7){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (kontributor)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (dewan editor)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (kontributor)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                        data:[
                            {
                              id: 1,
                              text: "Naskah suntingan",
                              nilai: 75
                            },
                            {
                              id: 2,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 3 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 4 && jenis == 1 || jenis == 2 || jenis == 3 || jenis == 4){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 5 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pengusul Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Pengusul Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Pengusul Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendaftaran",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Pemeriksaan subtansi",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Uji publik",
                              nilai: 60
                            },
                            {
                              id: 4,
                              text: "Sertifikat",
                              nilai: 100
                            },
                        ]
                    });
                }else if(kategori == 6 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                    $(".select_pencapain").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }
            });
            //edit
            $(".editselect_kategori").select2({
                data:[
                    {
                      id: 1,
                      text: "Penelitian"
                    },
                    {
                      id: 2,
                      text: "Menulis Buku"
                    },
                    {
                      id: 3,
                      text: "Menulis Karya Ilmiah yang dipublish di jurnal"
                    },
                    {
                      id: 4,
                      text: "Menulis Karya Ilmiah yang dipublish di seminar"
                    },
                    {
                      id: 5,
                      text: "Memperoleh hak paten"
                    },
                    {
                      id: 6,
                      text: "Menjadi narasumber"
                    },
                    {
                      id: 7,
                      text: "Menulis di media massa"
                    }
                ]
            });
            $(".editselect_jenis").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".editselect_anggota").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".editselect_pencapaian").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $("#editPenelitianModal").on('shown.bs.modal', function(){
                kategori = $('#editkategori').val();
                if (kategori == 1) {
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penelitian Kelompok",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Penelitian Mandiri",
                              sks: 4
                            }
                        ]
                    });
                }else if(kategori == 2){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Menulis buku internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Menulis buku yang akan diterbitkan",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Menyunting buku internasional",
                              sks: 4
                            },
                            {
                              id: 4,
                              text: "Menyunting buku yang akan diterbitkan",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Menerjemahkan buku",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Menulis buku yang tidak diterbitkan",
                              sks: 2
                            },
                            {
                              id: 7,
                              text: "Menyunting buku yang tidak diterbitkan",
                              sks: 1
                            },
                        ]
                    });
                }else if(kategori == 3){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Diterbitkan oleh jurnal international bereputasi",
                              sks: 7
                            },
                            {
                              id: 2,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional terakreditasi",
                              sks: 5
                            },
                            {
                              id: 3,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional tidak terakreditasi",
                              sks: 3
                            }
                        ]
                    });
                }else if(kategori == 4){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dimuat dalam prosiding seminar internasional",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Dimuat dalam prosiding seminar nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar internasional",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar nasional",
                              sks: 1
                            }
                        ]
                    });
                }else if(kategori == 5){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Paten internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Paten biasa",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Paten sederhana",
                              sks: 3
                            },
                        ]
                    });
                }else if(kategori == 6){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat internasional",
                              sks: 4
                            },
                            {
                              id: 2,
                              text: "Tingkat nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Tingkat daerah",
                              sks: 2
                            },
                        ]
                    });
                }else if(kategori == 7){ 
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true,
                              disabled: true,                           
                            }
                        ]
                    });
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected: true
                            },
                        ]
                    });
                }
            });
            $("#editPenelitianModal").on('shown.bs.modal', function(){
                kategori = $("#editkategori").val();
                jenis = $('#editjenis').val();
                if (kategori == 1 && jenis == 1) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks:2,
                              nilai:100
                            },
                            {
                              id: 2,
                              text: "Anggota",
                              sks:1,
                              nilai:100
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai:20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if (kategori == 1 && jenis == 2) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected:true
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai: 20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 1 || jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 3 || jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (tiap bab)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (tiap anggota dewan)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (tiap bab)",
                              nilai: 40
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 5){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penerjemah Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penerjemah Kelompok (ketua)",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penerjemah Kelompok (anggota)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Naskah",
                              nilai: 65
                            },
                            {
                              id: 2,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 3,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 6){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Anggota",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 2 && jenis == 7){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (kontributor)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (dewan editor)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (kontributor)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Naskah suntingan",
                              nilai: 75
                            },
                            {
                              id: 2,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 3 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 4 && jenis == 1 || jenis == 2 || jenis == 3 || jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 5 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pengusul Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Pengusul Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Pengusul Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendaftaran",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Pemeriksaan subtansi",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Uji publik",
                              nilai: 60
                            },
                            {
                              id: 4,
                              text: "Sertifikat",
                              nilai: 100
                            },
                        ]
                    });
                }else if(kategori == 6 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }
            });
            $("#editkategori").on('change', function(){
                kategori = $(this).val();
                if (kategori == 1) {
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penelitian Kelompok",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Penelitian Mandiri",
                              sks: 4
                            }
                        ]
                    });
                }else if(kategori == 2){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Menulis buku internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Menulis buku yang akan diterbitkan",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Menyunting buku internasional",
                              sks: 4
                            },
                            {
                              id: 4,
                              text: "Menyunting buku yang akan diterbitkan",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Menerjemahkan buku",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Menulis buku yang tidak diterbitkan",
                              sks: 2
                            },
                            {
                              id: 7,
                              text: "Menyunting buku yang tidak diterbitkan",
                              sks: 1
                            },
                        ]
                    });
                }else if(kategori == 3){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Diterbitkan oleh jurnal international bereputasi",
                              sks: 7
                            },
                            {
                              id: 2,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional terakreditasi",
                              sks: 5
                            },
                            {
                              id: 3,
                              text: "Diterbitkan oleh jurnal/majalah ilmiah nasional tidak terakreditasi",
                              sks: 3
                            }
                        ]
                    });
                }else if(kategori == 4){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dimuat dalam prosiding seminar internasional",
                              sks: 3
                            },
                            {
                              id: 2,
                              text: "Dimuat dalam prosiding seminar nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar internasional",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Disajikan dalam poster yang dipamerkan pada seminar nasional",
                              sks: 1
                            }
                        ]
                    });
                }else if(kategori == 5){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Paten internasional",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Paten biasa",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Paten sederhana",
                              sks: 3
                            },
                        ]
                    });
                }else if(kategori == 6){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat internasional",
                              sks: 4
                            },
                            {
                              id: 2,
                              text: "Tingkat nasional",
                              sks: 3
                            },
                            {
                              id: 3,
                              text: "Tingkat daerah",
                              sks: 2
                            },
                        ]
                    });
                }else if(kategori == 7){ 
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true,
                              disabled: true,                           
                            }
                        ]
                    });
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected: true
                            },
                        ]
                    });
                }
            });
            $("#editjenis").on('change', function(){
                kategori = $("#editkategori").val();
                jenis = $(this).val();
                if (kategori == 1 && jenis == 1) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks:2,
                              nilai:100
                            },
                            {
                              id: 2,
                              text: "Anggota",
                              sks:1,
                              nilai:100
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai:20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if (kategori == 1 && jenis == 2) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true,
                              selected:true
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Proposal Penelitian",
                              nilai: 20
                            },
                            {
                              id: 2,
                              text: "Persiapan Penelitian",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Pengumpulan Data",
                              nilai: 50
                            },
                            {
                              id: 4,
                              text: "Analisa Data",
                              nilai: 60
                            },
                            {
                              id: 5,
                              text: "Penulisan Laporan",
                              nilai: 85
                            },
                            {
                              id: 6,
                              text: "Penulisan Artikel Ilmiah",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 1 || jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 3 || jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (tiap bab)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (tiap anggota dewan)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (tiap bab)",
                              nilai: 40
                            }
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendahuluan",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Isi Buku",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penutup dan referensi",
                              nilai: 65
                            },
                            {
                              id: 4,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 5,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 5){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penerjemah Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penerjemah Kelompok (ketua)",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penerjemah Kelompok (anggota)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Naskah",
                              nilai: 65
                            },
                            {
                              id: 2,
                              text: "Persetujuan penerbit",
                              nilai: 75
                            },
                            {
                              id: 3,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 2 && jenis == 6){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Anggota",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 2 && jenis == 7){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Editor Tunggal (editor)",
                              nilai: 60
                            },
                            {
                              id: 2,
                              text: "Editor Tunggal (kontributor)",
                              nilai: 40
                            },
                            {
                              id: 3,
                              text: "Editor Kelompok (utama)",
                              nilai: 40
                            },
                            {
                              id: 4,
                              text: "Editor Kelompok (dewan editor)",
                              nilai: 20
                            },
                            {
                              id: 5,
                              text: "Editor Kelompok (kontributor)",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                        data:[
                            {
                              id: 1,
                              text: "Naskah suntingan",
                              nilai: 75
                            },
                            {
                              id: 2,
                              text: "Pencetakan selesai",
                              nilai: 100
                            }
                        ]
                    });
                }else if(kategori == 3 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 4 && jenis == 1 || jenis == 2 || jenis == 3 || jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Penulis Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Penulis Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Penulis Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }else if(kategori == 5 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Pengusul Tunggal",
                              nilai: 100
                            },
                            {
                              id: 2,
                              text: "Pengusul Utama",
                              nilai: 60
                            },
                            {
                              id: 3,
                              text: "Pengusul Pendamping",
                              nilai: 40
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Pendaftaran",
                              nilai: 10
                            },
                            {
                              id: 2,
                              text: "Pemeriksaan subtansi",
                              nilai: 30
                            },
                            {
                              id: 3,
                              text: "Uji publik",
                              nilai: 60
                            },
                            {
                              id: 4,
                              text: "Sertifikat",
                              nilai: 100
                            },
                        ]
                    });
                }else if(kategori == 6 && jenis == 1 || jenis == 2 || jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                    $(".editselect_pencapaian").select2({
                      
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              nilai: 100,
                              disabled: true
                            },
                        ]
                    });
                }
            });
            //ajax add data
            $('#add-data').submit(function(e){
                kategori = $(this).find('#kategori').select2('data');
                jenis = $(this).find('#jenis').select2('data');
                status_anggota = $(this).find('#status_anggota').select2('data');
                status_pencapaian = $(this).find('#status_pencapaian').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('kategori_id', kategori.id);
                formData.append('kategori', kategori.text);
                formData.append('jenis_id', jenis.id);
                formData.append('jenis', jenis.text);
                formData.append('sks', jenis.sks);
                formData.append('status_anggota', status_anggota.text);
                formData.append('anggota_id', status_anggota.id);
                formData.append('nilai_status_anggota', status_anggota.nilai);
                formData.append('sks_status_anggota', status_anggota.sks);
                formData.append('status_pencapaian', status_pencapaian.text);
                formData.append('pencapaian_id', status_pencapaian.id);
                formData.append('nilai_status_pencapaian', status_pencapaian.nilai);
                $.ajax({
                    url: '<?= site_url('PenelitianCntrl/addData') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        // getData('PenelitianCntrl/getTabel');
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        getSort('PenelitianCntrl/sorting', tahun, semester);
                        $('#addPenelitianModal').modal('hide');
                        notification._toast('Success','Input Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax edit data
            $('#edit-data').submit(function(e){
                kategori = $(this).find('#editkategori').select2('data');
                jenis = $(this).find('#editjenis').select2('data');
                status_anggota = $(this).find('#editstatus_anggota').select2('data');
                status_pencapaian = $(this).find('#editstatus_pencapaian').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('kategori_id', kategori.id);
                formData.append('kategori', kategori.text);
                formData.append('jenis_id', kategori.id);
                formData.append('jenis', jenis.text);
                formData.append('sks', jenis.sks);
                formData.append('status_anggota', status_anggota.text);
                formData.append('nilai_status_anggota', status_anggota.nilai);
                formData.append('anggota_id', status_anggota.id);
                formData.append('sks_status_anggota', status_anggota.sks);
                formData.append('status_pencapaian', status_pencapaian.text);
                formData.append('pencapaian_id', status_pencapaian.id);
                formData.append('nilai_status_pencapaian', status_pencapaian.nilai);
                $.ajax({
                    url: '<?= site_url('PenelitianCntrl/editData/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        // getData('PenelitianCntrl/getTabel');
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        getSort('PenelitianCntrl/sorting', tahun, semester);
                        $('#editPenelitianModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax file data
            $('#file-data').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('PenelitianCntrl/addFile/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(data){
                        $('#file-penelitian').html(data);
                        tooltip._tooltip();
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        notification._toast('Success','Upload File','success');
                    }
                });
            });

            //ajax hapus data
            $('#hapus-data').click(function(){
                $.ajax({
                    url:'<?= site_url('PenelitianCntrl/hapusData/') ?>',
                    data:{
                        id:id
                    },
                    success:function(data){
                        getData('PenelitianCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('.hapusModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            $('#hapus-datafile').click(function(){
                $.ajax({
                    url:'<?= site_url('PenelitianCntrl/hapusFile/') ?>',
                    data:{
                        id:id,
                        kategori:kategori,
                        idpenelitian:idpenelitian,
                    },
                    success:function(data){
                        $('#file-penelitian').html(data);
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('.hapusFileModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });

            //ajax sorting
            $('#sorttahun').on('change', function(){
                tahun = $(this).val();
                semester = $('#sortsemester').val();
                getSort('PenelitianCntrl/sorting', tahun, semester);
            });
            $('#sortsemester').on('change', function(){
                semester = $(this).val();
                tahun = $('#sorttahun').val();
                getSort('PenelitianCntrl/sorting', tahun, semester);
            });
            //tabel file

            //datepicker
            $('#datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //editdatepicker
            $('#datepicker1').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //select dosen dan matakuliah
            $('.select_penugasan').select2({
              ajax: {
                url: '<?= site_url('PenelitianCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.select_dokumen').select2({
              ajax: {
                url: '<?= site_url('PenelitianCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            //select dosen dan matakuliah
            $('.editselect_penugasan').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PenelitianCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PenelitianCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_dokumen').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PenelitianCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PenelitianCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
        })

    </script>
</body>

</html>