<div id="addPublikasiLainModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('PublikasiLainCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="dosen" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="satuan_atk" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL PUBLIKASI:</label>
                            <input type="text" class="form-control" id="judul" name="judul" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">MEDIA PUBLIKASI:</label>
                            <input type="text" class="form-control" id="media" name="media" required> 
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="nohp" class="control-label">WAKTU:</label>
                            <input type="text" class="form-control" id="waktu" name="waktu" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>

<div id="editPublikasiLainModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="editdosen" name="editdosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="editdosen" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL PUBLIKASI:</label>
                            <input type="text" class="form-control" id="editjudul" name="editjudul" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="editnama" class="control-label">MEDIA PUBLIKASI:</label>
                            <input type="text" class="form-control numeric add-satuan" id="editmedia" name="editmedia" required> 
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="tahun" class="control-label">WAKTU:</label>
                            <input type="text" class="form-control numeric add-satuan" id="editwaktu" name="editwaktu" required> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idpublikasilain">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>