<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">JUDUL</th>
                <th class="text-center">JENIS</th>
                <th class="text-center">TANGGAL</th>
                <th class="text-center">PUBLISHER</th>
                <th class="text-center">LINK</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->judul ?></td>
                <td class="text-center"><?= $value->jenis ?></td>
                <td class="text-center"><?= $value->tanggal ?></td>
                <td class="text-center"><?= $value->publisher ?></td>
                <td class="text-center"><?= $value->link ?></td>
                

                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Judul Pubikasi "><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Judul Publiksi"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('WebCntrl/getPublish') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editjudul").select2('val',data['judul']);
                form.find("#editjenis").select2('val',data['jenis']);
                form.find("#edittanggal").val(data['tanggal']);
                form.find("#editpublisher").val(data['publisher']);
                form.find("#editlink").val(data['link']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('WebCntrl/getPublish') ?>',
            data:{id:id},
            success:function(data){
                form.find("#judul").select2('val',data['judul']);
                form.find("#jenis").select2('val',data['jenis']);
                form.find("#tanggal").val(data['tanggal']);
                form.find("#publisher").val(data['publisher']);
                form.find("#link").val(data['link']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>