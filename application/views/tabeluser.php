<div class="table-responsive" id="div-atk">
    <table id="tabel-user" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama</th>
                <th>Username</th>
                <th>Level</th>
                <th>Email</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($datauser->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->nama ?></td>
                <td><?= $value->username ?></td>
                <td><?php if($value->level == '1'){
                    echo "Admin LPJ";
                    }else if($value->level == '2') {
                        echo "Pusat";
                    }else if($value->level == '3') {
                        echo "DPD";
                    } ?></td>
                <td><?= $value->email ?></td>
                <td>
                    <a href="#" class="editUser" data-toggle="modal" data-target="#editUserModal" id="<?=htmlspecialchars($value->id_user); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit User"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusUser" data-toggle="modal" data-target=".hapusUserModal" id="<?=htmlspecialchars($value->id_user); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus User"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editUser').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-user');
        $.ajax({
            url:'<?= site_url('UserCntrl/getDataUser') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama").val(data['nama']);
                form.find("#editusername").val(data['username']);
                form.find("#editemail").val(data['email']);
                form.find("#editpassword_lama").val(data['password']);
                form.find("#editpassword_baru").val(data['password']);
                form.find("#editlevel").val(data['level']);
                form.find("#iduser").val(data['id_user']);
            }
        });
    });
    $('.hapusUser').on('click',function(){
        iduser = $(this).attr('id');
    });
    $('.infoPrlngkpn').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-prlngkpn');
        $.ajax({
            url:'<?= site_url('KegCntrl/getDataPrlngkpn') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_prlngkpn").val(data['uraian_barang']);
                form.find("#infotanggal_prlngkpn").val(data['tanggal_prlngkpn']);
                form.find("#infosatuan_prlngkpn").val(data['satuan']);
                form.find("#infojumlah_prlngkpn").val(data['jumlah']);
                form.find("#infojumlahakhir_prlngkpn").val(data['jumlah_akhir']);
            }
        });
    });
    $('#tabel-user').DataTable({  
    });
</script>