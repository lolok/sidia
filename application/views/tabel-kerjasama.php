<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA KERJASAMA</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama_kerjasama ?></td>
                <td class="text-center">
                    <a href="#" class="daftarDosen" data-toggle="modal" data-target="#daftarDosenModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Daftar Dosen"><i class="ti-user"></i></button></a>
                </td>
                <td class="text-center"><?= $semester ?>-<?= $value->datatahun ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="fileadministrasi"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="File Administrasi"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="laporan"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Laporan"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="ppt"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="PPT"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="dokumentasi"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumentasi"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="data"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Data"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editKerjasamaModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Kerjasama"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Kerjasama"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //daftar dosen
    $('.daftarDosen').on('click',function(){
        id = $(this).attr('id');
        form = $('#form-dosen');
        $.ajax({
            url:'<?= site_url('KerjasamaCntrl/getDosen') ?>',
            data:{
                id:id,
            },
            success:function(data){
                $('#data-dosen').html(data);
                form.find('#id_data').val(id);
                tooltip._tooltip();
            }
        });
    });
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('KerjasamaCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_kerjasama").val(data['nama_kerjasama']);
                form.find("#tahun").val(data['tahun']);
                form.find("#semester").val(data['semester']);
                form.find("#id_kerjasama").val(data['id_kerjasama']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('KerjasamaCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-kerjasama').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('KerjasamaCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_kerjasama").val(data['nama_kerjasama']);
                form.find("#infoid_user").val(data['id_user']);
                form.find("#infotahun_kerjasama").val(data['tahun']);
                form.find("#infosemester_kerjasama").val(data['semester']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>