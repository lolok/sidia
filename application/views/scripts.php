<script src="<?= base_url('temp/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?= base_url('temp/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
<!--slimscroll JavaScript -->
<script src="<?= base_url('temp/asset/js/jquery.slimscroll.js') ?>"></script>
<!--Wave Effects -->
<script src="<?= base_url('temp/asset/js/waves.js') ?>"></script>
<!--Counter js -->
<script src="<?= base_url('temp/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/counterup/jquery.counterup.min.js') ?>"></script>
<!--Morris JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/raphael/raphael-min.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/morrisjs/morris.js') ?>"></script>
<!-- chartist chart -->
<script src="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>
<!-- Calendar JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
<script src='<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>'></script>
<script src="<?= base_url('temp/plugins/bower_components/calendar/dist/cal-init.js') ?>"></script>
<!-- Custom Theme JavaScript -->
<script src="<?= base_url('temp/asset/js/jasny-bootstrap.js')?>"></script>
<script src="<?= base_url('temp/asset/js/custom.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.js')?>"></script>
<!-- start - This is for export functionality only -->
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<!-- Select Script -->
<script src="<?= base_url('temp/plugins/bower_components/switchery/dist/switchery.min.js'); ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/custom-select/custom-select.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('temp/plugins/bower_components/bootstrap-select/bootstrap-select.min.js'); ?>" type="text/javascript"></script>
<script src="<?= base_url('temp/plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js'); ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js'); ?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('temp/plugins/bower_components/multiselect/js/jquery.multi-select.js'); ?>"></script>

<script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/toast-master/js/jquery.toast.js') ?>"></script>
<!-- Clock Plugin JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/clockpicker/dist/bootstrap-clockpicker.js') ?>"></script>
<!-- Color Picker Plugin JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') ?>"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') ?>"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') ?>"></script>
<script src="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
<script src="<?= base_url('temp/asset/js/cbpFWTabs.js') ?>"></script>
<!--Style Switcher -->
<script type="text/javascript">
(function() {
    [].slice.call(document.querySelectorAll('.sttabs')).forEach(function(el) {
        new CBPFWTabs(el);
    });
})();

//script
var notification = {
    _toast : function(heading, text, icon){
        $.toast({
            heading: heading,
            text: text,
            position: 'top-right',
            loaderBg: '#fff',
            icon: icon,
            hideAfter: 3500
        });
    }
};
var tooltip = {
    _tooltip : function(){
        $('.dotip').tooltip({
            content: function () {
                return $(this).prop('title');
            }
        });
    }
};
function getData(url) {
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            send:true 
        },
        success:function(data){
            $('#databody').html(data);
            tooltip._tooltip();
        }
    });
};
function getDataf(url) {
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            send:true 
        },
        success:function(data){
            $('#riwayat').html(data);
            tooltip._tooltip();
        }
    });
};
function getDatanf(url) {
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            send:true 
        },
        success:function(data){
            $('#riwayatnf').html(data);
            tooltip._tooltip();
        }
    });
};
function getSort(url,tahun,semester){
	$.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            tahun:tahun,
            semester:semester
        },
        success:function(data){
            $('#databody').html(data);
            tooltip._tooltip();
        }
    });
};
function getBkd(url,iddosen,idtabel) {
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            iddosen:iddosen
        },
        success:function(data){
            $(idtabel).html(data);
            tooltip._tooltip();
        }
    });
};
function getRiwayat(url,iddosen,idtabel) {
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            iddosen:iddosen
        },
        success:function(data){
            $(idtabel).html(data);
            tooltip._tooltip();
        }
    });
};
function getSortBkd(url,iddosen,tahun,semester,idtabel){
    $.ajax({
        url:'<?= site_url() ?>'+url,
        data:{
            tahun:tahun,
            semester:semester,
            iddosen:iddosen
        },
        success:function(data){
            $(idtabel).html(data);
            tooltip._tooltip();
        }
    });
};
function getSortRiwayat(url,iddosen,idtabel){
    $.ajax({
        url:'<?= site_url("") ?>'+url,
        data:{
            iddosen:iddosen
        },
        success:function(data){
            $(idtabel).html(data);
            tooltip._tooltip();
        }
    });
};
function getPrint(id, tahun, semester) {
    $.ajax({
        url:'<?= site_url('EbkdCntrl/getPrint') ?>',
        data:{
            send:true,
            id:id,
            tahun:tahun,
            semester:semester
        },
        success:function(data){
            $('.printableArea').html(data);
        }
    });
};
</script>
