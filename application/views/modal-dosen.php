<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('MatkulCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="" name="nip" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="nama" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">USERNAME DOSEN:</label>
                            <input type="text" class="form-control" id="username_dosen" name="username_dosen" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">PASSWORD DOSEN:</label>
                            <input type="password" class="form-control" id="password_dosen" name="password_dosen" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="editnip" name="editnip" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="editnama" name="editnama" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">USERNAME DOSEN:</label>
                            <input type="text" class="form-control" id="editusername_dosen" name="editusername_dosen" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NEW PASSWORD:</label>
                            <input type="password" class="form-control" id="editpassword_dosen" name="editpassword_dosen"> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="iddosen">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="infonip" name="nip" disabled> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="infonama" name="nama" disabled> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>