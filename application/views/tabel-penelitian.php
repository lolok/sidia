<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">NAMA PENELITIAN</th>
                <th class="text-center">STATUS</th>
                <th class="text-center">TANGGAL PENELITIAN</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->semester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center"><?= $value->nama_penelitian ?></td>
                <td class="text-center"><?= $value->kategori ?>, <?= $value->jenis ?></td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->tahun ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkasDokumen" data-toggle="modal" data-target="#fileModalDokumen" id="<?= $value->id_penelitian ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumen Berkas"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editPenelitianModal" id="<?= $value->id_penelitian ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Penelitian"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_penelitian ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Penelitian"><i class="icon-trash"></i></button></a>
                    <!-- <a href="#" class="infoData" data-toggle="modal" data-target="#infoPenelitianModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Penelitian"><i class="ti-more" ></i></button></a> -->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_nama_penelitian").val(data['nama_penelitian']);
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#datepicker1").val(data['masa_penugasan']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#edit_semester").val(data['semester']);
                form.find("#idpenelitian").val(data['id_penelitian']);
                form.find("#editkategori").select2('val',data['kategori_id']);
                form.find("#editjenis").select2('val',data['jenis_id']);
                form.find("#editstatus_anggota").select2('val',data['anggota_id']);
                form.find("#editstatus_pencapaian").select2('val',data['pencapaian_id']);
                form.find(".editselect_penugasan").select2('val',data['bukti_penugasan']);
                form.find(".editselect_dokumen").select2('val',data['bukti_dokumen']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-penelitian').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.fileBerkasDokumen').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#info_nama_penelitian").val(data['nama_penelitian']);
                form.find("#info_dosen").select2('val',data['id_user']);
                form.find("#infomasa_penugasan").val(data['masa_penugasan']);
                form.find("#info_tahun").val(data['tahun']);
                form.find("#info_semester").val(data['semester']);
                form.find("#infokategori").select2('val',data['kategori_id']);
                form.find("#infojenis").select2('val',data['jenis_id']);
                form.find("#infostatus_anggota").select2('val',data['anggota_id']);
                form.find("#infostatus_pencapaian").select2('val',data['pencapaian_id']);
                form.find("#infobukti_penugasan").val(data['bukti_penugasan']);
                form.find("#infobukti_dokumen").val(data['bukti_dokumen']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    
</script>