<div class="row">
    <div class="col-md-4">
        <h4 class="text-center">User Pusat</h4>
        <h5 class="text-center"><?= $tot_pusat ?></h5>
    </div>
    <div class="col-md-4">
        <h4 class="text-center">User Admin</h4>
        <h5 class="text-center"><?= $tot_admin ?></h5>
    </div>
    <div class="col-md-4">
        <h4 class="text-center">User DPD</h4>
        <h5 class="text-center"><?= $tot_dpd ?></h5>
    </div>
</div>