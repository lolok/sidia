<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('WebCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL:</label>
                            <input type="text" class="form-control select2" id="kepanitiaan" name="judul" required>
                            </input>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JENIS:</label>
                            <input type="text" class="form-control select2" id="kepanitiaan" name="jenis" required>
                            </input>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="satuan_atk" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL:</label>
                            <input type="date" class="form-control" id="datepicker" name="tanggal" required>
                            </input>  
                        </div>
                    </div>
                   <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PUBLISHER:</label>
                            <input type="text" class="form-control select2" id="kepanitiaan" name="publisher" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">LINK:</label>
                            <input type="text" class="form-control select2" id="kepanitiaan" name="link" required>
                            </input>  
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JUDUL:</label>
                            <input type="text" class="form-control select2" id="editjudul" name="editjudul"  required>
                            </input>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JENIS:</label>
                            <input type="text" class="form-control select2" id="editjenis" name="editjenis" required>
                            </input>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="satuan_atk" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL:</label>
                            <input type="text" class="form-control select2" id="edittanggal" name="edittanggal" required>
                            </input>  
                        </div>
                    </div>
                   <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PUBLISHER:</label>
                            <input type="text" class="form-control select2" id="editpublisher" name="editpublisher" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">LINK:</label>
                            <input type="text" class="form-control select2" id="editlink2" name="editlink" required>
                            </input>  
                        </div>
                    </div>
                    <input type="hidden" name="id" id="kepanitiaan">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA KEGIATAN:</label>
                            <input type="text" class="form-control select2" id="infonamkeg" name="infonamkeg" disabled>
                            </input>  
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="infodosen" name="infodosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infodosen" name="infodosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun" name="infotahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester" name="infosemester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-pengajaran">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>