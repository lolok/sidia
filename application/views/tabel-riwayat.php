<div class="table-responsive" id="datariwayat">
    <table id="tabelriwayat" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">NO.</th>
                <th class="text-center">DOSEN</th>
                <th class="text-center">PERGURUAN TINGGI</th>
                <th class="text-center">BIDANG ILMU</th>
                <th class="text-center">TAHUN MASUK - LULUS</th>
                <th class="text-center">JUDUL SKRIPSI</th>
                <th class="text-center">NAMA PEMBIMBING</th>
                <th class="text-center">KATEGORI</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <!-- Riwayat -->
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
					$i++;
					$kategori = $value->kategori;
					if($kategori == 1){
						$kategori = 'S1';
					}
					if($kategori == 2){
						$kategori = 'S2';
					}
					if($kategori == 3){
						$kategori = 'S3';
					}
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center"><?= $value->perguruan_tinggi ?></td>
                <td class="text-center"><?= $value->bidang_ilmu ?></td>
                <td class="text-center"><?= $value->tahun_masuk_lulus ?></td>
                <td class="text-center"><?= $value->judul_skripsi ?></td>
                <td class="text-center"><?= $value->nama_pembimbing ?></td>
                <td class="text-center"><?= $kategori ?></td>
				<td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editRiwayatModal" id="<?= $value->id_riwayat ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Personal"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_riwayat ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Personal"><i class="icon-trash"></i></button></a>
                  
                </td>
            </tr>
            <?php } ?>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
	
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('RiwayatCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_nama_pembimbing").val(data['nama_pembimbing']);
                form.find("#edit_judul").val(data['judul']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#edit_kategori").val(data['kategori']);
                form.find("#edit_bidang").val(data['bidang_ilmu']);
                form.find("#edit_perguruan_tinggi").val(data['perguruan_tinggi']);
                form.find("#edit_id_user").val(data['id_user']);
                form.find("#id_riwayat").val(data['id_riwayat']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabelriwayat').DataTable({   
    });
    $('select.form-select').select2();
</script>

