<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA</th>
                <th class="text-center">NIP</th>
                <th class="text-center">JABATAN FUNGSIONAL</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center"><?= $value->nip ?></td>
                <td class="text-center"><?= $value->jabatan_fungsional ?></td>
                
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editDatadiriModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Personal"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Personal"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoData" data-toggle="modal" data-target="#infoDatadiriModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Personal"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('DatadiriCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#info_nama").val(data['nama']);
                form.find("#info_jenkel").val(data['jenkel']);
                form.find("#info_jabatan_fungsional").val(data['jabatan_fungsional']);
                form.find("#info_nip").val(data['nip']);
                form.find("#info_nidn").val(data['nidn']);
                form.find("#info_tempat").val(data['tempat']);
                form.find("#info_tanggal").val(data['tanggal']);
                form.find("#info_email").val(data['email']);
                form.find("#info_no_hp").val(data['no_hp']);
                form.find("#info_alamat").val(data['alamat']);
                form.find("#info_no_telp").val(data['no_telp']);
                form.find("#info_s1").val(data['s1']);
                form.find("#info_s2").val(data['s2']);
                form.find("#info_s3").val(data['s3']);
                form.find("#info_matkul").val(data['matkul']);
                form.find("#info_bidang").val(data['bidang']);
				
                form.find("#info_nosertif").val(data['nosertif']);
                form.find("#info_perguruan_tinggi").val(data['perguruan_tinggi']);
                form.find("#info_status").val(data['status']);
                form.find("#info_jurusan").val(data['jurusan']);
                form.find("#info_prodi").val(data['prodi']);
                form.find("#info_jabatan").val(data['jabatan']);
                form.find("#info_ilmu").val(data['ilmu']);
				
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#info_id_user").val(data['id_user']);
                form.find("#info_id_data_diri").val(data['id_data_diri']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PersonalCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-personal').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('DatadiriCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
               form.find("#edit_nama").val(data['nama']);
                form.find("#edit_jenkel").val(data['jenkel']);
                form.find("#edit_jabatan_fungsional").val(data['jabatan_fungsional']);
                form.find("#edit_nip").val(data['nip']);
                form.find("#edit_nidn").val(data['nidn']);
                form.find("#edit_tempat").val(data['tempat']);
                form.find("#edit_tanggal").val(data['tanggal']);
                form.find("#edit_email").val(data['email']);
                form.find("#edit_no_hp").val(data['no_hp']);
                form.find("#edit_alamat").val(data['alamat']);
                form.find("#edit_no_telp").val(data['no_telp']);
                form.find("#edit_s1").val(data['s1']);
                form.find("#edit_s2").val(data['s2']);
                form.find("#edit_s3").val(data['s3']);
                form.find("#edit_matkul").val(data['matkul']);
                form.find("#edit_bidang").val(data['bidang']);
				
                form.find("#edit_nosertif").val(data['nosertif']);
                form.find("#edit_perguruan_tinggi").val(data['perguruan_tinggi']);
                form.find("#edit_status").val(data['status']);
                form.find("#edit_jurusan").val(data['jurusan']);
                form.find("#edit_prodi").val(data['prodi']);
                form.find("#edit_jabatan").val(data['jabatan']);
                form.find("#edit_ilmu").val(data['ilmu']);
                form.find("#edit_id_user").val(data['id_user']);
                form.find("#id_data_diri").val(data['id_data_diri']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $('select.form-select').select2();
</script>