<div id="addTransportasiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Transportasi</h4> </div>
            <div class="modal-body">
                <form id="add-transportasi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Petugas:</label>
                            <input type="text" class="form-control" id="nama_transportasi" name="nama_transportasi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editkegiatan" class="control-label">Kota:</label>
                            <select type="text" class="form-control select2" id="kota" name="kota" required>
                                <option selected disabled>Pilih Kota</option>
                                <?php foreach ($kat_kota->result() as $value) {?>
                                <option value="<?= $value->id_kota ?>"><?= $value->nama_kota ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal Mulai:</label>
                            <input type="text" class="form-control mulaipicker" id="tanggalawal_transportasi" name="tanggalawal_transportasi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Tanggal Selesai:</label>
                            <input type="text" class="form-control selesaipicker" id="tanggalakhir_transportasi" name="tanggalakhir_transportasi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_transportasi" name="satuan_transportasi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_transportasi" name="jumlah_transportasi" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control add-total" id="jumlahakhir_transportasi" name="jumlahakhir_transportasi" required onkeydown="return false"> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusTransportasiModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Perlengkapan</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus perlengkapan ?</h4>
                <small style="color: red">semua data perlengkapan akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-transportasi">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editTransportasiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Transportasi</h4> </div>
            <div class="modal-body">
                <form id="edit-transportasi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Petugas:</label>
                            <input type="text" class="form-control" id="editnama_transportasi" name="editnama_transportasi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editkegiatan" class="control-label">Kota:</label>
                            <select type="text" class="form-control select2" id="editkota" name="editkota" required>
                                <option selected disabled>Pilih Kota</option>
                                <?php foreach ($kat_kota->result() as $value) {?>
                                <option value="<?= $value->id_kota ?>"><?= $value->nama_kota ?></option>
                                <?php }?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal Mulai:</label>
                            <input type="text" class="form-control mulaipicker" id="edittanggalawal_transportasi" name="edittanggalawal_transportasi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Tanggal Selesai:</label>
                            <input type="text" class="form-control selesaipicker" id="edittanggalakhir_transportasi" name="edittanggalakhir_transportasi" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control edit-satuan numeric" id="editsatuan_transportasi" name="editsatuan_transportasi" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control edit-jumlah numeric" id="editjumlah_transportasi" name="editjumlah_transportasi" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control edit-total" id="editjumlahakhir_transportasi" name="editjumlahakhir_transportasi" onkeydown="return false"> 
                        </div>
                    </div>
                    <input type="hidden" name="idtransportasi" id="idtransportasi">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoTransportasiModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info Konsumsi</h4> </div>
            <div class="modal-body">
                <form id="info-transportasi" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Petugas:</label>
                            <input type="text" class="form-control" id="infonama_transportasi" name="infonama_transportasi" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="infokegiatan" class="control-label">Kota:</label>
                            <input type="text" class="form-control" id="infokota" name="infokota" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal Mulai:</label>
                            <input type="text" class="form-control mulaipicker" id="infotanggalawal_transportasi" name="infotanggalawal_transportasi" onkeydown="return false" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Tanggal Selesai:</label>
                            <input type="text" class="form-control selesaipicker" id="infotanggalakhir_transportasi" name="infotanggalakhir_transportasi" onkeydown="return false" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Satuan:</label>
                            <input type="text" class="form-control" id="infosatuan_transportasi" name="infosatuan_transportasi" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="infojam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control" id="infojumlah_transportasi" name="infojumlah_transportasi" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control" id="infojumlahakhir_transportasi" name="infojumlahakhir_transportasi" disabled> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>