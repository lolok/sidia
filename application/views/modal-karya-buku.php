<div id="addKaryaBukuModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('ArtikelIlmiahCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">NAMA DOSEN:</label>
                            <select type="text" class="form-control select2" id="dosen" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="dosen" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul_buku" class="control-label">JUDUL BUKU:</label>
                            <input type="text" class="form-control" id="judul_buku" name="judul_buku" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tahun" class="control-label">TAHUN:</label>
                            <input type="text" class="form-control" id="tahun" name="tahun" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="jumlah_halaman" class="control-label">JUMLAH HALAMAN:</label>
                            <input type="text" class="form-control" id="jumlah_halaman" name="jumlah_halaman" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="penerbit" class="control-label">PENERBIT:</label>
                            <input type="text" class="form-control" id="penerbit" name="penerbit" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editKaryaBukuModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="edit_dosen" name="edit_dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="edit_dosen" name="edit_dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_judul_buku" class="control-label">JUDUL BUKU:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_judul_buku" name="edit_judul_buku" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_tahun" class="control-label">TAHUN:</label>
                            <input type="text" class="form-control" id="edit_tahun" name="edit_tahun" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_jumlah_halaman" class="control-label">JUMLAH HALAMAN:</label>
                            <input type="text" class="form-control" id="edit_jumlah_halaman" name="edit_jumlah_halaman" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_penerbit" class="control-label">PENERBIT:</label>
                            <input type="text" class="form-control" id="edit_penerbit" name="edit_penerbit" required> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_karya_buku">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>