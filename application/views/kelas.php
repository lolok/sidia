<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-4 col-xs-12">
                        <div class="white-box">
                            <div class="user-bg" > 
                                <div class="overlay-box" style="background-color: #9598a9; opacity: 1;">
                                    <div class="user-content" style="margin-top: -20px">
                                        <i class="fa fa-calendar" style="color: white;font-size: 120px;margin-top: 40px;margin-bottom: 40px"></i>
                                        <h4 class="text-white" style="margin-top: -20px;color: #e2e3e4"><?= $title ?></h4></div>
                                </div>
                            </div>
                            <div class="user-btm-box" style="margin-bottom: -10px ">
                                <div id="countdata">
                                    <div class="row">

                                        <div class="col-md-7">
                                            <h4><b>Total Ruang</b></h4>
                                        </div>
                                        <div class="col-md-5 text-right">
                                            <h4>2</h4>
                                        </div>
                                            
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="col-md-12">
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px"><?= $title ?> </h3>
                                        
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addKelasModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Ruang" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="margin-bottom: -20px">
                                <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                                <option value="">Pilih Tahun</option>
                                                <option value="">2018</option>
                                                <option value="">2017</option>
                                                <option value="">2016</option>
                                                <option value="">2015</option>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                                <option value="">Pilih Semester</option>
                                                <option value="">Genap</option>
                                                <option value="">Ganjil</option>
                                            </select> 
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <hr>
                            <div id="databody">
                                <div class="table-responsive" id="div-atk">
                                    <table id="tabel-test" class="display nowrap" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th class="text-center">No.</th>
                                                <th class="text-center">MATA KULIAH</th>
                                                <th class="text-center">RUANG</th>
                                                <th class="text-center">DOSEN</th>
                                                <th class="text-center">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="text-center">1.</td>
                                                <td class="text-center">Kuliah Apa</td>
                                                <td class="text-center">A102</td>
                                                <td class="text-center">Reza Ilham Maulana</td>
                                                <td class="text-center">
                                                    <a href="#" class="editAtk" data-toggle="modal" data-target="#editKelasModal" id=""><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Kelas"><i class="ti-pencil-alt"></i></button></a>
                                                    <a href="#" class="hapusAtk" data-toggle="modal" data-target=".hapusAtkModal" id=""><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Kelas"><i class="icon-trash"></i></button></a>
                                                    <a href="#" class="infoAtk" data-toggle="modal" data-target="#infoKelasModal" id=""><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Ruang"><i class="ti-more" ></i></button></a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-test'); ?>              
                 <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <!-- Datatable -->
    <!-- <script type="text/javascript">
    var notification = {
        _toast : function(heading, text, icon){
            $.toast({
                heading: heading,
                text: text,
                position: 'top-right',
                loaderBg: '#fff',
                icon: icon,
                hideAfter: 3500
            });
        }
    };
    function getData() {
        $.ajax({
            url:'<?= site_url('KonsumsiCntrl/getKonsumsi') ?>',
            data:{
                send:true 
            },
            success:function(data){
                $('#databody').html(data);
            }
        });
    };
    function countData() {
        var id = '<?= $this->session->userdata('iduser'); ?>'
        $.ajax({
            url:'<?= site_url('KonsumsiCntrl/getCount') ?>',
            data:{
                id:id
            },
            success:function(data){
                $('#countdata').html(data);
            }
        });
    }
    $(document).ready(function() {
        getData();
        countData();
        $('#add-konsumsi').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '<?= site_url('KonsumsiCntrl/tambahKonsumsi') ?>',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(){
                    getData();
                    countData();
                    $('#addKonsumsiModal').modal('hide');
                    notification._toast('Success','Input Data Konsumsi','success');
                }
            });
        });
        $('#edit-konsumsi').submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: '<?= site_url('KonsumsiCntrl/editKonsumsi/') ?>',
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(){
                    getData();
                    countData();
                    $('#editKonsumsiModal').modal('hide');
                    notification._toast('Success','Update Data Konsumsi','success');
                }
            });
        });
        $('#hapus-konsumsi').click(function(){
            $.ajax({
                url:'<?= site_url('KonsumsiCntrl/deleteKonsumsi/') ?>',
                data:{
                    id:idkonsumsi
                },
                success:function(data){
                    getData();
                    countData();
                    $('.hapusKonsumsiModal').modal('hide');
                    notification._toast('Success','Delete Data Konsumsi','success');
                }
            });
        });
        $('.add-satuan').keyup(function(){
            sat = $(this).val();
            qty = $('.add-jumlah').val();
            val = qty * sat;
    
            $('.add-total').val(val);
        });
        $('.add-jumlah').keyup(function(){
            qty = $(this).val();
            sat = $('.add-satuan').val();
            val = qty * sat;
    
            $('.add-total').val(val);
        });
        $('.edit-satuan').keyup(function(){
            sat = $(this).val();
            qty = $('.edit-jumlah').val();
            val = qty * sat;
    
            $('.edit-total').val(val);
        });
        $('.edit-jumlah').keyup(function(){
            qty = $(this).val();
            sat = $('.edit-satuan').val();
            val = qty * sat;
    
            $('.edit-total').val(val);
        });
        jQuery('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.atkpicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.prlngkpnpicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.konsumsipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.mulaipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.selesaipicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        $(".numeric").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
            return;
        }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
        $(".select2").select2();
        $('.clockpicker').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $('#editjam').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'KegCntrl/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editnama").val(data['nama']);
                    $("#edittanggal").val(data['tanggal']);
                    $("#editjam").val(data['jam']);
                    $("#editlokasi").val(data['lokasi']);
                    $("#editdashuk").val(data['dashuk']);
                    $("#editkegiatan").val(data['katkeg']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'KegCntrl/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
        });

        $(".hapus").click(function(){
            id = $(this).attr('id');
        });
                                        

        $("#hapusKeg").click(function(){
            $.ajax({
                url:'KegCntrl/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            });
        });
        //honor script
        // 
        //end honor
    });
    </script> -->
</body>

</html>
