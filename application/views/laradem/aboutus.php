<!DOCTYPE html>
<html lang="en">
<?php 
    $no = 1;
    foreach($profile as $u) 
?>
<?php $this->load->view('laradem/head'); ?>

<body id="body">
  <div id="preloader"></div>

  <!--==========================
    Top Bar
  ============================-->
  <?php $this->load->view('laradem/topbar'); ?>

  <!--==========================
    Header
  ============================-->
  <?php $this->load->view('laradem/navbar'); ?>

  <!--==========================
    Intro Section
  ============================-->
  
  <section id="call-to-action" class="wow fadeInUp">
      <div class="container" style="margin-top: 18px">
        <div class="row">
          <div class="col-lg-9 text-left">
            <p class="cta-text">Laboratorium PWK bagian Pembangunan Tata Kota | Universitas Diponegoro Semarang.</p>
          </div>
          <div class="col-lg-3 text-center text-lg-right">
            <h3 class="cta-title">ABOUT US</h3>
            
          </div>
          
        </div>

      </div>
    </section><!-- #call-to-action -->

  <main id="main">

    <!--==========================
      About Section
    ============================-->
    <section id="about" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-7 about-img">
            <img src="<?= base_url('temp/laradem/img/team.png') ?>" alt="">
          </div>

          <div class="col-lg-5 content">
            <div class="section-header">
            <h2>WHO WE ARE</h2>
            <h3><?php echo $u->desc_laradem ?></h3>
          </div>
            <!-- <p>larem ipsum</p> -->
            <!-- <ul>
              <li><i class="ion-android-checkmark-circle"></i> Visi atau misi laradem 1</li>
              <li><i class="ion-android-checkmark-circle"></i> Visi atau misi laradem 2</li>
              <li><i class="ion-android-checkmark-circle"></i> Visi atau misi laradem 3</li>
              <li><i class="ion-android-checkmark-circle"></i> Visi atau misi laradem 4</li>
            </ul> -->

          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      About Section
    ============================-->

    <!--==========================
      Services Section
    ============================-->

    <!--==========================
      Clients Section
    ============================-->
    <!-- <section id="clients" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Clients</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div>

        <div class="owl-carousel clients-carousel">
          <img src="<?= base_url('temp/laradem/img/clients/client-1.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-2.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-3.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-4.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-5.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-6.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-7.png') ?>" alt="">
          <img src="<?= base_url('temp/laradem/img/clients/client-8.png') ?>" alt="">
        </div>

      </div>
    </section> --><!-- #clients -->

    <!--==========================
      Our Portfolio Section
    ============================-->
    <section id="portfolio" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Gallery</h2>
        </div>
      </div>

      <div class="container-fluid">
        <div class="row no-gutters">
          <?php 
              $no = 1;
              foreach($galery as $g){ 
          ?>
          <div class="col-lg-3 col-md-4">
            <div class="portfolio-item wow fadeInUp">
              <a href="<?= $g->path_file ?>" class="portfolio-popup">
                <img src="<?= $g->path_file ?>" alt="">
                <div class="portfolio-overlay">
                  <div class="portfolio-info"><h2 class="wow fadeInUp"><?= $g->nama_file ?></h2></div>
                </div>
              </a>
            </div>
          </div>
          <?php } ?>
          
        </div>

      </div>
    </section><!-- #portfolio -->

    <!--==========================
      Our Team Section
    ============================-->
    <section id="team" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Our Team</h2>
        </div>
        <div class="row">
         <?php 
              $i = 0;
              foreach($team as $t){ 
              $i++;
          ?>
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="<?= $t->path_file ?>" alt=""></div>
              <div class="details">
                <h4><?= $t->nama ?></h4>
                <span><?= $t->nip ?></span>
                <div class="social">
                  <h4><?= $t->status ?></h4>
                </div>
              </div>
            </div>
          </div>
          <?php } ?>

          

      </div>
    </section> --><!-- #team

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Contact Us</h3>
            <p class="cta-text"> Jika ada pertanyaan atau saran yang ingin disampaikan pada kita silahkan hubungi kontak info kami.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Contact Here</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <!-- <div class="section-header">
          <h2>Contact Us</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div> -->

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address>A108 Adam Street, NY 535022, USA</address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+155895548855">+1 5589 55488 55</a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:info@example.com">info@example.com</a></p>
            </div>
          </div>

        </div>
      </div>

      <!-- <div id="google-map" data-latitude="40.713732" data-longitude="-74.0092704"></div>

      <div class="container">
        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div> -->

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; <strong><?php echo $u->nama_web ?></strong>. LAB PWK.
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        <a href="https://bootstrapmade.com/">Universitas Diponegoro</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <?php $this->load->view('laradem/scripts'); ?>

</body>
</html>
