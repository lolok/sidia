<!-- JavaScript Libraries -->
<script src="<?= base_url('temp/laradem/lib/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/jquery/jquery-migrate.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/easing/easing.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/superfish/hoverIntent.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/superfish/superfish.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/wow/wow.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/owlcarousel/owl.carousel.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/magnific-popup/magnific-popup.min.js') ?>"></script>
<script src="<?= base_url('temp/laradem/lib/sticky/sticky.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>
<!-- Contact Form JavaScript File -->
<script src="<?= base_url('temp/laradem/contactform/contactform.js') ?>"></script>

<!-- Template Main Javascript File -->
<script src="<?= base_url('temp/laradem/js/main.js') ?>"></script>