<?php 
    $no = 1;
    foreach($profile as $u) 
?>
<!DOCTYPE html>
<html lang="en">
<?php $this->load->view('laradem/head'); ?>

<body id="body">
  <div id="preloader"></div>

  <!--==========================
    Top Bar
  ============================-->
  <?php $this->load->view('laradem/topbar'); ?>

  <!--=========================
    Header
  ===========================-->
  <?php $this->load->view('laradem/navbar'); ?>

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro" class="wow fadeIn">

    <div class="intro-content">
      <h2>Laboratorium <span>Wilayah</span><br>PWK UNDIP</h2>
      <div>
        <a href="#about" class="btn-get-started scrollto">About Us</a>
        <a href="#services" class="btn-projects scrollto">Our Publications</a>
      </div>
    </div>

    <div id="intro-carousel" class="owl-carousel" >
      <div class="item" style="background-image: url('temp/laradem/img/intro-carousel/1.jpg');"></div>
      <div class="item" style="background-image: url('temp/laradem/img/intro-carousel/2.jpg');"></div>
      <div class="item" style="background-image: url('temp/laradem/img/intro-carousel/3.jpg');"></div>
      <div class="item" style="background-image: url('temp/laradem/img/intro-carousel/4.jpg');"></div>
      <div class="item" style="background-image: url('temp/laradem/img/intro-carousel/5.jpg');"></div>
    </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Section
    ============================-->
    

    <!--==========================
      Services Section
    ============================-->
    <section id="services">
      <div class="container">
        <div class="section-header">
          <h2>Publications<a href="<?= site_url('publication') ?>" class="btn-get-started pull-right" style="margin-top: 30px">See More <i class="fa fa-arrow-right"></i></a></h2>
          <p><?php echo $u->nama_web ?>'s article that will be public on this site.</p>
        </div>

        <div class="row">
          <?php 
              $i = 0;
              foreach($publish as $v){ 
              $i++;
          ?>
          <div class="col-lg-6">
            <div class="box wow fadeInLeft">
              <div class="icon"><img src="<?= base_url('temp/laradem/img/book3.png') ?>" style="width: 80px"></div>
              <h4 class="title" style="font-size: 18px"><a href=""><?= $v->judul ?></a> <small><?= $v->jenis ?></small></h4>
              <p style="margin-top: -10px"><i class="fa fa-clock-o"></i> | <?= $v->tanggal ?> (Tanggal Publish)</p>
              <p style=""><i class="fa fa-user"></i> | <?= $v->publisher ?> (Publisher)</p>
              <p style=""><i class="fa fa-globe"></i> | <a href="<?= $v->link ?>" target="_blank">Link Publications</a></p>
            </div>
          </div>
          <?php } ?>
      </div>
    </section>
    <section id="testimonials" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">

          <h2>On Going Penelitian</h2>
          <p>On going penelitian that will be public on this site.</p>
        </div>
        <div class="owl-carousel testimonials-carousel">
            <?php 
                $no = 1;
                foreach($ongoing as $o){ 
            ?>
            <div class="testimonial-item">
              <p>
                <img src="<?= base_url('temp/laradem/img/quote-sign-left.png') ?>" class="quote-sign-left" alt="">
                <?= $o->desc_ongoing ?>
                <img src="<?= base_url('temp/laradem/img/quote-sign-right.png') ?>" class="quote-sign-right" alt="">
              </p>
              <img src="<?= base_url('temp/laradem/img/testimonial-1.jpg') ?>" class="testimonial-img" alt="">
              <h3><?= $o->nama_penelitian ?></h3>
              <h4>Sub Judul</h4>
            </div>
            <?php } ?>

            

        </div>

      </div>
    </section>

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action" class="wow fadeInUp">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Contact Us</h3>
            <p class="cta-text"> Jika ada pertanyaan atau saran yang ingin disampaikan pada kita silahkan hubungi kontak info kami.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle" href="#">Contact Here</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    <!--==========================
      Our Team Section
    ============================-->
    <!-- <section id="team" class="wow fadeInUp">
      <div class="container">
        <div class="section-header">
          <h2>Our Team</h2>
        </div>
        <div class="row">
          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team-1.jpg" alt=""></div>
              <div class="details">
                <h4>Walter White</h4>
                <span>Chief Executive Officer</span>
                <div class="social">
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-google-plus"></i></a>
                  <a href=""><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team-2.jpg" alt=""></div>
              <div class="details">
                <h4>Sarah Jhinson</h4>
                <span>Product Manager</span>
                <div class="social">
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-google-plus"></i></a>
                  <a href=""><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team-3.jpg" alt=""></div>
              <div class="details">
                <h4>William Anderson</h4>
                <span>CTO</span>
                <div class="social">
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-google-plus"></i></a>
                  <a href=""><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-6">
            <div class="member">
              <div class="pic"><img src="img/team-4.jpg" alt=""></div>
              <div class="details">
                <h4>Amanda Jepson</h4>
                <span>Accountant</span>
                <div class="social">
                  <a href=""><i class="fa fa-twitter"></i></a>
                  <a href=""><i class="fa fa-facebook"></i></a>
                  <a href=""><i class="fa fa-google-plus"></i></a>
                  <a href=""><i class="fa fa-linkedin"></i></a>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </section> --><!-- #team -->

    <!--==========================
      Contact Section
    ============================-->
    <section id="contact" class="wow fadeInUp">
      <div class="container">
        <!-- <div class="section-header">
          <h2>Contact Us</h2>
          <p>Sed tamen tempor magna labore dolore dolor sint tempor duis magna elit veniam aliqua esse amet veniam enim export quid quid veniam aliqua eram noster malis nulla duis fugiat culpa esse aute nulla ipsum velit export irure minim illum fore</p>
        </div> -->

        <div class="row contact-info">

          <div class="col-md-4">
            <div class="contact-address">
              <i class="ion-ios-location-outline"></i>
              <h3>Address</h3>
              <address><?php echo $u->alamat ?></address>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-phone">
              <i class="ion-ios-telephone-outline"></i>
              <h3>Phone Number</h3>
              <p><a href="tel:+<?php echo $u->no_hp ?>">+<?php echo $u->no_hp ?></a></p>
            </div>
          </div>

          <div class="col-md-4">
            <div class="contact-email">
              <i class="ion-ios-email-outline"></i>
              <h3>Email</h3>
              <p><a href="mailto:<?php echo $u->email ?>"><?php echo $u->email ?></a></p>
            </div>
          </div>

        </div>
      </div>

      <!-- <div id="google-map" data-latitude="40.713732" data-longitude="-74.0092704"></div>

      <div class="container">
        <div class="form">
          <div id="sendmessage">Your message has been sent. Thank you!</div>
          <div id="errormessage"></div>
          <form action="" method="post" role="form" class="contactForm">
            <div class="form-row">
              <div class="form-group col-md-6">
                <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                <div class="validation"></div>
              </div>
              <div class="form-group col-md-6">
                <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
                <div class="validation"></div>
              </div>
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
              <div class="validation"></div>
            </div>
            <div class="form-group">
              <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
              <div class="validation"></div>
            </div>
            <div class="text-center"><button type="submit">Send Message</button></div>
          </form>
        </div> -->

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="copyright">
        &copy; <strong><?php echo $u->nama_web ?></strong>. LAB PWK.
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Reveal
        -->
        <a href="https://bootstrapmade.com/">Universitas Diponegoro</a>
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <?php $this->load->view('laradem/scripts'); ?>

</body>
</html>
