<?php 
    $no = 1;
    foreach($profile as $u) 
?>
<section id="topbar" class="d-none d-lg-block">
  <div class="container clearfix">
    <div class="contact-info float-left">
      <i class="fa fa-envelope-o"></i> <a href="<?php echo $u->email ?>"><?php echo $u->email ?></a>
      <i class="fa fa-phone"></i> +<?php echo $u->no_hp ?>
    </div>
    <div class="social-links float-right">
      <a href="<?php echo $u->link_twitter ?>" class="twitter"><i class="fa fa-twitter"></i></a>
      <a href="<?php echo $u->link_fb ?>" class="facebook"><i class="fa fa-facebook"></i></a>
      <a href="<?php echo $u->link_ig ?>" class="instagram"><i class="fa fa-instagram"></i></a>
      <a href="<?php echo $u->link_linkedin ?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
    </div>
  </div>
</section>