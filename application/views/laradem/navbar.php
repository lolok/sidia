<?php 
    $no = 1;
    foreach($profile as $u) 
?>
<header id="header">
  <div class="container">

    <div id="logo" class="pull-left">
      <h1><a href="#body" class="scrollto"><?php echo $u->nama_web ?></span></a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="#body"><img src="img/logo.png" alt="" title="" /></a>-->
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu">
        <li <?php if($navbar == 1){?> class="menu-active" <?php } ?>><a href="<?= site_url('home') ?>">Home</a></li>
        <li <?php if($navbar == 2){?> class="menu-active" <?php } ?>><a href="<?= site_url('about') ?>">About Us</a></li>
        <li <?php if($navbar == 5){?> class="menu-active" <?php } ?>><a href="<?= site_url('publication') ?>">Publication</a></li>
        <!-- <li class="menu-has-children"><a href="">link</a>
          <ul>
            <li><a href="#">Drop Down 1</a></li>
            <li><a href="#">Drop Down 3</a></li>
            <li><a href="#">Drop Down 4</a></li>
            <li><a href="#">Drop Down 5</a></li>
          </ul>
        </li> -->
        <li <?php if($navbar == 6){?> class="menu-active" <?php } ?>><a href="<?= site_url('contactus') ?>">Contact Us</a></li>
        <li><a href="<?= site_url('sidia') ?>" class="btn-get-started scrollto">SIDIA</a></li>
      </ul>
    </nav><!-- #nav-menu-container -->
  </div>
</header><!-- #header -->