<?php 
    $no = 1;
    foreach($profile as $u) 
?>
<head>
  <meta charset="utf-8">
  <title><?php echo $u->nama_web ?> <?= $title ?></title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="<?= base_url('temp/plugins/images/undip.png') ?>" rel="icon">
  <link href="<?= base_url('temp/laradem/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800|Montserrat:300,400,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="<?= base_url('temp/laradem/lib/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="<?= base_url('temp/laradem/lib/font-awesome/css/font-awesome.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('temp/laradem/lib/animate/animate.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('temp/laradem/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('temp/laradem/lib/owlcarousel/assets/owl.carousel.min.css') ?>" rel="stylesheet">
  <link href="<?= base_url('temp/laradem/lib/magnific-popup/magnific-popup.css') ?>" rel="stylesheet">
  <link href="<?= base_url('temp/laradem/lib/ionicons/css/ionicons.min.css') ?>" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="<?= base_url('temp/laradem/css/style-custom.css') ?>" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Reveal
    Theme URL: https://bootstrapmade.com/reveal-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>