<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="col-md-2">
                                        <i class="fa fa-file-text" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center"><?= $title ?> </h3>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                        <?php if ($this->session->userdata('level') == 1) {?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortdosen" name="sortdosen" required>
                                                   <?php foreach($dosen->result() as $val){ ?>
                                                        <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        <?php }?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortsemester" name="kepanitiaan" required>
                                                    <option value="all">Semua Semester</option>
                                                    <option value="1">Semester Ganjil</option>
                                                    <option value="2">Semester Genap</option>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sorttahun" name="kepanitiaan" required>
                                                    <option value="all">Semua Tahun</option>
                                                    <?php foreach($tahun as $val){ ?>
                                                        <option value="<?= $val->year ?>">Tahun <?= $val->year ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        
                                    </form>
                                    <!-- <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addMatkulModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Pengajaran" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a> -->
                                    <div class="col-md-1">
                                    <a class="pull-right" href="#" id="print" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Cetak E-BKD" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> 
                                </div>
                                </div>
                            </div>
                            <div style="display: none">
                                <div class="printableArea" >
                                </div>
                            </div>
                            <hr>
                                <div class="sttabs tabs-style-bar">
                                    <nav>
                                        <ul id="tab-data">
                                            <li data-id="#datakuliah"><a href="#pendidikan" class=""><span>Pendidikan</span></a></li>
                                            <li data-id="#databimbingan"><a href="#penelitian" class=""><span>Penelitian</span></a></li>
                                            <li data-id="#datamenguji"><a href="#pengabdian" class=""><span>PKM</span></a></li>
                                            <li data-id="#datamenguji"><a href="#penunjang" class=""><span>Penunjang</span></a></li>
                                        </ul>
                                    </nav>
                            <hr>
                                    <div class="content-wrap">
                                        <section id="pendidikan">
                                            <div id="datapendidikan">
                                
                                            </div>
                                        </section>
                                        <section id="penelitian">
                                            <div id="datapenelitian">
                                
                                            </div>
                                        </section>
                                        <section id="pengabdian">
                                            <div id="datapengabdian">
                                
                                            </div>
                                        </section>
                                        <section id="penunjang">
                                            <div id="datapenunjang">
                                
                                            </div>
                                        </section>
                                    </div>
                                    <!-- /content -->
                                </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-ebkd'); ?>       
                <?php $this->load->view('modal-hapus'); ?>              
                 <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <script src="<?= base_url('temp/asset/js/jquery.PrintArea.js')?>" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            var sortdosen = $('#sortdosen').val();
            var sorttahun = $('#sorttahun').val();
            var sortsemester = $('#sortsemester').val();
            getPrint(sortdosen,sorttahun,sortsemester);
            getBkd('EbkdCntrl/getPendidikan',sortdosen,'#datapendidikan');
            getBkd('EbkdCntrl/getPenelitian',sortdosen,'#datapenelitian');
            getBkd('EbkdCntrl/getPkm',sortdosen,'#datapengabdian');
            getBkd('EbkdCntrl/getPenunjang',sortdosen,'#datapenunjang');
            //ajax sorting
            $('#sortdosen').on('change', function(){
                iddosen = $(this).val();
                tahun = $('#sorttahun').val();
                semester = $('#sortsemester').val();
                getSortBkd('EbkdCntrl/sortPendidikan',iddosen, tahun, semester, '#datapendidikan');
                getSortBkd('EbkdCntrl/sortPenelitian',iddosen, tahun, semester, '#datapenelitian');
                getSortBkd('EbkdCntrl/sortPkm',iddosen, tahun, semester, '#datapengabdian');
                getSortBkd('EbkdCntrl/sortPenunjang',iddosen, tahun, semester, '#datapenunjang');
            });
            $('#sorttahun').on('change', function(){
                iddosen = $('#sortdosen').val();
                tahun = $(this).val();
                semester = $('#sortsemester').val();
                getSortBkd('EbkdCntrl/sortPendidikan',iddosen, tahun, semester, '#datapendidikan');
                getSortBkd('EbkdCntrl/sortPenelitian',iddosen, tahun, semester, '#datapenelitian');
                getSortBkd('EbkdCntrl/sortPkm',iddosen, tahun, semester, '#datapengabdian');
                getSortBkd('EbkdCntrl/sortPenunjang',iddosen, tahun, semester, '#datapenunjang');
            });
            $('#sortsemester').on('change', function(){
                iddosen = $('#sortdosen').val();
                semester = $(this).val();
                tahun = $('#sorttahun').val();
                getSortBkd('EbkdCntrl/sortPendidikan',iddosen, tahun, semester, '#datapendidikan');
                getSortBkd('EbkdCntrl/sortPenelitian',iddosen, tahun, semester, '#datapenelitian');
                getSortBkd('EbkdCntrl/sortPkm',iddosen, tahun, semester, '#datapengabdian');
                getSortBkd('EbkdCntrl/sortPenunjang',iddosen, tahun, semester, '#datapenunjang');
            });
            //datepicker
            jQuery('#datepicker1').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#datepicker2').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //editdatepicker
            jQuery('#editdatepicker1').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#editdatepicker2').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //aktif bkd
            $('#statusButton').on('click', function(){
                statusval = $('#status').val();
                $.ajax({
                    url:'<?= site_url('EbkdCntrl/getStatusBkd') ?>',
                    data:{
                        id:id,
                        tabel:tabel,
                        idname:idname,
                        status:statusval 
                    },
                    success:function(data){
                        iddosen = $('#sortdosen').val();
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        if (kategori == 1) {
                            getBkd('EbkdCntrl/getPendidikan',sortdosen,'#datapendidikan');
                        }else if(kategori == 2){
                            getBkd('EbkdCntrl/getPenelitian',sortdosen,'#datapenelitian');
                        }else if(kategori == 3){
                            getBkd('EbkdCntrl/getPkm',sortdosen,'#datapengabdian');
                        }else if(kategori == 4){
                            getBkd('EbkdCntrl/getPenunjang',sortdosen,'#datapenunjang');
                        }
                        $('.statusModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            $('#deactiveButton').on('click', function(){
                if (kategori == 1) {
                    url = '<?= site_url('PengajaranCntrl/getStatusBkd') ?>';
                }else if(kategori == 2){
                    url = '<?= site_url('PembimbingCntrl/getStatusBkd') ?>';
                }else if(kategori == 3){
                    url = '<?= site_url('PembimbingCntrl/getStatusBkd') ?>';
                }
                $.ajax({
                    url:url,
                    data:{
                        id:id,
                        status:status 
                    },
                    success:function(data){
                        iddosen = $('#sortdosen').val();
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        if (kategori == 1) {
                            getSortBkd('PengajaranCntrl/sortingBkd',iddosen, tahun, semester, '#datakuliah');
                        }else if(kategori == 2){
                            getSortBkd('PembimbingCntrl/sortingBkd?kategori=1',iddosen, tahun, semester, '#databimbingan');
                        }else if(kategori == 3){
                            getSortBkd('PembimbingCntrl/sortingBkd?kategori=2',iddosen, tahun, semester, '#datamenguji');
                        }
                        $('.deactiveModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //print
            $("#print").click(function() {
                var mode = 'iframe'; //popup
                var close = mode == "popup";
                var options = {
                    mode: mode,
                    popClose: close,
                    extraCss: ('<?= base_url('temp/asset/css/print.css')?>')
                };
                $("div.printableArea").printArea(options);
            });
        })

    </script>
</body>

</html>
