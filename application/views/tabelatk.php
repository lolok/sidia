<div class="table-responsive" id="div-atk">
    <table id="tabel-atk" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Nama Barang</th>
                <th>Tanggal</th>
                <th>Satuan</th>
                <th>Jumlah Akhir</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($atk->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->uraian_barang ?></td>
                <td><?= $value->tanggal_atk ?></td>
                <td><?= number_format($value->satuan) ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editAtk" data-toggle="modal" data-target="#editAtkModal" id="<?=htmlspecialchars($value->id_atk); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Atk"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusAtk" data-toggle="modal" data-target=".hapusAtkModal" id="<?=htmlspecialchars($value->id_atk); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Atk"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoAtk" data-toggle="modal" data-target="#infoAtkModal" id="<?=htmlspecialchars($value->id_atk); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Atk"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editAtk').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-atk');
        $.ajax({
            url:'<?= site_url('AtkCntrl/getDataAtk') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_atk").val(data['uraian_barang']);
                form.find("#edittanggal_atk").val(data['tanggal_atk']);
                form.find("#editsatuan_atk").val(data['satuan']);
                form.find("#editjumlah_atk").val(data['jumlah']);
                form.find("#editjumlahakhir_atk").val(data['jumlah_akhir']);
                form.find("#idatk").val(data['id_atk']);
            }
        });
    });
    $('.hapusAtk').on('click',function(){
        idatk = $(this).attr('id');
    });
    $('.infoAtk').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-atk');
        $.ajax({
            url:'<?= site_url('AtkCntrl/getDataAtk') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_atk").val(data['uraian_barang']);
                form.find("#infotanggal_atk").val(data['tanggal_atk']);
                form.find("#infosatuan_atk").val(data['satuan']);
                form.find("#infojumlah_atk").val(data['jumlah']);
                form.find("#infojumlahakhir_atk").val(data['jumlah_akhir']);
            }
        });
    });
    $('#tabel-atk').DataTable({   
    });
</script>