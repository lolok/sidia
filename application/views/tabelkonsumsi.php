<div class="table-responsive" id="div-atk">
    <table id="tabel-konsumsi" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Uraian Barang</th>
                <th>Tanggal</th>
                <th>Satuan</th>
                <th>Jumlah Akhir</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($konsumsi->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->uraian_barang ?></td>
                <td><?= $value->tanggal_konsumsi ?></td>
                <td><?= number_format($value->satuan) ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editKonsumsi" data-toggle="modal" data-target="#editKonsumsiModal" id="<?=htmlspecialchars($value->id_konsumsi); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Konsumsi"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusKonsumsi" data-toggle="modal" data-target=".hapusKonsumsiModal" id="<?=htmlspecialchars($value->id_konsumsi); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Konsumsi"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoKonsumsi" data-toggle="modal" data-target="#infoKonsumsiModal" id="<?=htmlspecialchars($value->id_konsumsi); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Konsumsi"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editKonsumsi').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-konsumsi');
        $.ajax({
            url:'<?= site_url('KonsumsiCntrl/getDataKonsumsi') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_konsumsi").val(data['uraian_barang']);
                form.find("#edittanggal_konsumsi").val(data['tanggal_konsumsi']);
                form.find("#editsatuan_konsumsi").val(data['satuan']);
                form.find("#editjumlah_konsumsi").val(data['jumlah']);
                form.find("#editjumlahakhir_konsumsi").val(data['jumlah_akhir']);
                form.find("#idkonsumsi").val(data['id_konsumsi']);
            }
        });
    });
    $('.hapusKonsumsi').on('click',function(){
        idkonsumsi = $(this).attr('id');
    });
    $('.infoKonsumsi').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-konsumsi');
        $.ajax({
            url:'<?= site_url('KonsumsiCntrl/getDataKonsumsi') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_konsumsi").val(data['uraian_barang']);
                form.find("#infotanggal_konsumsi").val(data['tanggal_konsumsi']);
                form.find("#infosatuan_konsumsi").val(data['satuan']);
                form.find("#infojumlah_konsumsi").val(data['jumlah']);
                form.find("#infojumlahakhir_konsumsi").val(data['jumlah_akhir']);
            }
        });
    });
    $('#tabel-konsumsi').DataTable({     
    });
</script>