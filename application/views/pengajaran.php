<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="col-md-2">
                                        <i class="fa fa-graduation-cap" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center"><?= $title ?> </h3>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sorttahun" name="kepanitiaan" required>
                                                    <option value="all">Semua Tahun</option>
                                                    <?php foreach($tahun as $val){ ?>
                                                        <option value="<?= $val->year ?>">Tahun <?= $val->year ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortsemester" name="kepanitiaan" required>
                                                    <option value="all">Semua Semester</option>
                                                    <option value="1">Semester Ganjil</option>
                                                    <option value="2">Semester Genap</option>
                                                </select> 
                                            </div>
                                        </div>
                                    </form>
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addMatkulModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Pengajaran" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div id="databody">
                                
                            </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-pengajaran'); ?>              
                <?php $this->load->view('modal-hapus'); ?>              
                 <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            getData('PengajaranCntrl/getTabel');
            //ajax add data
            $('#add-data').submit(function(e){
                matkul = $(this).find('#matkul').select2('data');
                dosen = $(this).find('#dosen').select2('data');
                penugasan = $(this).find('#penugasan').select2('data');
                dokumen = $(this).find('#dokumen').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('matkul', matkul.id);
                formData.append('dosen', dosen.id);
                formData.append('bukti_penugasan', penugasan.text);
                formData.append('id_berkas_penugasan', penugasan.kategori);
                formData.append('bukti_dokumen', dokumen.text);
                formData.append('id_berkas_dokumen', dokumen.kategori);
                $.ajax({
                    url: '<?= site_url('PengajaranCntrl/addData') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        // getData('PengajaranCntrl/getTabel');
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('#addMatkulModal').modal('hide');
                        notification._toast('Success','Input Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax edit data
            $('#edit-data').submit(function(e){
                matkul = $(this).find('#editmatkul').select2('data');
                dosen = $(this).find('#editdosen').select2('data');
                penugasan = $(this).find('#editpenugasan').select2('data');
                dokumen = $(this).find('#editdokumen').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('matkul', matkul.id);
                formData.append('dosen', dosen.id);
                formData.append('bukti_penugasan', penugasan.text);
                formData.append('id_berkas_penugasan', penugasan.kategori);
                formData.append('bukti_dokumen', dokumen.text);
                formData.append('id_berkas_dokumen', dokumen.kategori);
                $.ajax({
                    url: '<?= site_url('PengajaranCntrl/editData/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        // getData('PengajaranCntrl/getTabel');
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('#editMatkulModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax file data
            $('#file-data').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('PengajaranCntrl/addFile/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(data){
                        $('#file-pengajaran').html(data);
                        tooltip._tooltip();
                        notification._toast('Success','Upload File','success');
                    }
                });
            });
            $('#hapus-datafile').click(function(){
                $.ajax({
                    url:'<?= site_url('PengajaranCntrl/hapusFile/') ?>',
                    data:{
                        id:id,
                        kategori:kategori,
                        idpengajaran:idpengajaran,
                    },
                    success:function(data){
                        $('#file-pengajaran').html(data);
                        $('.hapusFileModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax hapus data
            $('#hapus-data').click(function(){
                $.ajax({
                    url:'<?= site_url('PengajaranCntrl/hapusData/') ?>',
                    data:{
                        id:id
                    },
                    success:function(data){
                        // getData('PengajaranCntrl/getTabel');
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('.hapusModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            
            //ajax sorting
            $('#sorttahun').on('change', function(){
                tahun = $(this).val();
                semester = $('#sortsemester').val();
                getSort('PengajaranCntrl/sorting', tahun, semester);
            });
            $('#sortsemester').on('change', function(){
                semester = $(this).val();
                tahun = $('#sorttahun').val();
                getSort('PengajaranCntrl/sorting', tahun, semester);
            });
            //datepicker
            $('#datepicker1').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //editdatepicker
            $('#editdatepicker1').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //select dosen dan matakuliah
            $('.select_dosen').select2({
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getDosen') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.select_matkul').select2({
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getMatkul') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.select_matkul_dosen').select2({
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getMatkulDosen?id=').$this->session->userdata('iduser') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.select_penugasan').select2({
                ajax: {
                    url: '<?= site_url('PengajaranCntrl/getBerkas') ?>',
                    dataType: 'json',
                    results: function (data) {
                        return {results: data};
                    }
                }
            });
            $('.select_dokumen').select2({
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('#dosen').on('change',function(){
                data = $('.select_dosen').select2('data');
                id = data.id;
                $('.select_matkul').select2({
                  ajax: {
                    url: '<?= site_url('PengajaranCntrl/getMatkulDosen?id=') ?>'+id,
                    dataType: 'json',
                    results: function (data) {
                        return {results: data};
                    }
                  }
                });
            });
            //select dosen dan matakuliah
            $('.editselect_dosen').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.get("<?= site_url('PengajaranCntrl/getDosenId?id=')?>"+element.val(), function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getDosen') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_matkul').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.get("<?= site_url('PengajaranCntrl/getMatkulId?id=')?>"+element.val(), function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getMatkul') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_matkul_dosen').select2({
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getMatkulDosen?id=').$this->session->userdata('iduser') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_penugasan').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PengajaranCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text'],  "kategori" : res[0]['kategori']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_dokumen').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PengajaranCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PengajaranCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('#editdosen').on('change',function(){
                data = $('.editselect_dosen').select2('data');
                id = data.id;
                $('.editselect_matkul').select2({
                  ajax: {
                    url: '<?= site_url('PengajaranCntrl/getMatkulDosen?id=') ?>'+id,
                    dataType: 'json',
                    results: function (data) {
                        return {results: data};
                    }
                  }
                });
            });
        })

    </script>
</body>

</html>
