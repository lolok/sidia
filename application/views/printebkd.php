<!DOCTYPE html>
<html>
<style type="text/css" media="print">
	@page {size: landscape;margin: 10mm 15mm 10mm 15mm;}
	.tabelheader p{
		margin-bottom: -5px;
	}
	.tabeldata p{
		margin-top: -20px;
		font-size: 10pt;
	}
	.tabeldata .judul p{
		font-weight: bold;
	}
	.tabelbidang{
		font-family: "Times New Roman", Times, serif;
		font-style: italic;
		font-size: 10pt;
	}
	.tabelbidang th{
		border-top: 0.1px solid black;
		border-right: 0.1px solid black;
		border-bottom: 0.1px solid black;
		border-left: 0.1px solid black;
	}
	.tabelbidang td{
		font-family: "Times New Roman", Times, serif;
		font-style: normal;
		font-size: 10pt;
	}
</style>
<body>
<div class="contentprint">
	<table width="100%">
		<tr>
			<td class="text-center"><img src="<?= site_url('assets/logotutwuri.png') ?>" style="width: 100px"></td>
		</tr>
		<tr>
			<td class="text-center"><h1 style="font-size: 18pt">LAPORAN KINERJA DOSEN</h1></td>
		</tr>
	</table>
	<table class="tabelheader" width="100%" style="margin-top: 70px">
		<tr>
			<td style="width: 10%"></td>
			<td class="text-left" style="width: 30%">
				<p>NAMA</p>
			</td>
			<td class="text-left" style="width: 50%">
				<p>:  Nama Dosen</p>
			</td>
			<td style="width: 10%"></td>
		</tr>
		<tr>
			<td style="width: 10%"></td>
			<td class="text-left" style="width: 30%">
				<p>NOMOR SERTIFIKAT</p>
			</td>
			<td class="text-left" style="width: 50%">
				<p>:  Nama Dosen</p>
			</td>
			<td style="width: 10%"></td>
		</tr>
		<tr>
			<td style="width: 10%"></td>
			<td class="text-left" style="width: 30%">
				<p>FAKULTAS</p>
			</td>
			<td class="text-left" style="width: 50%">
				<p>:  Nama Dosen</p>
			</td>
			<td style="width: 10%"></td>
		</tr>
		<tr>
			<td style="width: 10%"></td>
			<td class="text-left" style="width: 30%">
				<p>PERGURUAN TINGGI</p>
			</td>
			<td class="text-left" style="width: 50%">
				<p>:  Nama Dosen</p>
			</td>
			<td style="width: 10%"></td>
		</tr>
		<tr>
			<td style="width: 10%"></td>
			<td class="text-left" style="width: 30%">
				<p>SEMESTER/TAHUN LAPORAN</p>
			</td>
			<td class="text-left" style="width: 50%">
				<p>:  Nama Dosen</p>
			</td>
			<td style="width: 10%"></td>
		</tr>
	</table>
	<table width="100%" style="margin-top: 180px">
		<tr>
			<td class="text-center"><h1 style="font-size: 18pt">KEMENTRIAN PENDIDIKAN DAN KEBUDAYAAN</h1></td>
		</tr>
		<tr>
			<td class="text-center"><h1 style="font-size: 18pt;margin-top: -10px">REPUBLIK INDONESIA</h1></td>
		</tr>
	</table>
	<table class="tabeldata" width="100%" style="margin-top: 180px">
		<tr>
			<td class="text-left" colspan="2"><p class="judul" style="margin-bottom: 20px;font-size: 10pt">I. IDENTITAS</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Nama</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>No. Sertifikat</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Pergutuan Tinggi</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Status</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Alamat PT</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Jurusan</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Program Studi</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Jab. Fungsional / Gol.</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Tempat - Tgl. Lahir</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>S1</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>S2</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>S3</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>Ilmu Yang ditekuni</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
		<tr>
			<td class="text-left" style="width:20%"><p>No. HP</p></td>
			<td class="text-left" style="width:80%"><p>: nama dosen</p></td>
		</tr>
	</table>
	<table class="tabelbidang" width="100%" style="margin-top: 50px">
            
        <tbody>
            <tr>
                <th rowspan="3" class="text-center" style="width: 2%">No.</th>
                <th rowspan="3" class="text-center" style="width: 20%">Kegiatan</th>
                <th colspan="2" class="text-center" style="width: 20%">Beban Kerja</th>
                <th rowspan="3" class="text-center" style="width: 8%">Masa Pelaksanaan Tugas</th>
                <th colspan="3" class="text-center" style="width: 20%">Kinerja</th>
                <th rowspan="3" class="text-center" style="width: 12%">Penilaian/ Rekomendasi Asesor</th>
            </tr>
            <tr>
                <th rowspan="2" class="text-center">Bukti Penugasan</th>
                <th rowspan="2" class="text-center">SKS</th>
                <th rowspan="2" class="text-center">Bukti Kinerja</th>
                <th colspan="2" class="text-center">Capaian</th>
            </tr>
            <tr>
                <th class="text-center">%</th>
                <th class="text-center">SKS</th>
            </tr>
        	<tr>
        		<td class="text-left" colspan="12">II. BIDANG PENDIDIKAN</td>
        	</tr>
        	<!-- Pendidikan -->
        	<!-- Pengajaran -->
            <?php 
                $i = 0;
                foreach($pengajaran->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
                if ($value->jenjang == 'D3') {
                    $penugasan = "Keputusan Dekan Sekolah Vokasi Universitas";
                }else{
                    $penugasan = "Keputusan Dekan Fakultas Teknik Universitas";
                }
                if ($value->jenjang == 'D3') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S1') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S2'){
                    if ($value->mhs <= 25) {
                        $persen = 100;
                    }else if($value->mhs > 25 && $value->mhs <= 50){
                        $persen = 150;
                    }else if($value->mhs > 50 && $value->mhs <= 75){
                        $persen = 200;
                    }
                }
                $total = $value->jml_tatapmuka/$value->total_tatapmuka*$value->sks*$persen/100;
                
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Memberikan kuliah pada jenjang <?= $value->jenjang ?> mata kuliah <?= $value->matkul ?>, kelas <?= $value->kelas ?>, <?= $value->sks ?> SKS, <?= $value->dosen ?> dosen, <?= $value->jml_tatapmuka ?> tatap muka dari <?= $value->total_tatapmuka ?>, jumlah mahasiswa <?= $value->mhs ?>. <b>( <?= $value->jml_tatapmuka ?>/<?= $value->total_tatapmuka ?> x <?= $value->sks ?> sks x <?= $persen ?>% )</b></td>
                <td class="text-center"><?= $value->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $value->bukti_dokumen ?></td>
                <td class="text-center"><?= $persen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php } ?>
            <!-- Bimbingan -->
            <?php 
                foreach ($pembimbingan->result() as $key) {
                    $i++;
                    if ($key->jenis == 1) {
                        $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                        $total = $mhs/6;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Tugas Akhir mahasiswa S1 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key1){ ?><?= $key1->nama ?> (NIM. <?= $key1->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100%</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }else if($key->jenis == 2){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Tesis mahasiswa S2 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key2){ ?><?= $key2->nama ?> (NIM. <?= $key2->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }else if($key->jenis == 3){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Disertasi mahasiswa S3 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key3){ ?><?= $key3->nama ?> (NIM. <?= $key3->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }}?>
            <!-- Pengujian -->
            <?php 
                foreach ($pengujian->result() as $key) {
                    $i++;
                    if ($key->jenis == 1) {
                        $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                        $total = $mhs/6;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Tugas Akhir mahasiswa S1 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key1){ ?><?= $key1->nama ?> (NIM. <?= $key1->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }else if($key->jenis == 2){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Tesis mahasiswa S2 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key2){ ?><?= $key2->nama ?> (NIM. <?= $key2->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }else if($key->jenis == 3){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Disertasi mahasiswa S3 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key3){ ?><?= $key3->nama ?> (NIM. <?= $key3->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>SELESAI
                    <?php }else if($value->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($value->status_ebkd == 2){?>GAGAL
                    <?php }else if($value->status_ebkd == 3){?>LAINNYA
                    <?php }else if($value->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php }}?>
            <tr>
                <td class="text-left" colspan="12">III. BIDANG PENELITIAN DAN PENGEMBANGAN ILMU</td>
            </tr>
            <!-- Penelitian -->
            <?php 
                $i = 0;
                foreach($penelitian->result() as $pen){ 
                $i++;
                if ($pen->status_anggota == 'Tidak ada pilihan') {
                    $status = '';
                }else{
                    $status = 'sebagai '.$pen->status_anggota;
                }
                if($pen->sks_status_anggota != 0){
                    $sks = $pen->sks_status_anggota;
                }else{
                    $sks = $pen->sks;
                }
                $total = $pen->nilai_status_anggota/100*$pen->nilai_status_pencapaian/100*$sks;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left"><?= $pen->jenis ?> <?= $status ?> dengan judul <?= $pen->nama_penelitian ?> <b>( <?= $pen->nilai_status_anggota?>% x <?=$pen->nilai_status_pencapaian?>% x <?=$sks;?> )</b></td>
                <td class="text-center"><?= $pen->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $pen->masa_penugasan ?></td>
                <td class="text-center"><?= $pen->bukti_dokumen ?></td>
                <td class="text-center"><?= $pen->nilai_status_pencapaian ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($pen->status_ebkd == 0) {?>SELESAI
                    <?php }else if($pen->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($pen->status_ebkd == 2){?>GAGAL
                    <?php }else if($pen->status_ebkd == 3){?>LAINNYA
                    <?php }else if($pen->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td class="text-left" colspan="12">IV. BIDANG PENGABDIAN KEPADA MASYARAKAT</td>
            </tr>
            <!-- Pengabdian -->
            <?php 
                $i = 0;
                foreach($pkm->result() as $peng){ 
                $i++;
                $total = $peng->nilai_status_anggota/100*$peng->nilai_status_pencapaian/100*$peng->sks;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left"><?= $peng->kategori ?> "<?= $peng->nama_pkm ?>" <b>( <?= $peng->nilai_status_anggota?>% x <?=$peng->nilai_status_pencapaian?>% x <?=$peng->sks;?> )</b></td>
                <td class="text-center"><?= $peng->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center"><?= $peng->masa_penugasan ?></td>
                <td class="text-center"><?= $peng->bukti_dokumen ?></td>
                <td class="text-center"><?= $peng->nilai_status_pencapaian ?></td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($peng->status_ebkd == 0) {?>SELESAI
                    <?php }else if($peng->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($peng->status_ebkd == 2){?>GAGAL
                    <?php }else if($peng->status_ebkd == 3){?>LAINNYA
                    <?php }else if($peng->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php } ?>
            <tr>
                <td class="text-left" colspan="12">V. PENUNJANG TRIDHARMA PERGURUAN TINGGI</td>
            </tr>
            <!-- Penunjang -->
            <?php 
                $i = 0;
                foreach($penunjang->result() as $penun){ 
                $i++;
                if ($penun->jml_mhs != 0) {
                    $sks = $penun->jml_mhs/12;
                }else if($penun->jml_dosen != 0){
                    $sks = $penun->jml_dosen/8;
                }else{
                    $sks = $penun->sks;
                }
                if ($penun->status_anggota != 'Tidak ada pilihan') {
                    $status = ''.$penun->status_anggota;
                }else{
                    $status = '';
                }
                $total = $sks;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left"><?= $penun->jenis ?> <?= $status ?></b></td>
                <td class="text-center"><?= $penun->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center"><?= $penun->masa_penugasan ?></td>
                <td class="text-center"><?= $penun->bukti_dokumen ?></td>
                <td class="text-center">100</td>
                <td class="text-center"><?= number_format($total,3, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($penun->status_ebkd == 0) {?>SELESAI
                    <?php }else if($penun->status_ebkd == 1){?>LANJUTKAN
                    <?php }else if($penun->status_ebkd == 2){?>GAGAL
                    <?php }else if($penun->status_ebkd == 3){?>LAINNYA
                    <?php }else if($penun->status_ebkd == 4){?>BEBAN LEBIH
                    <?php }?>
                </td>
            </tr>
            <?php } ?>
        </tbody>
	</table>
</div>
</body>
</html>