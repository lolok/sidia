<div id="addAtkModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Atk</h4> </div>
            <div class="modal-body">
                <form id="add-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control numeric add-total" id="total_atk" name="jumlahakhir_atk" required onkeydown="return false"> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusAtkModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Alat Tulis Kerja</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus alat tulis kerja ?</h4>
                <small style="color: red">semua data atk akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-atk">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editAtkModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Isian</h4> </div>
            <div class="modal-body">
                <form id="edit-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="editnama_atk" name="editnama_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="edittanggal_atk" name="edittanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric edit-satuan" id="editsatuan_atk" name="editsatuan_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric edit-jumlah" id="editjumlah_atk" name="editjumlah_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control numeric edit-total" id="editjumlahakhir_atk" name="editjumlahakhir_atk" required onkeydown="return false">
                        </div>
                    </div>
                    <input type="hidden" name="idatk" id="idatk">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoAtkModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info ATK</h4> </div>
            <div class="modal-body">
                <form id="info-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="infonama_atk" name="infonama_atk" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="infotanggal_atk" name="infotanggal_atk" onkeydown="return false" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control" id="infosatuan_atk" name="infosatuan_atk" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="infojam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control" id="infojumlah_atk" name="infojumlah_atk" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control" id="infojumlahakhir_atk" name="infojumlahakhir_atk" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>