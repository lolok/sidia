<head>
	<script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
	<script src="<?= base_url('temp/plugins/bower_components/jquery/dist/tambah.js'); ?>"></script>
</head>

<div id="adddatadiriModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> 
            </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('DatadiriCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="satuan_atk" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JENIS KELAMIN:</label>
                            <input type="text" class="form-control numeric add-satuan" name="jenkel" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN FUNGSIONAL:</label>
                            <input type="text" class="form-control numeric add-satuan" name="jabatan_fungsional" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control numeric add-satuan" name="nip" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIDN:</label>
                            <input type="text" class="form-control numeric add-satuan" name="nidn" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" name="tempat" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="datepicker" name="tanggal" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-satuan" name="email" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR HP:</label>
                            <input type="text" class="form-control numeric add-satuan" name="no_hp" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="" name="alamat" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR TELEPON KANTOR:</label>
                            <input type="text" class="form-control numeric add-satuan" name="no_telp" required>  
                        </div>
                    </div>
					<hr>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">LULUSAN YANG DIHASILKAN:</label>
                        </div>
                    </div>
					<div class="col-md-4">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="" name="s1" required>
                        </div>
                    </div>
					<div class="col-md-4">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="" name="s2" required>
                        </div>
                    </div>
					<div class="col-md-4">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="" name="s3" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">BIDANG KEAHLIAN DAN FOKUS PENELITIAN:</label>
                            <input type="text" class="form-control" id="" name="bidang" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">EBKD:</label>
                        </div>
                    </div>
					<hr>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. SERTIFIKAT:</label>
                            <input type="text" class="form-control" id="" name="nosertif" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERGURUAN TINGGI:</label>
                            <input type="text" class="form-control" id="" name="perguruan_tinggi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="" name="status" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="" name="jurusan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PROGRAM STUDI:</label>
                            <input type="text" class="form-control" id="" name="prodi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN:</label>
                            <input type="text" class="form-control" id="" name="jabatan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU YANG DITEKUNI:</label>
                            <input type="text" class="form-control" id="" name="ilmu" required>
                        </div>
                    </div>
					
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editDatadiriModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
					  <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control" id="edit_id_user" name="editdosen" required> 
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control" id="id_user" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JENIS KELAMIN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_jenkel" name="edit_jenkel" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN FUNGSIONAL:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_jabatan_fungsional" name="edit_jabatan_fungsional" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_nip" name="edit_nip" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIDN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_nidn" name="edit_nidn" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_tempat" name="edit_tempat" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="datepicker" name="edit_tanggal" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_email" name="edit_email" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR HP:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_no_hp" name="edit_no_hp" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="edit_alamat" name="edit_alamat" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR TELEPON KANTOR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_no_telp" name="edit_no_telp" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="edit_s1" name="edit_s1" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="edit_s2" name="edit_s2" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="edit_s3" name="edit_s3" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH YANG DIAMPU:</label>
                            <input type="text" class="form-control" id="edit_matkul" name="edit_matkul" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">BIDANG KEAHLIAN DAN FOKUS PENELITIAN:</label>
                            <input type="text" class="form-control" id="edit_bidang" name="edit_bidang" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">EBKD:</label>
                        </div>
                    </div>
					<hr>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. SERTIFIKAT:</label>
                            <input type="text" class="form-control" id="edit_nosertif" name="edit_nosertif" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERGURUAN TINGGI:</label>
                            <input type="text" class="form-control" id="edit_perguruan_tinggi" name="edit_perguruan_tinggi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="edit_status" name="edit_status" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="edit_jurusan" name="edit_jurusan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PROGRAM STUDI:</label>
                            <input type="text" class="form-control" id="edit_prodi" name="edit_prodi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN:</label>
                            <input type="text" class="form-control" id="edit_jabatan" name="edit_jabatan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU YANG DITEKUNI:</label>
                            <input type="text" class="form-control" id="edit_ilmu" name="edit_ilmu" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="tahun" name="edittahun_kerjasama" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="semester" name="editsemester_kerjasama" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_data_diri">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoDatadiriModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ID DATA DIRI:</label>
                            <input type="text" class="form-control" id="info_id_data_diri" required> 
                        </div>
                    </div>
					<?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="info_id_user" name="dosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infoid_user" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JENIS KELAMIN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_jenkel" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN FUNGSIONAL:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_jabatan_fungsional" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_nip" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIDN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_nidn" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_tempat" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL LAHIR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_tanggal" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_email" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR HP:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_no_hp" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="info_alamat" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOMOR TELEPON KANTOR:</label>
                            <input type="text" class="form-control numeric add-satuan" id="info_no_telp" required>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="info_s1" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="info_s2" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="info_s3" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH YANG DIAMPU:</label>
                            <input type="text" class="form-control" id="info_matkul" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">BIDANG KEAHLIAN DAN FOKUS PENELITIAN:</label>
                            <input type="text" class="form-control" id="info_bidang" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. SERTIFIKAT:</label>
                            <input type="text" class="form-control" id="info_nosertif" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERGURUAN TINGGI:</label>
                            <input type="text" class="form-control" id="info_perguruan_tinggi" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="info_status" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="info_jurusan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PROGRAM STUDI:</label>
                            <input type="text" class="form-control" id="info_prodi"  required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN:</label>
                            <input type="text" class="form-control" id="info_jabatan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU YANG DITEKUNI:</label>
                            <input type="text" class="form-control" id="info_ilmu" required>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun_kerjasama" name="infotahun_kerjasama" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester_kerjasama" name="infosemester_kerjasama" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>