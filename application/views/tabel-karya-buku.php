<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">JUDUL BUKU</th>
                <th class="text-center">TAHUN</th>
                <th class="text-center">JUMLAH HALAMAN</th>
                <th class="text-center">PENERBIT</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama_user ?></td>
                <td class="text-center"><?= $value->judul_buku ?></td>
                <td class="text-center"><?= $value->tahun ?></td>
                <td class="text-center"><?= $value->jumlah_halaman ?></td>
                <td class="text-center"><?= $value->penerbit ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editKaryaBukuModal" id="<?= $value->id ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Karya Buku"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-tqoggle="tooltip" title="Hapus Karya Buku"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('KaryaBukuCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#edit_judul_buku").val(data['judul_buku']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#edit_jumlah_halaman").val(data['jumlah_halaman']);
                form.find("#edit_penerbit").val(data['penerbit']);
                form.find("#id_karya_buku").val(data['id_karya_buku']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('KaryaBukuCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonip").val(data['nip']);
                form.find("#infonama").val(data['nama']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>