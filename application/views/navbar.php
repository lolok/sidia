<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <div class="top-left-part" style="width: 10%">
            <!-- Logo -->
            <a class="logo" href="<?= site_url('dashboard') ?>">
                <!-- Logo icon image, you can use font-icon also --><b>
                <!--This is dark logo icon--><img src="<?= base_url('temp/plugins/images/undip.png') ?>" alt="home" class="dark-logo" width="38" style="margin-left: 5px;margin-bottom: 5px"/><!--This is light logo icon--><img src="<?= base_url('temp/plugins/images/logoundip.jpg') ?>" alt="home" class="light-logo" />
             </b>
                <!-- Logo text image you can use text also --><span class="hidden-xs">
                <!--This is dark logo text--><span style="margin-left: 8px;">SIDIA</span><!--This is light logo text--><img src="<?= base_url('temp/plugins/images/logoundip.jpg') ?>" alt="home" class="light-logo" />
             </span> </a>
        </div>
        <!-- /Logo -->
        <!-- Search input and Toggle icon -->
        <ul class="nav navbar-top-links navbar-left">
            <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
        </ul>
        <ul class="nav navbar-top-links navbar-right pull-right">
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="<?= base_url('temp/plugins/images/users/default.jpg') ?>" alt="user-img" width="36" class="img-circle"><b class="hidden-xs"><?= strtoupper($this->session->userdata('nama')); ?></b><span class="caret"></span> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li>
                        <div class="dw-user-box">
                            <div class="u-img"><img src="<?= base_url('temp/plugins/images/users/default.jpg') ?>" alt="user" /></div>
                            <div class="u-text">
                                <h4><?= strtoupper($this->session->userdata('nama')); ?></h4>
                                <p class="text-muted"><?= $this->session->userdata('email'); ?></p><a href="#" class="btn btn-rounded btn-danger btn-sm">View Profile</a></div>
                        </div>
                    </li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#" data-toggle="modal" data-target=".logoutModal"><i class="fa fa-power-off"></i> Logout</a></li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
    </div>
    <!-- /.navbar-header -->
    <!-- /.navbar-top-links -->
    <!-- /.navbar-static-side -->
</nav>