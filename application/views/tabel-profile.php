<?php 
    $i = 0;
    foreach($tabel->result() as $value){ 
    $i++;
    
    }
?>
<div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-info">
                            <div class="panel-wrapper collapse in" aria-expanded="true">
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form">
                                        <div class="form-body">
                                            <h3 class="box-title">Profil Web</h3>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Nama Web:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <?= $value->nama_web ?> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Email :</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <?= $value->email ?> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Nomor Kontak:</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <?= $value->no_hp ?> </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Deskripsi :</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <?= $value->desc_laradem ?>  </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                            <!--/row-->
                                            <h3 class="box-title">Additional Info</h3>
                                            <hr class="m-t-0 m-b-40">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Alamat :</label>
                                                        <div class="col-md-9">
                                                            <p class="form-control-static"> <?= $value->alamat ?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3">Link facebook :</label>
                                                        <div class="col-md-9">
                                                            <a href="<?= $value->link_fb ?>"; ?>
                                                            <p class="form-control-static"> <?= $value->link_fb ?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Link Instagram :</label>
                                                        <div class="col-md-9">
                                                            <a href="<?= $value->link_ig ?>"; ?>
                                                            <p class="form-control-static"> <?= $value->link_ig ?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                         <label class="control-label col-md-3">Link twitter :</label>
                                                        <div class="col-md-9">
                                                            <a href="<?= $value->link_twitter ?>"; ?>
                                                            <p class="form-control-static"> <?= $value->link_twitter ?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--/span-->
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="control-label col-md-3">Link LinkedIn :</label>
                                                        <div class="col-md-9">
                                                            <a href="<?= $value->link_linkedin ?>"; ?>
                                                            <p class="form-control-static"> <?= $value->link_linkedin ?></p>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>

                                                <!--/span-->
                                            </div>
                                            <!--/row-->
                                                <!--/span-->
                                    </form>

                                    <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="row">
                                                        <div class="col-md-offset-3 col-md-9">
                                                            <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id_profile='1' ?>" >
                                                            <button type="submit" class="btn btn-info" data-toggle="tooltip" title="Edit Profile"> <i class="fa fa-pencil"></i> Edit</button>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6"> </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('WebCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnamaweb").val('val',data['nama_web']);
                form.find("#editemail").val('val',data['email']);
                form.find("#editnohp").val(data['no_hp']);
                form.find("#editalamat").val(data['alamat']);
                form.find("#editlink_fb").val(data['link_fb']);
                form.find("#editlink_twitter").val(data['link_twitter']);
                form.find("#editlink_ig").val(data['link_ig']);
                form.find("#editlink_ig").val(data['link_ig']);
                form.find("#editlink_linkedin").val(data['link_linkedin']);
                form.find("#editdesc").val(data['desc_laradem']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('WebCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnamaweb").(data['nama_web']);
                form.find("#editemail").val('val',data['email']);
                form.find("#editnohp").val(data['no_hp']);
                form.find("#editalamat").val(data['alamat']);
                form.find("#editlink_fb").val(data['link_fb']);
                form.find("#editlink_twitter").val(data['link_twitter']);
                form.find("#editlink_ig").val(data['link_ig']);
                form.find("#editlink_linkedin").val(data['link_linkedin']);
                form.find("#editdesc").val(data['desc_laradem']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>