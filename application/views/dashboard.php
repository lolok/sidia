<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('temp/plugins/images/undip.png') ?>">
    <title><?= $title ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('temp/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="<?= base_url('temp/asset/css/animate.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url('temp/asset/css/style.css') ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url('temp/asset/css/colors/blue.css') ?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-xs-12">
                          <div class="row">
                              <div class="col-lg-12 col-sm-12 col-xs-12">
                                  <div class="white-box">
                                      <h3 class="box-title">SALDO AWAL</h3>
                                      <ul class="list-inline m-t-30 p-t-10 two-part">
                                          <li><i class="icon-people text-info"></i></li>
                                          <li class="text-right"><span class="counter"></span></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="col-lg-21 col-sm-12 col-xs-12">
                                  <div class="white-box">
                                      <h3 class="box-title">PENGELUARAN</h3>
                                      <ul class="list-inline m-t-30 p-t-10 two-part">
                                          <li><i class="mdi mdi-clipboard-text text-purple"></i></li>
                                          <li class="text-right"><span class="counter"></span></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="col-lg-12 col-sm-12 col-xs-12">
                                  <div class="white-box">
                                      <h3 class="box-title">SALDO SISA</h3>
                                      <ul class="list-inline m-t-30 p-t-10 two-part">
                                          <li><i class="mdi mdi-check text-success"></i></li>
                                          <li class="text-right"><span class=""></span></li>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                    <div class="col-md-12 col-lg-8 col-sm-12 m-b-30">
                        <div id="calendar"></div>
                    </div>
                    <!-- BEGIN MODAL -->
                    <div class="modal fade none-border" id="my-event">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><strong>Add Event</strong></h4>
                                </div>
                                <div class="modal-body"></div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-success save-event waves-effect waves-light">Create event</button>
                                    <button type="button" class="btn btn-danger delete-event waves-effect waves-light" data-dismiss="modal">Delete</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Modal Add Category -->
                    <div class="modal fade none-border" id="add-new-event">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title"><strong>Add</strong> a category</h4>
                                </div>
                                <div class="modal-body">
                                    <form>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <label class="control-label">Category Name</label>
                                                <input class="form-control form-white" placeholder="Enter name" type="text" name="category-name" />
                                            </div>
                                            <div class="col-md-6">
                                                <label class="control-label">Choose Category Color</label>
                                                <select class="form-control form-white" data-placeholder="Choose a color..." name="category-color">
                                                    <option value="success">Success</option>
                                                    <option value="danger">Danger</option>
                                                    <option value="info">Info</option>
                                                    <option value="primary">Primary</option>
                                                    <option value="warning">Warning</option>
                                                    <option value="inverse">Inverse</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger waves-effect waves-light save-category" data-dismiss="modal">Save</button>
                                    <button type="button" class="btn btn-white waves-effect" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <!--row -->
                <!-- /.row -->
                
                <!-- ============================================================== -->
                <!-- end right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('temp/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url('temp/asset/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('temp/asset/js/waves.js') ?>"></script>
    <!--Counter js -->
    <script src="<?= base_url('temp/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/counterup/jquery.counterup.min.js') ?>"></script>
    <!--Morris JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/raphael/raphael-min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/morrisjs/morris.js') ?>"></script>
    <!-- chartist chart -->
    <script src="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>
    <!-- Calendar JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <script src='<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>'></script>
    <script src="<?= base_url('temp/plugins/bower_components/calendar/dist/cal-init.js') ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('temp/asset/js/custom.min.js') ?>"></script>
    
    <script src="<?= base_url('temp/plugins/bower_components/toast-master/js/jquery.toast.js') ?>"></script>
    <!--Style Switcher -->
    <script src="<?= base_url('temp/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') ?>"></script>
</body>

</html>
