

<div id="addDosenModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">NO TELP:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editDosenModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">NO TELP:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoDosenModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NIP:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA DOSEN:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">EMAIL:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">NO TELP:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_atk" name="jumlah_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="addRuangModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">KODE RUANG:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA RUANG:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editRuangModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">KODE RUANG:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA RUANG:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoRuangModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">KODE RUANG:</label>
                            <input type="text" class="form-control" id="nama_atk" name="nama_atk" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">NAMA RUANG:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="satuan_atk" required> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="addKelasModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Mata Kuliah</option>
                                <option value="">Matakuliah A</option>
                                <option value="">Matakuliah B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">RUANG:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Ruang</option>
                                <option value="">Ruang A</option>
                                <option value="">Ruang B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Dosen</option>
                                <option value="">Dosen A</option>
                                <option value="">Dosen B</option>
                            </select> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Tahun</option>
                                <option value="">2018</option>
                                <option value="">2017</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Semester</option>
                                <option value="">Genap</option>
                                <option value="">Ganjil</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editKelasModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Mata Kuliah</option>
                                <option value="">Matakuliah A</option>
                                <option value="">Matakuliah B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">RUANG:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Ruang</option>
                                <option value="">Ruang A</option>
                                <option value="">Ruang B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Dosen</option>
                                <option value="">Dosen A</option>
                                <option value="">Dosen B</option>
                            </select> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Tahun</option>
                                <option value="">2018</option>
                                <option value="">2017</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Semester</option>
                                <option value="">Genap</option>
                                <option value="">Ganjil</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoKelasModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-atk" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Mata Kuliah</option>
                                <option value="">Matakuliah A</option>
                                <option value="">Matakuliah B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">RUANG:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Ruang</option>
                                <option value="">Ruang A</option>
                                <option value="">Ruang B</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Dosen</option>
                                <option value="">Dosen A</option>
                                <option value="">Dosen B</option>
                            </select> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Tahun</option>
                                <option value="">2018</option>
                                <option value="">2017</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="kepanitiaan" required>
                                <option value="">Pilih Semester</option>
                                <option value="">Genap</option>
                                <option value="">Ganjil</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>