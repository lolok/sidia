<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th colspan="2" class="text-center">KINERJA</th>
                <th rowspan="2" class="text-center">STATUS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
                <th class="text-center">BUKTI DOKUMEN</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->data_status_anggota == 1) {
                    $status_anggota = 'Penulis Utama Artikel';
                }else{
                    $status_anggota = 'Penulis Anggota Artikel';
                }
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $status_anggota ?> berjudul "<?= $value->nama_penelitian ?>"</td>
                <td class="text-center">Surat Tugas Ketua Departemen Teknik</td>
                <td class="text-center">-</td>
                <td class="text-center"><?= $value->tanggal_penelitian ?></td>
                <td class="text-center">Artikel</td>
                <td class="text-center">-</td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_penelitian" status="1" data-title="Status E-BKD" data-id="<?= $value->id ?>" kategori="2" data-target=".activeModal" data-toggle="modal"><span class="label label-danger label-rouded" style="">Tidak Aktif</span></a>
                    <?php }else{?>
                        <a href="" class="dotip aktif_penelitian" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" kategori="2" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Aktif</span></a>
                    <?php }?> 
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_nama_penelitian").val(data['nama_penelitian']);
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#edit_status_anggota").val(data['status_anggota']);
                form.find("#edit_tanggal_penelitian").val(data['tanggal_penelitian']);
                form.find("#edit_tahun").val(data['tahun']);
                form.find("#edit_semester").val(data['semester']);
                form.find("#idpenelitian").val(data['id_penelitian']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_penelitian').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-penelitian').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PenelitianCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#info_nama_penelitian").val(data['nama_penelitian']);
                form.find("#info_dosen").select2('val',data['id_user']);
                form.find("#info_status_anggota").val(data['status_anggota']);
                form.find("#info_tanggal_penelitian").val(data['tanggal_penelitian']);
                form.find("#info_tahun").val(data['tahun']);
                form.find("#info_semester").val(data['semester']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({ 
     ordering: false,  
    });
    $(".select2").select2();
</script>