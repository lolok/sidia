<div id="addUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah User</h4> </div>
            <div class="modal-body">
                <form id="add-user" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="nama" name="nama_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Username:</label>
                            <input type="text" class="form-control" id="username" name="username_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Email:</label>
                            <input type="email" class="form-control" id="email" name="email_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Password:</label>
                            <input type="password" class="form-control" id="password" name="password_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Level:</label>
                            <select type="text" class="form-control" id="level" name="level_user" required>
                                <option selected="">Pilih level</option>
                                <option value="1">Admin LPJ</option>
                                <option value="2">Pusat</option>
                                <option value="3">DPD</option>
                            </select> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Saldo:</label>
                            <input type="text" class="form-control" id="saldo" name="saldo" required> 
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusUserModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus User</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus user ?</h4>
                <small style="color: red">semua data user akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-user">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit User</h4> </div>
            <div class="modal-body">
                <form id="edit-user" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="editnama" name="editnama_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Username:</label>
                            <input type="text" class="form-control" id="editusername" name="editusername_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Email:</label>
                            <input type="email" class="form-control" id="editemail" name="editemail_user" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Password:</label>
                            <input type="password" class="form-control" id="editpassword_baru" name="editpassword_userbaru" required> 
                            <input type="hidden" class="form-control" id="editpassword_lama" name="editpassword_userlama" > 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Level:</label>
                            <select type="text" class="form-control" id="editlevel" name="editlevel_user" required>
                                <option selected="">Pilih level</option>
                                <option value="1">Admin LPJ</option>
                                <option value="2">Pusat</option>
                                <option value="3">DPD</option>
                            </select> 
                        </div>
                    </div>
                    <input type="hidden" name="iduser" id="iduser">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info Konsumsi</h4> </div>
            <div class="modal-body">
                <form id="info-user" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama:</label>
                            <input type="text" class="form-control" id="editnama" name="editnama_user" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Username:</label>
                            <input type="text" class="form-control" id="editusername" name="editusername_user" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Email:</label>
                            <input type="email" class="form-control" id="editemail" name="editemail_user" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Password:</label>
                            <input type="password" class="form-control" id="editpassword_baru" name="editpassword_userbaru" disabled> 
                            <input type="hidden" class="form-control" id="editpassword_lama" name="editpassword_userlama" > 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Level:</label>
                            <select type="text" class="form-control" id="editlevel" name="editlevel_user" disabled>
                                <option selected="">Pilih level</option>
                                <option value="1">Admin LPJ</option>
                                <option value="2">Pusat</option>
                                <option value="3">DPD</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>