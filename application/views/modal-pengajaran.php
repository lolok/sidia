<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('PengajaranCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <div type="text" class="form-control select_dosen" id="dosen" name="dosen" required>
                             </div>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="dosen" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <div type="text" class="form-control select_matkul" id="matkul" name="matkul" required>
                            </div>  
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control select_matkul_dosen" id="matkul" name="matkul" required></select>  
                        </div>
                    </div> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="datepicker1" name="masa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">JUMLAH TATAP MUKA:</label>
                            <input type="number" class="form-control" id="jml_tatapmuka" name="jml_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TOTAL TATAP MUKA:</label>
                            <input type="number" class="form-control" id="total_tatapmuka" name="total_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="tahun" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="semester" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <div type="text" class="form-control select_penugasan" id="penugasan" name="penugasan" required>
                             </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <div type="text" class="form-control select_dokumen" id="dokumen" name="dokumen" required>
                             </div> 
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <div type="text" class="form-control editselect_dosen" id="editdosen" name="editdosen" required>
                             </div>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="editdosen" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <div type="text" class="form-control editselect_matkul" id="editmatkul" name="editmatkul" required>
                            </div>  
                        </div>
                    </div>
                    <?php }else{ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control editselect_matkul_dosen" id="editmatkul" name="editmatkul" required></select>  
                        </div>
                    </div> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="editdatepicker1" name="editmasa_penugasan" required> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">JUMLAH TATAP MUKA:</label>
                            <input type="number" class="form-control" id="editjml_tatapmuka" name="editjml_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TOTAL TATAP MUKA:</label>
                            <input type="number" class="form-control" id="edittotal_tatapmuka" name="edittotal_tatapmuka" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edittahun" name="edittahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="editsemester" name="editsemester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <div type="text" class="form-control editselect_penugasan" id="editpenugasan" name="editpenugasan" required>
                             </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <div type="text" class="form-control editselect_dokumen" id="editdokumen" name="editdokumen" required>
                             </div> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idpengajaran">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">MATA KULIAH:</label>
                            <select type="text" class="form-control select2" id="infomatkul" name="infomatkul" disabled>
                                <?php foreach($matkul->result() as $val){ ?>
                                    <option value="<?= $val->id_matkul ?>"><?= $val->nama_matkul ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama" class="control-label">MATA KULIAH:</label>
                                <select type="text" class="form-control select2" id="infomatkul" name="infomatkul" disabled>
                                    <?php foreach($matkul->result() as $val){ ?>
                                        <option value="<?= $val->id_matkul ?>"><?= $val->nama_matkul ?></option>
                                    <?php } ?>
                                </select>  
                            </div>
                        </div>
                    <?php } ?>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="infodosen" name="infodosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infodosen" name="infodosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal_penelitian" class="control-label">MASA PENUGASAN:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="" name="infomasa_penugasan" disabled> 
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">JUMLAH TATAP MUKA:</label>
                            <input type="number" class="form-control" id="infojml_tatapmuka" name="infojml_tatapmuka" disabled>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TOTAL TATAP MUKA:</label>
                            <input type="number" class="form-control" id="infototal_tatapmuka" name="infototal_tatapmuka" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun" name="infotahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester" name="infosemester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI PENUGASAN:</label>
                            <input type="text" class="form-control" id="infobukti_penugasan" name="infobukti_penugasan" disabled>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>BUKTI DOKUMEN:</label>
                            <input type="text" class="form-control" id="infobukti_dokumen" name="infobukti_dokumen" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div id="file-pengajaran">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>