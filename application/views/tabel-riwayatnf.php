<div class="table-responsive" id="div-atk">
    <table id="tabel-riwayatnf" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">NO.</th>
                <th class="text-center">NAMA KEGIATAN</th>
                <th class="text-center">PENYELENGGARA</th>
                <th class="text-center">TEMPAT</th>
                <th class="text-center">WAKTU</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <!-- Riwayat -->
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
					$i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama_kegiatan ?></td>
                <td class="text-center"><?= $value->penyelenggara ?></td>
                <td class="text-center"><?= $value->tempat?></td>
                <td class="text-center"><?= $value->waktu ?></td>
				<td class="text-center">
                    <a href="#" class="editDatanf" data-toggle="modal" data-target="#editRiwayatnfModal" id="<?= $value->id_riwayatnf ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Riwayat Non Formal"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusDatanf" data-toggle="modal" data-target=".hapusModalnf" id="<?= $value->id_riwayatnf ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Riwayat Non formal"><i class="icon-trash"></i></button></a>
                  
                </td>
            </tr>
            <?php } ?>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
	
    $('.hapusDatanf').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.editDatanf').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-datanf');
        $.ajax({
            url:'<?= site_url('RiwayatCntrl/getDatanf') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_nama_kegiatan").val(data['nama_kegiatan']);
                form.find("#edit_penyelenggara").val(data['penyelenggara']);
                form.find("#edit_tempat").val(data['tempat']);
                form.find("#edit_waktu").val(data['waktu']);
                form.find("#edit_dosen").val(data['id_user']);
                form.find("#id_riwayatnf").val(data['id_riwayatnf']);
            }
        });
    });
</script>
<script type="text/javascript">
    //atur data table buat tabel
    $('#tabel-riwayatnf').DataTable({   
    });
    $('select.form-select').select2();
    
</script>