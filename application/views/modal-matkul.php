<div id="addMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('MatkulCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama" class="control-label">KODE MATKUL:</label>
                            <input type="text" class="form-control" id="" name="kode_matkul" required> 
                        </div>
                    </div>
                    <!-- <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">MATA KULIAH:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_atk" name="nama_matkul" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JENJANG:</label>
                            <select type="text" class="form-control" id="jenjang" name="jenjang" required>
                                <option value="D3">D3</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">KELAS:</label>
                            <select type="text" class="form-control" id="kelas" name="kelas" required>
                                <option value="A">KELAS A</option>
                                <option value="B">KELAS B</option>
                                <option value="C">KELAS C</option>
                                <option value="D">KELAS D</option>
                                <option value="A1">KELAS A1</option>
                                <option value="A2">KELAS A2</option>
                                <option value="B1">KELAS B1</option>
                                <option value="B2">KELAS B2</option>
                                <option value="C1">KELAS C1</option>
                                <option value="C2">KELAS C2</option>
                                <option value="-">MKP</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH SKS:</label>
                            <input type="number" class="form-control" id="sks_matkul" name="sks_matkul" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH MHS:</label>
                            <input type="number" class="form-control" id="mhs_matkul" name="mhs" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="tahun_matkul" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="semester_matkul" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>KONTRAK KULIAH:</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="filekontrak[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>ABSEN DOSEN:</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="absendosen[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>ABSEN MAHASISWA:</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="absenmhs[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama" class="control-label">ID MATA KULIAH:</label>
                            <input type="text" class="form-control" id="editkode_matkul" name="editkode_matkul" required> 
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">MATA KULIAH:</label>
                            <input type="text" class="form-control numeric add-satuan" id="editnama_matkul" name="nama_matkul" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JENJANG:</label>
                            <select type="text" class="form-control" id="editjenjang" name="editjenjang" required>
                                <option value="D3">D3</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">KELAS:</label>
                            <select type="text" class="form-control" id="editkelas" name="editkelas" required>
                                <option value="A">KELAS A</option>
                                <option value="B">KELAS B</option>
                                <option value="C">KELAS C</option>
                                <option value="D">KELAS D</option>
                                <option value="-">Tidak Ada Kelas</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH SKS:</label>
                            <input type="number" class="form-control" id="editsks" name="editsks" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH MHS:</label>
                            <input type="number" class="form-control" id="editmhs" name="editmhs" required> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edittahun_matkul" name="edittahun_matkul" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="editsemester_matkul" name="editsemester_matkul" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idmatkul">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="nama" class="control-label">ID MATA KULIAH:</label>
                            <input type="text" class="form-control" id="infokode_matkul" name="infonama_atk" disabled> 
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">MATA KULIAH:</label>
                            <input type="text" class="form-control numeric add-satuan" id="infonama_matkul" name="satuan_atk" disabled> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JENJANG:</label>
                            <select type="text" class="form-control" id="infojenjang" name="infojenjang" disabled>
                                <option value="D3">D3</option>
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                                <option value="S3">S3</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="infojam" class="control-label">KELAS:</label>
                            <select type="text" class="form-control" id="infokelas" name="infokelas" disabled>
                                <option value="A">KELAS A</option>
                                <option value="B">KELAS B</option>
                                <option value="C">KELAS C</option>
                                <option value="D">KELAS D</option>
                                <option value="-">Tidak Ada Kelas</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="infojam" class="control-label">JUMLAH SKS:</label>
                            <input type="number" class="form-control" id="infosks" name="infosks" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH MAHASISWA:</label>
                            <input type="number" class="form-control" id="infomhs" name="infomhs" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">JUMLAH DOSEN:</label>
                            <input type="number" class="form-control" id="infodosen" name="infodosen" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun_matkul" name="infotahun_matkul" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester_matkul" name="infosemester_matkul" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="dosenMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">DOSEN MATAKULIAH</h4> </div>
            <div class="modal-body">
                <form id="form-dosen" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>DOSEN :</label>
                            <select type="text" class="form-control select_dosen" id="dosen" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>STATUS :</label>
                            <select type="text" class="form-control" id="status" name="status_dosen" required>
                                <option value="1">Dosen Koordinator</option>
                                <option value="2">Dosen Anggota</option>
                            </select>  
                        </div>
                    </div>
                    <input type="hidden" name="id_matkul" id="id_matkul">
                    <div class="col-md-12">
                        <div id="data-dosen">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-pengajaran">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>