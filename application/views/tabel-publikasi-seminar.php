<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">NAMA PERTEMUAN</th>
                <th class="text-center">JUDUL ARTIKEL</th>
                <th class="text-center">WAKTU</th>
                <th class="text-center">TEMPAT</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama_user ?></td>
                <td class="text-center"><?= $value->nama_pertemuan ?></td>
                <td class="text-center"><?= $value->judul_artikel ?></td>
                <td class="text-center"><?= $value->waktu ?></td>
                <td class="text-center"><?= $value->tempat ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editPublikasiSeminarModal" id="<?= $value->id ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Publikasi Seminar"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-tqoggle="tooltip" title="Hapus Publikasi Seminar"><i class="icon-trash"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PublikasiSeminarCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#edit_dosen").select2('val',data['id_user']);
                form.find("#edit_nama_pertemuan").val(data['nama_pertemuan']);
                form.find("#edit_judul_artikel").val(data['judul_artikel']);
                form.find("#edit_waktu").val(data['waktu']);
                form.find("#edit_tempat").val(data['tempat']);
                form.find("#id_publikasi_seminar").val(data['id_publikasi_seminar']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PublikasiSeminarCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonip").val(data['nip']);
                form.find("#infonama").val(data['nama']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>