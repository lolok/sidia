<div id="addPrlngkpnModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Tambah Perlengkapan</h4> </div>
            <div class="modal-body">
                <form id="add-prlngkpn" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="nama_prlngkpn" name="nama_prlngkpn" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control prlngkpnpicker" id="tanggal_prlngkpn" name="tanggal_prlngkpn" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric add-satuan" id="satuan_prlngkpn" name="satuan_prlngkpn" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric add-jumlah" id="jumlah_prlngkpn" name="jumlah_prlngkpn" required >  
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control add-total" id="total_prlngkpn" name="jumlahakhir_prlngkpn" onkeydown="return false" > 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade hapusPrlngkpnModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title">Hapus Perlengkapan</h5> </div>
            <div class="modal-body">
                <h4>Apakah anda yakin untuk menghapus perlengkapan ?</h4>
                <small style="color: red">semua data perlengkapan akan ikut terhapus</small>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapus-prlngkpn">Hapus</a>
            </div>
        </div>
    </div>
</div>
<div id="editPrlngkpnModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Perlengkapan</h4> </div>
            <div class="modal-body">
                <form id="edit-prlngkpn" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="editnama_prlngkpn" name="editnama_prlngkpn" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control prlngkpnpicker" id="edittanggal_prlngkpn" name="edittanggal_prlngkpn" onkeydown="return false" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control numeric edit-satuan" id="editsatuan_prlngkpn" name="editsatuan_prlngkpn" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="editjam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control numeric edit-jumlah" id="editjumlah_prlngkpn" name="editjumlah_prlngkpn" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control numeric edit-total" id="editjumlahakhir_prlngkpn" name="editjumlahakhir_prlngkpn" onkeydown="return false">
                        </div>
                    </div>
                    <input type="hidden" name="idprlngkpn" id="idprlngkpn">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoPrlngkpnModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Info Perlengkapan</h4> </div>
            <div class="modal-body">
                <form id="info-prlngkpn" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Uraian Barang:</label>
                            <input type="text" class="form-control" id="infonama_prlngkpn" name="infonama_prlngkpn" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control prlngkpnpicker" id="infotanggal_prlngkpn" name="infotanggal_prlngkpn" onkeydown="return false" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">Satuan:</label>
                            <input type="text" class="form-control" id="infosatuan_prlngkpn" name="infosatuan_prlngkpn" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="infojam" class="control-label">Jumlah:</label>
                            <input type="text" class="form-control" id="infojumlah_prlngkpn" name="infojumlah_prlngkpn" disabled> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">Total:</label>
                            <input type="text" class="form-control" id="infojumlahakhir_prlngkpn" name="infojumlahakhir_prlngkpn" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>