<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">NAMA PENUNJANG</th>
                <th class="text-center">SEMESTER</th>
                <th class="text-center">BERKAS</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->semester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center"><?= $value->nampen ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->tahun ?></td>
                <td class="text-center">
                    <a href="#" class="fileBerkasDokumen" data-toggle="modal" data-target="#fileModalDokumen" id="<?= $value->id_penunjang ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumen Berkas"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id_penunjang ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Penunjang"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_penunjang ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Penunjang"><i class="icon-trash"></i></button></a>
                    <!-- <a href="#" class="infoData" data-toggle="modal" data-target="#infoMatkulModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Penunjang"><i class="ti-more" ></i></button></a> -->
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnampen").val(data['nampen']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editkategori").select2('val',data['kategori']);
                form.find("#editjenis").select2('val',data['jenis']);
                form.find("#editstatus_anggota").select2('val',data['status_anggota']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find(".editselect_penugasan").select2('val',data['bukti_penugasan']);
                form.find(".editselect_dokumen").select2('val',data['bukti_dokumen']);
                form.find("#editbukti_dokumen").val(data['bukti_dokumen']);
                form.find("#id_penunjang").val(data['id_penunjang']);

                if (data['jml_mhs'] != 0) {
                    form.find('#editjml_mhs').css('display', 'block');
                    form.find('#editjml_mhs').val(data['jml_mhs']);
                }else{
                    form.find('#editjml_mhs').css('display', 'none');
                }

                if (data['jml_dosen'] != 0) {
                    form.find('#editjml_dosen').css('display', 'block');
                    form.find('#editjml_dosen').val(data['jml_dosen']);
                }else{
                    form.find('#editjml_dosen').css('display', 'none');
                }
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });

    $('.fileBerkasDokumen').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });

    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PenunjangCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['nampen']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $(".select2").select2();
</script>