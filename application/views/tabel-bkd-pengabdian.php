<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th rowspan="2" class="text-center">STATUS</th>
                <th rowspan="2" class="text-center">BERKAS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->namkeg ?></td>
                <td class="text-center"><?= $value->namauser ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->datatahun ?></td>
                <!-- <td class="text-center"><?= $pengunduh ?></td> -->
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="skpen"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="SK/ Penugasan"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="sertifikat"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Sertifikat"><i class="ti-files"></i></button></a>
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="undangan"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Undangan"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengabdian" status="1" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".activeModal" data-toggle="modal"><span class="label label-danger label-rouded" style="">Tidak Aktif</span></a>
                    <?php }else{?>
                        <a href="" class="dotip aktif_pengabdian" status="0" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Aktif</span></a>
                    <?php }?>   
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkasDokumen" data-toggle="modal" data-target="#fileModal" id="<?= $value->id_pkm ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Dokumen Berkas"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('KegiatanCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editmatkul").select2('val',data['namkeg']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#id_kegiatan").val(data['id_kegiatan']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_pengabdian').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
    });
    $('.fileBerkasDokumen').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PkmCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('KegiatanCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['namkeg']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({  
        ordering:false, 
    });
    $(".select2").select2();
</script>