<div class="table-responsive" id="div-atk">
    <table id="tabel-transportasi" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No.</th>
                <th>Petugas</th>
                <th>Kota</th>
                <th>Tanggal</th>
                <th>Total</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no=0; 
            foreach ($transportasi->result() as $value) {
            $no++;    ?>
            <tr>
                <td><?= $no ?></td>
                <td><?= $value->nama_petugas ?></td>
                <td><?= $value->nama_kota ?></td>
                <td><?= $value->tanggal_pergi ?>-<?= $value->tanggal_pulang ?></td>
                <td><?= number_format($value->jml_netto) ?></td>
                <td>
                    <a href="#" class="editTransportasi" data-toggle="modal" data-target="#editTransportasiModal" id="<?=htmlspecialchars($value->id_transportasi); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Transportasi"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusTransportasi" data-toggle="modal" data-target=".hapusTransportasiModal" id="<?=htmlspecialchars($value->id_transportasi); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Transportasi"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoTransportasi" data-toggle="modal" data-target="#infoTransportasiModal" id="<?=htmlspecialchars($value->id_transportasi); ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Transportasi"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    $('.editTransportasi').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-transportasi');
        $.ajax({
            url:'<?= site_url('TransportasiCntrl/getDataTransportasi') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama_transportasi").val(data['nama_transportasi']);
                form.find("#editkota").val(data['kota']);
                form.find("#edittanggalawal_transportasi").val(data['tanggal_pergi']);
                form.find("#edittanggalakhir_transportasi").val(data['tanggal_pulang']);
                form.find("#editsatuan_transportasi").val(data['satuan']);
                form.find("#editjumlah_transportasi").val(data['jumlah']);
                form.find("#editjumlahakhir_transportasi").val(data['jumlah_akhir']);
                form.find("#idtransportasi").val(data['id_transportasi']);
            }
        });
    });
    $('.hapusTransportasi').on('click',function(){
        idtransportasi = $(this).attr('id');
    });
    $('.infoTransportasi').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-transportasi');
        $.ajax({
            url:'<?= site_url('TransportasiCntrl/getDataTransportasi') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama_transportasi").val(data['nama_transportasi']);
                form.find("#infokota").val(data['namakota']);
                form.find("#infotanggalawal_transportasi").val(data['tanggal_pergi']);
                form.find("#infotanggalakhir_transportasi").val(data['tanggal_pulang']);
                form.find("#infosatuan_transportasi").val(data['satuan']);
                form.find("#infojumlah_transportasi").val(data['jumlah']);
                form.find("#infojumlahakhir_transportasi").val(data['jumlah_akhir']);
            }
        });
    });
    $('#tabel-transportasi').DataTable({   
    });
</script>