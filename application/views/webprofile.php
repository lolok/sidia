<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('temp/plugins/images/undip.png') ?>">
    <title><?= $title ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('temp/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="<?= base_url('temp/asset/css/animate.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url('temp/asset/css/style.css') ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url('temp/asset/css/colors/green-dark.css') ?>" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12 col-xs-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="user-bg" style="height: 300px;"> 
                                        <div class="overlay-box" style="background: url('temp/plugins/images/login-register.jpg');padding-bottom: 30%;margin-top:-55px;padding-top:35%;overflow: hidden;display: block;background-size: cover;background-position: top;height: 100% ">
                                            <div class="row" style="margin-top: 20px">
                                                <div class="col-sm-12 col-md-12 col-lg-12" style="">
                                                    <div class="user-content" style="margin-left: 10px">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <h3 class="text-center">DAFTAR MENU ADMINISTRASI</h3>
                                    <div class="user-btm-box" style="margin-top: -30px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('administrasi/matakuliah') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-book text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Mata Kuliah</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="<?= site_url('administrasi/dosen') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-users text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Dosen</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="<?= site_url('administrasi/ruang') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-list text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Ruang</p> 
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="<?= site_url('administrasi/kelas') ?>" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-calendar text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Jadwal Kelas</p> 
                                        </div>
                                    </div>
                                    <!-- <div class="user-btm-box" style="margin-top: -30px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-book text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Mata Kuliah</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-users text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Dosen</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-list text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Daftar Ruang</p> 
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-calendar text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Jadwal Kelas</p> 
                                        </div>
                                    </div>
                                    <div class="user-btm-box" style="margin-top: -30px">
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-copy text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Data Kerjasama</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                            <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-user text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Data Personal</p>
                                          </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-archive text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Data CV</p> 
                                        </div>
                                        <div class="col-md-3 col-sm-3 col-xs-6 text-center" style="margin-top: 20px">
                                           <a href="" ><button type="button" class="btn btn-default btn-circle btn-xl" style="width: 100px;height: 100px;border-radius: 50px;font-size: 50px;background-color: #3bd0cc"><i class="fa fa-graduation-cap text-white" style=""></i></button></a>
                                            <p class="" style="font-size: 16px;margin-top: 10px">Tri Dharma PT</p> 
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                <!--row -->
                <!-- /.row -->
                
                <!-- ============================================================== -->
                <!-- end right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('temp/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url('temp/asset/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('temp/asset/js/waves.js') ?>"></script>
    <!--Counter js -->
    <script src="<?= base_url('temp/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/counterup/jquery.counterup.min.js') ?>"></script>
    <!--Morris JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/raphael/raphael-min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/morrisjs/morris.js') ?>"></script>
    <!-- chartist chart -->
    <script src="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>
    <!-- Calendar JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <script src='<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>'></script>
    <script src="<?= base_url('temp/plugins/bower_components/calendar/dist/cal-init.js') ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('temp/asset/js/custom.js') ?>"></script>
    
    <script src="<?= base_url('temp/plugins/bower_components/toast-master/js/jquery.toast.js') ?>"></script>
    <!--Style Switcher -->
    <script src="<?= base_url('temp/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') ?>"></script>
</body>

</html>
