<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NAMA</th>
                <th class="text-center">DATA PERSONAL</th>
                <th class="text-center">BERKAS PERSONAL</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center">
                    <a href="#" class="infoData" data-toggle="modal" data-target="#infoPersonalModal" id="<?= $value->id ?>" kategori="data"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Data Dosen"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $value->id ?>" kategori="berkas"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Berkas Dosen"><i class="ti-files"></i></button></a>
                </td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editPersonalModal" id="<?= $value->id ?>" ><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Personal"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Hapus Personal"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoData" data-toggle="modal" data-target="#infoPersonalModal" id="<?= $value->id ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Detail Personal"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PersonalCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnama").val(data['nama']);
                form.find("#editnosertif").val(data['nosertif']);
                form.find("#editperting").val(data['perting']);
                form.find("#editstatus").val(data['status']);
                form.find("#editalamat").val(data['alamat']);
                form.find("#editjurusan").val(data['jurusan']);
                form.find("#editprodi").val(data['prodi']);
                form.find("#editjabatan").val(data['jabatan']);
                form.find("#edittempat").val(data['tempat']);
                form.find("#edittanggal").val(data['tanggal']);
                form.find("#edits1").val(data['s1']);
                form.find("#edits2").val(data['s2']);
                form.find("#edits3").val(data['s3']);
                form.find("#editilmu").val(data['ilmu']);
                form.find("#editnohape").val(data['nohape']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#idpersonal").val(data['id_personal']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PersonalCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-personal').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PersonalCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonama").val(data['nama']);
                form.find("#infonosertif").val(data['nosertif']);
                form.find("#infoperting").val(data['perting']);
                form.find("#infostatus").val(data['status']);
                form.find("#infoalamat").val(data['alamat']);
                form.find("#infojurusan").val(data['jurusan']);
                form.find("#infoprodi").val(data['prodi']);
                form.find("#infojabatan").val(data['jabatan']);
                form.find("#infotempat").val(data['tempat']);
                form.find("#infotanggal").val(data['tanggal']);
                form.find("#infos1").val(data['s1']);
                form.find("#infos2").val(data['s2']);
                form.find("#infos3").val(data['s3']);
                form.find("#infoilmu").val(data['ilmu']);
                form.find("#infonohape").val(data['nohape']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#idpersonal").val(data['id_personal']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
    $('select.form-select').select2();
</script>