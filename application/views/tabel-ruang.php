<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th class="text-center">No.</th>
                <th class="text-center">NIP</th>
                <th class="text-center">NAMA DOSEN</th>
                <th class="text-center">AKSI</th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $i=0;
            foreach ($tabel->result() as $value) { 
                $i++;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nip ?></td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center">
                    <a href="#" class="editData" data-toggle="modal" data-target="#editMatkulModal" id="<?= $value->id_user ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Edit Dosen"><i class="ti-pencil-alt"></i></button></a>
                    <a href="#" class="hapusData" data-toggle="modal" data-target=".hapusModal" id="<?= $value->id_user ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Dosen"><i class="icon-trash"></i></button></a>
                    <a href="#" class="infoData" data-toggle="modal" data-target="#infoMatkulModal" id="<?= $value->id_user ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Dosen"><i class="ti-more" ></i></button></a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('DosenCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editnip").val(data['nip']);
                form.find("#editnama").val(data['nama']);
                form.find("#iddosen").val(data['id_user']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('DosenCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infonip").val(data['nip']);
                form.find("#infonama").val(data['nama']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({   
    });
</script>