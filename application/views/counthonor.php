<div class="row">
    <div class="col-md-5">
        <h4><b>Total Panitia</b></h4>
    </div>
    <div class="col-md-7 text-right">
        <h4><?= $totalpanitia ?></h4>
    </div>
    <div class="col-md-5">
        <h4><b>Total Netto</b></h4>
    </div>
    <div class="col-md-7 text-right">
        <h4>Rp. <?= number_format($totalnetto) ?></h4>
    </div>
</div>