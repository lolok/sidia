<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet" />
    
    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />
<!-- Sweet Alert Css -->
    <link href="<?php echo base_url('mou/plugins/sweetalert/sweetalert.css'); ?>" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
           <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li class="active">
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">face</i>
                            <span>Profil</span>
                        </a>
                    </li>
                    <li>
                        <?php foreach($profil->result() as $result){ 
                            if($result->level == 1){
                            ?>
                        <a href="<?php echo site_url('DashAdmin'); ?>">
                            <i class="material-icons">arrow_back</i>
                            <span>Kembali</span>
                        </a>
                        <?php }else if($result->level == 2){?>
                        <a href="<?php echo site_url('DashUser'); ?>">
                            <i class="material-icons">arrow_back</i>
                            <span>Kembali</span>
                        </a>
                        <?php }else if($result->level == 3){?>
                        <a href="<?php echo site_url('DashFak'); ?>">
                            <i class="material-icons">arrow_back</i>
                            <span>Kembali</span>
                        </a>
                        <?php }}?>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-blue" style="margin-bottom:-10px">
                            <h2>
                                Profil
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <i class="material-icons">description</i>
                                </li>
                            </ul>
                        </div>
                        <div class="body m-t-10">
                            <div class="row">
                                <div class="col-md-3">
                                    <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="300" height="300" alt="User" alt="User" style="margin-top:-10px"/>
                                </div>
                                <div class="col-md-9">
                                    <?php foreach($profil->result() as $result){ ?>
                                        <div class="row">
                                            <div class="col-md-2">
                                                <label class="m-t-5">Username</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" name="username" placeholder="-" value="<?php echo htmlspecialchars($result->username);?>" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Nama</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->nama);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Email</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->email);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">No. HP</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->nohp);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Nama Perusahaan</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->namaper);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>  
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Email Perusahaan</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->emailper);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Provinsi</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                        <input type="text" class="form-control" value="<?php echo htmlspecialchars($result->provinsi);?>" name="bidang" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Kota</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                         <input type="text" value="<?php echo htmlspecialchars($result->kota);?>" class="form-control" name="negara" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">Alamat</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                         <input type="text" value="<?php echo htmlspecialchars($result->alamat);?>" max="15" class="form-control" name="periode" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="m-t-5">NO telepon</label>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group form-float">
                                                    <div class="form-line">
                                                         <input type="text" value="<?php echo htmlspecialchars($result->telp);?>" class="form-control" name="negara" placeholder="-" disabled>
                                                    </div>
                                                </div>
                                            </div>
                               
                                
                                        </div>
                                        <div class="row right m-r-10">
                                            <a class="edit btn bg-teal waves-effect m-l-20 m-b-25 " data-toggle="modal" data-target=".editModal"  id="<?php echo htmlspecialchars($result->id_user); ?>" title="Edit Profil">
                                                <h4 class="m-b-10"><i class="material-icons">edit</i> EDIT PROFIL</h4>
                                            </a>
                                        </div>
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
        <div class="modal fade editModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-blue"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Edit Profil</h4>
                        <i class="material-icons right" style="margin-top:-40px">info</i>
                    </div>
                    <div class="modal-body" >
                        <form id="editform" action="#" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-2">
                                    <label class="m-t-5">Username</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infousername" name="username" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Password</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" class="form-control" id="infopass" name="password" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Nama</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonama" name="nama" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Email</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoemail" name="email" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">No. HP</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonohp" name="nohp" placeholder="-" >
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Nama Perusahaan</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonamaper" name="namaper" placeholder="-" >
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Email Perusahaan</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoemailper" name="emailper" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Provinsi</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoprovinsi" name="provinsi" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Kota</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infokota" class="form-control" name="kota" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Alamat</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infoalamat" class="form-control" name="alamat" placeholder="-" >
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">NO telepon</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotelp" class="form-control" name="telp" placeholder="-" >
                                        </div>
                                    </div>
                                </div>  
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade delModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk membatalkan kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-del" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade kosongModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >File belum tersedia !</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade nasModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload File Naskah PKs</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upload1" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label class="form-label">File Naskah PKS</label>
                                    <div class="form-line">
                                        <input name="fileNaskah" id="fileNaskah" type="file" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="modal fade upModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload File M.O.U</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upload" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label class="form-label">File MOU</label>
                                    <div class="form-line">
                                        <input name="fileMOU" id="fileMOU" type="file" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>
    
    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    
    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/autosize/autosize.js');?>"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/momentjs/moment.js');?>"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js');?>"></script>
    <script>
    $(function () {
        //Textare auto growth
        autosize($('textarea.auto-growth'));

        //Datetimepicker plugin
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });

        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.datepicker1').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.timepicker').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            clearButton: true,
            date: false
        });
    });
    </script>
   
    <script>
    $(document).ready(function(){
        var check1=0; var id;var namaOld=""; var editCheck;

        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'ProfilAdmin/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#infousername").val(data['username']);
                    $("#infopass").val(data['password']);
                    $("#infonama").val(data['nama']);
                    $("#infoemail").val(data['email']);
                    $("#infonohp").val(data['nohp']);
                    $("#infonamaper").val(data['namaper']);
                    $("#infoemailper").val(data['emailper']);
                    $("#infoprovinsi").val(data['provinsi']);
                    $("#infokota").val(data['kota']);
                    $("#infoalamat").val(data['alamat']);
                    $("#infotelp").val(data['telp']);
                    
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'ProfilAdmin/edit/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $(".acc").click(function(){
            id = $(this).attr('id');
        });

        $(".can").click(function(){
            id = $(this).attr('id');
        });

        $(".up").click(function(){
            id = $(this).attr('id');
        });

        $(".nas").click(function(){
            id = $(this).attr('id');
        });

        $(".detsts").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'KerjaFakCtrl/detStat/'+id,
                data:{send:true},
                success:function(msg){
                    $("#detStat").html(msg);
                }
            });
        });


        $("#confirm-acc").click(function(){
            $.ajax({
                url:'MOUAdmin/accept/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-del").click(function(){
            $.ajax({
                url:'KerjaFakCtrl/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'KerjaFakCtrl/upload/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $("#upload1").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'KerjaFakCtrl/upload1/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

    });

    </script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
        $('#upload').validate({
            rules: {
                'fileMOU': {
                     remote: {
                        url: "KerjaFakCtrl/cekFileMOU",
                        type: "post",
                        data: {
                          fileMOU: function() {
                            return $( "#fileMOU" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'fileMOU':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });

        $('#upload1').validate({
            rules: {
                'fileNaskah': {
                     remote: {
                        url: "KerjaFakCtrl/cekFileNaskah",
                        type: "post",
                        data: {
                          fileMOU: function() {
                            return $( "#fileNaskah" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'fileNaskah':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });

        $('#add').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "KerjaFakCtrl/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "KerjaFakCtrl/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "KerjaFakCtrl/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "KerjaFakCtrl/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
        
        $('#editform').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "DashUser/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "DashUser/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "DashUser/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "DashUser/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            ordering: false,
            paging: false,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-all').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>
    <script>
        $(function () {
            new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
            new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
            new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
        });

        function getChartJs(type) {
            var config = null;

            if (type === 'bar') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perusahaan",
                            data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Perguruan Tinggi",
                            data: [28, 48, 40, 19, 86, 27, 80, 40, 19, 86, 27, 80],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Perbankan",
                            data: [11, 54, 30, 29, 46, 57, 20, 30, 29, 46, 57, 20],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Luar Negeri",
                            data: [18, 28, 50, 49, 56, 37, 50, 50, 49, 56, 37, 50],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }, {
                            label: "Lainnya",
                            data: [38, 58, 30, 59, 36, 17, 70, 30, 59, 36, 17, 70],
                            backgroundColor: 'rgba(233, 30, 99, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'radar') {
                config = {
                    type: 'radar',
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            data: [65, 25, 90, 81, 56, 55, 40],
                            borderColor: 'rgba(0, 188, 212, 0.8)',
                            backgroundColor: 'rgba(0, 188, 212, 0.5)',
                            pointBorderColor: 'rgba(0, 188, 212, 0)',
                            pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                            pointBorderWidth: 1
                        }, {
                                label: "My Second dataset",
                                data: [72, 48, 40, 19, 96, 27, 100],
                                borderColor: 'rgba(233, 30, 99, 0.8)',
                                backgroundColor: 'rgba(233, 30, 99, 0.5)',
                                pointBorderColor: 'rgba(233, 30, 99, 0)',
                                pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                                pointBorderWidth: 1
                            }]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            else if (type === 'pie') {
                config = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [225, 50, 100, 40],
                            backgroundColor: [
                                "rgb(233, 30, 99)",
                                "rgb(255, 193, 7)",
                                "rgb(0, 188, 212)",
                                "rgb(139, 195, 74)"
                            ],
                        }],
                        labels: [
                            "Pink",
                            "Amber",
                            "Cyan",
                            "Light Green"
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            return config;
        }
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>