<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />
    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>
    <!-- Sweet Alert Css -->
    <link href="<?php echo base_url('mou/plugins/sweetalert/sweetalert.css'); ?>" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashUser'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li  class="active">
                        <a href="<?php echo site_url('MOUUser'); ?>">
                            <i class="material-icons">description</i>
                            <span>Tambah Kerjasama</span>
                        </a>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-blue">
                            <h2>TAMBAH KERJASAMA</h2>
                            <ul class="header-dropdown m-r--5">
                                
                                <i class="material-icons">add</i>
                                   
                            </ul>
                        </div>
                        <div class="body">

                            <form id="add" action="<?php echo site_url('DashUser/addKerjasama'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Bidang Kerjasama</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Kategori</label> 
                                        <select type="text" name="kategori" class="form-control show-tick" required>
                                            <option disabled selected>Pilih</option>
                                            <option value="1">Perusahaan</option>
                                            <option value="2">Perguruan Tinggi</option>
                                            <option value="3">Perbankan</option>
                                            <option value="4">Luar Negeri</option>
                                            <option value="5">Lainnya</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12"> 
                                    <div class="form-group form-float">
                                        <label class="form-label">Judul Usulan</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="Masukan Judul Usulan Kerjasama Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Manfaat untuk Mitra</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="manfaat_m" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap Mitra Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                         <label class="form-label">Manfaat untuk UNDIP</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="manfaat_u" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap UNDIP" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Negara</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="negara" placeholder="Masukan Negara Mitra Anda" required>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Periode</label>
                                        <div class="form-line">
                                            <input type="number" max="15" class="form-control" name="periode" placeholder="Masukan Periode Kerjasama Anda (Max 15 Tahun)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 1</label>
                                        <div class="form-line">
                                            <input name="file1" type="file" required />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 2</label>
                                        <div class="form-line">
                                            <input name="file2" type="file"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 3</label>
                                        <div class="form-line">
                                            <input name="file3" type="file"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 4</label>
                                        <div class="form-line">
                                            <input name="file4" type="file"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Tabel Rekap Kerjasama</h4>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Tahun</th>
                                    <th>Perusahaan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Perbankan</th>
                                    <th>Luar Negeri</th>
                                    <th>Lainnya</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>Tahun</th>
                                    <th>Perusahaan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Perbankan</th>
                                    <th>Luar Negeri</th>
                                    <th>Lainnya</th>
                                </tr>
                            </tfoot>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Juni</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="largeModalLabel">Tabel Daftar M.O.U</h4>
                    </div>
                    <div class="modal-body" >
                        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect">SAVE CHANGES</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- JQuery Steps Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-steps/jquery.steps.js'); ?>"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
   <script>
    $(function () {
    $('#sign_up').validate({
            rules: {
                'terms': {
                    required: true
                },
                'confirm': {
                    equalTo: '[name="password"]'
                },
                'username': {
                     remote: {
                        url: "DaftarCntrl/cekUser",
                        type: "post",
                        data: {
                          username: function() {
                            return $( "#username" ).val();
                          }
                        }
                    }
                },
                'email': {
                     remote: {
                        url: "DaftarCntrl/cekEmail",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#email" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'confirm': {
                    equalTo: 'Masukan password yang sama.',
                    required: 'Confirm tidak boleh kosong.'
                },
                'username':{
                    remote: 'Username sudah terdaftar.',
                    required: 'Username tidak boleh kosong.'
                },
                'password':{
                    required: 'Password tidak boleh kosong.'
                },
                'nama':{
                    required: 'Password tidak boleh kosong.'
                },
                'email':{
                    remote: 'Email sudah terdaftar.',
                    required: 'Email tidak boleh kosong.'
                },
                'hape':{
                    required: 'No. HP tidak boleh kosong.'
                },
                'namaper':{
                    required: 'Nama Perusahaan tidak boleh kosong.'
                },
                'emailper':{
                    required: 'Email Perusahaan tidak boleh kosong.'
                },
                'provinsi':{
                    required: 'Provinsi tidak boleh kosong.'
                },
                'kota':{
                    required: 'Kota tidak boleh kosong.'
                },
                'alamat':{
                    required: 'Alamat Perusahaan tidak boleh kosong.'
                },
                'telp':{
                    required: 'Telepon tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>