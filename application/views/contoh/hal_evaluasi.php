<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet" />
    
    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />
<!-- Sweet Alert Css -->
    <link href="<?php echo base_url('mou/plugins/sweetalert/sweetalert.css'); ?>" rel="stylesheet" />
    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
           <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashFakultas'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('KerjaFakCtrl'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Kerjasama</span>
                        </a>
                    </li>
                    <li >
                        <a href="<?php echo site_url('InisiasiCtrl'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Inisiasi</span>
                        </a>
                    </li>
                     <li  class="active">
                        <a href="<?php echo site_url('EvalFak'); ?>">
                            <i class="material-icons">edit</i>
                            <span>Evaluasi</span>
                        </a>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">

            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    
                    <div class="card">
                        <div class="header bg-light-blue" style="margin-bottom:-10px">
                            <h2>
                                Survei Kerjasama
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <i class="material-icons">description</i>
                                </li>
                            </ul>
                        </div>
                        <?php if($survei > 0){?>
                        <div class="body m-t-10">
                            <h3>Terimakasih telah melakukan survei.</h3>
                        </div>
                        
                        <?php }else{ ?>
                        <div class="body m-t-10">
                            <h4>Answer the following questionnaire according to your opinion on our service/Berikan Penilaian Anda untuk layanan di bawah ini</h4>
                            <br>

                            <form action="<?php echo site_url('EvalFak/tambahSurvei'); ?>" method="POST" enctype="multipart/form-data">
                                <table>
                                    <thead>
                                        <th></th>
                                        <th style="font-size:20px" align="center">Very disagree/Sangat tidak setuju</th>
                                        <th style="font-size:20px" align="center">Disagree/Tidak setuju</th>
                                        <th style="font-size:20px" align="center">Neutral/Netral</th>
                                        <th style="font-size:20px" align="center">Agree/Setuju</th>
                                    </thead>
                                    <tbody>
                                        <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >Fast process in drafting the MoU or MoA <br><b>Proses pembuatan naskah kerjasama dilakukan dengan cepat dan tepat</b></td>
                                            <td align="center">
                                                <input name="sur1" type="radio" id="sur11" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur11"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur1" type="radio" id="sur12" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur12"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur1" type="radio" id="sur13" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur13"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur1" type="radio" id="sur14" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur14"></label>
                                                
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >The Staff of UNDIP Collaboration Unit responds to my inquiries precisely and professionally<br><b>Staff kerjasama bekerja profesional dan responsif memenuhi kebutuhan mitra</b></td>
                                            <td align="center">
                                                <input name="sur2" type="radio" id="sur21" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur21"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur2" type="radio" id="sur22" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur22"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur2" type="radio" id="sur23" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur23"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur2" type="radio" id="sur24" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur24"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >UNDIP provides an excellent assistance to fulfill our inquiries<br><b>UNDIP memberikan pendampingan yang terbaik untuk memenuhi kebutuhan kami(mitra kerjasama)</b></td>
                                            <td align="center">
                                                <input name="sur3" type="radio" id="sur31" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur31"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur3" type="radio" id="sur32" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur32"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur3" type="radio" id="sur33" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur33"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur3" type="radio" id="sur34" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur34"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >This cooperation is conducted in accordance with our agreement<br><b>Kerjasama/kegiatan ini sesuai dengan harapan kami(mitra)</b></td>
                                            <td align="center">
                                                <input name="sur4" type="radio" id="sur41" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur41"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur4" type="radio" id="sur42" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur42"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur4" type="radio" id="sur43" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur43"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur4" type="radio" id="sur44" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur44"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >Our Institution gains a worthwhile benefit through the collaboration with UNDIP<br><b>Kami(mitra kerjasama) mendapatkan hal yang berguna dari kerjasama antara institusi kami dan UNDIP</b></td>
                                            <td align="center">
                                                <input name="sur5" type="radio" id="sur51" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur51"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur5" type="radio" id="sur52" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur52"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur5" type="radio" id="sur53" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur53"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur5" type="radio" id="sur54" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur54"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >The Implementation of the Agreement is in accordance to the MoU<br><b>Implementasi kerjasama sesuai dengan MoU</b></td>
                                            <td align="center">
                                                <input name="sur6" type="radio" id="sur61" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur61"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur6" type="radio" id="sur62" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur62"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur6" type="radio" id="sur63" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur63"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur6" type="radio" id="sur64" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur64"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >Final Report of the Collaboration is explicit and punctual<br><b>Kami(mitra kerjasama) akan kembali ke UNDIP di masa mendatang untuk kerjasama/acara lain</b></td>
                                            <td align="center">
                                                <input name="sur7" type="radio" id="sur71" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur71"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur7" type="radio" id="sur72" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur72"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur7" type="radio" id="sur73" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur73"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur7" type="radio" id="sur74" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur74"></label>
                                                
                                            </td>
                                        </tr>
                                         <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" >We plan to cooperate with UNDIP in the near future</td>
                                            <td align="center">
                                                <input name="sur8" type="radio" id="sur81" class="radio-col-blue with-gap" value="1"/>
                                                <label for="sur81"></label>
                                            </td>
                                            <td align="center">
                                                <input name="sur8" type="radio" id="sur82" class="radio-col-blue with-gap" value="2"/>
                                                <label for="sur82"></label>                                           
                                            </td>
                                            <td align="center">
                                                <input name="sur8" type="radio" id="sur83" class="radio-col-blue with-gap" value="3"/>
                                                <label for="sur83"></label>
                                                
                                            </td>
                                            <td align="center">
                                                <input name="sur8" type="radio" id="sur84" class="radio-col-blue with-gap" value="4"/>
                                                <label for="sur84"></label>
                                                
                                            </td>
                                        </tr>
                                        <tr><td>&nbsp</td></tr>    
                                        <tr style="margin-top:20px">
                                            <td width="30%" style="font-size:20px" ></td>
                                            <td align="center">
                                               
                                            </td>
                                            <td align="center">
                                                                                        
                                            </td>
                                            <td align="center">
                                               
                                                
                                            </td>
                                            <td align="right">
                                                 <button type="submit" class="btn btn-link waves-effect right">SUBMIT</button>
                                            </td>
                                        </tr>

                                    </tbody>
                                   
                                    
                                </table>
                    
                            </form>
                            
                        </div>
                        <?php }?>
                    </div>

                </div>
                

                <!-- #END# Bar Chart -->
            </div>
        </div>
         <!-- Modal Detail Rekap -->
        <div class="modal fade detstatModal" id="" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Detail Penstatusan</h4>
                        <i class="material-icons right" style="margin-top:-40px">done</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-detstat" id="detStat">
                            
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->


        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade delModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk membatalkan kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-del" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>
    
    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    
    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/autosize/autosize.js');?>"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/momentjs/moment.js');?>"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js');?>"></script>
    <script>
    $(function () {
        //Textare auto growth
        autosize($('textarea.auto-growth'));

        //Datetimepicker plugin
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });

        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.datepicker1').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.timepicker').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            clearButton: true,
            date: false
        });
    });
    </script>
    <script>
    $(document).ready(function(){

        $("#subkategori1").hide();
        $("#subkategori2").hide();
        $("#subkategori3").hide();
        $("#subkategori4").hide();
        $("#asubkategori1").hide();
        $("#asubkategori2").hide();
        $("#asubkategori3").hide();
        $("#asubkategori4").hide();
        $("#editsubkategori1").hide();
        $("#editsubkategori2").hide();
        $("#editsubkategori3").hide();
        $("#editsubkategori4").hide();


        $("#kategori").change(function(){
            x = document.getElementById("kategori").value;
            if (x==1) {
               $("#subkategori1").show();
               $("#subkategori").hide();
               $("#subkategori2").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
            }else if (x==2){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
               $("#subkategori2").show(); 
            }else if (x==3){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori2").hide();
               $("#subkategori4").hide();
               $("#subkategori3").show(); 
            }else if (x==4){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori2").hide();
               $("#subkategori4").show(); 
            }
        });

         $("#kategori1").change(function(){
            x = document.getElementById("kategori1").value;
            if (x==1) {
               $("#asubkategori1").show();
               $("#asubkategori").hide();
               $("#asubkategori2").hide();
               $("#asubkategori3").hide();
               $("#asubkategori4").hide();
            }else if (x==2){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori3").hide();
               $("#asubkategori4").hide();
               $("#asubkategori2").show(); 
            }else if (x==3){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori2").hide();
               $("#asubkategori4").hide();
               $("#asubkategori3").show(); 
            }else if (x==4){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori3").hide();
               $("#asubkategori2").hide();
               $("#asubkategori4").show(); 
            }
        });
        
         $("#editkategori1").change(function(){
            x = document.getElementById("editkategori1").value;
            if (x==1) {
               $("#editsubkategori1").show();
               $("#editsubkategori").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori4").hide();
            }else if (x==2){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori4").hide();
               $("#editsubkategori2").show(); 
            }else if (x==3){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori4").hide();
               $("#editsubkategori3").show(); 
            }else if (x==4){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori4").show(); 
            }
        });
            
            
    });

    </script>
    <script>
    $(document).ready(function(){
        var check1=0; var id;var namaOld=""; var editCheck;

        $(".info").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'DashUser/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#infomitra").val(data['mitra']);
                    $("#infojudul").val(data['judul']);
                    $("#infotahun").val(data['tahun']);
                    $("#infotglusul").val(data['tgl_usul']);
                    $("#infotgllama").val(data['tgl_lama']);
                    $("#infokategori").val(data['kategori']);
                    $("#infosubkategori").val(data['subkategori']);
                    $("#infopengusul").val(data['pengusul']);
                    $("#infobidang1").val(data['bidang1']);
                    $("#infobidang2").val(data['bidang2']);
                    $("#infobidang3").val(data['bidang3']);
                    $("#infobidang4").val(data['bidang4']);
                    $("#infobidang5").val(data['bidang5']);
                    $("#infonegara").val(data['negara']);
                    if($('#infobidang1').val()!=0) {
                        $('input[name="infobidang1"]').prop('checked', true);
                    }else if($('#infobidang1').val()==0) {
                        $('input[name="infobidang1"]').prop('checked', false);
                    };
                    if($('#infobidang2').val()!=0) {
                        $('input[name="infobidang2"]').prop('checked', true);
                    }else if($('#infobidang2').val()==0) {
                        $('input[name="infobidang2"]').prop('checked', false);
                    };
                    if($('#infobidang3').val()!=0) {
                        $('input[name="infobidang3"]').prop('checked', true);
                    }else if($('#infobidang3').val()==0) {
                        $('input[name="infobidang3"]').prop('checked', false);
                    };
                    if($('#infobidang4').val()!=0) {
                        $('input[name="infobidang4"]').prop('checked', true);
                    }else if($('#infobidang4').val()==0) {
                        $('input[name="infobidang4"]').prop('checked', false);
                    };
                    if($('#infobidang5').val()!=0) {
                        $('input[name="infobidang5"]').prop('checked', true);
                    }else if($('#infobidang5').val()==0) {
                        $('input[name="infobidang5"]').prop('checked', false);
                    };
                    $("#infoperiode").val(data['periode']);
                    $("#infomanfaatm").val(data['manfaat_m']);
                    $("#infomanfaatu").val(data['manfaat_u']);
                    $("#editberkas1").click(function(){
                        file = (data['berkas1']);
                        folder = (data['folder']);
                        
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas2").click(function(){
                        file = (data['berkas2']);
                        folder = (data['folder']);
                        
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas3").click(function(){
                        file = (data['berkas3']);
                        folder = (data['folder']);
                        
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas4").click(function(){
                        file = (data['berkas4']);
                        folder = (data['folder']);
                        
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                }
            });
        });

        $(".edit").click(function(){
            id = $(this).attr('id');
           $.ajax({
                url:'DashUser/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editmitra").val(data['mitra']);
                    $("#editjenis").val(data['jenis']);
                    $("#editjudul").val(data['judul']);
                    $("#edittahun").val(data['tahun']);
                    $("#edittglusul").val(data['tgl_usul']);
                    $("#edittgllama").val(data['tgl_lama']);
                    $("#editkategori").val(data['kategori']);
                    $("#editsubkategori").val(data['subkategori']);
                    $("#editpengusul").val(data['pengusul']);
                    $("#bidang1").val(data['bidang1']);
                    $("#bidang2").val(data['bidang2']);
                    $("#bidang3").val(data['bidang3']);
                    $("#bidang4").val(data['bidang4']);
                    $("#bidang5").val(data['bidang5']);
                    $("#editnegara").val(data['negara']);
                    if($('#bidang1').val()!=0) {
                        $('input[name="editbidang1"]').prop('checked', true);
                    }else if($('#infobidang1').val()==0) {
                        $('input[name="editbidang1"]').prop('checked', false);
                    };
                    if($('#bidang2').val()!=0) {
                        $('input[name="editbidang2"]').prop('checked', true);
                    }else if($('#editbidang2').val()==0) {
                        $('input[name="editbidang2"]').prop('checked', false);
                    };
                    if($('#bidang3').val()!=0) {
                        $('input[name="editbidang3"]').prop('checked', true);
                    }else if($('#editbidang3').val()==0) {
                        $('input[name="editbidang3"]').prop('checked', false);
                    };
                    if($('#bidang4').val()!=0) {
                        $('input[name="editbidang4"]').prop('checked', true);
                    }else if($('#editbidang4').val()==0) {
                        $('input[name="editbidang4"]').prop('checked', false);
                    };
                    if($('#bidang5').val()!=0) {
                        $('input[name="editbidang5"]').prop('checked', true);
                    }else if($('#editbidang5').val()==0) {
                        $('input[name="editbidang5"]').prop('checked', false);
                    };
                    $("#editperiode").val(data['periode']);
                    $("#editmanfaatm").val(data['manfaat_m']);
                    $("#editmanfaatu").val(data['manfaat_u']);
                    $("#editberkas1").click(function(){
                        file = (data['berkas1']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas2").click(function(){
                        file = (data['berkas2']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas3").click(function(){
                        file = (data['berkas3']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas4").click(function(){
                        file = (data['berkas4']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'DashUser/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $(".acc").click(function(){
            id = $(this).attr('id');
        });

        $(".can").click(function(){
            id = $(this).attr('id');
        });

        $(".up").click(function(){
            id = $(this).attr('id');
        });


        $("#confirm-acc").click(function(){
            $.ajax({
                url:'MOUAdmin/accept/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-del").click(function(){
            $.ajax({
                url:'DashUser/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $(".detsts").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'DashUser/detStat/'+id,
                data:{send:true},
                success:function(msg){
                    $("#detStat").html(msg);
                }
            });
        });

        $("#upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upload/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

    });

    </script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
        $('#add').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "DashUser/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "DashUser/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "DashUser/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "DashUser/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
        
        $('#editform').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "MOUAdmin/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "MOUAdmin/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "MOUAdmin/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "MOUAdmin/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            ordering: false,
            paging: false,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-all').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>
    <script>
        $(function () {
            new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
            new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
            new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
        });

        function getChartJs(type) {
            var config = null;

            if (type === 'bar') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perusahaan",
                            data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Perguruan Tinggi",
                            data: [28, 48, 40, 19, 86, 27, 80, 40, 19, 86, 27, 80],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Perbankan",
                            data: [11, 54, 30, 29, 46, 57, 20, 30, 29, 46, 57, 20],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Luar Negeri",
                            data: [18, 28, 50, 49, 56, 37, 50, 50, 49, 56, 37, 50],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }, {
                            label: "Lainnya",
                            data: [38, 58, 30, 59, 36, 17, 70, 30, 59, 36, 17, 70],
                            backgroundColor: 'rgba(233, 30, 99, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'radar') {
                config = {
                    type: 'radar',
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            data: [65, 25, 90, 81, 56, 55, 40],
                            borderColor: 'rgba(0, 188, 212, 0.8)',
                            backgroundColor: 'rgba(0, 188, 212, 0.5)',
                            pointBorderColor: 'rgba(0, 188, 212, 0)',
                            pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                            pointBorderWidth: 1
                        }, {
                                label: "My Second dataset",
                                data: [72, 48, 40, 19, 96, 27, 100],
                                borderColor: 'rgba(233, 30, 99, 0.8)',
                                backgroundColor: 'rgba(233, 30, 99, 0.5)',
                                pointBorderColor: 'rgba(233, 30, 99, 0)',
                                pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                                pointBorderWidth: 1
                            }]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            else if (type === 'pie') {
                config = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [225, 50, 100, 40],
                            backgroundColor: [
                                "rgb(233, 30, 99)",
                                "rgb(255, 193, 7)",
                                "rgb(0, 188, 212)",
                                "rgb(139, 195, 74)"
                            ],
                        }],
                        labels: [
                            "Pink",
                            "Amber",
                            "Cyan",
                            "Light Green"
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            return config;
        }
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>