<thead>
    <tr>
        <th>No.</th>
        <th>Kegiatan</th>
        <th>Proses</th>
        <th>Tanggal Update</th>
        <th>Jam Update</th>
        <th>Pengubah</th>
    </tr>
</thead>
<tbody>
    <?php 
    $no=1;
    foreach ($detstat->result() as $result){
        ?>

        <tr>
          <td><?php echo htmlspecialchars($no); ?></td>   
          <td><?php echo htmlspecialchars($result->kegiatan); ?></td> 
          <td><?php echo htmlspecialchars($result->proses); ?></td> 
          <td><?php echo htmlspecialchars($result->tgl_update); ?></td> 
          <td><?php echo htmlspecialchars($result->jam); ?></td>
          <td><?php echo htmlspecialchars($result->nama); ?></td>
        </tr>

    <?php $no++; }?>
</tbody>