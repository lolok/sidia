<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

     <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-facebook" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-twitter" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-linkedin" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-instagram" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-rss" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <!-- #END# Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
             <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('IndexCntrl'); ?>">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('DaftarKerjasama'); ?>">
                            <i class="material-icons">description</i>
                            <span>Daftar Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li  class="active">
                        <a href="<?php echo site_url('StatKerjasama'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Statistik Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('LoginCntrl'); ?>">
                            <i class="material-icons">person</i>
                            <span>Login</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-blue" style="margin-bottom:-10px">
                            <h2>
                                Statistik Kerjasama Universitas Diponegoro
                            </h2>
                            <ul class="header-dropdown m-r-5" style="margin-top:-5px">  
                                <li>

                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#tahun1" data-toggle="tab">
                                        <?php echo $tahun1 ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#tahun2" data-toggle="tab">
                                        <?php echo $tahun2 ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#tahun3" data-toggle="tab">
                                        <?php echo $tahun3 ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#tahun4" data-toggle="tab">
                                        <?php echo $tahun4 ?>
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#tahun5" data-toggle="tab">
                                        <?php echo $tahun5 ?>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="tahun1">
                                    <canvas id="bar_chart1" height="100"></canvas>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10 m-r-10" data-toggle="modal" data-target="#rekapModal1">REKAP KERJASAMA</button>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10" data-toggle="modal" data-target="#detailModal1">DETAIL KERJASAMA</button>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="tahun2">
                                    <canvas id="bar_chart2" height="100"></canvas>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10 m-r-10" data-toggle="modal" data-target="#rekapModal2">REKAP KERJASAMA</button>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10" data-toggle="modal" data-target="#detailModal2">DETAIL KERJASAMA</button>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="tahun3">
                                    <canvas id="bar_chart3" height="100"></canvas>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10 m-r-10" data-toggle="modal" data-target="#rekapModal3">REKAP KERJASAMA</button>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10" data-toggle="modal" data-target="#detailModal3">DETAIL KERJASAMA</button>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="tahun4">
                                    <canvas id="bar_chart4" height="100"></canvas>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10 m-r-10" data-toggle="modal" data-target="#rekapModal4">REKAP KERJASAMA</button>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10" data-toggle="modal" data-target="#detailModal4">DETAIL KERJASAMA</button>
                                </div>
                                <div role="tabpanel" class="tab-pane fade in" id="tahun5">
                                    <canvas id="bar_chart5" height="100"></canvas>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10 m-r-10" data-toggle="modal" data-target="#rekapModal5">REKAP KERJASAMA</button>
                                    <button type="button" class="btn btn-lg waves-effect bg-light-blue m-t-10" data-toggle="modal" data-target="#detailModal5">DETAIL KERJASAMA</button>
                                </div>
                            </div>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="rekapModal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Rekap Kerjasama Tahun <?php echo $tahun1 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Pemerintah</th>
                                    <th>Lembaga Swasta</th>
                                    <th>Dunia Industri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td><?php echo $per11 ?></td>
                                    <td><?php echo $pem11 ?></td>
                                    <td><?php echo $lem11 ?></td>
                                    <td><?php echo $iem11 ?></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td><?php echo $per12 ?></td>
                                    <td><?php echo $pem12 ?></td>
                                    <td><?php echo $lem12 ?></td>
                                    <td><?php echo $iem12 ?></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td><?php echo $per13 ?></td>
                                    <td><?php echo $pem13 ?></td>
                                    <td><?php echo $lem13 ?></td>
                                    <td><?php echo $iem13 ?></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td><?php echo $per14 ?></td>
                                    <td><?php echo $pem14 ?></td>
                                    <td><?php echo $lem14 ?></td>
                                    <td><?php echo $iem14 ?></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td><?php echo $per15 ?></td>
                                    <td><?php echo $pem15 ?></td>
                                    <td><?php echo $lem15 ?></td>
                                    <td><?php echo $iem15 ?></td>
                                </tr>
                                <tr>
                                    <td>Juni</td> 
                                    <td><?php echo $per16 ?></td>
                                    <td><?php echo $pem16 ?></td>
                                    <td><?php echo $lem16 ?></td>
                                    <td><?php echo $iem16 ?></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td><?php echo $per17 ?></td>
                                    <td><?php echo $pem17 ?></td>
                                    <td><?php echo $lem17 ?></td>
                                    <td><?php echo $iem17 ?></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td><?php echo $per18 ?></td>
                                    <td><?php echo $pem18 ?></td>
                                    <td><?php echo $lem18 ?></td>
                                    <td><?php echo $iem18 ?></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td><?php echo $per19 ?></td>
                                    <td><?php echo $pem19 ?></td>
                                    <td><?php echo $lem19 ?></td>
                                    <td><?php echo $iem19 ?></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td><?php echo $per110 ?></td>
                                    <td><?php echo $pem110 ?></td>
                                    <td><?php echo $lem110 ?></td>
                                    <td><?php echo $iem110 ?></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td><?php echo $per111 ?></td>
                                    <td><?php echo $pem111 ?></td>
                                    <td><?php echo $lem111 ?></td>
                                    <td><?php echo $iem111 ?></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td><?php echo $per112 ?></td>
                                    <td><?php echo $pem112 ?></td>
                                    <td><?php echo $lem112 ?></td>
                                    <td><?php echo $iem112 ?></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="rekapModal2" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Rekap Kerjasama Tahun <?php echo $tahun2 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Pemerintah</th>
                                    <th>Lembaga Swasta</th>
                                    <th>Dunia Industri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td><?php echo $per21 ?></td>
                                    <td><?php echo $pem21 ?></td>
                                    <td><?php echo $lem21 ?></td>
                                    <td><?php echo $iem21 ?></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td><?php echo $per22 ?></td>
                                    <td><?php echo $pem22 ?></td>
                                    <td><?php echo $lem22 ?></td>
                                    <td><?php echo $iem22 ?></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td><?php echo $per23 ?></td>
                                    <td><?php echo $pem23 ?></td>
                                    <td><?php echo $lem23 ?></td>
                                    <td><?php echo $iem23 ?></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td><?php echo $per24 ?></td>
                                    <td><?php echo $pem24 ?></td>
                                    <td><?php echo $lem24 ?></td>
                                    <td><?php echo $iem24 ?></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td><?php echo $per25 ?></td>
                                    <td><?php echo $pem25 ?></td>
                                    <td><?php echo $lem25 ?></td>
                                    <td><?php echo $iem25 ?></td>
                                </tr>
                                <tr>
                                    <td>Juni</td> 
                                    <td><?php echo $per26 ?></td>
                                    <td><?php echo $pem26 ?></td>
                                    <td><?php echo $lem26 ?></td>
                                    <td><?php echo $iem26 ?></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td><?php echo $per27 ?></td>
                                    <td><?php echo $pem27 ?></td>
                                    <td><?php echo $lem27 ?></td>
                                    <td><?php echo $iem27 ?></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td><?php echo $per28 ?></td>
                                    <td><?php echo $pem28 ?></td>
                                    <td><?php echo $lem28 ?></td>
                                    <td><?php echo $iem28 ?></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td><?php echo $per29 ?></td>
                                    <td><?php echo $pem29 ?></td>
                                    <td><?php echo $lem29 ?></td>
                                    <td><?php echo $iem29 ?></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td><?php echo $per210 ?></td>
                                    <td><?php echo $pem210 ?></td>
                                    <td><?php echo $lem210 ?></td>
                                    <td><?php echo $iem210 ?></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td><?php echo $per211 ?></td>
                                    <td><?php echo $pem211 ?></td>
                                    <td><?php echo $lem211 ?></td>
                                    <td><?php echo $iem211 ?></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td><?php echo $per212 ?></td>
                                    <td><?php echo $pem212 ?></td>
                                    <td><?php echo $lem212 ?></td>
                                    <td><?php echo $iem212 ?></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="rekapModal3" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Rekap Kerjasama Tahun <?php echo $tahun3 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Pemerintah</th>
                                    <th>Lembaga Swasta</th>
                                    <th>Dunia Industri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td><?php echo $per31 ?></td>
                                    <td><?php echo $pem31 ?></td>
                                    <td><?php echo $lem31 ?></td>
                                    <td><?php echo $iem31 ?></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td><?php echo $per32 ?></td>
                                    <td><?php echo $pem32 ?></td>
                                    <td><?php echo $lem32 ?></td>
                                    <td><?php echo $iem32 ?></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td><?php echo $per33 ?></td>
                                    <td><?php echo $pem33 ?></td>
                                    <td><?php echo $lem33 ?></td>
                                    <td><?php echo $iem33 ?></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td><?php echo $per34 ?></td>
                                    <td><?php echo $pem34 ?></td>
                                    <td><?php echo $lem34 ?></td>
                                    <td><?php echo $iem34 ?></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td><?php echo $per35 ?></td>
                                    <td><?php echo $pem35 ?></td>
                                    <td><?php echo $lem35 ?></td>
                                    <td><?php echo $iem35 ?></td>
                                </tr>
                                <tr>
                                    <td>Juni</td>
                                    <td><?php echo $per36 ?></td>
                                    <td><?php echo $pem36 ?></td>
                                    <td><?php echo $lem36 ?></td>
                                    <td><?php echo $iem36 ?></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td><?php echo $per37 ?></td>
                                    <td><?php echo $pem37 ?></td>
                                    <td><?php echo $lem37 ?></td>
                                    <td><?php echo $iem37 ?></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td><?php echo $per38 ?></td>
                                    <td><?php echo $pem38 ?></td>
                                    <td><?php echo $lem38 ?></td>
                                    <td><?php echo $iem38 ?></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td><?php echo $per39 ?></td>
                                    <td><?php echo $pem39 ?></td>
                                    <td><?php echo $lem39 ?></td>
                                    <td><?php echo $iem39 ?></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td><?php echo $per310 ?></td>
                                    <td><?php echo $pem310 ?></td>
                                    <td><?php echo $lem310 ?></td>
                                    <td><?php echo $iem310 ?></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td><?php echo $per311 ?></td>
                                    <td><?php echo $pem311 ?></td>
                                    <td><?php echo $lem311 ?></td>
                                    <td><?php echo $iem311 ?></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td><?php echo $per312 ?></td>
                                    <td><?php echo $pem312 ?></td>
                                    <td><?php echo $lem312 ?></td>
                                    <td><?php echo $iem312 ?></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="rekapModal4" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Rekap Kerjasama Tahun <?php echo $tahun4 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Pemerintah</th>
                                    <th>Lembaga Swasta</th>
                                    <th>Dunia Industri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td><?php echo $per41 ?></td>
                                    <td><?php echo $pem41 ?></td>
                                    <td><?php echo $lem41 ?></td>
                                    <td><?php echo $iem41 ?></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td><?php echo $per42 ?></td>
                                    <td><?php echo $pem42 ?></td>
                                    <td><?php echo $lem42 ?></td>
                                    <td><?php echo $iem42 ?></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td><?php echo $per43 ?></td>
                                    <td><?php echo $pem43 ?></td>
                                    <td><?php echo $lem43 ?></td>
                                    <td><?php echo $iem43 ?></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td><?php echo $per44 ?></td>
                                    <td><?php echo $pem44 ?></td>
                                    <td><?php echo $lem44 ?></td>
                                    <td><?php echo $iem44 ?></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td><?php echo $per45 ?></td>
                                    <td><?php echo $pem45 ?></td>
                                    <td><?php echo $lem45 ?></td>
                                    <td><?php echo $iem45 ?></td>
                                </tr>
                                <tr>
                                    <td>Juni</td> 
                                    <td><?php echo $per46 ?></td>
                                    <td><?php echo $pem46 ?></td>
                                    <td><?php echo $lem46 ?></td>
                                    <td><?php echo $iem46 ?></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td><?php echo $per47 ?></td>
                                    <td><?php echo $pem47 ?></td>
                                    <td><?php echo $lem47 ?></td>
                                    <td><?php echo $iem47 ?></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td><?php echo $per48 ?></td>
                                    <td><?php echo $pem48 ?></td>
                                    <td><?php echo $lem48 ?></td>
                                    <td><?php echo $iem48 ?></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td><?php echo $per49 ?></td>
                                    <td><?php echo $pem49 ?></td>
                                    <td><?php echo $lem49 ?></td>
                                    <td><?php echo $iem49 ?></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td><?php echo $per410 ?></td>
                                    <td><?php echo $pem410 ?></td>
                                    <td><?php echo $lem410 ?></td>
                                    <td><?php echo $iem410 ?></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td><?php echo $per411 ?></td>
                                    <td><?php echo $pem411 ?></td>
                                    <td><?php echo $lem411 ?></td>
                                    <td><?php echo $iem411 ?></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td><?php echo $per412 ?></td>
                                    <td><?php echo $pem412 ?></td>
                                    <td><?php echo $lem412 ?></td>
                                    <td><?php echo $iem412 ?></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="rekapModal5" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Rekap Kerjasama Tahun <?php echo $tahun5 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                            <thead>
                                <tr>
                                    <th>Bulan</th>
                                    <th>Perguruan Tinggi</th>
                                    <th>Pemerintah</th>
                                    <th>Lembaga Swasta</th>
                                    <th>Dunia Industri</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Januari</td> 
                                    <td><?php echo $per51 ?></td>
                                    <td><?php echo $pem51 ?></td>
                                    <td><?php echo $lem51 ?></td>
                                    <td><?php echo $iem51 ?></td>
                                </tr>
                                <tr>
                                    <td>Februari</td> 
                                    <td><?php echo $per52 ?></td>
                                    <td><?php echo $pem52 ?></td>
                                    <td><?php echo $lem52 ?></td>
                                    <td><?php echo $iem52 ?></td>
                                </tr>
                                <tr>
                                    <td>Maret</td> 
                                    <td><?php echo $per53 ?></td>
                                    <td><?php echo $pem53 ?></td>
                                    <td><?php echo $lem53 ?></td>
                                    <td><?php echo $iem53 ?></td>
                                </tr>
                                <tr>
                                    <td>April</td> 
                                    <td><?php echo $per54 ?></td>
                                    <td><?php echo $pem54 ?></td>
                                    <td><?php echo $lem54 ?></td>
                                    <td><?php echo $iem54 ?></td>
                                </tr>
                                <tr>
                                    <td>Mei</td> 
                                    <td><?php echo $per55 ?></td>
                                    <td><?php echo $pem55 ?></td>
                                    <td><?php echo $lem55 ?></td>
                                    <td><?php echo $iem55 ?></td>
                                </tr>
                                <tr>
                                    <td>Juni</td> 
                                    <td><?php echo $per56 ?></td>
                                    <td><?php echo $pem56 ?></td>
                                    <td><?php echo $lem56 ?></td>
                                    <td><?php echo $iem56 ?></td>
                                </tr>
                                <tr>
                                    <td>Juli</td> 
                                    <td><?php echo $per57 ?></td>
                                    <td><?php echo $pem57 ?></td>
                                    <td><?php echo $lem57 ?></td>
                                    <td><?php echo $iem57 ?></td>
                                </tr>
                                <tr>
                                    <td>Agustus</td> 
                                    <td><?php echo $per58 ?></td>
                                    <td><?php echo $pem58 ?></td>
                                    <td><?php echo $lem58 ?></td>
                                    <td><?php echo $iem58 ?></td>
                                </tr>
                                <tr>
                                    <td>September</td> 
                                    <td><?php echo $per59 ?></td>
                                    <td><?php echo $pem59 ?></td>
                                    <td><?php echo $lem59 ?></td>
                                    <td><?php echo $iem59 ?></td>
                                </tr>
                                <tr>
                                    <td>Oktober</td> 
                                    <td><?php echo $per510 ?></td>
                                    <td><?php echo $pem510 ?></td>
                                    <td><?php echo $lem510 ?></td>
                                    <td><?php echo $iem510 ?></td>
                                </tr>
                                <tr>
                                    <td>November</td> 
                                    <td><?php echo $per511 ?></td>
                                    <td><?php echo $pem511 ?></td>
                                    <td><?php echo $lem511 ?></td>
                                    <td><?php echo $iem511 ?></td>
                                </tr>
                                <tr>
                                    <td>Desember</td> 
                                    <td><?php echo $per512 ?></td>
                                    <td><?php echo $pem512 ?></td>
                                    <td><?php echo $lem512 ?></td>
                                    <td><?php echo $iem512 ?></td>
                                </tr> 
                            </tbody>
                        </table>    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="largeModal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-exportable1">
                            <thead>
                                <tr>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Bidang</th>
                                    <th>Status</th>
                                    <th>Tanggal Usulan</th>
                                    <th>Periode</th>
                                    <th>Negara</th>
                                    <th>MOU</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($tabkerjasama->result() as $result){
                                  if(($result->status)==1){?>
                                    <tr>
                                      <td><?php echo htmlspecialchars($result->namaper); ?></td> 
                                      <td><?php switch($result->kategori){
                                        case "1": echo "Perusahaan"; break;
                                        case "2": echo "Perguruan Tinggi"; break;
                                        case "3": echo "Perbankan"; break;
                                        case "4": echo "Luar Negeri"; break;
                                        case "5": echo "Lainnya"; break;
                                        }?></td>
                                      <td><?php echo htmlspecialchars($result->bidang); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DITERIMA</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                      <td><?php echo htmlspecialchars($result->negara); ?></td>
                                      <td><?php if($result->status == 1){?>
                                        <a href="<?php echo site_url('DashUser/lihatFile/reza-lampujalan-1.pdf/pdf');?>" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button></a>
                                        <?php }else{ ?>
                                        <button type="button" class="btn bg-teal waves-effect" disabled="disabled" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama tidak tersedia">
                                            <i class="material-icons">description</i>
                                        </button>
                                        <?php } ?></td>
                                    </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="terimaModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Diterima Tahun <?php echo $tahun ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">done</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-terima">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter->result() as $result){
                                    if(($result->status)==1){
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="syncModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-light-blue">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Proses Tahun <?php echo $tahun ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">sync</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-sync">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter->result() as $result){
                                    if(($result->status)==2){
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }}?>
                            </tbody>
                            
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="clearModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama akan Expired Tahun <?php echo $tahun ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">clear</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-clear">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $no=1; 
                                foreach ($tabmou1->result() as $result){ 
                                  $date1=date_create($result->tgl_lama);
                                  $date2=date_create(date("Y-m-d"));
                                  $diff=date_diff($date1,$date2);
                                  $total=$diff->y * 12 + $diff->m;                       
                                ?>
                            <?php if ($total<=1 AND $date1>$date2){?>
                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>
                                <?php $no++;}}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="allModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama UNDIP</h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabkerjasama->result() as $result){
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="detailModal1" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun1 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Status</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter->result() as $result){
                                    
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DISAHKAN</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="detailModal2" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun2 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Status</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter2->result() as $result){
                                    
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DISAHKAN</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="detailModal3" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun3 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Status</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter3->result() as $result){
                                    
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DITERIMA</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="detailModal4" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun4 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Status</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter4->result() as $result){
                                    
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DITERIMA</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="detailModal5" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Tahun <?php echo $tahun5 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">description</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-all">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Status</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabter5->result() as $result){
                                    
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DITERIMA</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->

        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>

    <script>
    $(document).ready(function(){
        $("#bar1").hide();
    });
    </script>

    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            ordering: false,
            paging: false,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-all').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>
    <script>
        $(function () {
            new Chart(document.getElementById("bar_chart1").getContext("2d"), getChartJs('bar1'));
            new Chart(document.getElementById("bar_chart2").getContext("2d"), getChartJs('bar2'));
            new Chart(document.getElementById("bar_chart3").getContext("2d"), getChartJs('bar3'));
            new Chart(document.getElementById("bar_chart4").getContext("2d"), getChartJs('bar4'));
            new Chart(document.getElementById("bar_chart5").getContext("2d"), getChartJs('bar5'));
            new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
            new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
        });

        function getChartJs(type) {
            var config = null;

            if (type === 'bar1') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perguruan Tinggi",
                            data: [<?php echo $per11 ?>, <?php echo $per12 ?>, <?php echo $per13 ?>, <?php echo $per14 ?>, <?php echo $per15 ?>, <?php echo $per16 ?>, <?php echo $per17 ?>, <?php echo $per18 ?>, <?php echo $per19 ?>, <?php echo $per110 ?>, <?php echo $per111 ?>, <?php echo $per112 ?>],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Pemerintah",
                            data: [<?php echo $pem11 ?>, <?php echo $pem12 ?>, <?php echo $pem13 ?>, <?php echo $pem14 ?>, <?php echo $pem15 ?>, <?php echo $pem16 ?>, <?php echo $pem17 ?>, <?php echo $pem18 ?>, <?php echo $pem19 ?>, <?php echo $pem110 ?>, <?php echo $pem111 ?>, <?php echo $pem112 ?>],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Lembaga Swasta",
                            data: [<?php echo $lem11 ?>, <?php echo $lem12 ?>, <?php echo $lem13 ?>, <?php echo $lem14 ?>, <?php echo $lem15 ?>, <?php echo $lem16 ?>, <?php echo $lem17 ?>, <?php echo $lem18 ?>, <?php echo $lem19 ?>, <?php echo $lem110 ?>, <?php echo $lem111 ?>, <?php echo $lem112 ?>],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Dunia Industri",
                            data: [<?php echo $iem11 ?>, <?php echo $iem12 ?>, <?php echo $iem13 ?>, <?php echo $iem14 ?>, <?php echo $iem15 ?>, <?php echo $iem16 ?>, <?php echo $iem17 ?>, <?php echo $iem18 ?>, <?php echo $iem19 ?>, <?php echo $iem110 ?>, <?php echo $iem111 ?>, <?php echo $iem112 ?>],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'bar2') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perguruan Tinggi",
                            data: [<?php echo $per21 ?>, <?php echo $per22 ?>, <?php echo $per23 ?>, <?php echo $per24 ?>, <?php echo $per25 ?>, <?php echo $per26 ?>, <?php echo $per27 ?>, <?php echo $per28 ?>, <?php echo $per29 ?>, <?php echo $per210 ?>, <?php echo $per211 ?>, <?php echo $per212 ?>],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Pemerintah",
                            data: [<?php echo $pem21 ?>, <?php echo $pem22 ?>, <?php echo $pem23 ?>, <?php echo $pem24 ?>, <?php echo $pem25 ?>, <?php echo $pem26 ?>, <?php echo $pem27 ?>, <?php echo $pem28 ?>, <?php echo $pem29 ?>, <?php echo $pem210 ?>, <?php echo $pem211 ?>, <?php echo $pem212 ?>],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Lembaga Swasta",
                            data: [<?php echo $lem21 ?>, <?php echo $lem22 ?>, <?php echo $lem23 ?>, <?php echo $lem24 ?>, <?php echo $lem25 ?>, <?php echo $lem26 ?>, <?php echo $lem27 ?>, <?php echo $lem28 ?>, <?php echo $lem29 ?>, <?php echo $lem210 ?>, <?php echo $lem211 ?>, <?php echo $lem212 ?>],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Dunia Industri",
                            data: [<?php echo $iem21 ?>, <?php echo $iem22 ?>, <?php echo $iem23 ?>, <?php echo $iem24 ?>, <?php echo $iem25 ?>, <?php echo $iem26 ?>, <?php echo $iem27 ?>, <?php echo $iem28 ?>, <?php echo $iem29 ?>, <?php echo $iem210 ?>, <?php echo $iem211 ?>, <?php echo $iem212 ?>],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'bar3') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perguruan Tinggi",
                            data: [<?php echo $per31 ?>, <?php echo $per32 ?>, <?php echo $per33 ?>, <?php echo $per34 ?>, <?php echo $per35 ?>, <?php echo $per36 ?>, <?php echo $per37 ?>, <?php echo $per38 ?>, <?php echo $per39 ?>, <?php echo $per310 ?>, <?php echo $per311 ?>, <?php echo $per312 ?>],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Pemerintah",
                            data: [<?php echo $pem31 ?>, <?php echo $pem32 ?>, <?php echo $pem33 ?>, <?php echo $pem34 ?>, <?php echo $pem35 ?>, <?php echo $pem36 ?>, <?php echo $pem37 ?>, <?php echo $pem38 ?>, <?php echo $pem39 ?>, <?php echo $pem310 ?>, <?php echo $pem311 ?>, <?php echo $pem312 ?>],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Lembaga Swasta",
                            data: [<?php echo $lem31 ?>, <?php echo $lem32 ?>, <?php echo $lem33 ?>, <?php echo $lem34 ?>, <?php echo $lem35 ?>, <?php echo $lem36 ?>, <?php echo $lem37 ?>, <?php echo $lem38 ?>, <?php echo $lem39 ?>, <?php echo $lem310 ?>, <?php echo $lem311 ?>, <?php echo $lem312 ?>],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Dunia Industri",
                            data: [<?php echo $iem31 ?>, <?php echo $iem32 ?>, <?php echo $iem33 ?>, <?php echo $iem34 ?>, <?php echo $iem35 ?>, <?php echo $iem36 ?>, <?php echo $iem37 ?>, <?php echo $iem38 ?>, <?php echo $iem39 ?>, <?php echo $iem310 ?>, <?php echo $iem311 ?>, <?php echo $iem312 ?>],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'bar4') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perguruan Tinggi",
                            data: [<?php echo $per41 ?>, <?php echo $per42 ?>, <?php echo $per43 ?>, <?php echo $per44 ?>, <?php echo $per45 ?>, <?php echo $per46 ?>, <?php echo $per47 ?>, <?php echo $per48 ?>, <?php echo $per49 ?>, <?php echo $per410 ?>, <?php echo $per411 ?>, <?php echo $per412 ?>],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Pemerintah",
                            data: [<?php echo $pem41 ?>, <?php echo $pem42 ?>, <?php echo $pem43 ?>, <?php echo $pem44 ?>, <?php echo $pem45 ?>, <?php echo $pem46 ?>, <?php echo $pem47 ?>, <?php echo $pem48 ?>, <?php echo $pem49 ?>, <?php echo $pem410 ?>, <?php echo $pem411 ?>, <?php echo $pem412 ?>],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Lembaga Swasta",
                            data: [<?php echo $lem41 ?>, <?php echo $lem42 ?>, <?php echo $lem43 ?>, <?php echo $lem44 ?>, <?php echo $lem45 ?>, <?php echo $lem46 ?>, <?php echo $lem47 ?>, <?php echo $lem48 ?>, <?php echo $lem49 ?>, <?php echo $lem410 ?>, <?php echo $lem411 ?>, <?php echo $lem412 ?>],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Dunia Industri",
                            data: [<?php echo $iem41 ?>, <?php echo $iem42 ?>, <?php echo $iem43 ?>, <?php echo $iem44 ?>, <?php echo $iem45 ?>, <?php echo $iem46 ?>, <?php echo $iem47 ?>, <?php echo $iem48 ?>, <?php echo $iem49 ?>, <?php echo $iem410 ?>, <?php echo $iem411 ?>, <?php echo $iem412 ?>],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'bar5') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perguruan Tinggi",
                            data: [<?php echo $per51 ?>, <?php echo $per52 ?>, <?php echo $per53 ?>, <?php echo $per54 ?>, <?php echo $per55 ?>, <?php echo $per56 ?>, <?php echo $per57 ?>, <?php echo $per58 ?>, <?php echo $per59 ?>, <?php echo $per510 ?>, <?php echo $per511 ?>, <?php echo $per512 ?>],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Pemerintah",
                            data: [<?php echo $pem51 ?>, <?php echo $pem52 ?>, <?php echo $pem53 ?>, <?php echo $pem54 ?>, <?php echo $pem45 ?>, <?php echo $pem56 ?>, <?php echo $pem57 ?>, <?php echo $pem58 ?>, <?php echo $pem59 ?>, <?php echo $pem510 ?>, <?php echo $pem511 ?>, <?php echo $pem512 ?>],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Lembaga Swasta",
                            data: [<?php echo $lem51 ?>, <?php echo $lem52 ?>, <?php echo $lem53 ?>, <?php echo $lem54 ?>, <?php echo $lem55 ?>, <?php echo $lem56 ?>, <?php echo $lem57 ?>, <?php echo $lem58 ?>, <?php echo $lem59 ?>, <?php echo $lem510 ?>, <?php echo $lem511 ?>, <?php echo $lem512 ?>],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Dunia Industri",
                            data: [<?php echo $iem51 ?>, <?php echo $iem52 ?>, <?php echo $iem53 ?>, <?php echo $iem54 ?>, <?php echo $iem55 ?>, <?php echo $iem56 ?>, <?php echo $iem57 ?>, <?php echo $iem58 ?>, <?php echo $iem59 ?>, <?php echo $iem510 ?>, <?php echo $iem511 ?>, <?php echo $iem512 ?>],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'radar') {
                config = {
                    type: 'radar',
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            data: [65, 25, 90, 81, 56, 55, 40],
                            borderColor: 'rgba(0, 188, 212, 0.8)',
                            backgroundColor: 'rgba(0, 188, 212, 0.5)',
                            pointBorderColor: 'rgba(0, 188, 212, 0)',
                            pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                            pointBorderWidth: 1
                        }, {
                                label: "My Second dataset",
                                data: [72, 48, 40, 19, 96, 27, 100],
                                borderColor: 'rgba(233, 30, 99, 0.8)',
                                backgroundColor: 'rgba(233, 30, 99, 0.5)',
                                pointBorderColor: 'rgba(233, 30, 99, 0)',
                                pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                                pointBorderWidth: 1
                            }]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            else if (type === 'pie') {
                config = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [225, 50, 100, 40],
                            backgroundColor: [
                                "rgb(233, 30, 99)",
                                "rgb(255, 193, 7)",
                                "rgb(0, 188, 212)",
                                "rgb(139, 195, 74)"
                            ],
                        }],
                        labels: [
                            "Pink",
                            "Amber",
                            "Cyan",
                            "Light Green"
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            
            return config;
        }
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>