<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

   <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashAdmin'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('MOUAdmin'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola MOU</span>
                        </a>
                    </li>
                    <li   class="active">
                        <a href="<?php echo site_url('KelolaInisiasi'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Inisiasi</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('KelolaCntrl'); ?>">
                            <i class="material-icons">group</i>
                            <span>Kelola Pengguna</span>
                        </a>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="margin-bottom:-10px">
                            <h2>
                                Kelola Inisiasi
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Fakultas</th>
                                    <th>Pengusul</th>
                                    <th>Tgl Pengusulan</th>
                                    <th>Institutsi</th>
                                    <th>Tgl Mulai</th>
                                    <th>Tgl Selesai</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabkerjasama->result() as $result){
                                  ?>
                                    <tr>
                                    <td><?php echo htmlspecialchars($no); ?></td> 
                                      <td><?php echo htmlspecialchars($result->nama); ?></td> 
                                      <td><?php echo htmlspecialchars($result->namapengusul); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tglpengusulan); ?></td> 
                                      <td></td> 
                                      <td><?php echo htmlspecialchars($result->bulanusulan); ?>-<?php echo htmlspecialchars($result->tahunusulan); ?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->bulanrencana); ?>-<?php echo htmlspecialchars($result->tahunrencana); ?>
                                      </td>
                                      <td>
                                        <a class="inf btn btn-primary waves-effect" data-toggle="modal" data-target=".infoModal" id="<?php echo htmlspecialchars($result->nomorpendaftaran); ?>" title="Detail Kerjasama">
                                            <i class="material-icons">info</i>
                                        </a>

                                        
                                       </td>
                                    </tr>
                                <?php $no++; }?>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
        <!-- Modal INFO -->
        <div class="modal fade infoModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Detail Kerjasama</h4>
                        <i class="material-icons right" style="margin-top:-40px">info</i>
                    </div>
                    <div class="modal-body" >

                            <div class="row">
                                <div class="col-md-2">
                                    <label class="m-t-5">Pengusul</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infopengusul" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tahun</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infotahun" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Mitra</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infomitra" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Kategori</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infokategori" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Sub Kategori</label>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infosubkategori" name="bidang" placeholder="Masukan Bidang Kerjasama Anda" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Detail Kerjasama</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infojudul" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="Masukan Judul Usulan Kerjasama Anda" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Manfaat Untuk Mitra</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infomanfaatm" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="-" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Manfaat Untuk UNDIP</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infomanfaatu" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="-" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Negara</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infonegara" class="form-control" name="negara" placeholder="Masukan Negara Mitra Anda" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Periode</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infoperiode" max="15" class="form-control" name="periode" placeholder="Masukan Periode Kerjasama Anda (Max 15 Tahun)" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tanggal Usul</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotglusul" class="form-control" name="negara" placeholder="Masukan Negara Mitra Anda" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tanggal Selesai</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotgllama" max="15" class="form-control" name="periode" placeholder="Kerjasama Belum diterima" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="m-b-15">Bidang</label>
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang1" id="infobidang1" class="chk-col-teal filled-in " disabled />
                                     <label for="infobidang1">PENDIDIKAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang2" id="infobidang2" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang2">PENELITIAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-4">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang3" id="infobidang3" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang3">PENGABDIAN MASYARAKAT</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang4" id="infobidang4" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang4">BEASISWA</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang5" id="infobidang5" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang5">LAIN-LAIN</label>
                                    </div> 
                                </div>
                                <br>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 1 : </label>
                                    <a href="" id="editberkas1" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 2 : </label>
                                    <a href="" id="editberkas2" rel="nofollow" target="_blank1">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 3 : </label>
                                    <a href="" id="editberkas3" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 4 : </label>
                                    <a href="" id="editberkas4" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                
                            </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade canModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk menolak kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-can" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade kosongModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >FILE MOU belum tersedia</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade accModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk menerima kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-teal">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-acc" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade upModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload File M.O.U</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upload" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label class="form-label">File MOU</label>
                                    <div class="form-line">
                                        <input name="fileMOU" id="fileMOU" type="file" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>
    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <script>
    $(document).ready(function(){

        $("#subkategori1").hide();
        $("#subkategori2").hide();
        $("#subkategori3").hide();
        $("#subkategori4").hide();


        $("#kategori").change(function(){
            x = document.getElementById("kategori").value;
            if (x==1) {
               $("#subkategori1").show();
               $("#subkategori").hide();
               $("#subkategori2").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
            }else if (x==2){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
               $("#subkategori2").show(); 
            }else if (x==3){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori2").hide();
               $("#subkategori4").hide();
               $("#subkategori3").show(); 
            }else if (x==4){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori2").hide();
               $("#subkategori4").show(); 
            }
        });
      
            
            
    });

    </script>

    <script>
    $(document).ready(function(){
        var check1=0; var id;var namaOld=""; var editCheck;

        $(".info").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'MOUAdmin/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#infomitra").val(data['mitra']);
                    $("#infojudul").val(data['judul']);
                    $("#infotahun").val(data['tahun']);
                    $("#infotglusul").val(data['tgl_usul']);
                    $("#infotgllama").val(data['tgl_lama']);
                    $("#infokategori").val(data['kategori']);
                    $("#infosubkategori").val(data['subkategori']);
                    $("#infopengusul").val(data['pengusul']);
                    $("#infobidang1").val(data['bidang1']);
                    $("#infobidang2").val(data['bidang2']);
                    $("#infobidang3").val(data['bidang3']);
                    $("#infobidang4").val(data['bidang4']);
                    $("#infobidang5").val(data['bidang5']);
                    $("#infonegara").val(data['negara']);
                    if($('#infobidang1').val()!=0) {
                        $('input[name="infobidang1"]').prop('checked', true);
                    }else if($('#infobidang1').val()==0) {
                        $('input[name="infobidang1"]').prop('checked', false);
                    };
                    if($('#infobidang2').val()!=0) {
                        $('input[name="infobidang2"]').prop('checked', true);
                    }else if($('#infobidang2').val()==0) {
                        $('input[name="infobidang2"]').prop('checked', false);
                    };
                    if($('#infobidang3').val()!=0) {
                        $('input[name="infobidang3"]').prop('checked', true);
                    }else if($('#infobidang3').val()==0) {
                        $('input[name="infobidang3"]').prop('checked', false);
                    };
                    if($('#infobidang4').val()!=0) {
                        $('input[name="infobidang4"]').prop('checked', true);
                    }else if($('#infobidang4').val()==0) {
                        $('input[name="infobidang4"]').prop('checked', false);
                    };
                    if($('#infobidang5').val()!=0) {
                        $('input[name="infobidang5"]').prop('checked', true);
                    }else if($('#infobidang5').val()==0) {
                        $('input[name="infobidang5"]').prop('checked', false);
                    };
                    if($(this).val(data['periode'])>'0'){
                        $("#infoperiode").val(data['thn_lama']);
                    }else{
                        $("#infoperiode").val(data['periode']);
                    };
                    $("#infomanfaat_m").val(data['manfaat_m']);
                    $("#infomanfaat_u").val(data['manfaat_u']);
                    $("#editberkas1").click(function(){
                        file = (data['berkas1']);
                        bid = (data['bidang']);
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas2").click(function(){
                        file = (data['berkas2']);
                        bid = (data['bidang']);
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas3").click(function(){
                        file = (data['berkas3']);
                        bid = (data['bidang']);
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas4").click(function(){
                        file = (data['berkas4']);
                        bid = (data['bidang']);
                        window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                    });
                }
            });
        });

        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'DashUser/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editbidang1").val(data['bidang']);
                    $("#editkategori1").val(data['kategori']);
                    $("#editjudul1").val(data['judul']);
                    $("#editmanfaatm1").val(data['manfaat_m']);
                    $("#editmanfaatu1").val(data['manfaat_u']);
                    $("#editnegara1").val(data['negara']);
                    $("#editperiode1").val(data['periode']);
                    $("#editfile11").val(data['berkas1']);
                    $("#editfile21").val(data['berkas2']);
                    $("#editfile31").val(data['berkas3']);
                    $("#editfile41").val(data['berkas4']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'DashUser/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $(".acc").click(function(){
            id = $(this).attr('id');
        });

        $(".can").click(function(){
            id = $(this).attr('id');
        });

        $(".up").click(function(){
            id = $(this).attr('id');
        });


        $("#confirm-acc").click(function(){
            $.ajax({
                url:'MOUAdmin/accept/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-can").click(function(){
            $.ajax({
                url:'MOUAdmin/cancel/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upload/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

    });

    </script>

    <script>
    $(function () {
         $('#upload').validate({
            rules: {
                'fileMOU': {
                     remote: {
                        url: "MOUAdmin/cekFile",
                        type: "post",
                        data: {
                          fileMOU: function() {
                            return $( "#fileMOU" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'fileMOU':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>
       

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>