<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

   <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />

     <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashAdmin'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li  class="active">
                        <a href="<?php echo site_url('MOUAdmin'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Kerjasama</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('KelolaInisiasi'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Inisiasi</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('KelolaCntrl'); ?>">
                            <i class="material-icons">group</i>
                            <span>Kelola Pengguna</span>
                        </a>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
             <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-teal hover-zoom-effect">
                        <div class="icon">
                            <a href="#" data-toggle="modal" data-target="#terimaModal"><i class="material-icons">done</i></a>
                        </div>
                        <div class="content">
                            <?php 
                                $i=0;
                                foreach ($tabkerjasama1->result() as $result){
                                    if($result->status == 1){
                                        $i++;
                                    }    
                                }
                            ?>
                            <div class="text">KERJASAMA DISAHKAN</div>
                            <div class="number"><?php echo $i ?></div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-blue hover-zoom-effect">
                        <div class="icon">
                            <a href="#" data-toggle="modal" data-target="#syncModal"><i class="material-icons">sync</i></a>
                        </div>
                        <div class="content">
                            <?php 
                                $u=0;
                                foreach ($tabkerjasama1->result() as $result){
                                    if($result->status == 2){
                                        $u++;
                                    }    
                                }
                            ?>
                            <div class="text">KERJASAMA PROSES</div>
                            <div class="number"><?php echo $u ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-red hover-zoom-effect">
                        <div class="icon">
                            <a href="#" data-toggle="modal" data-target="#clearModal"><i class="material-icons">warning</i></a>
                        </div>
                        <div class="content">
                            <?php 
                                $e=0; 
                                foreach ($tabkerjasama1->result() as $result){ 
                                  $date1=date_create($result->tgl_lama);
                                  $date2=date_create(date("Y-m-d"));
                                  $diff=date_diff($date1,$date2);
                                  $total=$diff->y * 12 + $diff->m;
                                   if ($total<=1 AND $date1>$date2){
                                  $e++;
                                }
                                }          
                            ?>
                            <div class="text">KERJSAMA AKAN EXPIRED</div>
                            <div class="number"><?php echo $e ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-zoom-effect">
                        <div class="icon">
                            <a href="#" data-toggle="modal" data-target="#addModal"><i class="material-icons">add</i></a>
                        </div>
                        <div class="content">
                            <div class="text">TAMBAH PENGAJUAN</div>
                            <div class="number">KERJASAMA</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="margin-bottom:-10px">
                            <h2>
                                Kelola Kerjasama
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            
                            <form action="<?php echo site_url('MOUAdmin/pilihKat'); ?>" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <select type="text" id="kategori" name="kategori" class="form-control show-tick">
                                                <option value="0" disabled selected>Pilih Kategori</option>
                                                <option value="1">Perguruan Tinggi</option>
                                                <option value="2">Pemerintah</option>
                                                <option value="3">Lembaga Swasta</option>
                                                <option value="4">Dunia Industri</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-3">
                                        <div class="form-group form-float" id="subkategori">
                                            <select type="text"  name="subkategori" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>   
                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori1">
                                            <select type="text"  name="subkategori1" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                
                                                <?php 
                                                foreach ($tabkat1->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori2">
                                            <select type="text" name="subkategori2" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>

                                                
                                                <?php 
                                                foreach ($tabkat2->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>

                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori3">
                                            <select type="text"  name="subkategori3" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                <?php 
                                                foreach ($tabkat3->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>

                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori4">
                                            <select type="text"  name="subkategori4" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                <?php 
                                                foreach ($tabkat4->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <select type="text" id="negara" name="negara" class="form-control show-tick">
                                                <option disabled selected>Pilih Negara</option>
                                                <?php 
                                                foreach ($negara->result() as $result){?>
                                                    <option value="<?php echo $result->negara?>"><?php echo $result->negara?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-float">
                                            <select type="text" id="tahun" name="tahun" class="form-control show-tick">
                                                <option disabled selected>Pilih Tahun</option>
                                                <option value="<?php echo $tahun1 ?>"><?php echo $tahun1 ?></option>
                                                <option value="<?php echo $tahun2 ?>"><?php echo $tahun2 ?></option>
                                                <option value="<?php echo $tahun3 ?>"><?php echo $tahun3 ?></option>
                                                <option value="<?php echo $tahun4 ?>"><?php echo $tahun4 ?></option>
                                                <option value="<?php echo $tahun5 ?>"><?php echo $tahun5 ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-link waves-effect right">SUBMIT</button>
                                    </div>  
                                    
                                </div>
                    
                            </form>
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Pengusul</th>
                                    <th>Mitra</th>
                                    <th>Jenis</th>
                                    <th>Kategori</th>
                                    <th>Subkategori</th>
                                    <th>Status</th>
                                    <th>Tahun</th>
                                    <th>Negara</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabkerjasama->result() as $result){
                                  ?>
                                    <tr>
                                    <td><?php echo htmlspecialchars($no); ?></td> 
                                      <td><?php echo htmlspecialchars($result->nama); ?></td> 
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->jenis); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php switch($result->status){
                                        case "1": echo "<p class='col-teal'><b>DISAHKAN</b></p>"; break;
                                        case "2": echo "<p class='col-blue'><b>PROSES</b></p>"; break;
                                        case "3": echo "<p class='col-red'><b>DITOLAK</b></p>"; break;
                                        }?>
                                      </td>
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->negara); ?></td>
                                      <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn bg-teal dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Aksi <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a class="info" href="" data-toggle="modal" data-target=".infoModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">info</i> Detail</a></li>
                                                <li><a class="detsts" href="" data-toggle="modal" data-target=".detstatModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">info</i> Detail Status</a></li>
                                                <li><a class="upsts" href="" data-toggle="modal" data-target=".upstatModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">edit</i> Penstatusan</a></li>
                                                <?php if($result->jenis == 'M.O.U'){?>    
                                                    <?php if($result->filemou == null){?>
                                                    <li><a href="#" data-toggle="modal" data-target=".kosongModal"><i class="material-icons">description</i>File M.O.U.</a></li>
                                                    <li><a class="up" href="" data-toggle="modal" data-target=".upModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">file_upload</i>Upload MOU</a></li>
                                                    <?php }else{ ?>
                                                    <li><a href="<?php echo site_url('MOUAdmin/lihatFile/'.$result->username.'/mou/'.$result->filemou.'/pdf');?>" rel="nofollow" target="_blank"><i class="material-icons">description</i>File M.O.U.</a></li>
                                                    <li><a class="up" href="" data-toggle="modal" data-target=".upModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">file_upload</i>Upload MOU</a></li>
                                                    <?php } ?>     
                                                <?php }else{ ?>   
                                                    <?php if($result->filenaskah == null){?>
                                                    <li><a href="#" data-toggle="modal" data-target=".kosongModal"><i class="material-icons">description</i>Naskah PKS</a></li>
                                                    <li><a class="nas" href="" data-toggle="modal" data-target=".nasModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">file_upload</i>Upload PKS</a></li>
                                                    <?php }else{ ?>
                                                    <li><a href="<?php echo site_url('MOUAdmin/lihatFile/'.$result->username.'/naskah/'.$result->filenaskah.'/pdf');?>" rel="nofollow" target="_blank"><i class="material-icons">description</i>Naskah PKS</a></li>
                                                    <li><a class="nas" href="" data-toggle="modal" data-target=".nasModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">file_upload</i>Upload PKS</a></li>
                                                    <?php } ?>    
                                                <?php } ?>                                     
                                                
                                                <li><a class="acc" href="" data-toggle="modal" data-target=".accModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">done</i>Disahkan</a></li>
                                                <li><a class="can" href="" data-toggle="modal" data-target=".canModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">clear</i>Ditolak</a></li>
                                                <?php if($result->level == 1){?>    
                                                <li><a class="edit" href="" data-toggle="modal" data-target=".editModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">edit</i>Edit</a></li>
                                                <li><a class="del" href="" data-toggle="modal" data-target=".delModal" id="<?php echo htmlspecialchars($result->id_mou); ?>"><i class="material-icons">clear</i>Batalkan</a></li>
                                                <?php }?>  
                                            </ul>
                                        </div>

                                        
                                       </td>
                                    </tr>
                                <?php $no++; }?>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade detstatModal" id="" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Detail Penstatusan</h4>
                        <i class="material-icons right" style="margin-top:-40px">done</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-detstat" id="detStat">
                            
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal INFO -->
        <div class="modal fade infoModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Detail Kerjasama</h4>
                        <i class="material-icons right" style="margin-top:-40px">info</i>
                    </div>
                    <div class="modal-body" >

                            <div class="row">
                                <div class="col-md-2">
                                    <label class="m-t-5">Pengusul</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infopengusul" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tahun</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infotahun" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Mitra</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infomitra" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Jenis</label>
                                </div>
                                <div class="col-md-10">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infojenis" name="jenis" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Kategori</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infokategori" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Sub Kategori</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infosubkategori" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Detail Kerjasama</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infojudul" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="-a Anda" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Manfaat Untuk Mitra</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infomanfaatm" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="-" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Manfaat Untuk UNDIP</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="infomanfaatu" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="-" disabled></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Negara</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infonegara" class="form-control" name="negara" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Periode</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infoperiode" max="15" class="form-control" name="periode" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tanggal Usul</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotglusul" class="form-control" name="negara" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Tanggal Selesai</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotgllama" max="15" class="form-control" name="periode" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="m-b-15">Bidang</label>
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang1" id="infobidang1" class="chk-col-teal filled-in " disabled />
                                     <label for="infobidang1">PENDIDIKAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang2" id="infobidang2" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang2">PENELITIAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-4">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang3" id="infobidang3" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang3">PENGABDIAN MASYARAKAT</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang4" id="infobidang4" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang4">BEASISWA</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="infobidang5" id="infobidang5" class="chk-col-teal filled-in"  disabled />
                                     <label for="infobidang5">LAIN-LAIN</label>
                                    </div> 
                                </div>
                                <br>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 1 : </label>
                                    <a href="" id="editberkas1" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 2 : </label>
                                    <a href="" id="editberkas2" rel="nofollow" target="_blank1">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 3 : </label>
                                    <a href="" id="editberkas3" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-25 m-r-10">Berkas 4 : </label>
                                    <a href="" id="editberkas4" rel="nofollow" target="_blank">  
                                        <button type="button" class="btn bg-teal waves-effect" data-toggle="tooltip" data-placement="bottom" title="File MOU Kerjasama"> <i class="material-icons">description</i>
                                        </button>
                                    </a>
                                </div>
                                
                            </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade canModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk menolak kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-can" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade kosongModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >File belum tersedia !</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade accModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk menerima kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-teal">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-acc" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div> 
        <!-- Small Size -->
        <div class="modal fade upModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload File M.O.U</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upload" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label class="form-label">File MOU</label>
                                    <div class="form-line">
                                        <input name="fileMOU" id="fileMOU" type="file" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade nasModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload File Naskah PKs</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upload1" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">    
                            <div class="col-md-12">
                                <div class="form-group form-float">
                                    <label class="form-label">File Naskah PKS</label>
                                    <div class="form-line">
                                        <input name="fileNaskah" id="fileNaskah" type="file" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade upstatModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="smallModalLabel" >Upload Status Proses</h4>
                        <i class="material-icons right" style="margin-top:-40px">file_upload</i>
                    </div>
                    <div class="modal-body">
                        <form id="upstat" action="" method="POST" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <label class="m-t-5 m-b-5">Status Proses</label>
                            </div>    
                            <div class="col-md-6">
                                <div class="form-group form-float">
                                    <select type="text" id="kegiatan" name="kegiatan" class="form-control show-tick">
                                        <option disabled selected>Pilih Kegiatan</option>
                                        <?php 
                                        foreach ($keg->result() as $result){?>
                                            <option value="<?php echo $result->id_kegiatan?>"><?php echo $result->kegiatan?></option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-float" id="proses">
                                    <select type="text" id="proses" name="prose1s" class="form-control show-tick">
                                        <option disabled selected>Pilih Status</option>
                                           
                                    </select>
                                </div>
                                <div class="form-group form-float" id="proses1">
                                    <select type="text" id="proses1" name="proses1" class="form-control show-tick">
                                        <option disabled selected>Pilih Status</option>
                                        <?php 
                                        foreach ($stat->result() as $result){?>
                                            <?php if($result->id_kegiatan == 1){?> 
                                            <option value="<?php echo $result->id_proses?>"><?php echo $result->proses?></option>
                                            <?php }?>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="form-group form-float" id="proses2">
                                    <select type="text" id="proses2" name="proses2" class="form-control show-tick">
                                        <option disabled selected>Pilih Status</option>
                                        <?php 
                                        foreach ($stat->result() as $result){?>
                                            <?php if($result->id_kegiatan == 2){?> 
                                            <option value="<?php echo $result->id_proses?>"><?php echo $result->proses?></option>
                                            <?php }?>
                                        <?php }?>
                                    </select>
                                </div>
                                <div class="form-group form-float" id="proses3">
                                    <select type="text" id="proses3" name="proses3" class="form-control show-tick">
                                        <option disabled selected>Pilih Status</option>
                                        <?php 
                                        foreach ($stat->result() as $result){?>
                                            <?php if($result->id_kegiatan == 3){?> 
                                            <option value="<?php echo $result->id_proses?>"><?php echo $result->proses?></option>
                                            <?php }?>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="terimaModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Disahkan Tahun <?php echo $tahun1 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">done</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-terima">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabkerjasama->result() as $result){
                                    if(($result->status)==1){
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="syncModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-light-blue">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Proses Tahun <?php echo $tahun1 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">sync</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-sync">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabkerjasama->result() as $result){
                                    if(($result->status)==2){
                                    ?>

                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="clearModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title m-b-15" id="largeModalLabel">Tabel Daftar Kerjasama Expired Tahun <?php echo $tahun1 ?></h4>
                        <i class="material-icons right" style="margin-top:-40px">clear</i>
                    </div>
                    <div class="modal-body" >
                        <table class="table table-bordered table-striped table-hover dataTable js-clear">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $no=1; 
                                foreach ($tabkerjasama->result() as $result){ 
                                  $date1=date_create($result->tgl_lama);
                                  $date2=date_create(date("Y-m-d"));
                                  $diff=date_diff($date1,$date2);
                                  $total=$diff->y * 12 + $diff->m;                       
                                ?>
                            <?php if ($total<=1 AND $date1>$date2){?>
                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>
                                <?php $no++;}}?>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal Detail Rekap -->
        <div class="modal fade" id="addModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-cyan"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Tambah Kerjasama</h4>
                        <i class="material-icons right" style="margin-top:-40px">add</i>
                    </div>
                    <div class="modal-body" >
                        <form id="add" action="<?php echo site_url('MOUAdmin/addKerjasama'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Mitra</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="mitra" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label">Kategori</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <select type="text" id="kategori1" name="kategori1" class="form-control show-tick">
                                            <option value="0" disabled selected>Pilih Kategori</option>
                                            <option value="1">Perguruan Tinggi</option>
                                            <option value="2">Pemerintah</option>
                                            <option value="3">Lembaga Swasta</option>
                                            <option value="4">Dunia Industri</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group form-float" id="asubkategori">
                                        <select type="text"  name="asubkategori" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>   
                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="asubkategori1">
                                        <select type="text"  name="asubkategori1" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            
                                            <?php 
                                            foreach ($tabkat1->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="asubkategori2">
                                        <select type="text" name="asubkategori2" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>

                                            
                                            <?php 
                                            foreach ($tabkat2->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>

                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="asubkategori3">
                                        <select type="text"  name="asubkategori3" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            <?php 
                                            foreach ($tabkat3->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>

                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="asubkategori4">
                                        <select type="text"  name="asubkategori4" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            <?php 
                                            foreach ($tabkat4->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12"> 
                                    <div class="form-group form-float">
                                        <label class="form-label">Detail Kerjasama</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="judul" placeholder="Masukan Judul Usulan Kerjasama Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Manfaat untuk Mitra</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="manfaat_m" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap Mitra Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                         <label class="form-label">Manfaat untuk UNDIP</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" name="manfaat_u" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap UNDIP" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                    <label class="form-label">Negara</label>
                                        <select type="text"  name="negara" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Negara</option>
                                            
                                            <?php 
                                            foreach ($negara->result() as $result){?>
                                                <option value="<?php echo $result->negara?>"><?php echo $result->negara?></option>
                                            <?php }?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Periode</label>
                                        <div class="form-line">
                                            <input type="number" max="15" class="form-control" name="periode" placeholder="Masukan Periode Kerjasama Anda (Max 15 Tahun)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tanggal Usul</label>
                                        <div class="form-line">
                                            <input type="text" name="tglusul" class="datepicker form-control" placeholder="Pilih tanggal Usulan">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tanggal Selesai</label>
                                        <div class="form-line">
                                            <input type="text" name="tglsls" class="datepicker1 form-control" placeholder="Pilih tanggal Selesai">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="m-b-15">Bidang</label>
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="bidang1" id="bidang1" class="chk-col-teal filled-in " value="1" />
                                     <label for="bidang1">PENDIDIKAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="bidang2" id="bidang2" class="chk-col-teal filled-in" value="1"  />
                                     <label for="bidang2">PENELITIAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-4">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="bidang3" id="bidang3" class="chk-col-teal filled-in" value="1"  />
                                     <label for="bidang3">PENGABDIAN MASYARAKAT</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="bidang4" id="bidang4" class="chk-col-teal filled-in" value="1"  />
                                     <label for="bidang4">BEASISWA</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="bidang5" id="bidang5" class="chk-col-teal filled-in"  value="1" />
                                     <label for="bidang5">LAIN-LAIN</label>
                                    </div> 
                                </div>
                                <br>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 1</label>
                                        <div class="form-line">
                                            <input name="file1" type="file"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 2</label>
                                        <div class="form-line">
                                            <input name="file2" type="file"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 3</label>
                                        <div class="form-line">
                                            <input name="file3" type="file"  />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 4</label>
                                        <div class="form-line">
                                            <input name="file4" type="file"  />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- -->
         <!-- Small Size -->
        <div class="modal fade delModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk membatalkan kerjasama ?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-del" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade editModal" id="" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Edit Kerjasama</h4>
                        <i class="material-icons right" style="margin-top:-40px">edit</i>
                    </div>
                    <div class="modal-body" >
                        <form id="editform" action="" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Mitra</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="editmitra" name="editmitra" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="form-label">Kategori</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <select type="text" id="editkategori1" name="editkategori1" class="form-control show-tick">
                                            <option value="0" disabled selected>Pilih Kategori</option>
                                            <option value="1">Perguruan Tinggi</option>
                                            <option value="2">Pemerintah</option>
                                            <option value="3">Lembaga Swasta</option>
                                            <option value="4">Dunia Industri</option>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group form-float" id="editsubkategori">
                                        <select type="text"  name="editsubkategori" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>   
                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="editsubkategori1">
                                        <select type="text"  name="editsubkategori1" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            
                                            <?php 
                                            foreach ($tabkat1->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="editsubkategori2">
                                        <select type="text" name="editsubkategori2" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>

                                            
                                            <?php 
                                            foreach ($tabkat2->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>

                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="editsubkategori3">
                                        <select type="text"  name="editsubkategori3" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            <?php 
                                            foreach ($tabkat3->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>

                                        </select>
                                    </div>
                                    <div class="form-group form-float" id="editsubkategori4">
                                        <select type="text"  name="editsubkategori4" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Sub Kategori</option>
                                            <?php 
                                            foreach ($tabkat4->result() as $result){?>
                                                <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="col-md-12"> 
                                    <div class="form-group form-float">
                                        <label class="form-label">Detail Kerjasama</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" id="editjudul" name="editjudul" placeholder="Masukan Judul Usulan Kerjasama Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Manfaat untuk Mitra</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" id="editmanfaatm" name="editmanfaat_m" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap Mitra Anda" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                         <label class="form-label">Manfaat untuk UNDIP</label>
                                        <div class="form-line">
                                            <textarea type="text" cols="30" rows="3" class="form-control no-resize" id="editmanfaatu" name="editmanfaat_u" placeholder="Masukan Manfaat dari Kerjasama Anda terhadap UNDIP" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                    <label class="form-label">Negara</label>
                                        <select type="text"  id="editnegara" name="editnegara" class="form-control show-tick" required>
                                            <option value="0" disabled selected>Pilih Negara</option>
                                            
                                            <?php 
                                            foreach ($negara->result() as $result){?>
                                                <option value="<?php echo $result->negara?>"><?php echo $result->negara?></option>
                                            <?php }?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Periode</label>
                                        <div class="form-line">
                                            <input type="number" max="15" class="form-control" id="editperiode" name="editperiode" placeholder="Masukan Periode Kerjasama Anda (Max 15 Tahun)" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tanggal Usul</label>
                                        <div class="form-line">
                                            <input type="text" id="edittglusul" name="edittglusul" class="datepicker form-control" placeholder="Pilih tanggal Usulan">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tanggal Selesai</label>
                                        <div class="form-line">
                                            <input type="text" id="edittgllama" name="edittglsls" class="datepicker1 form-control" placeholder="Pilih tanggal Selesai">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <label class="m-b-15">Bidang</label>
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="editbidang1" id="editbidang1" class="chk-col-teal filled-in " value="1" />
                                     <label for="editbidang1">PENDIDIKAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="editbidang2" id="editbidang2" class="chk-col-teal filled-in" value="1"  />
                                     <label for="editbidang2">PENELITIAN</label>
                                    </div> 
                                </div>
                                <div class="col-md-4">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="editbidang3" id="editbidang3" class="chk-col-teal filled-in" value="1"  />
                                     <label for="editbidang3">PENGABDIAN MASYARAKAT</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="editbidang4" id="editbidang4" class="chk-col-teal filled-in" value="1"  />
                                     <label for="editbidang4">BEASISWA</label>
                                    </div> 
                                </div>
                                <div class="col-md-2">   
                                    <div class="demo-checkbox">
                                     <input type="checkbox" name="editbidang5" id="editbidang5" class="chk-col-teal filled-in"  value="1" />
                                     <label for="editbidang5">LAIN-LAIN</label>
                                    </div> 
                                </div>
                                <br>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 1</label>
                                        <div class="form-line">
                                            <input name="editfile1" type="file" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 2</label>
                                        <div class="form-line">
                                            <input name="editfile2" type="file" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 3</label>
                                        <div class="form-line">
                                            <input name="editfile3" type="file" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berkas 4</label>
                                        <div class="form-line">
                                            <input name="editfile4" type="file" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- -->
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Autosize Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/autosize/autosize.js');?>"></script>

    <!-- Moment Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/momentjs/moment.js');?>"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js');?>"></script>
    <script>
    $(function () {
        //Textare auto growth
        autosize($('textarea.auto-growth'));

        //Datetimepicker plugin
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'dddd DD MMMM YYYY - HH:mm',
            clearButton: true,
            weekStart: 1
        });

        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.datepicker1').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });

        $('.timepicker').bootstrapMaterialDatePicker({
            format: 'HH:mm',
            clearButton: true,
            date: false
        });
    });
    </script>

    <script>
    $(document).ready(function(){

        $("#subkategori1").hide();
        $("#subkategori2").hide();
        $("#subkategori3").hide();
        $("#subkategori4").hide();
        $("#asubkategori1").hide();
        $("#asubkategori2").hide();
        $("#asubkategori3").hide();
        $("#asubkategori4").hide();
        $("#editsubkategori1").hide();
        $("#editsubkategori2").hide();
        $("#editsubkategori3").hide();
        $("#editsubkategori4").hide();
        $("#proses1").hide();
        $("#proses2").hide();
        $("#proses3").hide();


        $("#kategori").change(function(){
            x = document.getElementById("kategori").value;
            if (x==1) {
               $("#subkategori1").show();
               $("#subkategori").hide();
               $("#subkategori2").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
            }else if (x==2){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
               $("#subkategori2").show(); 
            }else if (x==3){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori2").hide();
               $("#subkategori4").hide();
               $("#subkategori3").show(); 
            }else if (x==4){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori2").hide();
               $("#subkategori4").show(); 
            }
        });

         $("#kategori1").change(function(){
            x = document.getElementById("kategori1").value;
            if (x==1) {
               $("#asubkategori1").show();
               $("#asubkategori").hide();
               $("#asubkategori2").hide();
               $("#asubkategori3").hide();
               $("#asubkategori4").hide();
            }else if (x==2){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori3").hide();
               $("#asubkategori4").hide();
               $("#asubkategori2").show(); 
            }else if (x==3){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori2").hide();
               $("#asubkategori4").hide();
               $("#asubkategori3").show(); 
            }else if (x==4){
               $("#asubkategori").hide();
               $("#asubkategori1").hide();
               $("#asubkategori3").hide();
               $("#asubkategori2").hide();
               $("#asubkategori4").show(); 
            }
        });
        
        $("#editkategori1").change(function(){
            x = document.getElementById("editkategori1").value;
            if (x==1) {
               $("#editsubkategori1").show();
               $("#editsubkategori").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori4").hide();
            }else if (x==2){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori4").hide();
               $("#editsubkategori2").show(); 
            }else if (x==3){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori4").hide();
               $("#editsubkategori3").show(); 
            }else if (x==4){
               $("#editsubkategori").hide();
               $("#editsubkategori1").hide();
               $("#editsubkategori3").hide();
               $("#editsubkategori2").hide();
               $("#editsubkategori4").show(); 
            }
        });

        $("#kegiatan").change(function(){
            x = document.getElementById("kegiatan").value;
            if (x==1) {
               $("#proses1").show();
               $("#proses").hide();
               $("#proses3").hide();
               $("#proses2").hide();
            }else if (x==2){
               $("#proses1").hide();
               $("#proses3").hide();
               $("#proses").hide();
               $("#proses2").show(); 
            }else if (x==3){
               $("#proses1").hide();
               $("#proses3").show();
               $("#proses").hide();
               $("#proses2").hide(); 
            }
        });
      
            
            
    });

    </script>

    <script>
    $(document).ready(function(){
        var check1=0; var id;var namaOld=""; var editCheck;

        $(".info").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'MOUAdmin/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#infomitra").val(data['mitra']);
                    $("#infojenis").val(data['jenis']);
                    $("#infojudul").val(data['judul']);
                    $("#infotahun").val(data['tahun']);
                    $("#infotglusul").val(data['tgl_usul']);
                    $("#infotgllama").val(data['tgl_lama']);
                    $("#infokategori").val(data['kategori']);
                    $("#infosubkategori").val(data['subkategori']);
                    $("#infopengusul").val(data['pengusul']);
                    $("#infobidang1").val(data['bidang1']);
                    $("#infobidang2").val(data['bidang2']);
                    $("#infobidang3").val(data['bidang3']);
                    $("#infobidang4").val(data['bidang4']);
                    $("#infobidang5").val(data['bidang5']);
                    $("#infonegara").val(data['negara']);
                    if($('#infobidang1').val()!=0) {
                        $('input[name="infobidang1"]').prop('checked', true);
                    }else if($('#infobidang1').val()==0) {
                        $('input[name="infobidang1"]').prop('checked', false);
                    };
                    if($('#infobidang2').val()!=0) {
                        $('input[name="infobidang2"]').prop('checked', true);
                    }else if($('#infobidang2').val()==0) {
                        $('input[name="infobidang2"]').prop('checked', false);
                    };
                    if($('#infobidang3').val()!=0) {
                        $('input[name="infobidang3"]').prop('checked', true);
                    }else if($('#infobidang3').val()==0) {
                        $('input[name="infobidang3"]').prop('checked', false);
                    };
                    if($('#infobidang4').val()!=0) {
                        $('input[name="infobidang4"]').prop('checked', true);
                    }else if($('#infobidang4').val()==0) {
                        $('input[name="infobidang4"]').prop('checked', false);
                    };
                    if($('#infobidang5').val()!=0) {
                        $('input[name="infobidang5"]').prop('checked', true);
                    }else if($('#infobidang5').val()==0) {
                        $('input[name="infobidang5"]').prop('checked', false);
                    };
                    $("#infoperiode").val(data['periode']);
                    $("#infomanfaatm").val(data['manfaat_m']);
                    $("#infomanfaatu").val(data['manfaat_u']);
                    $("#editberkas1").click(function(){
                        file = (data['berkas1']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas2").click(function(){
                        file = (data['berkas2']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas3").click(function(){
                        file = (data['berkas3']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas4").click(function(){
                        file = (data['berkas4']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                }
            });
        });

        $(".edit").click(function(){
            id = $(this).attr('id');
           $.ajax({
                url:'MOUAdmin/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editmitra").val(data['mitra']);
                    $("#editjenis").val(data['jenis']);
                    $("#editjudul").val(data['judul']);
                    $("#edittahun").val(data['tahun']);
                    $("#edittglusul").val(data['tgl_usul']);
                    $("#edittgllama").val(data['tgl_lama']);
                    $("#editkategori").val(data['kategori']);
                    $("#editsubkategori").val(data['subkategori']);
                    $("#editpengusul").val(data['pengusul']);
                    $("#bidang1").val(data['bidang1']);
                    $("#bidang2").val(data['bidang2']);
                    $("#bidang3").val(data['bidang3']);
                    $("#bidang4").val(data['bidang4']);
                    $("#bidang5").val(data['bidang5']);
                    $("#editnegara").val(data['negara']);
                    if($('#bidang1').val()!=0) {
                        $('input[name="editbidang1"]').prop('checked', true);
                    }else if($('#infobidang1').val()==0) {
                        $('input[name="editbidang1"]').prop('checked', false);
                    };
                    if($('#bidang2').val()!=0) {
                        $('input[name="editbidang2"]').prop('checked', true);
                    }else if($('#editbidang2').val()==0) {
                        $('input[name="editbidang2"]').prop('checked', false);
                    };
                    if($('#bidang3').val()!=0) {
                        $('input[name="editbidang3"]').prop('checked', true);
                    }else if($('#editbidang3').val()==0) {
                        $('input[name="editbidang3"]').prop('checked', false);
                    };
                    if($('#bidang4').val()!=0) {
                        $('input[name="editbidang4"]').prop('checked', true);
                    }else if($('#editbidang4').val()==0) {
                        $('input[name="editbidang4"]').prop('checked', false);
                    };
                    if($('#bidang5').val()!=0) {
                        $('input[name="editbidang5"]').prop('checked', true);
                    }else if($('#editbidang5').val()==0) {
                        $('input[name="editbidang5"]').prop('checked', false);
                    };
                    $("#editperiode").val(data['periode']);
                    $("#editmanfaatm").val(data['manfaat_m']);
                    $("#editmanfaatu").val(data['manfaat_u']);
                    $("#editberkas1").click(function(){
                        file = (data['berkas1']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas2").click(function(){
                        file = (data['berkas2']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas3").click(function(){
                        file = (data['berkas3']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                    $("#editberkas4").click(function(){
                        file = (data['berkas4']);
                        folder = (data['folder']);
                        pengusul = (data['usrnm']);
                        
                        window.open('DashUser/lihatFile/'+pengusul+'/berkas/'+folder+'/'+file+'/pdf','_blank');
                    });
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $(".acc").click(function(){
            id = $(this).attr('id');
        });

        $(".can").click(function(){
            id = $(this).attr('id');
        });

        $(".up").click(function(){
            id = $(this).attr('id');
        });

        $(".upsts").click(function(){
            id = $(this).attr('id');
        });

        $(".nas").click(function(){
            id = $(this).attr('id');
        });
                                        

         $(".detsts").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'MOUAdmin/detStat/'+id,
                data:{send:true},
                success:function(msg){
                    $("#detStat").html(msg);
                }
            });
        });


        $("#confirm-acc").click(function(){
            $.ajax({
                url:'MOUAdmin/accept/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-can").click(function(){
            $.ajax({
                url:'MOUAdmin/cancel/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-del").click(function(){
            $.ajax({
                url:'MOUAdmin/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upload/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $("#upload1").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upload1/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $("#upstat").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upstat/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

    });

    </script>

    <script>
    $(function () {
         $('#upload').validate({
            rules: {
                'fileMOU': {
                     remote: {
                        url: "MOUAdmin/cekFileMOU",
                        type: "post",
                        data: {
                          fileMOU: function() {
                            return $( "#fileMOU" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'fileMOU':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });

        $('#upload1').validate({
            rules: {
                'fileNaskah': {
                     remote: {
                        url: "MOUAdmin/cekFileNaskah",
                        type: "post",
                        data: {
                          fileMOU: function() {
                            return $( "#fileNaskah" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'fileNaskah':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
        
        $('#add').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "MOUAdmin/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "MOUAdmin/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "MOUAdmin/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "MOUAdmin/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
        
        $('#editform').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "MOUAdmin/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "MOUAdmin/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "MOUAdmin/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "MOUAdmin/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>       

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            iDisplayLength: 50,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-detstat').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>