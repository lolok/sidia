<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css');?>" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-facebook" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-twitter" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-linkedin" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-instagram" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-rss" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <!-- #END# Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('IndexCntrl'); ?>">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo site_url('DaftarKerjasama'); ?>">
                            <i class="material-icons">description</i>
                            <span>Daftar Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('StatKerjasama'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Statistik Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('LoginCntrl'); ?>">
                            <i class="material-icons">person</i>
                            <span>Login</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Line Chart -->
                
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-blue" style="margin-bottom:-10px">
                            <h2>
                                Daftar Kerjasama
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <i class="material-icons">description</i>
                                </li>
                            </ul>
                        </div>
                        <div class="body m-t-10">
                             <form action="<?php echo site_url('DaftarKerjasama/pilihKat'); ?>" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <select type="text" id="kategori" name="kategori" class="form-control show-tick">
                                                <option value="0" disabled selected>Pilih Kategori</option>
                                                <option value="1">Perguruan Tinggi</option>
                                                <option value="2">Pemerintah</option>
                                                <option value="3">Lembaga Swasta</option>
                                                <option value="4">Dunia Industri</option>
                                            </select>
                                        </div>
                                    </div> 
                                    <div class="col-md-3">
                                        <div class="form-group form-float" id="subkategori">
                                            <select type="text"  name="subkategori" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>   
                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori1">
                                            <select type="text"  name="subkategori1" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                
                                                <?php 
                                                foreach ($tabkat1->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>
                                                
                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori2">
                                            <select type="text" name="subkategori2" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>

                                                
                                                <?php 
                                                foreach ($tabkat2->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>

                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori3">
                                            <select type="text"  name="subkategori3" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                <?php 
                                                foreach ($tabkat3->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>

                                            </select>
                                        </div>
                                        <div class="form-group form-float" id="subkategori4">
                                            <select type="text"  name="subkategori4" class="form-control show-tick" required>
                                                <option value="0" disabled selected>Pilih Sub Kategori</option>
                                                <?php 
                                                foreach ($tabkat4->result() as $result){?>
                                                    <option value="<?php echo $result->id_kategori?>"><?php echo $result->subkategori?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group form-float">
                                            <select type="text" id="negara" name="negara" class="form-control show-tick">
                                                <option disabled selected>Pilih Negara</option>
                                                <?php 
                                                foreach ($negara->result() as $result){?>
                                                    <option value="<?php echo $result->negara?>"><?php echo $result->negara?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group form-float">
                                            <select type="text" id="tahun" name="tahun" class="form-control show-tick">
                                                <option disabled selected>Pilih Tahun</option>
                                                <option value="<?php echo $tahun1 ?>"><?php echo $tahun1 ?></option>
                                                <option value="<?php echo $tahun2 ?>"><?php echo $tahun2 ?></option>
                                                <option value="<?php echo $tahun3 ?>"><?php echo $tahun3 ?></option>
                                                <option value="<?php echo $tahun4 ?>"><?php echo $tahun4 ?></option>
                                                <option value="<?php echo $tahun5 ?>"><?php echo $tahun5 ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn btn-link waves-effect right">SUBMIT</button>
                                    </div>  
                                    
                                </div>
                    
                            </form>
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable1">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Mitra</th>
                                    <th>Kategori</th>
                                    <th>Sub Kategori</th>
                                    <th>Negara</th>
                                    <th>Tahun</th>
                                    <th>Periode</th>
                                    <th>Tanggal Mulai</th>
                                    <th>Tanggal Selesai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $no=1;
                                foreach ($tabmou->result() as $result){?>
                                    <tr>
                                      <td><?php echo htmlspecialchars($no); ?></td>   
                                      <td><?php echo htmlspecialchars($result->mitra); ?></td> 
                                      <td><?php echo htmlspecialchars($result->kategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->subkategori); ?></td> 
                                      <td><?php echo htmlspecialchars($result->negara); ?></td>
                                      <td><?php echo htmlspecialchars($result->tahun); ?></td>
                                      <td><?php echo htmlspecialchars($result->periode); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_usul); ?></td>
                                      <td><?php echo htmlspecialchars($result->tgl_lama); ?></td>
                                    </tr>

                                <?php $no++; }?>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <script>
    $(document).ready(function(){

        $("#subkategori1").hide();
        $("#subkategori2").hide();
        $("#subkategori3").hide();
        $("#subkategori4").hide();


        $("#kategori").change(function(){
            x = document.getElementById("kategori").value;
            if (x==1) {
               $("#subkategori1").show();
               $("#subkategori").hide();
               $("#subkategori2").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
            }else if (x==2){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori4").hide();
               $("#subkategori2").show(); 
            }else if (x==3){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori2").hide();
               $("#subkategori4").hide();
               $("#subkategori3").show(); 
            }else if (x==4){
               $("#subkategori").hide();
               $("#subkategori1").hide();
               $("#subkategori3").hide();
               $("#subkategori2").hide();
               $("#subkategori4").show(); 
            }
        });

        var check1=0; var id;var namaOld=""; var editCheck;

        $("#receptvas").change(function(){
            if($('#receptvas').prop('checked')){
                $(".receptvase").toggle();
                $('input[name="receptvase"]').prop('required',true);
            } else{
                $(".receptvase").hide();
                $('input[name="receptvase"]').prop('required',false);
                $('input[name="receptvase"]').prop('value',0);  
            }
        });

        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'DashUser/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editbidang1").val(data['bidang']);
                    $("#editkategori1").val(data['kategori']);
                    $("#editjudul1").val(data['judul']);
                    $("#editmanfaatm1").val(data['manfaat_m']);
                    $("#editmanfaatu1").val(data['manfaat_u']);
                    $("#editnegara1").val(data['negara']);
                    $("#editperiode1").val(data['periode']);
                    $("#editfile11").val(data['berkas1']);
                    $("#editfile21").val(data['berkas2']);
                    $("#editfile31").val(data['berkas3']);
                    $("#editfile41").val(data['berkas4']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'DashUser/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $("#confirm-del").click(function(){
            $.ajax({
                url:'DashUser/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

    });

    </script>

    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            ordering: false,
            paging: false,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            iDisplayLength: 50,
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-all').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>
</body>

</html>