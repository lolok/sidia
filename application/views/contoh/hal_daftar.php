<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

</head>

<body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">SIMOK<b>Undip</b></a>
            <small>Sistem Monitoring Pelaksanaan Kerjasama Universitas Diponegoro</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" action="<?php echo site_url('DaftarCntrl/daftar'); ?>" method="POST">
                    <div class="msg">Daftar Akun</div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" minlength="3" maxlength="15"  class="form-control" id="username" name="username" placeholder="Username" required autofocus>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock</i>
                                </span>
                                <div class="form-line">
                                    <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
                                </div>
                            </div>
                            <hr>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" minlength="3" maxlength="25" class="form-control" name="nama" placeholder="Nama" required>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <div class="form-line">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Alamat Email" required>
                                </div>
                            </div>
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="material-icons">phone_iphone</i>
                                </span>
                                <div class="form-line">
                                    <input type="number" class="form-control" name="hape" placeholder="Nomor HP" required>
                                </div>
                            </div>
                            <hr>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="namaper" placeholder="Nama Perusahaan" required autofocus>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <div class="form-line">
                                    <input type="email" class="form-control" name="emailper" placeholder="Email Perusahaan" required>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">place</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="provinsi" placeholder="Provinsi" required>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">place</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="kota" placeholder="Kota" required>
                                </div>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">place</i>
                                </span>
                                <div class="form-line">
                                    <input type="text" class="form-control" name="alamat" placeholder="Alamat" required>
                                </div>
                            </div>
                            <div class="input-group">
                                 <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <div class="form-line">
                                    <input type="number" class="form-control" name="telp" placeholder="Nomor Telp" required>
                                </div>
                            </div>
                        
                    

                    <button class="btn btn-block btn-lg bg-light-blue waves-effect" type="submit">SIGN UP</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <a href="<?php echo site_url('LoginCntrl'); ?>">Kembali ke Login</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
    $('#sign_up').validate({
            rules: {
                'terms': {
                    required: true
                },
                'confirm': {
                    equalTo: '[name="password"]'
                },
                'username': {
                     remote: {
                        url: "DaftarCntrl/cekUser",
                        type: "post",
                        data: {
                          username: function() {
                            return $( "#username" ).val();
                          }
                        }
                    }
                },
                'email': {
                     remote: {
                        url: "DaftarCntrl/cekEmail",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#email" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'confirm': {
                    equalTo: 'Masukan password yang sama.',
                    required: 'Confirm tidak boleh kosong.'
                },
                'username':{
                    remote: 'Username sudah terdaftar.',
                    required: 'Username tidak boleh kosong.'
                },
                'password':{
                    required: 'Password tidak boleh kosong.'
                },
                'nama':{
                    required: 'nama tidak boleh kosong.'
                },
                'email':{
                    remote: 'Email sudah terdaftar.',
                    required: 'Email tidak boleh kosong.'
                },
                'hape':{
                    required: 'No. HP tidak boleh kosong.'
                },
                'namaper':{
                    required: 'Nama Perusahaan tidak boleh kosong.'
                },
                'emailper':{
                    required: 'Email Perusahaan tidak boleh kosong.'
                },
                'provinsi':{
                    required: 'Provinsi tidak boleh kosong.'
                },
                'kota':{
                    required: 'Kota tidak boleh kosong.'
                },
                'alamat':{
                    required: 'Alamat Perusahaan tidak boleh kosong.'
                },
                'telp':{
                    required: 'Telepon tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>

    <!-- Bootstrap Notify Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-notify/bootstrap-notify.js');?>"></script>

    
    <?php if($report!=0){?>
    <script>
    $(function () {
        $('document').ready(function () {
            var placementFrom = 'top';
            var placementAlign = 'center';
            var animateEnter = $(this).data('animate-enter');
            var animateExit = $(this).data('animate-exit');
            var colorName = 'bg-red';

            showNotification(colorName, null, placementFrom, placementAlign, animateEnter, animateExit);
        });
    });

    function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
        if (colorName === null || colorName === '') { colorName = 'bg-black'; }
        if (text === null || text === '') { text = 'Username atau Password salah !'; }
        if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
        if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
        var allowDismiss = true;

        $.notify({
            message: text
        },
            {
                type: colorName,
                allow_dismiss: allowDismiss,
                newest_on_top: true,
                timer: 1500,
                placement: {
                    from: placementFrom,
                    align: placementAlign
                },
                animate: {
                    enter: animateEnter,
                    exit: animateExit
                },
                template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                '<span data-notify="icon"></span> ' +
                '<span data-notify="title">{1}</span> ' +
                '<span data-notify="message">{2}</span>' +
                '<div class="progress" data-notify="progressbar">' +
                '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                '</div>' +
                '<a href="{3}" target="{4}" data-notify="url"></a>' +
                '</div>'
            });
    }
    </script>
    <?php }?>
</body>

</html>