<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li>
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashAdmin'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li  >
                        <a href="<?php echo site_url('MOUAdmin'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola MOU</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('KelolaInisiasi'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Inisiasi</span>
                        </a>
                    </li>
                    <li class="active">
                        <a href="<?php echo site_url('KelolaCntrl'); ?>">
                            <i class="material-icons">group</i>
                            <span>Kelola Pengguna</span>
                        </a>
                    </li>
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- Line Chart -->
                <!-- #END# Line Chart -->
                <!-- Bar Chart -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header" style="margin-bottom:-10px">
                            <h2>
                                Daftar Pengguna
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);">Action</a></li>
                                        <li><a href="javascript:void(0);">Another action</a></li>
                                        <li><a href="javascript:void(0);">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row">
                                <a class="del btn bg-teal waves-effect m-l-20 m-b-25 " data-toggle="modal" data-target=".addModal" title="Tambah Pengguna">
                                    <h4 class="m-b-10"><i class="material-icons">add</i> TAMBAH FAKULTAS</h4>
                                </a>
                            </div>
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable1">
                            <thead>
                                <tr>
                                    <th>Username</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No HP</th>
                                    <th>Perusahaan</th>
                                    <th>Kota</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                foreach ($tabuser->result() as $result){
                                  if(($result->level)>1){?>
                                    <tr>
                                      <td><?php echo htmlspecialchars($result->username); ?></td> 
                                      <td><?php echo htmlspecialchars($result->nama); ?></td>
                                      <td><?php echo htmlspecialchars($result->email); ?></td>
                                      <td><?php echo htmlspecialchars($result->nohp); ?></td>
                                      <td><?php echo htmlspecialchars($result->namaper); ?></td>
                                      <td><?php echo htmlspecialchars($result->kota); ?></td>
                                      <td><?php switch($result->level){
                                        case "2": echo "Pengguna"; break;
                                        case "3": echo "Fakultas"; break;
                                        }?></td>
                                      <td>
                                        <a class="inf btn btn-primary waves-effect" data-toggle="modal" data-target=".infoModal" id="<?php echo htmlspecialchars($result->id_user); ?>" title="Detail Pengguna">
                                            <i class="material-icons">info</i>
                                        </a>
                                        <a class="del btn bg-red waves-effect" data-toggle="modal" data-target=".delModal" id="<?php echo htmlspecialchars($result->id_user); ?>" title="Hapus Pengguna">
                                            <i class="material-icons">clear</i>
                                        </a>
                                        
                                       </td>
                                    </tr>
                                <?php }}?>
                            </tbody>
                        </table>
                        </div>
                        
                    </div>

                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
        <!-- Modal Detail Rekap -->
        <div class="modal fade addModal" id="" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-teal"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Tambah Pengguna Fakultas</h4>
                        <i class="material-icons right" style="margin-top:-40px">add</i>
                    </div>
                    <div class="modal-body" >
                        <form id="add" action="<?php echo site_url('KelolaCntrl/tambahPengguna'); ?>" method="POST" enctype="multipart/form-data">
                            <div class="row">

                                <div class="col-md-4">
                                    <label class="m-t-5">Username</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="username" name="username" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="m-t-5">Nama</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="nama" name="nama" placeholder="" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="m-t-5">Password</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="password" class="form-control" name="password" minlength="6" placeholder="" required>
                                        </div>                                    
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="m-t-5">Confirm Password</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="password" class="form-control" name="confirm" minlength="6" placeholder="" required>
                                        </div>
                                    </div>  
                                </div>
                                
                                
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Modal INFO -->
        <div class="modal fade infoModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-blue"> 
                        <h4 class="modal-title m-b-15" id="largeModalLabel" >Detail Pengguna</h4>
                        <i class="material-icons right" style="margin-top:-40px">info</i>
                    </div>
                    <div class="modal-body" >

                            <div class="row">
                                <div class="col-md-2">
                                    <label class="m-t-5">Username</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infousername" name="username" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Nama</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonama" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Email</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoemail" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">No. HP</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonohp" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Nama Perusahaan</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infonamaper" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>  
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Email Perusahaan</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoemailper" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Provinsi</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" id="infoprovinsi" name="bidang" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Kota</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infokota" class="form-control" name="negara" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">Alamat</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infoalamat" max="15" class="form-control" name="periode" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <label class="m-t-5">NO telepon</label>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                             <input type="text" id="infotelp" class="form-control" name="negara" placeholder="-" disabled>
                                        </div>
                                    </div>
                                </div>
                               
                                
                            </div>
                    </div>
                    <div class="modal-footer">
                        
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- -->
        <!-- Small Size -->
        <div class="modal fade delModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-red">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk menghapus user?</h4>
                    </div>
                    <div class="modal-body bg-red">

                    </div>
                    <div class="modal-footer">
                        <a href="" id="confirm-del" type="button" class="btn btn-link waves-effect">IYA</a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Small Size -->
        <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel">Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

     <!-- Validation Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(document).ready(function(){
        var check1=0; var id;var namaOld=""; var editCheck;

        $(".inf").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'KelolaCntrl/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#infousername").val(data['username']);
                    $("#infonama").val(data['nama']);
                    $("#infoemail").val(data['email']);
                    $("#infonohp").val(data['nohp']);
                    $("#infonamaper").val(data['namaper']);
                    $("#infoemailper").val(data['emailper']);
                    $("#infoprovinsi").val(data['provinsi']);
                    $("#infokota").val(data['kota']);
                    $("#infoalamat").val(data['alamat']);
                    $("#infotelp").val(data['telp']);
                    
                }
            });
        });

        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'DashUser/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editbidang1").val(data['bidang']);
                    $("#editkategori1").val(data['kategori']);
                    $("#editjudul1").val(data['judul']);
                    $("#editmanfaatm1").val(data['manfaat_m']);
                    $("#editmanfaatu1").val(data['manfaat_u']);
                    $("#editnegara1").val(data['negara']);
                    $("#editperiode1").val(data['periode']);
                    $("#editfile11").val(data['berkas1']);
                    $("#editfile21").val(data['berkas2']);
                    $("#editfile31").val(data['berkas3']);
                    $("#editfile41").val(data['berkas4']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'DashUser/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

        $(".del").click(function(){
            id = $(this).attr('id');
        });

        $(".acc").click(function(){
            id = $(this).attr('id');
        });

        $(".can").click(function(){
            id = $(this).attr('id');
        });

        $(".up").click(function(){
            id = $(this).attr('id');
        });


        $("#confirm-acc").click(function(){
            $.ajax({
                url:'MOUAdmin/accept/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#confirm-del").click(function(){
            $.ajax({
                url:'KelolaCntrl/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            })
        });

        $("#upload").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'MOUAdmin/upload/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
      
        });

    });

    </script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            order: [[ 3, "desc" ]],
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            order: [[ 7, "desc" ]],
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    </script>
    <script>
    $(function () {
    $('#add').validate({
            rules: {
                'terms': {
                    required: true
                },
                'confirm': {
                    equalTo: '[name="password"]'
                },
                'username': {
                     remote: {
                        url: "KelolaCntrl/cekUser",
                        type: "post",
                        data: {
                          username: function() {
                            return $( "#username" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'confirm': {
                    equalTo: 'Masukan password yang sama.',
                    required: 'Confirm tidak boleh kosong.'
                },
                'username':{
                    remote: 'Username sudah terdaftar.',
                    required: 'Username tidak boleh kosong.'
                },
                'password':{
                    required: 'Password tidak boleh kosong.'
                },
                'nama':{
                    required: 'nama tidak boleh kosong.'
                },
                'email':{
                    remote: 'Email sudah terdaftar.',
                    required: 'Email tidak boleh kosong.'
                },
                'hape':{
                    required: 'No. HP tidak boleh kosong.'
                },
                'namaper':{
                    required: 'Nama Perusahaan tidak boleh kosong.'
                },
                'emailper':{
                    required: 'Email Perusahaan tidak boleh kosong.'
                },
                'provinsi':{
                    required: 'Provinsi tidak boleh kosong.'
                },
                'kota':{
                    required: 'Kota tidak boleh kosong.'
                },
                'alamat':{
                    required: 'Alamat Perusahaan tidak boleh kosong.'
                },
                'telp':{
                    required: 'Telepon tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>