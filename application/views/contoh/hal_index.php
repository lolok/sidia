﻿<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <!-- Notifications -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-facebook" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-twitter" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-linkedin" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-instagram" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button">
                            <i class="material-icons fa fa-rss" style="font-size:20px; margin-right:10px; margin-top:2px"></i>
                        </a>
                    </li>
                    <!-- #END# Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li class="active">
                        <a href="<?php echo site_url('IndexCntrl'); ?>">
                            <i class="material-icons">home</i>
                            <span>Home</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('DaftarKerjasama'); ?>">
                            <i class="material-icons">description</i>
                            <span>Daftar Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('StatKerjasama'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Statistik Kerjasama UNDIP</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo site_url('LoginCntrl'); ?>">
                            <i class="material-icons">person</i>
                            <span>Login</span>
                        </a>
                    </li>
                    
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <!-- With Captions -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header ">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active">
                                    <a href="#home_with_icon_title" data-toggle="tab">
                                        TENTANG KAMI
                                    </a>
                                </li>
                                <li role="presentation">
                                    <a href="#profile_with_icon_title" data-toggle="tab">
                                        ALUR PENGAJUAN KERJASAMA
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane fade in active" id="home_with_icon_title">
                                    <h2 style="margin-top:-10px;">SISTEM KERJASAMA UNDIP</h2>
                                    <hr style="border-color:black">
                                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                        <!-- Indicators -->
                                        <ol class="carousel-indicators">
                                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                        </ol>

                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner" role="listbox">
                                            <div class="item active">
                                                <img src="<?php echo site_url('mou/images/image-gallery/21.jpg'); ?>" />
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo site_url('mou/images/image-gallery/22.jpg'); ?>" />
                                            </div>
                                            <div class="item">
                                                <img src="<?php echo site_url('mou/images/image-gallery/23.jpg'); ?>" />
                                            </div>
                                        </div>

                                        <!-- Controls -->
                                        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </div>
                                    <p style="margin-top:20px">
                                        Sistem kerjasama UNDIP ini bertujuan untuk mempermudah pengajuan kerjasama pada UNDIP dengan via online. Dengan adanya sistem ini kerjasama
                                        UNDIP dapat dimonitoring dan diinformasikan secara efektif. 
                                    </p>
                                </div>
                                <div role="tabpanel" class="tab-pane fade" id="profile_with_icon_title">
                                    <h2 style="margin-top:-10px;">PENGAJUAN KERJASAMA UNDIP</h2>
                                    <hr style="border-color:black">
                                    <ol>
                                        <li>Mitra membuat Akun. <span style="color:red">(satu mitra hanya dapat memiliki satu akun)</span></li>
                                        <li>Mitra login dengan Akun yang sudah teregistrasi.</li>
                                        <li>Mitra menambahkan pengajuan kerjasama dan mengisi form pengajuan kerjasama sesuai dengan ketenetuan dan kebutuhan kerjasama. </li>
                                        <li>Mitra menunggu kabar penerimaan pengajuan dari UNDIP.</li>
                                        <li>Jika kerjasama diterima maka mitra akan mendapatkan email dan file kesepakatan kerjasama (MOU) dari UNDIP.</li>
                                    </ol>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <!-- #END# With Captions -->
            </div>
        </div>
    </section>

    <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>

    <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>