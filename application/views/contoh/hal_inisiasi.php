<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Sistem Monitoring Kerjasama UNDIP</title>

    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('mou/favicon.ico');?>" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap/css/bootstrap.css'); ?>" rel="stylesheet"/>

    <!-- Font Awesome -->
    <link href="<?php echo base_url('mou/plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet"/>

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url('mou/plugins/node-waves/waves.css'); ?>" rel="stylesheet"/>

    <!-- Select Effect Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-select/css/bootstrap-select.css'); ?>" rel="stylesheet"/>

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css'); ?>" rel="stylesheet"/>

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css');?>" rel="stylesheet">

    <!-- Animation Css -->
    <link href="<?php echo base_url('mou/plugins/animate-css/animate.css'); ?>" rel="stylesheet"/>

    <!-- Custom Css -->
    <link href="<?php echo base_url('mou/css/style.css'); ?>" rel="stylesheet"/>

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url('mou/css/themes/all-themes.css'); ?>" rel="stylesheet"/>
</head>

<body class="theme-blue">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="preloader">
                <div class="spinner-layer pl-light-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Top Bar -->
    <nav class="navbar">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
                <a href="javascript:void(0);" class="bars"></a>
                <a class="navbar-brand" href="#">
                    <i><img src="<?php echo base_url("mou/images/simok.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-bottom:-10px;margin-top:-10px"/></i>
                    <span style="margin-left:10px">SISTEM MONITORING PELAKSANAAN KERJASAMA UNDIP</span>
                </a>
            </div>
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <!-- Call Search -->
                   
                    <!-- #END# Call Search -->
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" style="margin-right:10px">
                            
                            <text style="font-size:18px;margin-right:10px;text-transform:uppercase"><?php echo ($this->session->userdata("nama"));?></text>
                            <img src="<?php echo base_url("mou/images/user-male-icon.png"); ?>" width="40" height="40" alt="User" alt="User" style="margin-top:-10px"/>

                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="<?php echo site_url('ProfilAdmin'); ?>">
                                    <i class="material-icons">person</i>
                                    <span>Profil</span>
                                </a>
                            </li>
                            <li> 
                                <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                                    <i class="material-icons">exit_to_app</i>
                                    <span>Logout</span>
                                </a>
                            </li>                          
                        </ul>
                    </li>
                    <!-- Notifications -->

                </ul>
            </div>
        </div>
    </nav>
    <!-- #Top Bar -->

    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info" style="background:white;">
                
            <img src="<?php echo base_url("mou/images/logoundip.jpg"); ?>" height="100%" alt="User" style="margin-left:80px"/>
            
                
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header" style="background-color:#2196F3;color:white">MENU</li>
                    <li>
                        <a href="<?php echo site_url('DashFakultas'); ?>">
                            <i class="material-icons">equalizer</i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                     <li>
                        <a href="<?php echo site_url('KerjaFakCtrl'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Kerjasama</span>
                        </a>
                    </li>
                    <li  class="active">
                        <a href="<?php echo site_url('InisiasiCtrl'); ?>">
                            <i class="material-icons">description</i>
                            <span>Kelola Inisiasi</span>
                        </a>
                    </li>
                     <li>
                        <a href="<?php echo site_url('EvalFak'); ?>">
                            <i class="material-icons">edit</i>
                            <span>Evaluasi</span>
                        </a>
                    </li>   
                    <li>
                         <a href="#" data-toggle="modal" data-color="red" data-target="#logoutModal">
                            <i class="material-icons">exit_to_app</i>
                            <span>Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <div class="copyright">
                    &copy; 2017 <a href="javascript:void(0);">Universitas Diponegoro</a>.
                </div>
                <div class="version">
                    <b>www.undip.ac.id</b> 
                </div>
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header bg-light-blue" style="margin-bottom:-10px">
                            <h2>
                                Formulir Pengusulan/Inisiasi Kerjasama Institusi
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <i class="material-icons">description</i>
                                </li>
                            </ul>
                        </div>
                        <div class="body m-t-10">
                        <form id="add" action="<?php echo site_url('InisiasiCtrl/addInisiasi'); ?>" method="POST" enctype="multipart/form-data">
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Usulan</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Fakultas/Unit </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="fakultas" placeholder="Masukan Nama Fakultas Anda" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label class="form-label">Tanggal </label>
                                        <div class="form-line">
                                             <input type="text" name="tglpengusulan" class="datepicker form-control" placeholder="Please choose a date..." >
                                        </div>
                                    </div>
                                </div>
                        </div>
                        
                        <div class="row">
                                 <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Pengusul</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Nama</label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="namapengusul" placeholder="Masukan Nama Pengusul" required>
                                        </div>
                                    </div>
                                </div><div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">NIP </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="nip" placeholder="Masukkan NIP Pengusul" required>
                                        </div>
                                    </div>
                                </div><div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Telp/Fax </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="telp" placeholder="Masukkan No Telp Pengusul" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Nomor HP </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="hp" placeholder="Masukkan No HP Pengusul" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Email </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="email" placeholder="Masukkan Email Pengusul" required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Calon Mitra Kerjasama</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jurusan/Program Studi </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="caljurusan" placeholder="Masukkan Nama jurusan calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Fakultas/Unit Kerja </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="calfakultas" placeholder="Masukkan Nama Fakultas calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Institusi </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="calinstitusi" placeholder="Masukkan Nama Institusi calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Alamat Website </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="website" placeholder="Masukkan Alamat Website calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Nama Kontak </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="kontak" placeholder="Masukkan Nama Kontak calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jabatan </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="jabatan" placeholder="Masukkan Jabatan calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Alamat Surat </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="alamatsurat" placeholder="Masukkan Alamat Surat calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Telp/Fax </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="caltelp" placeholder="Masukkan Telp/Fax calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Nomor HP </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="calhp" placeholder="Masukkan HP calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Email </label>
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="calemail" placeholder="Masukkan HP calon mitra Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="m-t-5">Profil Singkat Calon Mitra</label>
                                </div>
                                <div class="col-md-9">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <textarea type="text" id="profilcalon" cols="30" rows="3" class="form-control no-resize" name="profilcalon" placeholder="Masukkan profil singkat calon mitra Kerjasama"></textarea>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Usulan/Perkiraan Waktu Penandatanganan Kerjasama</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Bulan </label>
                                            <select name="bulanusulan" class="form-control show-tick">
                                                <option value="januari">Januari</option>
                                                <option value="januari">Februari</option>
                                                <option value="januari">Maret</option>
                                                <option value="januari">April</option>
                                                <option value="januari">Mei</option>
                                                <option value="januari">Juni</option>
                                                <option value="januari">Juli</option>
                                                <option value="januari">Agustus</option>
                                                <option value="januari">September</option>
                                                <option value="januari">Oktober</option>
                                                <option value="januari">November</option>
                                                <option value="januari">Desember</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tahun</label>
                                        <div class="form-line">
                                            <select name="tahunusulan" class="form-control show-tick">
                                            <?php
                                                date_default_timezone_set("Asia/Jakarta");
                                                $timestamp = time();
                                                $tahun = date("Y", $timestamp);
                                                for($i=$tahun;$i<=$tahun+20;$i++){
                                            ?>
                                                <option value="<?php echo $i?>"><?php echo $i ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Perlu Acara Seremoni</label>
                                       <div class="form-group">
                                    <input type="radio" name="usulanseremoni" id="yes" value="Y" class="with-gap">
                                    <label for="yes">Yes</label>

                                    <input type="radio" name="usulanseremoni" id="no" value="N" class="with-gap">
                                    <label for="no" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">       
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tingkat Kerjasama Yang Diusulkan</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="tingkatusulan" placeholder="Jelaskan Usulan Kerjasama" required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Berhubungan dengan Kerjasama yang Memerlukan Izin DIKTI</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                   <div class="form-group">
                                    <input type="radio" name="izindikti" id="yes1" value="Y" class="with-gap">
                                    <label for="yes1">Yes</label>

                                    <input type="radio" name="izindikti" id="no1" value="N" class="with-gap">
                                    <label for="no1" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                     <label for="male">Jika 'Ya' Lampirkan Persyaratan DIKTI</label>
                                        <input type="file" id="file" name="fileizindikti" />
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jenis Kerjasama</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                            <label class="form-label">Jenis Kerjasama</label>
                                            <div class="form-group form-float form-line">
                                                <select id="jenis" type="text" name="jenis" class="form-control show-tick" required>
                                                    <option disabled selected>Jenis Kerjasama</option>
                                                    <?php foreach ($jenis->result() as $result){?>
                                                    <option value="<?php echo $result->id_jenis;?>"><?php echo $result->jenis ?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    <script type="text/javascript"> 
                                            $("#jenis").change(function(){
                                               var jenis = {jenis:$("#jenis").val()};
                                                   $.ajax({
                                               type: "POST",
                                               url : "<?php echo base_url(); ?>index.php/InisiasiCtrl/get_subjenis",
                                               data: jenis,
                                               success: function(msg){
                                               $('#subjenis').html(msg);
                                               }
                                            });
                                              });
                                       </script>
                                    <div class="col-md-6">
                                            <label class="form-label">Subjenis Kerjasama</label>
                                            <div class="form-group form-float form-line">
                                                <select id="subjenis" type="text" name="subjenis" class="form-control show-tick" >
                                                    <option disabled selected> Pilih Subjenis</option>
                                                </select>
                                            </div>
                                        </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Sebutkan kegiatan bersama yang pernah dilakukan sebelumnya dengan calon mitra</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <input type="text" class="form-control" name="kegiatanmitra" placeholder="Masukkan Histori Kegiatan sebelumnya Jika ada" required>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Apakah Kegiatan ini akan diawali dengan melibatkan lebih dari 1 fakultas</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-group">
                                    <input type="radio" name="partisipasi" id="yes2" value="Y" class="with-gap">
                                    <label for="yes2">Yes</label>

                                    <input type="radio" name="partisipasi" id="no2" value="N" class="with-gap">
                                    <label for="no2" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                     <label for="male">Jika 'Ya' Sebutkan</label>
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="namapartisipasi" placeholder="Masukkan Mitra Lain yang Bekerjasama"></textarea>
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Rencana Jangka Waktu Kerjasama</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Bulan </label>
                                        <div class="form-line">
                                            <select name="bulanrencana" class="form-control     show-tick">
                                                <option value="januari">Januari</option>
                                                <option value="januari">Februari</option>
                                                <option value="januari">Maret</option>
                                                <option value="januari">April</option>
                                                <option value="januari">Mei</option>
                                                <option value="januari">Juni</option>
                                                <option value="januari">Juli</option>
                                                <option value="januari">Agustus</option>
                                                <option value="januari">September</option>
                                                <option value="januari">Oktober</option>
                                                <option value="januari">November</option>
                                                <option value="januari">Desember</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <label class="form-label">Tahun</label>
                                        <div class="form-line">
                                        <select name="tahunrencana" class="form-control show-tick">
                                            <?php
                                                date_default_timezone_set("Asia/Jakarta");
                                                $timestamp = time();
                                                $tahun = date("Y", $timestamp);
                                                for($i=$tahun;$i<=$tahun+20;$i++){
                                            ?>
                                                <option value="<?php echo $i?>"><?php echo $i ?></option>
                                            <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>   
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Sebutkan Aktifitas Yang Diharapkan Akan Dapat Dilakukan Selama Jangka Waktu Kerjasama Ini</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="aktivitas" placeholder="Sebutkan Harapan Kegiatan"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jelaskan Bagaimana Aktifitas ini Konsisten Dengan Rensra UNDIP</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="konsistensirensa" placeholder="Jelaskan Hubungan Konsistensi Dengan Rensra"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jelaskan Bagaimana Aktifitas Kerjasama ini Akan Dikelola</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="proseskelola" placeholder="Jelaskan proses Kelola Aktifitas"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Jelaskan Keunggulan Strategis Calon Mitra yang Dapat Memberikan Manfaat bagi UNDIP</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="keunggulan" placeholder="Masukkan keuntungan Mitra apabila bekerjasama"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Sebutkan jika ada faktor resiko dan keselamatan yang perli diskusikan dalam kerjasama ini</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="faktor" placeholder="Masukan Resiko dan Keselamatan dalam Kerjasama"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Sebutkan kemungkinan tantangan yang akan dihadapi dalam kerjasama ini</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="tantangan" placeholder="Masukan Tantangan Dalam Kerjasama"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Usulan solusi mengatasi tantangan dalam kerjasama ini</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                        <textarea type="text" id="editjudul" cols="30" rows="3" class="form-control no-resize" name="solusitantangan" placeholder="Solusi Mengatasi Tantangan"></textarea>
                                    </div>
                                </div> 
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Apakah memerlukan fasilitas, dukungan dana, sumber daya manusia yang diperlukan </label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-group">
                                    <input type="radio" name="dana" id="yes3" value="Y" class="with-gap">
                                    <label for="yes3">Yes</label>

                                    <input type="radio" name="dana" id="no3" value="N"class="with-gap">
                                    <label for="no3" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                     <label for="male">Jika 'Ya' Lampirkan perkiraan biaya dan sumber dana </label>
                                        <input type="file" id="file" name="filedana" />
                                    </div>
                                </div>
                        </div>
                        <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Apakah softcopy draft MoU terlampir</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-group">
                                    <input type="radio" name="mou" id="yes4" value="Y" class="with-gap">
                                    <label for="yes4">Yes</label>

                                    <input type="radio" name="mou" id="no4" value="N" class="with-gap">
                                    <label for="no4" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                     <label for="male">Jika 'Ya' Lampirkan MOU </label>
                                        <input type="file" id="file" name="filemou" />
                                    </div>
                                </div>
                        </div>
                         <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group form-float">
                                        <label class="form-label">Apakah softcopy draft MoA terlampir</label> <hr style="border-color: black; margin-top:-1px">
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <div class="form-group form-float">
                                       <div class="form-group">
                                    <input type="radio" name="moa" id="yes5" value="Y" class="with-gap">
                                    <label for="yes5">Yes</label>

                                    <input type="radio" name="moa" id="no5" value="N" class="with-gap">
                                    <label for="no5" class="m-l-20">No</label>
                                </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-float">
                                     <label for="male">Jika 'Ya' Lampirkan MOA </label>
                                        <input type="file" id="file" name="filemoa" />
                                    </div>
                                </div>
                                </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-link waves-effect">SUBMIT</button>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                    </div>






                    </form>
                        </div>
                    </div>


                </div>
                <!-- #END# Bar Chart -->
            </div>
        </div>
         <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content modal-col-red">
                    <div class="modal-header">
                        <h4 class="modal-title" id="smallModalLabel" >Apakah anda yakin untuk logout ?</h4>
                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">
                        <a href="<?php echo site_url('LoginCntrl/logoutProcess'); ?>"><button type="button" class="btn btn-link waves-effect">IYA</button></a>
                        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">TIDAK</button>
                    </div>
                </div>
            </div>
        </div>
        </section>
 <!-- Jquery Core Js -->
    <script src="<?php echo base_url('mou/plugins/jquery/jquery.min.js'); ?>"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap/js/bootstrap.js'); ?>"></script>
 

    <!-- Slimscroll Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-slimscroll/jquery.slimscroll.js'); ?>"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/node-waves/waves.js'); ?>"></script>

    <!-- Chart Plugins Js -->
    <script src="<?php echo base_url('mou/plugins/chartjs/Chart.bundle.js'); ?>"></script>

     <!-- Bootstrap Material Datetime Picker Plugin Js -->
     <script src="<?php echo base_url('mou/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js'); ?>"></script>

    <!-- Select Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/bootstrap-select/js/bootstrap-select.js'); ?>"></script>
    
    <!-- Jquery Validation Plugin Css -->
    <script src="<?php echo base_url('mou/plugins/jquery-validation/jquery.validate.js'); ?>"></script>
    
    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/sweetalert/sweetalert.min.js'); ?>"></script>

    <!-- Sweet Alert Plugin Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(document).ready(function(){

        $("#prodi1").hide();
        $("#prodi2").hide();
        $("#prodi3").hide();
        $('#prodi').prop('required',false);
        $('#prodi1').prop('required',false);
        $('#prodi2').prop('required',false);
        $('#prodi3').prop('required',false);

        $("#choicedeg").change(function(){
            x = document.getElementById("choicedeg").value;
            var id = x.options[x.selectedIndex].value;
            if (id==1) {
               $("#prodi1").show();
               $("#prodi").hide();
               $("#prodi2").hide();
               $("#prodi3").hide();
            }else if (id==2){
               $("#prodi2").show();
               $("#prodi1").hide();
               $("#prodi").hide();
               $("#prodi3").hide();
            }else if (id==3){
               $("#prodi3").show();
               $("#prodi").hide();
               $("#prodi2").hide();
               $("#prodi1").hide();
            }
        });
    });
    </script>
    <script>
    $(document).ready(function(){
    var check1=0; var id;var namaOld=""; var editCheck;

    $(".inf").click(function(){
        id = $(this).attr('id');
        $.ajax({
            url:'DashUser/getData/'+id,
            data:{send:true},
            success:function(data){
                $("#editbidang").val(data['bidang']);
                $("#editkategori").val(data['kategori']);
                if($('#editkategori').val('1')) {
                    $('#editkategori').prop('value','Perusahaan');
                }else if($('#kelas').val('2')){
                    $('#editkategori').prop('value','Perguruan Tinggi');
                }else if($('#kelas').val('3')){
                    $('#editkategori').prop('value','Perbankan');
                }else if($('#kelas').val('4')){
                    $('#editkategori').prop('value','Luar Negeri');
                }else if($('#kelas').val('5')){
                    $('#editkategori').prop('value','Lainnya');
                };
                $("#editjudul").val(data['judul']);
                $("#editmanfaatm").val(data['manfaat_m']);
                $("#editmanfaatu").val(data['manfaat_u']);
                $("#editnegara").val(data['negara']);
                $("#editperiode").val(data['periode']);
                $("#editusul").val(data['tgl_usul']);
                $("#editsls").val(data['tgl_lama']);
                $("#editberkas1").click(function(){
                    file = (data['berkas1']);
                    bid = (data['bidang']);
                    window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                });
                $("#editberkas2").click(function(){
                    file = (data['berkas2']);
                    bid = (data['bidang']);
                    window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                });
                $("#editberkas3").click(function(){
                    file = (data['berkas3']);
                    bid = (data['bidang']);
                    window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                });
                $("#editberkas4").click(function(){
                    file = (data['berkas4']);
                    bid = (data['bidang']);
                    window.open('DashUser/lihatFile/'+'<?php echo ($this->session->userdata("nama"));?>'+'/'+bid+'/'+file+'/pdf','_blank');
                });
            }
        });
    });

    $(".edit").click(function(){
        id = $(this).attr('id');
        $.ajax({
            url:'DashUser/getData/'+id,
            data:{send:true},
            success:function(data){
                $("#editbidang1").val(data['bidang']);
                $("#editkategori1").val(data['kategori']);
                $("#editjudul1").val(data['judul']);
                $("#editmanfaatm1").val(data['manfaat_m']);
                $("#editmanfaatu1").val(data['manfaat_u']);
                $("#editnegara1").val(data['negara']);
                $("#editperiode1").val(data['periode']);
                $("#editfile11").val(data['berkas1']);
                $("#editfile21").val(data['berkas2']);
                $("#editfile31").val(data['berkas3']);
                $("#editfile41").val(data['berkas4']);
            }
        });
    });

    $("#editform").submit(function(e){
        e.preventDefault();
        var formData = new FormData($(this)[0]);
        $.ajax({
            url:'DashUser/update/'+id,
            data:formData,
            type:'POST',
            contentType: false,
            processData: false,
            success:function(data){
                window.location.reload(true);
            }
        });
  
    });

    $(".del").click(function(){
        id = $(this).attr('id');
    });

    $("#confirm-del").click(function(){
        $.ajax({
            url:'DashUser/delete/'+id,
            data:{send:true},
            success:function(data){
                window.location.reload(true);
            }
        })
    });

});

    </script>

    <!-- Jquery DataTable Plugin Js -->
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/jquery.dataTables.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.flash.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/jszip.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/pdfmake.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/vfs_fonts.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.html5.min.js');?>"></script>
    <script src="<?php echo base_url('mou/plugins/jquery-datatable/extensions/export/buttons.print.min.js');?>"></script>

    <!-- Custom Js -->
    <script src="<?php echo base_url('mou/js/admin.js'); ?>"></script>
    <script>
    $(function () {
        $('#add').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "DashUser/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "DashUser/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "DashUser/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "DashUser/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
        
        $('#editform').validate({
            rules: {
                'file1': {
                     remote: {
                        url: "DashUser/cekFile1",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file1" ).val();
                          }
                        }
                    }
                },
                'file2': {
                     remote: {
                        url: "DashUser/cekFile2",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file2" ).val();
                          }
                        }
                    }
                },
                'file3': {
                     remote: {
                        url: "DashUser/cekFile3",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file3" ).val();
                          }
                        }
                    }
                },
                'file4': {
                     remote: {
                        url: "DashUser/cekFile4",
                        type: "post",
                        data: {
                          email: function() {
                            return $( "#file4" ).val();
                          }
                        }
                    }
                }
            },
            messages:{
                'bidang': {
                    required: 'Bidang tidak boleh kosong.'
                },
                'kategori':{
                    required: 'Kategori tidak boleh kosong.'
                },
                'negara':{
                    required: 'Negara tidak boleh kosong.'
                },
                'periode':{
                    required: 'Periode tidak boleh kosong.'
                },
                'file1':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file2':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file3':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                },
                'file4':{
                    remote: 'Format file harus PDF',
                    required: 'Berkas tidak boleh kosong.'
                }

            },
            highlight: function (input) {
                console.log(input);
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.input-group').append(error);
                $(element).parents('.form-group').append(error);
            }
        });
    });
    </script>
    <script>
    $(function () {
        $('.js-basic-example').DataTable({
            responsive: true
        });

        //Exportable table
        $('.js-exportable').DataTable({
            ordering: false,
            paging: false,
            responsive: true,
            dom: 'Bfrtip',
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-exportable1').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-terima').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-sync').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-clear').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
        $('.js-all').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'print', 'excel', 'pdf', 'csv'
            ]
        });
    });
    </script>
    <script>
        $(function () {
            new Chart(document.getElementById("bar_chart").getContext("2d"), getChartJs('bar'));
            new Chart(document.getElementById("radar_chart").getContext("2d"), getChartJs('radar'));
            new Chart(document.getElementById("pie_chart").getContext("2d"), getChartJs('pie'));
        });

        function getChartJs(type) {
            var config = null;

            if (type === 'bar') {
                config = {
                    type: 'bar',
                    data: {
                        labels: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
                        datasets: [{
                            label: "Perusahaan",
                            data: [65, 59, 80, 81, 56, 55, 40, 80, 81, 56, 55, 40],
                            backgroundColor: 'rgb(233, 30, 99)'
                        }, {
                            label: "Perguruan Tinggi",
                            data: [28, 48, 40, 19, 86, 27, 80, 40, 19, 86, 27, 80],
                            backgroundColor: 'rgb(0, 188, 212)'
                        }, {
                            label: "Perbankan",
                            data: [11, 54, 30, 29, 46, 57, 20, 30, 29, 46, 57, 20],
                            backgroundColor: 'rgb(0, 150, 136)'
                        }, {
                            label: "Luar Negeri",
                            data: [18, 28, 50, 49, 56, 37, 50, 50, 49, 56, 37, 50],
                            backgroundColor: 'rgba(0, 188, 212, 0.8)'
                        }, {
                            label: "Lainnya",
                            data: [38, 58, 30, 59, 36, 17, 70, 30, 59, 36, 17, 70],
                            backgroundColor: 'rgba(233, 30, 99, 0.8)'
                        }]
                    },
                    options: {

                        responsive: true,
                        legend: {
                          display: true,
                          position: 'top',
                          labels: {
                            fontColor: "black",
                          }
                        }
                    }
                }
            }
            else if (type === 'radar') {
                config = {
                    type: 'radar',
                    data: {
                        labels: ["January", "February", "March", "April", "May", "June", "July"],
                        datasets: [{
                            label: "My First dataset",
                            data: [65, 25, 90, 81, 56, 55, 40],
                            borderColor: 'rgba(0, 188, 212, 0.8)',
                            backgroundColor: 'rgba(0, 188, 212, 0.5)',
                            pointBorderColor: 'rgba(0, 188, 212, 0)',
                            pointBackgroundColor: 'rgba(0, 188, 212, 0.8)',
                            pointBorderWidth: 1
                        }, {
                                label: "My Second dataset",
                                data: [72, 48, 40, 19, 96, 27, 100],
                                borderColor: 'rgba(233, 30, 99, 0.8)',
                                backgroundColor: 'rgba(233, 30, 99, 0.5)',
                                pointBorderColor: 'rgba(233, 30, 99, 0)',
                                pointBackgroundColor: 'rgba(233, 30, 99, 0.8)',
                                pointBorderWidth: 1
                            }]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            else if (type === 'pie') {
                config = {
                    type: 'pie',
                    data: {
                        datasets: [{
                            data: [225, 50, 100, 40],
                            backgroundColor: [
                                "rgb(233, 30, 99)",
                                "rgb(255, 193, 7)",
                                "rgb(0, 188, 212)",
                                "rgb(139, 195, 74)"
                            ],
                        }],
                        labels: [
                            "Pink",
                            "Amber",
                            "Cyan",
                            "Light Green"
                        ]
                    },
                    options: {
                        responsive: true,
                        legend: false
                    }
                }
            }
            return config;
        }
    </script>
        <!-- Demo Js -->
    <script src="<?php echo base_url('mou/js/demo.js'); ?>"></script>
</body>

</html>