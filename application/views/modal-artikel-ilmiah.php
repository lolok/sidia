<div id="addArtikelIlmiahModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('ArtikelIlmiahCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">NAMA DOSEN:</label>
                            <select type="text" class="form-control select2" id="dosen" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="dosen" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="judul_artikel" class="control-label">JUDUL ARTIKEL:</label>
                            <input type="text" class="form-control" id="judul_artikel" name="judul_artikel" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama_jurnal" class="control-label">NAMA JURNAL:</label>
                            <input type="text" class="form-control" id="nama_jurnal" name="nama_jurnal" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="volume_nomor_tahun" class="control-label">VOLUME/NOMOR/TAHUN:</label>
                            <input type="text" class="form-control" id="volume_nomor_tahun" name="volume_nomor_tahun" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="kepanitiaan" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editArtikelIlmiahModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="edit_dosen" name="edit_dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="edit_dosen" name="edit_dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_judul_artikel" class="control-label">JUDUL ARTIKEL:</label>
                            <input type="text" class="form-control" id="edit_judul_artikel" name="edit_judul_artikel" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_nama_jurnal" class="control-label">NAMA JURNAL:</label>
                            <input type="text" class="form-control numeric add-satuan" id="edit_nama_jurnal" name="edit_nama_jurnal" required> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="edit_volume_nomor_tahun" class="control-label">VOLUME/NOMOR/TAHUN:</label>
                            <input type="text" class="form-control" id="edit_volume_nomor_tahun" name="edit_volume_nomor_tahun" required> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edit_tahun" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edit_tahun" name="edit_tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="edit_semester" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="edit_semester" name="edit_semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_artikel_ilmiah">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>