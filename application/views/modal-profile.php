<div id="editMatkulModal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Web:</label>
                            <input type="text" class="form-control select2" id="editnamaweb" name="editnamaweb" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Email:</label>
                            <input type="text" class="form-control select2" id="editemail" name="editemail" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nomor Kontak:</label>
                            <input type="text" class="form-control select2" id="editnohp" name="editnohp" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Alamat:</label>
                            <input type="text" class="form-control select2" id="editalamat" name="editalamat" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <label for="basic-url">facebook URL</label>
                            <div class="input-group m-b-30">
                                <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                                <input type="text" class="form-control" id="editlink_fb" name="editlink_fb" aria-describedby="basic-addon3">
                            </div>
                    </div>
                    <div class="col-md-6">
                        <label for="basic-url">Instagram URL</label>
                            <div class="input-group m-b-30">
                                <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                                <input type="text" class="form-control" id="editlink_ig" name="editlink_ig" aria-describedby="basic-addon3">
                            </div>
                    </div>
                    <div class="col-md-6">
                        <label for="basic-url">twitter URL</label>
                            <div class="input-group m-b-30">
                                <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                                <input type="text" class="form-control" id="editlink_twitter" name="editlink_twitter" aria-describedby="basic-addon3">
                            </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                        <label for="basic-url">LinkedIn URL</label>
                            <div class="input-group m-b-30">
                                <span class="input-group-addon" id="basic-addon3">https://example.com/users/</span>
                                <input type="text" class="form-control" id="editlink_linkedin" name="editlink_linkedin" aria-describedby="basic-addon3">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Deskripsi Singkat:</label>
                            <textarea class="form-control select2" id="editdesc" name="editdesc" required rows="5"></textarea>
                            </input>  
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_profile">
                
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect waves-light">Simpan</button>
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoMatkulModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nama Web:</label>
                            <input type="text" class="form-control select2" id="editnamaweb" name="editnamaweb" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Email:</label>
                            <input type="text" class="form-control select2" id="editemail" name="editemail" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Nomor Kontak:</label>
                            <input type="text" class="form-control select2" id="editnohp" name="editnohp" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">Alamat:</label>
                            <input type="text" class="form-control select2" id="editalamat" name="editalamat" required>
                            </input>  
                        </div>
                    </div>
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Link facebook:</label>
                            <input type="text" class="form-control select2" id="editlink_fb" name="editlink_fb" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Link Instagram:</label>
                            <input type="text" class="form-control select2" id="editlink_ig" name="editlink_ig" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Link twitter:</label>
                            <input type="text" class="form-control select2" id="editlink_twitter" name="editlink_twitter" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Link LinkedIn:</label>
                            <input type="text" class="form-control select2" id="editlink_linkedin" name="editlink_linkedin" required>
                            </input>  
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Deskripsi Singkat:</label>
                            <input type="text" class="form-control select2" id="editdesc" name="editdesc" required>
                            </input>  
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_profile">
                </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>