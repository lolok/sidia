<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('temp/plugins/images/undip.png') ?>">
    <title><?= $title ?></title>
    <!-- Bootstrap Core CSS -->
    <link href="<?= base_url('temp/asset/bootstrap/dist/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- Menu CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css') ?>" rel="stylesheet">
    <!-- toast CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/toast-master/css/jquery.toast.css') ?>" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/morrisjs/morris.css') ?>" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css') ?>" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css') ?>" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css')?>" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') ?>" rel="stylesheet">
    <!-- Calendar CSS -->
    <link href="<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.css') ?>" rel="stylesheet" />
    <!-- animation CSS -->
    <link href="<?= base_url('temp/asset/css/animate.css') ?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?= base_url('temp/asset/css/style.css') ?>" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?= base_url('temp/asset/css/colors/blue.css') ?>" id="theme" rel="stylesheet">
    <style type="text/css">
        .clockpicker-popover {
            z-index: 999999 !important;
        };
    </style>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title"><?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row row-in">
                                <div class="col-lg-4 col-sm-12 row-in-br">
                                    <ul class="col-in ">
                                        <li>
                                            <span class="circle circle-md bg-info m-t-15"><i class="ti-clipboard"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h4 class="pull-right">Total Kegiatan</h4>
                                            <h3 class="counter text-right m-t-15">-</h3>
                                            
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-sm-12 row-in-br  b-r-none">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-danger m-t-15"><i class="ti-shopping-cart"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h4 class="pull-right">Total Pengeluaran</h4>
                                            <h3 class="counter text-right m-t-15">-</h3>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-lg-4 col-sm-12 b-0">
                                    <ul class="col-in">
                                        <li>
                                            <span class="circle circle-md bg-success m-t-15"><i class=" ti-wallet"></i></span>
                                        </li>
                                        <li class="col-last">
                                            <h4 class="pull-right">Total Saldo</h4>
                                            <h3 class="counter text-right m-t-15">-</h3>
                                            
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-4">
                                    <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px">Daftar Kegiatan </h3>
                                </div>
                                <div class="col-md-8">
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addKegiatanModal"><span class="circle circle-sm bg-success di"><i class="ti-plus"></i></span><span> &nbsp Tambah Kegiatan</span></a>
                                </div>
                            </div>
                            <hr>
                            <div class="table-responsive">
                                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>Nama Kegiatan</th>
                                            <th>Kategori</th>
                                            <th>Tanggal</th>
                                            <th>Jam</th>
                                            <th>Lokasi</th>
                                            <th>Dasar Hukum</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no=0; 
                                        foreach ($kegiatan->result() as $value) {
                                        $no++;    ?>
                                        <tr>
                                            <td><?= $no ?></td>
                                            <td><?= $value->nama ?></td>
                                            <td><?= $value->kategori ?></td>
                                            <td><?= $value->tanggal ?></td>
                                            <td><?= $value->jam ?></td>
                                            <td><?= $value->lokasi ?></td>
                                            <td><?= $value->dasar_hukum ?></td>
                                            <td>
                                                <a href="#" class="edit" data-toggle="modal" data-target="#editKegiatanModal" id="<?=htmlspecialchars($value->id_kegiatan); ?>"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Edit Kegiatan"><i class="ti-pencil-alt"></i></button></a>
                                                <a href="#" class="hapus" data-toggle="modal" data-target=".hapusKegiatanModal" id="<?=htmlspecialchars($value->id_kegiatan); ?>"><button type="button" class="btn btn-danger btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Hapus Kegiatan"><i class="icon-trash"></i></button></a>
                                                <a href="<?= site_url('kegiatan/detail?id='.$value->id_kegiatan) ?>"><button type="button" class="btn btn-info btn-outline btn-circle btn-sm m-r-5" data-toggle="tooltip" title="Detail Kegiatan"><i class="ti-more" ></i></button></a>
                                            </td>
                                        </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <div class="modal fade hapusKegiatanModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h5 class="modal-title">Hapus Kegiatan</h5> </div>
                            <div class="modal-body">
                                <h4>Apakah anda yakin untuk menghapus kegiatan ?</h4>
                                <small style="color: red">semua data kegiatan akan ikut terhapus</small>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                <a type="button" class="btn btn-danger waves-effect waves-light" id="hapusKeg">Hapus</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="editKegiatanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Edit Kegiatan</h4> </div>
                            <div class="modal-body">
                                <form id="editform" action="#" method="POST" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editnama" class="control-label">Nama Kegiatan:</label>
                                            <input type="text" class="form-control" id="editnama" name="editnama" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editkegiatan" class="control-label">Jenis Kegiatan:</label>
                                            <select type="text" class="form-control" id="editkegiatan" name="editkegiatan" required>
                                                <option disabled>Pilih Kategori Kegiatan</option>
                                                <?php foreach ($kat_kegiatan->result() as $value) {?>
                                                <option value="<?= $value->id_katkeg ?>"><?= $value->kategori ?></option>
                                                <?php }?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="edittanggal" class="control-label">Tanggal:</label>
                                            <input type="text" class="form-control editdatepicker" id="edittanggal" name="edittanggal" onkeydown="return false" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editjam" class="control-label">Jam:</label>
                                            <input type="text" class="form-control editclockpicker" id="editjam" name="editjam" onkeydown="return false" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editlokasi" class="control-label">Lokasi:</label>
                                            <input type="text" class="form-control" id="editlokasi" name="editlokasi" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editdashuk" class="control-label">Dasar Hukum:</label>
                                            <input type="text" class="form-control" id="editdashuk" name="editdashuk" required> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="addKegiatanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h4 class="modal-title">Tambah Kegiatan</h4> </div>
                            <div class="modal-body">
                                <form action="<?= site_url('KegCntrl/tambahKegiatan') ?>" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Nama Kegiatan:</label>
                                            <input type="text" class="form-control" id="nama" name="nama" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nama" class="control-label">Jenis Kegiatan:</label>
                                            <select type="text" class="form-control" id="kegiatan" name="kegiatan" required>
                                                <option value="">Pilih Kategori Kegiatan</option>
                                                <?php foreach ($kat_kegiatan->result() as $value) {?>
                                                <option value="<?= $value->id_katkeg ?>"><?= $value->kategori ?></option>
                                                <?php }?>
                                            </select> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tanggal" class="control-label">Tanggal:</label>
                                            <input type="text" class="form-control" id="datepicker" name="tanggal" onkeydown="return false" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="editjam" class="control-label">Jam:</label>
                                            <input type="text" class="form-control clockpicker" id="jam" name="jam" onkeydown="return false" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nohp" class="control-label">Lokasi:</label>
                                            <input type="text" class="form-control" id="lokasi" name="lokasi" required> 
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="nohp" class="control-label">Dasar Hukum:</label>
                                            <input type="text" class="form-control" id="dashuk" name="dashuk" required> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery/dist/jquery.min.js') ?>"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?= base_url('temp/asset/bootstrap/dist/js/bootstrap.min.js') ?>"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') ?>"></script>
    <!--slimscroll JavaScript -->
    <script src="<?= base_url('temp/asset/js/jquery.slimscroll.js') ?>"></script>
    <!--Wave Effects -->
    <script src="<?= base_url('temp/asset/js/waves.js') ?>"></script>
    <!--Counter js -->
    <script src="<?= base_url('temp/plugins/bower_components/waypoints/lib/jquery.waypoints.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/counterup/jquery.counterup.min.js') ?>"></script>
    <!--Morris JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/raphael/raphael-min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/morrisjs/morris.js') ?>"></script>
    <!-- chartist chart -->
    <script src="<?= base_url('temp/plugins/bower_components/chartist-js/dist/chartist.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') ?>"></script>
    <!-- Calendar JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <script src='<?= base_url('temp/plugins/bower_components/calendar/dist/fullcalendar.min.js') ?>'></script>
    <script src="<?= base_url('temp/plugins/bower_components/calendar/dist/cal-init.js') ?>"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?= base_url('temp/asset/js/custom.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/datatables/jquery.dataTables.min.js')?>"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script><!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/moment/moment.js') ?>"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/clockpicker/dist/bootstrap-clockpicker.js') ?>"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js') ?>"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') ?>"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?= base_url('temp/plugins/bower_components/timepicker/bootstrap-timepicker.min.js') ?>"></script>
    <script src="<?= base_url('temp/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') ?>"></script>
    <!-- Datatable -->
    <script type="text/javascript">
    $(document).ready(function() {
        $('#kegiatanForm').css('display','none');
        $('#myTable').DataTable();
        $('#example23').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
        jQuery('#datepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        jQuery('.editdatepicker').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        $('.clockpicker').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $('#editjam').clockpicker({
            donetext: 'Done',
            twelvehour: false,
        }).find('input').change(function() {
            console.log(this.value);
        });
        $(".edit").click(function(){
            id = $(this).attr('id');
            $.ajax({
                url:'KegCntrl/getData/'+id,
                data:{send:true},
                success:function(data){
                    $("#editnama").val(data['nama']);
                    $("#edittanggal").val(data['tanggal']);
                    $("#editjam").val(data['jam']);
                    $("#editlokasi").val(data['lokasi']);
                    $("#editdashuk").val(data['dashuk']);
                    $("#editkegiatan").val(data['katkeg']);
                }
            });
        });

        $("#editform").submit(function(e){
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url:'KegCntrl/update/'+id,
                data:formData,
                type:'POST',
                contentType: false,
                processData: false,
                success:function(data){
                    window.location.reload(true);
                }
            });
        });

        $(".hapus").click(function(){
            id = $(this).attr('id');
        });
                                        

        $("#hapusKeg").click(function(){
            $.ajax({
                url:'KegCntrl/delete/'+id,
                data:{send:true},
                success:function(data){
                    window.location.reload(true);
                }
            });
        });
    });
    </script>
</body>

</html>
