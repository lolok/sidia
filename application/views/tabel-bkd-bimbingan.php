<div class="table-responsive" id="div-atk">
    <table id="tabel-data-bimbingan" class="display nowrap" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th rowspan="2" class="text-center">KINERJA</th>
                <th rowspan="2" class="text-center">STATUS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $i = 0;
                foreach($tabel->result() as $value){ 
                $i++;
                if ($value->semester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
                if ($value->kategori == 1){
                    $kategori = 'Membimbing';
                }else{
                    $kategori = 'Menguji';
                }
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-center"><?= $value->nama ?></td>
                <td class="text-center"><?= $value->judul ?></td>
                <td class="text-center"><?= $value->nama_mhs ?></td>
                <td class="text-center"><?= $semester ?>-<?= $value->tahun ?></td>
                <td class="text-center"><?= $kategori ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_bimbingan" status="1" data-title="Status E-BKD" data-id="<?= $value->id_pembimbingan ?>" kategori="2" data-target=".activeModal" data-toggle="modal"><span class="label label-danger label-rouded" style="">Tidak Aktif</span></a>
                    <?php }else{?>
                        <a href="" class="dotip aktif_bimbingan" status="0" data-title="Status E-BKD" data-id="<?= $value->id_pembimbingan ?>" kategori="2" data-target=".deactiveModal" data-toggle="modal"><span class="label label-success label-rouded">Aktif</span></a>
                    <?php }?> 
                </td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editjudul").val(data['judul']);
                form.find("#editkategori").select2('val',data['kategori']);
                form.find("#editnim").val(data['nim']);
                form.find("#editnama").val(data['nama']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#idpembimbingan").val(data['id_pembimbingan']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_bimbingan').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
        kategori = $(this).attr('kategori');
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getFileTable') ?>',
            data:{
                id:id,
                kategori:kategori 
            },
            success:function(data){
                $('#file-pengajaran').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infojudul").val(data['judul']);
                form.find("#infokategori").select2('val',data['kategori']);
                form.find("#infonim").val(data['nim']);
                form.find("#infonama").val(data['nama']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data-bimbingan').DataTable({  
        ordering: false 
    });
    $(".select2").select2();
</script>