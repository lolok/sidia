<div id="addRiwayatnfModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> 
            </div>
            <div class="modal-body">
                <form id="add-datanf" action="<?= site_url('RiwayatCntrl/addDatanf')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA KEGIATAN:</label>
                            <input type="text" class="form-control" name="kegiatan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PENYELENGGARA:</label>
                            <input type="text" class="form-control" name="penyelenggara" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT:</label>
                            <input type="text" class="form-control" name="tempat" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">WAKTU:</label>
                            <input type="text" class="form-control" name="waktu" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editRiwayatnfModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-datanf" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">Tanggal:</label>
                            <input type="text" class="form-control atkpicker" id="tanggal_atk" name="tanggal_atk" onkeydown="return false" required> 
                        </div>
                    </div> -->
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="dosen" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="edit_dosen" name="edit_dosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA KEGIATAN:</label>
                            <input type="text" class="form-control" id="edit_nama_kegiatan" name="edit_kegiatan" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">PENYELENGGARA:</label>
                            <input type="text" class="form-control" id="edit_penyelenggara" name="edit_penyelenggara" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT:</label>
                            <input type="text" class="form-control" id="edit_tempat" name="edit_tempat" required>
                        </div>
                    </div>
					<div class="col-md-12">
                        <div class="form-group">
                            <label for="nama" class="control-label">WAKTU:</label>
                            <input type="text" class="form-control" id="edit_waktu" name="edit_waktu" required>
                        </div>
                    </div>
                    <input type="hidden" name="id" id="id_riwayatnf">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>