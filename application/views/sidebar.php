<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Menu</span></h3> </div>
        <ul class="nav" id="side-menu">
            <li class="user-pro">
                <a href="javascript:void(0);" class="waves-effect"><img src="<?= base_url('temp/plugins/images/users/default.jpg') ?>" alt="user-img" class="img-circle"> <span class="hide-menu"> <?= strtoupper($this->session->userdata('nama')) ?></span>
                </a>
            </li>
            <li> <a href="<?= site_url('dashboard') ?>" class="waves-effect"><i class="mdi mdi-home fa-fw" data-icon="v"></i> <span class="hide-menu"> Home</span></a>
            </li>
            <li class="devider"></li>
            <li> <a href="<?= site_url('administrasi') ?>" class="waves-effect"><i class="fa fa-archive fa-fw" data-icon="v"></i> <span class="hide-menu"> Administrasi<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li style="display: none"> <a href="<?= site_url('administrasi') ?>" class="waves-effect"><i class="fa fa-book fa-fw" data-icon="v"></i> <span class="hide-menu"> Menu</span></a> </li>
                    <li> <a href="<?= site_url('administrasi/dosen') ?>" class="waves-effect"><i class="fa fa-users fa-fw" data-icon="v"></i> <span class="hide-menu"> Daftar Dosen</span></a> </li>
                    <li> <a href="<?= site_url('administrasi/matakuliah') ?>" class="waves-effect"><i class="fa fa-book fa-fw" data-icon="v"></i> <span class="hide-menu"> Daftar Mata Kuliah</span></a> </li>
                    <!-- <li> <a href="<?= site_url('administrasi/ruang') ?>" class="waves-effect"><i class="fa fa-list fa-fw" data-icon="v"></i> <span class="hide-menu"> Daftar Ruang</span></a> </li>
                    <li> <a href="<?= site_url('administrasi/kelas') ?>" class="waves-effect"><i class="fa fa-calendar fa-fw" data-icon="v"></i> <span class="hide-menu"> Jadwal Kelas</span></a> </li> -->
                </ul>
            </li>
            <li class="devider"></li>
            <li> <a href="<?= site_url('data') ?>" class="waves-effect"><i class="fa fa-scribd fa-fw" data-icon="v"></i> <span class="hide-menu"> Pendataan SIDIA<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li style="display: none"> <a href="<?= site_url('data') ?>" class="waves-effect"><i class="fa fa-copy fa-fw" data-icon="v"></i> <span class="hide-menu"> Menu</span></a></li>
                    <li> <a href="<?= site_url('data/kerjasama') ?>" class="waves-effect"><i class="fa fa-copy fa-fw" data-icon="v"></i> <span class="hide-menu"> Data Kerjasama</span></a>
                    </li>
                    <li> <a href="<?= site_url('data/berkas') ?>" class="waves-effect"><i class="fa fa-archive fa-fw" data-icon="v"></i> <span class="hide-menu"> Berkas</span></a>
                    </li>
                    <li> <a href="<?= site_url('data/tridarma') ?>" class="waves-effect"><i class="fa fa-graduation-cap fa-fw" data-icon="v"></i> <span class="hide-menu"> Tri Dharma PT <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-third-level">
                            <li style="display: none"> <a href="<?= site_url('data/tridarma') ?>"><i class="fa-fw">-</i><span class="hide-menu">Pengajaran</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/pengajaran') ?>"><i class="fa-fw">-</i><span class="hide-menu">Pengajaran</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/pembimbingan') ?>"><i class="fa-fw">-</i><span class="hide-menu">Pembimbingan</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/penelitian') ?>"><i class="fa-fw">-</i><span class="hide-menu">Penelitian</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/pkm') ?>"><i class="fa-fw">-</i><span class="hide-menu">PKM</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/penunjang') ?>"><i class="fa-fw">-</i><span class="hide-menu">Penunjang</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/publikasi') ?>"><i class="fa-fw">-</i><span class="hide-menu">Publikasi</span></a> </li>
                            <li> <a href="<?= site_url('data/tridarma/kegiatan') ?>"><i class="fa-fw">-</i><span class="hide-menu">Kegiatan</span></a> </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="devider"></li>
            <li> <a href="<?= site_url('cv') ?>" class="waves-effect"><i class="fa fa-file-text fa-fw" data-icon="v"></i> <span class="hide-menu"> Pendataan CV<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li style="display: none"> <a href="<?= site_url('cv') ?>" class="waves-effect"><i class="fa fa-copy fa-fw" data-icon="v"></i> <span class="hide-menu"> Menu</span></a></li>
                    <li> <a href="<?= site_url('cv/datadiri') ?>" class="waves-effect"><i class=" fa-fw" data-icon="v">-</i> <span class="hide-menu"> Data Pribadi</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/riwayat') ?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Riwayat Pendidikan</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/artikel-ilmiah') ?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Publikasi Artikel Ilmiah</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/publikasi-seminar') ?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Publikasi Seminar</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/karya-buku') ?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Karya Buku</span></a>
                    </li>
                    <li> <a href="#" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Perolehan HKI</span></a>
                    </li>
                    <li> <a href="#" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Kebijakan Publik</span></a>
                    </li>
                    <li> <a href="#" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Penghargaan</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/publikasilain')?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Publikasi Lainnya</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/narasumber')?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Narasumber</span></a>
                    </li>
                    <li> <a href="<?= site_url('cv/kegiatanlain')?>" class="waves-effect"><i class="fa-fw" data-icon="v">-</i> <span class="hide-menu"> Kegiatan Profesi Lainnya</span></a>
                    </li>
                </ul>
            </li>
            <li class="devider"></li>
                        <li> <a href="<?= site_url('web') ?>" class="waves-effect"><i class="fa fa-edge fa-fw" data-icon="v"></i> <span class="hide-menu"> Pendataan WEB<span class="fa arrow"></span></span></a>
                <ul class="nav nav-second-level">
                    <li style="display: none"> <a href="<?= site_url('data') ?>" class="waves-effect"><i class="fa fa-copy fa-fw" data-icon="v"></i> <span class="hide-menu"> Menu</span></a></li>
                    <li> <a href="<?= site_url('web/profile') ?>" class="waves-effect"><i class=" fa-fw" data-icon="v">-</i> <span class="hide-menu"> Web Profile</span></a>
                    </li>
                    <li> <a href="<?= site_url('web/publikasi') ?>" class="waves-effect"><i class="fa fa-user fa-fw" data-icon="v"></i> <span class="hide-menu"> Publikasi</span></a>
                    </li>
                    <li> <a href="<?= site_url('info') ?>" class="waves-effect"><i class="fa fa-graduation-cap fa-fw" data-icon="v"></i> <span class="hide-menu"> Additional Info <span class="fa arrow"></span></span></a>
                        <ul class="nav nav-third-level">
                            <li style="display: none"> <a href="<?= site_url('info/index') ?>"><i class="fa-fw">-</i><span class="hide-menu"></span></a> </li>
                            <li> <a href="<?= site_url('info/ongoing') ?>"><i class="fa-fw">-</i><span class="hide-menu">On Going Penelitian</span></a> </li>
                            <li> <a href="<?= site_url('gallery') ?>"><i class="fa-fw">-</i><span class="hide-menu">Gallery</span></a> </li>
                            <li> <a href="<?= site_url('info/team') ?>"><i class="fa-fw">-</i><span class="hide-menu">Our Team</span></a> </li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li class="devider"></li>
            <li> <a href="#" class="waves-effect"><i class="fa fa-file-text fa-fw" data-icon="v"></i> <span class="hide-menu"> SKP</span></a></li>
            <li class="devider"></li>
            <li> <a href="<?= site_url('ebkd') ?>" class="waves-effect"><i class="fa fa-file-text fa-fw" data-icon="v"></i> <span class="hide-menu"> E-BKD</span></a>
            </li>
            <li class="devider"></li>
            <li><a href="#" data-toggle="modal" data-target=".logoutModal" class="waves-effect"><i class="mdi mdi-logout fa-fw"></i> <span class="hide-menu">Log out</span></a></li>
            <li class="devider"></li>
        </ul>
    </div>
</div>
<div class="modal fade logoutModal" tabindex="-1" role="dialog" aria-labelledby="addOrder" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <h5>Apakah anda yakin untuk logout ?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <a type="button" class="btn btn-danger waves-effect waves-light" href="<?= site_url('logout') ?>">Logout</a>
            </div>
        </div>
    </div>
</div>