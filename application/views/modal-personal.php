<div id="addPersonalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">TAMBAH <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="add-data" action="<?= site_url('PersonalCntrl/addData')?>" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="" class="control-label">NAMA PERSONAL:</label>
                            <input type="text" class="form-control select2" id="" name="nama" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOSERTIF:</label>
                            <input type="text" class="form-control select2" id="" name="nosertif" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERTING:</label>
                            <input type="text" class="form-control " id="" name="perting" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="" name="status" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="" name="alamat" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="" name="jurusan" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PRODI:</label>
                            <input type="text" class="form-control" id="" name="prodi" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN:</label>
                            <input type="text" class="form-control" id="" name="jabatan" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT:</label>
                            <input type="text" class="form-control" id="" name="tempat" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL:</label>
                            <input type="text" class="form-control" id="datepicker" name="tanggal" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="" name="s1" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="" name="s2" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="" name="s3" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU:</label>
                            <input type="text" class="form-control" id="" name="ilmu" required>
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. HP:</label>
                            <input type="text" class="form-control" id="" name="nohape" required>
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="" name="dosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric " id="" name="dosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control " id="" name="tahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control " id="" name="semester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>BERKAS:</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="berkasdosen[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Tambah</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="editPersonalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">EDIT <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="edit-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA PERSONAL:</label>
                            <input type="text" class="form-control" id="editnama" name="editnama_personal" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOSERTIF:</label>
                            <input type="text" class="form-control" id="editnosertif" name="editnosertif" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERTING:</label>
                            <input type="text" class="form-control" id="editperting" name="editperting" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="editstatus" name="editstatus" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="editalamat" name="editalamat" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="editjurusan" name="editjurusan" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PRODI:</label>
                            <input type="text" class="form-control" id="editprodi" name="editprodi" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JABATAN:</label>
                            <input type="text" class="form-control" id="editjabatan" name="editjabatan" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT:</label>
                            <input type="text" class="form-control" id="edittempat" name="edittempat" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TANGGAL:</label>
                            <input type="text" class="form-control" id="edittanggal" name="edittanggal" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="edits1" name="edits1" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="edits2" name="edits2" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="edits3" name="edits3" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU:</label>
                            <input type="text" class="form-control" id="editilmu" name="editilmu" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. HP:</label>
                            <input type="text" class="form-control" id="editnohape" name="editnohape" required> 
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="editdosen" name="editdosen" required>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control select2" id="editdosen" name="editdosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="edittahun" name="edittahun" required>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="editsemester" name="editsemester" required>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                    <input type="hidden" name="id" id="idpersonal">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-success waves-effect waves-light">Edit</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="infoPersonalModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">INFO <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="info-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ID PERSONAL:</label>
                            <input type="text" class="form-control" id="idpersonal" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NAMA:</label>
                            <input type="text" class="form-control" id="infonama" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NOSERTIF:</label>
                            <input type="text" class="form-control" id="infonosertif" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PERTING:</label>
                            <input type="text" class="form-control" id="infoperting" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">STATUS:</label>
                            <input type="text" class="form-control" id="infostatus" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ALAMAT:</label>
                            <input type="text" class="form-control" id="infoalamat" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">JURUSAN:</label>
                            <input type="text" class="form-control" id="infojurusan" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">PRODI:</label>
                            <input type="text" class="form-control" id="infoprodi" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">TEMPAT:</label>
                            <input type="text" class="form-control" id="infotempat" required> 
                        </div>
                    </div>
					<div class="col-md-6">
							<div class="form-group">
								<label for="nama" class="control-label">TANGGAL:</label>
								<input type="text" class="form-control" id="infotanggal" required> 
							</div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S1:</label>
                            <input type="text" class="form-control" id="infos1" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S2:</label>
                            <input type="text" class="form-control" id="infos2" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">S3:</label>
                            <input type="text" class="form-control" id="infos3" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">ILMU:</label>
                            <input type="text" class="form-control" id="infoilmu" required> 
                        </div>
                    </div>
					<div class="col-md-6">
                        <div class="form-group">
                            <label for="nama" class="control-label">NO. HP:</label>
                            <input type="text" class="form-control" id="infonohape" required> 
                        </div>
                    </div>
                    <?php if($this->session->userdata('level')==1){ ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tanggal" class="control-label">DOSEN:</label>
                            <select type="text" class="form-control select2" id="infodosen" name="infodosen" disabled>
                                <?php foreach($dosen->result() as $val){ ?>
                                    <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                <?php } ?>
                            </select>  
                        </div>
                    </div>
                    <?php }else{ ?>
                        <input type="hidden" class="form-control numeric add-satuan" id="infodosen" name="infodosen" value="<?= $this->session->userdata('iduser') ?>"> 
                    <?php } ?>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">TAHUN:</label>
                            <select type="text" class="form-control" id="infotahun" name="infotahun" disabled>
                                <?php foreach($tahun as $val){ ?>
                                    <option value="<?= $val->year ?>"><?= $val->year ?></option>
                                <?php } ?>
                            </select> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="nohp" class="control-label">SEMESTER:</label>
                            <select type="text" class="form-control" id="infosemester" name="infosemester" disabled>
                                <option value="1">Ganjil</option>
                                <option value="2">Genap</option>
                            </select> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>
<div id="fileModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
                <h4 class="modal-title">FILE BERKAS <?= $title ?></h4> </div>
            <div class="modal-body">
                <form id="file-data" action="#" method="POST" enctype="multipart/form-data">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>FILE :</label>
                            <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                <div class="form-control" data-trigger="fileinput"> <i class="glyphicon glyphicon-file fileinput-exists"></i> <span class="fileinput-filename"></span></div> <span class="input-group-addon btn btn-default btn-file"> <span class="fileinput-new">Select file</span> <span class="fileinput-exists">Change</span>
                                <input type="file" name="fileberkas[]" multiple> </span> <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a> 
                                <input type="hidden" id="kategori_file" name="kategori">
                                <input type="hidden" id="id_file" name="id">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="file-personal">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-success waves-effect" >Simpan</button>
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">Tutup</button>
            </form>
            </div>
        </div>
    </div>
</div>