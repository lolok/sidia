<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="col-md-2">
                                        <i class="fa fa-graduation-cap" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center"><?= $title ?> </h3>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <!-- <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-4 pull-right">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortdosen" name="sortdosen" required>
                                                   <?php foreach($dosen->result() as $val){ ?>
                                                        <option value="<?= $val->id_user ?>"><?= $val->nama ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                    </form> -->
                                    <!-- <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addMatkulModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Pengajaran" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                                <div class="sttabs tabs-style-bar">
                                    <nav>
                                        <ul id="tab-data">
                                            <li data-id="#datariwayat"><a href="#riwayat" class=""><span>Riwayat Pendidikan Formal</span></a></li>
                                            <li data-id="#datariwayatnf"><a href="#riwayatnf" class=""><span>Riwayat Pendidikan Non Formal</span></a></li>
                                        </ul>
                                    </nav>
                            <hr>
                                    <div class="content-wrap">
                                        <section id="riwayat">
                                            <div class="row">
                                                <div class="col-md-12" style="margin-bottom: 20px">
                                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addRiwayatModal">
                                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Riwayat Pendidikan Formal" data-placement="bottom"><i class="ti-plus"></i></span>
                                                    </a>
                                                </div>
                                                <div class="col-md-12">
                                                    <div id="datariwayat">
                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                        <section id="riwayatnf">
											<div class="row">
												<div class="col-md-12" style="margin-bottom: 20px">
                                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addRiwayatnfModal">
                                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Riwayat Pendidikan Formal" data-placement="bottom"><i class="ti-plus"></i></span>
                                                    </a>
                                                </div>
											</div>
											<div class="col-md-12">
												<div id="datariwayatnf">
									
												</div>
											</div>
                                        </section>
                                    </div>
                                    <!-- /content -->
                                </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-hapus'); ?>
                <?php $this->load->view('modal-riwayat'); ?>
                <?php $this->load->view('modal-riwayatnf'); ?>
                 <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            var sortdosen = $('#sortdosen').val();
            getRiwayat('RiwayatCntrl/getTabel',sortdosen,'#datariwayat');
            getRiwayat('RiwayatCntrl/getTabelnf',sortdosen,'#datariwayatnf');
            //ajax sorting
            $('#sortdosen').on('change', function(){
                iddosen = $(this).val();
                getSortRiwayat('RiwayatCntrl/getTabel',iddosen, '#datariwayat');
				alert (iddosen);
                getSortRiwayat('RiwayatCntrl/getTabelnf',iddosen, '#datariwayatnf');
            });
            //select
            $('.select2').select2();
            //datepicker
            jQuery('#datepicker1').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#datepicker2').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //editdatepicker
            jQuery('#editdatepicker1').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            jQuery('#editdatepicker2').datepicker({
                autoclose: true,
                todayHighlight: true
            });
            //aktif bkd
            $('#statusButton').on('click', function(){
                statusval = $('#status').val();
                $.ajax({
                    url:'<?= site_url('EbkdCntrl/getStatusBkd') ?>',
                    data:{
                        id:id,
                        tabel:tabel,
                        idname:idname,
                        status:statusval 
                    },
                    success:function(data){
                        iddosen = $('#sortdosen').val();
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        if (kategori == 1) {
                            getBkd('EbkdCntrl/getPendidikan',sortdosen,'#datapendidikan');
                        }else if(kategori == 2){
                            getBkd('EbkdCntrl/getPenelitian',sortdosen,'#datapenelitian');
                        }
                        $('.statusModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            $('#deactiveButton').on('click', function(){
                if (kategori == 1) {
                    url = '<?= site_url('PengajaranCntrl/getStatusBkd') ?>';
                }else if(kategori == 2){
                    url = '<?= site_url('PembimbingCntrl/getStatusBkd') ?>';
                }else if(kategori == 3){
                    url = '<?= site_url('PembimbingCntrl/getStatusBkd') ?>';
                }
                $.ajax({
                    url:url,
                    data:{
                        id:id,
                        status:status 
                    },
                    success:function(data){
                        iddosen = $('#sortdosen').val();
                        tahun = $('#sorttahun').val();
                        semester = $('#sortsemester').val();
                        if (kategori == 1) {
                            getSortBkd('PengajaranCntrl/sortingBkd',iddosen, tahun, semester, '#datakuliah');
                        }else if(kategori == 2){
                            getSortBkd('PembimbingCntrl/sortingBkd?kategori=1',iddosen, tahun, semester, '#databimbingan');
                        }else if(kategori == 3){
                            getSortBkd('PembimbingCntrl/sortingBkd?kategori=2',iddosen, tahun, semester, '#datamenguji');
                        }
                        $('.deactiveModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
        })

    </script>
	
	<script type="text/javascript">
        $(document).ready(function() {
            getData('RiwayatCntrl/getTabel');
            //ajax add data
            $('#add-data').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('RiwayatCntrl/addData') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getDataf('RiwayatCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('MatkulCntrl/sorting', tahun, semester);
                        $('#addRiwayatModal').modal('hide');
                        notification._toast('Success','Input Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax edit data
            $('#edit-data').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('RiwayatCntrl/editData/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getDataf('RiwayatCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('#editRiwayatModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax hapus data
            $('#hapus-data').click(function(){
                $.ajax({
                    url:'<?= site_url('RiwayatCntrl/hapusData/') ?>',
                    data:{
                        id:id
                    },
                    success:function(data){
                        getDataf('RiwayatCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('MatkulCntrl/sorting', tahun, semester);
                        $('.hapusModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //tabel file
        })

    </script>
	
	<script type="text/javascript">
        $(document).ready(function() {
            getData('RiwayatCntrl/getTabelnf');
            //ajax add data
            $('#add-datanf').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('RiwayatCntrl/addDatanf') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getDatanf('RiwayatCntrl/getTabelnf');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('MatkulCntrl/sorting', tahun, semester);
                        $('#addRiwayatnfModal').modal('hide');
                        notification._toast('Success','Input Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax edit data
            $('#edit-datanf').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('RiwayatCntrl/editDatanf/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getDatanf('RiwayatCntrl/getTabelnf');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('#editRiwayatnfModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax hapus data
            $('#hapus-datanf').click(function(){
                $.ajax({
                    url:'<?= site_url('RiwayatCntrl/hapusDatanf/') ?>',
                    data:{
                        id:id
                    },
                    success:function(data){
                        getDatanf('RiwayatCntrl/getTabelnf');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('MatkulCntrl/sorting', tahun, semester);
                        $('.hapusModalnf').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //tabel file
        })

    </script>
	
	
</body>

</html>
