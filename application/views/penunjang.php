<!DOCTYPE html>
<html lang="en">

<head>
    <?php $this->load->view('headsidia') ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <?php $this->load->view('navbar'); ?>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php $this->load->view('sidebar'); ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">SIDIA | <?= $title ?></h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <h4 class="pull-right"><?= $date ?></h4>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="white-box">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="col-md-2">
                                        <i class="fa fa-graduation-cap" style="font-size: 50px;color: #3cd0cc"></i>
                                    </div>
                                    <div class="col-md-10">
                                        
                                        <h3 class="box-title" style="margin-top: 10px;margin-bottom: -10px;vertical-align: center"><?= $title ?> </h3>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <form id="add-honor" action="#" method="POST" enctype="multipart/form-data">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sorttahun" name="kepanitiaan" required>
                                                    <option value="all">Semua Tahun</option>
                                                    <?php foreach($tahun as $val){ ?>
                                                        <option value="<?= $val->year ?>">Tahun <?= $val->year ?></option>
                                                    <?php } ?>
                                                </select> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <select type="text" class="form-control" id="sortsemester" name="kepanitiaan" required>
                                                    <option value="all">Semua Semester</option>
                                                    <option value="1">Semester Ganjil</option>
                                                    <option value="2">Semester Genap</option>
                                                </select> 
                                            </div>
                                        </div>
                                    </form>
                                    <a class="pull-right" href="javascript:void(0)" data-toggle="modal" data-target="#addMatkulModal">
                                        <span class="circle circle-sm bg-success di" data-toggle="tooltip" title="Tambah Data Penunjang" data-placement="bottom"><i class="ti-plus"></i></span>
                                    </a><!-- 
                                    <a class="pull-right" href="javascript:void(0)" style="margin-right: 10px">
                                        <span class="circle circle-sm bg-info di" data-toggle="tooltip" title="Print Konsumsi" data-placement="bottom"><i class="ti-printer"></i></span>
                                    </a> -->
                                </div>
                            </div>
                            <hr>
                            <div id="databody">
                                
                            </div>
                    </div>
                </div>
                <!-- /.row -->
                <?php $this->load->view('modal-penunjang'); ?>              
                <?php $this->load->view('modal-hapus'); ?>              
                 <!--row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- wallet, & manage users widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
                    <!-- col-md-9 -->
                    <!-- /col-md-3 -->
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Profile, & inbox widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- calendar & todo list widgets -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- Blog-component -->
                <!-- ============================================================== -->

            </div>
            <!-- /.container-fluid -->
            <?php $this->load->view('footer'); ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <?php $this->load->view('scripts') ?>
    <script type="text/javascript">
        $(document).ready(function() {
            getData('PenunjangCntrl/getTabel');
            //ajax add data
            $('#add-data').submit(function(e){
                kategori = $(this).find('#kategori').select2('data');
                jenis = $(this).find('#jenis').select2('data');
                anggota = $(this).find('.select_anggota').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('kategori_id', kategori.id);
                formData.append('kategori', kategori.text);
                formData.append('jenis_id', jenis.id);
                formData.append('jenis', jenis.text);
                formData.append('sks', anggota.sks);
                formData.append('status_anggota', anggota.text);
                formData.append('anggota_id', anggota.id);
                $.ajax({
                    url: '<?= site_url('PenunjangCntrl/addData') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getData('PenunjangCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('MatkulCntrl/sorting', tahun, semester);
                        $('#addMatkulModal').modal('hide');
                        notification._toast('Success','Input Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax edit data
            $('#edit-data').submit(function(e){
                kategori = $(this).find('#editkategori').select2('data');
                jenis = $(this).find('#editjenis').select2('data');
                anggota = $(this).find('#editstatus_anggota').select2('data');
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                formData.append('kategori_id', kategori.id);
                formData.append('kategori', kategori.text);
                formData.append('jenis_id', jenis.id);
                formData.append('jenis', jenis.text);
                formData.append('sks', anggota.sks);
                formData.append('status_anggota', anggota.text);
                formData.append('anggota_id', anggota.id);
                $.ajax({
                    url: '<?= site_url('PenunjangCntrl/editData/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(){
                        getData('PenunjangCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('#editMatkulModal').modal('hide');
                        notification._toast('Success','Update Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax file data
            $('#file-data').submit(function(e){
                e.preventDefault();
                var formData = new FormData($(this)[0]);
                $.ajax({
                    url: '<?= site_url('PenunjangCntrl/addFile/') ?>',
                    data:formData,
                    type:'POST',
                    contentType: false,
                    processData: false,
                    success:function(data){
                        $('#file-pengajaran').html(data);
                        tooltip._tooltip();
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        notification._toast('Success','Upload File','success');
                    }
                });
            });
            //ajax hapus data
            $('#hapus-data').click(function(){
                $.ajax({
                    url:'<?= site_url('PenunjangCntrl/hapusData/') ?>',
                    data:{
                        id:id
                    },
                    success:function(data){
                        getData('PenunjangCntrl/getTabel');
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('.hapusModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            $('#hapus-datafile').click(function(){
                $.ajax({
                    url:'<?= site_url('PenunjangCntrl/hapusFile/') ?>',
                    data:{
                        id:id,
                        kategori:kategori,
                        id_penunjang:id_penunjang,
                    },
                    success:function(data){
                        $('#file-pengajaran').html(data);
                        // tahun = $('#sorttahun').val();
                        // semester = $('#sortsemester').val();
                        // getSort('PengajaranCntrl/sorting', tahun, semester);
                        $('.hapusFileModal').modal('hide');
                        notification._toast('Success','Delete Data','success');
                        tooltip._tooltip();
                    }
                });
            });
            //ajax sorting
            $('#sorttahun').on('change', function(){
                tahun = $(this).val();
                semester = $('#sortsemester').val();
                getSort('PenunjangCntrl/sorting', tahun, semester);
            });
            $('#sortsemester').on('change', function(){
                semester = $(this).val();
                tahun = $('#sorttahun').val();
                getSort('PenunjangCntrl/sorting', tahun, semester);
            });
            //datepicker
            $('#datepicker').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //editdatepicker
            $('#datepicker1').daterangepicker({
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                locale: {
                    format: "DD MMMM YYYY",
                    separator: " s.d ",
                }
            });
            //select dosen dan matakuliah
            $('.select_penugasan').select2({
              ajax: {
                url: '<?= site_url('PenunjangCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.select_dokumen').select2({
              ajax: {
                url: '<?= site_url('PenunjangCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            //select dosen dan matakuliah
            $('.editselect_penugasan').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PenunjangCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PenunjangCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            $('.editselect_dokumen').select2({
            initSelection: function (element, callback) {
                if(element.val() != ''){
                    $.post("<?= site_url('PenunjangCntrl/getBerkasId')?>",{val:element.val()}, function(res){
                        var data = {"id" : res[0]['id'], "text" : res[0]['text']};
                        callback(data)
                    });
                } else {
                    var data = {"id" : "", "text" : ""};
                    callback(data);
                }
            },
              ajax: {
                url: '<?= site_url('PenunjangCntrl/getBerkas') ?>',
                dataType: 'json',
                results: function (data) {
                    return {results: data};
                }
              }
            });
            //add
            $(".select_dosen").select2();
            $(".select_kategori").select2({
                data:[
                    {
                      id: 1,
                      text: "Pembinaan Sivitas Akademika",
                    },
                    {
                      id: 2,
                      text: "Administrasi dan Manajemen",
                    },
                    {
                      id: 3,
                      text: "Tugas Penunjang Lainnya",
                    }
                ]
            });
            $(".select_jenis").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".select_anggota").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $("#kategori").on('change', function(){
                kategori = $(this).val();
                if (kategori == 1) {
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Bimbingan akademik",
                            },
                            {
                              id: 2,
                              text: "Bimbingan dan Konseling (BKMF atau BKMU)",
                            },
                            {
                              id: 3,
                              text: "Pimpinan UKM",
                            },
                            {
                              id: 4,
                              text: "Organisasi sosial intern",
                            }
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 2){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Jabatan struktural",
                            },
                            {
                              id: 2,
                              text: "Jabatan non struktural",
                            },
                            {
                              id: 3,
                              text: "Panitia Ad Hoc",
                            },
                            {
                              id: 4,
                              text: "Panitia tetap",
                            }
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 3){
                    $(".select_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Peserta seminar/workshop",
                            },
                            {
                              id: 2,
                              text: "Sebagai asesor beban kerja dosen",
                            },
                            {
                              id: 3,
                              text: "Reviewer artikel jurnal ilmiah internasional",
                            },
                            {
                              id: 4,
                              text: "Reviewer artikel jurnal ilmiah nasional",
                            }
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }
            });
            $("#jenis").on('change', function(){
                jenis = $(this).val();
                kategori = $('#kategori').val();
                if (kategori == 1 && (jenis == 1 || jenis == 2)) {
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              selected: true,
                              sks: 1,
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'block');
                    $("#nilai_mhs").val(1);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 1 && (jenis == 3 || jenis == 4)){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 1){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dekan",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Pembantu Dekan",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Ketua Jurusan/Prodi",
                              sks: 3
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 2){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Sekretaris Jurusan/Prodi",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Ketua Program R2",
                              sks: 2
                            },
                            {
                              id: 3,
                              text: "Sekretaris Program R2",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Kepala Laboratorium",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Sekretaris Senat Fakultas",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Anggota Senat Fakultas",
                              sks: 1
                            },
                            {
                              id: 7,
                              text: "Ketua Rumpun Keminatan",
                              sks: 1
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 3){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 1
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                            {
                              id: 3,
                              text: "Anggota",
                              sks: 1
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 4){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 1){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat nasional",
                              sks: 0.1
                            },
                            {
                              id: 2,
                              text: "Tingkat internasional",
                              sks: 0.2
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 2){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#nilai_dosen").val(1);
                    $("#jml_dosen").css('display', 'block');
                }else if(kategori == 3 && jenis == 3){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 4){
                    $(".select_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.1,
                              selected: true
                            },
                        ]
                    });
                    $("#jml_mhs").css('display', 'none');
                    $("#nilai_mhs").val(0);
                    $("#jml_dosen").css('display', 'none');
                    $("#nilai_dosen").val(0);
                }
            });
            //edit
            $(".editselect_kategori").select2({
                data:[
                    {
                      id: 1,
                      text: "Pembinaan Sivitas Akademika",
                    },
                    {
                      id: 2,
                      text: "Administrasi dan Manajemen",
                    },
                    {
                      id: 3,
                      text: "Tugas Penunjang Lainnya",
                    }
                ]
            });
            $(".editselect_jenis").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $(".editselect_anggota").select2({
                data:[
                    {
                      id: 1,
                      text: "Silahkan Pilih Kategori terlebih dahulu",
                      disabled: true
                    }
                ]
            });
            $("#editkategori").on('change', function(){
                kategori = $(this).val();
                if (kategori == 1) {
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Bimbingan akademik",
                            },
                            {
                              id: 2,
                              text: "Bimbingan dan Konseling (BKMF atau BKMU)",
                            },
                            {
                              id: 3,
                              text: "Pimpinan UKM",
                            },
                            {
                              id: 4,
                              text: "Organisasi sosial intern",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Jabatan struktural",
                            },
                            {
                              id: 2,
                              text: "Jabatan non struktural",
                            },
                            {
                              id: 3,
                              text: "Panitia Ad Hoc",
                            },
                            {
                              id: 4,
                              text: "Panitia tetap",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Peserta seminar/workshop",
                            },
                            {
                              id: 2,
                              text: "Sebagai asesor beban kerja dosen",
                            },
                            {
                              id: 3,
                              text: "Reviewer artikel jurnal ilmiah internasional",
                            },
                            {
                              id: 4,
                              text: "Reviewer artikel jurnal ilmiah nasional",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }
            });
            $("#editjenis").on('change', function(){
                jenis = $(this).val();
                kategori = $('#editkategori').val();
                if (kategori == 1 && (jenis == 1 || jenis == 2)) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              selected: true,
                              sks: 1,
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'block');
                    $("#editnilai_mhs").val(1);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 1 && (jenis == 3 || jenis == 4)){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 1){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dekan",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Pembantu Dekan",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Ketua Jurusan/Prodi",
                              sks: 3
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Sekretaris Jurusan/Prodi",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Ketua Program R2",
                              sks: 2
                            },
                            {
                              id: 3,
                              text: "Sekretaris Program R2",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Kepala Laboratorium",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Sekretaris Senat Fakultas",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Anggota Senat Fakultas",
                              sks: 1
                            },
                            {
                              id: 7,
                              text: "Ketua Rumpun Keminatan",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 1
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                            {
                              id: 3,
                              text: "Anggota",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 1){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat nasional",
                              sks: 0.1
                            },
                            {
                              id: 2,
                              text: "Tingkat internasional",
                              sks: 0.2
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editnilai_dosen").val(1);
                    $("#editjml_dosen").css('display', 'block');
                }else if(kategori == 3 && jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }
            });
            $("#editMatkulModal").on('shown.bs.modal', function(){
                kategori = $('#editkategori').val();
                if (kategori == 1) {
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Bimbingan akademik",
                            },
                            {
                              id: 2,
                              text: "Bimbingan dan Konseling (BKMF atau BKMU)",
                            },
                            {
                              id: 3,
                              text: "Pimpinan UKM",
                            },
                            {
                              id: 4,
                              text: "Organisasi sosial intern",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Jabatan struktural",
                            },
                            {
                              id: 2,
                              text: "Jabatan non struktural",
                            },
                            {
                              id: 3,
                              text: "Panitia Ad Hoc",
                            },
                            {
                              id: 4,
                              text: "Panitia tetap",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3){
                    $(".editselect_jenis").select2({
                        data:[
                            {
                              id: 1,
                              text: "Peserta seminar/workshop",
                            },
                            {
                              id: 2,
                              text: "Sebagai asesor beban kerja dosen",
                            },
                            {
                              id: 3,
                              text: "Reviewer artikel jurnal ilmiah internasional",
                            },
                            {
                              id: 4,
                              text: "Reviewer artikel jurnal ilmiah nasional",
                            }
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }
            });
            $("#editMatkulModal").on('shown.bs.modal', function(){
                jenis = $('#editjenis').val();
                kategori = $('#editkategori').val();
                if (kategori == 1 && (jenis == 1 || jenis == 2)) {
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              selected: true,
                              sks: 1,
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'block');
                    $("#editnilai_mhs").val(1);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 1 && (jenis == 3 || jenis == 4)){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 1){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Dekan",
                              sks: 5
                            },
                            {
                              id: 2,
                              text: "Pembantu Dekan",
                              sks: 4
                            },
                            {
                              id: 3,
                              text: "Ketua Jurusan/Prodi",
                              sks: 3
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Sekretaris Jurusan/Prodi",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Ketua Program R2",
                              sks: 2
                            },
                            {
                              id: 3,
                              text: "Sekretaris Program R2",
                              sks: 2
                            },
                            {
                              id: 4,
                              text: "Kepala Laboratorium",
                              sks: 2
                            },
                            {
                              id: 5,
                              text: "Sekretaris Senat Fakultas",
                              sks: 2
                            },
                            {
                              id: 6,
                              text: "Anggota Senat Fakultas",
                              sks: 1
                            },
                            {
                              id: 7,
                              text: "Ketua Rumpun Keminatan",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 1
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                            {
                              id: 3,
                              text: "Anggota",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 2 && jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Ketua",
                              sks: 2
                            },
                            {
                              id: 2,
                              text: "Sekretaris",
                              sks: 1
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 1){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tingkat nasional",
                              sks: 0.1
                            },
                            {
                              id: 2,
                              text: "Tingkat internasional",
                              sks: 0.2
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 2){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editnilai_dosen").val(1);
                    $("#editjml_dosen").css('display', 'block');
                }else if(kategori == 3 && jenis == 3){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.2,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }else if(kategori == 3 && jenis == 4){
                    $(".editselect_anggota").select2({
                        data:[
                            {
                              id: 1,
                              text: "Tidak ada pilihan",
                              sks: 0.1,
                              selected: true
                            },
                        ]
                    });
                    $("#editjml_mhs").css('display', 'none');
                    $("#editnilai_mhs").val(0);
                    $("#editjml_dosen").css('display', 'none');
                    $("#editnilai_dosen").val(0);
                }
            });
        })

    </script>
</body>

</html>
