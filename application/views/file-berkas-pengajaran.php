<table class="table" id="table-file">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama File</th>
            <th>File</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $i = 0;
        foreach($kontrak->result() as $value){
        $i++;
        ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $value->nama_file ?></td>
                <td>
                    <a href="<?= site_url('')?><?= $value->path_file?> " class="editData" id="<?= $value->id_file_matkul ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                </td>
            </tr>
        <?php }
        foreach ($absendosen->result() as $key) {
           $i++;
        ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $key->nama_file ?></td>
                <td>
                    <a href="<?= site_url('')?><?= $key->path_file?> " class="editData" id="<?= $key->id_file_matkul ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                </td>
            </tr>
        <?php }
        foreach ($absenmhs->result() as $res) {
           $i++;
        ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $res->nama_file ?></td>
                <td>
                    <a href="<?= site_url('')?><?= $res->path_file?> " class="editData" id="<?= $res->id_file_matkul ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                </td>
            </tr>
        <?php }
        foreach ($berkas->result() as $ber) {
           $i++;
        ?>
            <tr>
                <td><?= $i ?></td>
                <td><?= $ber->nama_file ?></td>
                <td>
                    <a href="<?= site_url('')?><?= $ber->path_file?> " class="editData" id="<?= $ber->id_berkas ?>" target="_blank"><button type="button" class="btn btn-success btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Lihat File"><i class="ti-file"></i></button></a>
                </td>
            </tr>
        <?php }?>
    </tbody>
</table>
<script type="text/javascript">
    $('.hapusFileData').on('click',function(){
        id = $(this).attr('id');
        kategori = $(this).attr('kategori');
        idpengajaran = $(this).attr('id-data');
    });
</script>
