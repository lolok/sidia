<div class="table-responsive" id="div-atk">
    <table id="tabel-data" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th rowspan="2" class="text-center">No.</th>
                <th rowspan="2" class="text-center">JENIS KEGIATAN</th>
                <th colspan="2" class="text-center">BEBAN KERJA</th>
                <th rowspan="2" class="text-center">MASA PENUGASAN</th>
                <th colspan="2" class="text-center">KINERJA</th>
                <th rowspan="2" class="text-center">STATUS</th>
                <th rowspan="2" class="text-center">BERKAS</th>
            </tr>
            <tr>
                <th class="text-center">BUKTI PENUGASAN</th>
                <th class="text-center">SKS</th>
                <th class="text-center">BUKTI KINERJA</th>
                <th class="text-center">SKS</th>
            </tr>
        </thead>
        <tbody>
            <!-- Pengajaran -->
            <?php 
                $i = 0;
                foreach($pengajaran->result() as $value){ 
                $i++;
                if ($value->datasemester == 1) {
                    $semester = 'Ganjil';
                }else{
                    $semester = 'Genap';
                }
                if ($value->level == 1) {
                    $pengunduh = 'Dosen';
                }else{
                    $pengunduh = 'Admin';
                }
                if ($value->jenjang == 'D3') {
                    $penugasan = "Keputusan Dekan Sekolah Vokasi Universitas";
                }else{
                    $penugasan = "Keputusan Dekan Fakultas Teknik Universitas";
                }
                if ($value->jenjang == 'D3') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S1') {
                    if ($value->mhs <= 40) {
                        $persen = 100;
                    }else if($value->mhs > 40 && $value->mhs <= 80){
                        $persen = 150;
                    }else if($value->mhs > 80 && $value->mhs <= 120){
                        $persen = 200;
                    }
                }else if ($value->jenjang == 'S2'){
                    if ($value->mhs <= 25) {
                        $persen = 100;
                    }else if($value->mhs > 25 && $value->mhs <= 50){
                        $persen = 150;
                    }else if($value->mhs > 50 && $value->mhs <= 75){
                        $persen = 200;
                    }
                }
                $total = $value->jml_tatapmuka/$value->total_tatapmuka*$value->sks*$persen/100;
                
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Memberikan kuliah pada jenjang <?= $value->jenjang ?> mata kuliah <?= $value->matkul ?>, kelas <?= $value->kelas ?>, <?= $value->sks ?> SKS, <?= $value->dosen ?> dosen, <?= $value->jml_tatapmuka ?> tatap muka dari <?= $value->total_tatapmuka ?>, jumlah mahasiswa <?= $value->mhs ?>. <b>( <?= $value->jml_tatapmuka ?>/<?= $value->total_tatapmuka ?> x <?= $value->sks ?> sks x <?= $persen ?>% )</b></td>
                <td class="text-center"><?= $value->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $value->masa_penugasan ?></td>
                <td class="text-center"><?= $value->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($value->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" tabel-name="tb_pengajaran" id-name="id_pengajaran" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($value->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" tabel-name="tb_pengajaran" id-name="id_pengajaran" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($value->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" tabel-name="tb_pengajaran" id-name="id_pengajaran" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($value->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" tabel-name="tb_pengajaran" id-name="id_pengajaran" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($value->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" tabel-name="tb_pengajaran" id-name="id_pengajaran" data-title="Status E-BKD" data-id="<?= $value->id ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkasPengajaran" data-toggle="modal" data-target="#fileModal" id="<?= $value->idmatkul ?>" penugasan="<?= $value->bukti_penugasan ?>" dokumen="<?= $value->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php } ?>
            <!-- Bimbingan -->
            <?php 
                foreach ($pembimbingan->result() as $key) {
                    $i++;
                    if ($key->jenis == 1) {
                        $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                        $total = $mhs/6;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Tugas Akhir mahasiswa S1 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key1){ ?><?= $key1->nama ?> (NIM. <?= $key1->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }else if($key->jenis == 2){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Tesis mahasiswa S2 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key2){ ?><?= $key2->nama ?> (NIM. <?= $key2->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }else if($key->jenis == 3){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Membimbing Disertasi mahasiswa S3 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key3){ ?><?= $key3->nama ?> (NIM. <?= $key3->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }}?>
            <!-- Pengujian -->
            <?php 
                foreach ($pengujian->result() as $key) {
                    $i++;
                    if ($key->jenis == 1) {
                        $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                        $total = $mhs/6;
            ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Tugas Akhir mahasiswa S1 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key1){ ?><?= $key1->nama ?> (NIM. <?= $key1->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }else if($key->jenis == 2){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Tesis mahasiswa S2 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key2){ ?><?= $key2->nama ?> (NIM. <?= $key2->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }else if($key->jenis == 3){
                    $mhs = $this->Crud->readMhsPembimbingan($key->id_pembimbingan)->num_rows();
                    $total = $mhs/6; ?>
            <tr>
                <td class="text-center"><?= $i ?>.</td>
                <td class="text-left" style="width: 20px;">Menguji Disertasi mahasiswa S3 sebanyak <?= $mhs ?> orang, a.n. <?php foreach($this->Crud->readMhsPembimbingan($key->id_pembimbingan)->result() as $key3){ ?><?= $key3->nama ?> (NIM. <?= $key3->nim ?>), <?php } ?></b></td>
                <td class="text-center"><?= $key->bukti_penugasan ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center"><?= $key->masa_penugasan ?></td>
                <td class="text-center"><?= $key->bukti_dokumen ?></td>
                <td class="text-center"><?= number_format($total,2, '.', '') ?></td>
                <td class="text-center">
                    <?php if ($key->status_ebkd == 0) {?>
                        <a href="" class="dotip aktif_pengajaran" status="0" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded" style="">Selesai</span></a>
                    <?php }else if($key->status_ebkd == 1){?>
                        <a href="" class="dotip aktif_pengajaran" status="1" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lanjutkan</span></a>
                    <?php }else if($key->status_ebkd == 2){?>
                        <a href="" class="dotip aktif_pengajaran" status="2" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Gagal</span></a>
                    <?php }else if($key->status_ebkd == 3){?>   
                        <a href="" class="dotip aktif_pengajaran" status="3" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Lainnya</span></a>
                    <?php }else if($key->status_ebkd == 4){?>
                        <a href="" class="dotip aktif_pengajaran" status="4" data-title="Status E-BKD" tabel-name="tb_pembimbingan" id-name="id_pembimbingan" data-id="<?= $key->id_pembimbingan ?>" data-target=".statusModal" data-toggle="modal"><span class="label label-success label-rouded">Beban Lebih</span></a>
                    <?php }?>
                </td>
                <td class="text-center">
                    <a href="#" class="fileBerkas" data-toggle="modal" data-target="#fileModal" id="<?= $key->id_pembimbingan ?>" penugasan="<?= $key->bukti_penugasan ?>" dokumen="<?= $key->bukti_dokumen ?>"><button type="button" class="btn btn-warning btn-outline btn-circle btn-sm m-r-5 dotip" data-toggle="tooltip" title="Kontrak Kuliah"><i class="ti-files"></i></button></a>
                </td>
            </tr>
            <?php }}?>
        </tbody>
    </table>
</div>
<script type="text/javascript">
    //ajax ambil data dan ngelempar ID data
    $('.editData').on('click',function(){
        id = $(this).attr('id');
        form = $('#edit-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#editmatkul").select2('val',data['id_matkul']);
                form.find("#editdosen").select2('val',data['id_user']);
                form.find("#editdatepicker1").val(data['masa_penugasan1']);
                form.find("#editdatepicker2").val(data['masa_penugasan2']);
                form.find("#editjml_tatapmuka").val(data['jml_tatapmuka']);
                form.find("#edittotal_tatapmuka").val(data['total_tatapmuka']);
                form.find("#editsemester").val(data['semester']);
                form.find("#edittahun").val(data['tahun']);
                form.find("#idpengajaran").val(data['id_pengajaran']);
            }
        });
    });
    $('.hapusData').on('click',function(){
        id = $(this).attr('id');
    });
    $('.aktif_pengajaran').on('click', function(){
        id = $(this).attr('data-id');
        status = $(this).attr('status');
        tabel = $(this).attr('tabel-name');
        idname = $(this).attr('id-name');
        kategori = 1;
        $('#status').val(status);
    });
    $('.fileBerkas').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PembimbingCntrl/getFileBerkasTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.fileBerkasPengajaran').on('click',function(){
        id = $(this).attr('id');
        file1 = $(this).attr('penugasan');
        file2 = $(this).attr('dokumen');
        form = $('#file-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getFileTable') ?>',
            type: "POST",
            data:{
                id:id,
                file1:file1,
                file2:file2 
            },
            success:function(data){
                $('#file-berkas').html(data);
                form.find('#kategori_file').val(kategori);
                form.find('#id_file').val(id);
                tooltip._tooltip();
            }
        });
    });
    $('.infoData').on('click',function(){
        id = $(this).attr('id');
        form = $('#info-data');
        $.ajax({
            url:'<?= site_url('PengajaranCntrl/getData') ?>',
            data:{id:id},
            success:function(data){
                form.find("#infomatkul").select2('val',data['id_matkul']);
                form.find("#infodosen").select2('val',data['id_user']);
                form.find("#infosemester").val(data['semester']);
                form.find("#infotahun").val(data['tahun']);
            }
        });
    });
    //atur data table buat tabel
    $('#tabel-data').DataTable({
        ordering: false,   
        columns: [
            null,
            { "width": "30%" },
            { "width": "15%" },
            { "width": "5%" },
            { "width": "20%" },
            { "width": "15%" },
            { "width": "5%" },
            null,
            null
        ]
    });
    $(".select2").select2();
    
</script>