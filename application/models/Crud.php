<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->load->database();
	}

	function create($table, $data){
		$query = $this->db->insert($table, $data);
		return $query;
	}

	function readLast($table,$id)
	{
		$this->db->select($id.' as id');
		$this->db->from($table);
		$this->db->order_by($id, 'DESC');
		$this->db->limit('1');

		$query = $this->db->get('');

		foreach ($query->result() as $key) {
			$idtabel = $key->id;
		}

		return $idtabel;
	}

	function cekUsername($id, $name){
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('tb_user.id_user!=', $id);
		$this->db->where('tb_user.username=', $name);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function cekUsernameAdd($name){
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('tb_user.username=', $name);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	//berkas
	function readBerkas(){
		$this->db->select('*');
		$this->db->from('file_berkas');

		$query = $this->db->get('');

		return $query;
	}

	function readFileBerkas($file1, $file2){
		$this->db->select('*');
		$this->db->from('file_berkas');
		$this->db->where('nama_file=', $file1);
		$this->db->or_where('nama_file=', $file2);

		$query = $this->db->get('');

		return $query;
	}

	function readFileBerkas1($file){
		$this->db->select('*');
		$this->db->from('file_berkas');
		$this->db->where('nama_file=', $file);

		$query = $this->db->get('');

		return $query;
	}

	function readDataBerkas($kategori1, $kategori2){
		$this->db->select('DISTINCT(file_berkas.nama_file), file_berkas.kategori_file');
		$this->db->from('file_berkas');
		$this->db->where('file_berkas.kategori_file =', $kategori1);
		$this->db->or_where('file_berkas.kategori_file =', $kategori2);

		$query = $this->db->get('');

		return $query;
	}

	function readDataBerkasId($kategori1,$kategori2,$val){
		$this->db->select('DISTINCT(file_berkas.nama_file), file_berkas.kategori_file');
		$this->db->from('file_berkas');
		$this->db->where('file_berkas.kategori_file =', $kategori1);
		$this->db->or_where('file_berkas.kategori_file =', $kategori2);
		$this->db->where('file_berkas.nama_file=', $val);

		$query = $this->db->get('');

		return $query;
	}

	function readBerkasID($id){
		$this->db->select('*');
		$this->db->from('file_berkas');
		$this->db->where('id_berkas=', $id);

		$query = $this->db->get('');

		return $query;
	}


	function readDataByID($tabel,$idtabel,$id){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where($idtabel.'=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readMatkul(){
		$this->db->select('*');
		$this->db->from('tb_matkul');

		$query = $this->db->get('');

		return $query;
	}

	function readMatkulId($id){
		$this->db->select('*');
		$this->db->from('tb_matkul');
		$this->db->where('id_matkul=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readMatkulDosen($id){
		$this->db->select('*');
		$this->db->from('tb_dosen_matkul');
		$this->db->join('tb_matkul','tb_dosen_matkul.id_matkul=tb_matkul.id_matkul');
		$this->db->where('tb_dosen_matkul.id_user=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readDosen(){
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('tb_user.level=2');

		$query = $this->db->get('');

		return $query;
	}

	function readDosenId($id){
		$this->db->select('*');
		$this->db->from('tb_user');
		$this->db->where('tb_user.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readDosenMatkul($id){
		$this->db->select('*');
		$this->db->from('tb_dosen_matkul');
		$this->db->join('tb_user','tb_dosen_matkul.id_user=tb_user.id_user');
		$this->db->where('tb_dosen_matkul.id_matkul=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readDosenKerjasama($id){
		$this->db->select('*');
		$this->db->from('tb_dosen_kerjasama');
		$this->db->join('tb_user','tb_dosen_kerjasama.id_user=tb_user.id_user');
		$this->db->where('tb_dosen_kerjasama.id_kerjasama=', $id);

		$query = $this->db->get('');

		return $query;
	}

	//pembimbingan
	function readPembimbingan(){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readBkdPembimbingan1($id){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.id_user',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readBkdPembimbingan($id,$kategori){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.id_user',$id);
		$this->db->where('tb_pembimbingan.kategori',$kategori);

		$query = $this->db->get('');

		return $query;
	}

	function readPembimbinganByID($id){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->where('tb_pembimbingan.id_pembimbingan=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readMhsPembimbingan($id){
		$this->db->select('*');
		$this->db->from('tb_mhs_pembimbingan');
		$this->db->where('id_pembimbingan=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readMhsPembimbinganBkd($id,$kategori,$jenis){
		$this->db->select('*');
		$this->db->from('detail_pembimbingan');
		$this->db->where('id_user=',$id);
		$this->db->where('kategori=',$kategori);
		$this->db->where('jenis=',$jenis);
		$this->db->where('nama_mhs!=','');

		$query = $this->db->get('');

		return $query;
	}

	function readFilePembimbinganALL($id){
		$this->db->select('*');
		$this->db->from('file_pembimbingan');
		$this->db->where('file_pembimbingan.id_pembimbingan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePembimbingan($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_pembimbingan');
		$this->db->where('file_pembimbingan.id_pembimbingan=', $id);
		$this->db->where('file_pembimbingan.kategori_file=', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePembimbinganID($id){
		$this->db->select('*');
		$this->db->from('file_pembimbingan');
		$this->db->where('file_pembimbingan.id_file_pembimbingan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunPembimbingan($value){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPembimbingan($value){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllPembimbingan($tahun,$semester){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.tahun=', $tahun);
		$this->db->where('tb_pembimbingan.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdTahunPembimbingan($value,$id,$kategori){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.tahun=', $value);
		$this->db->where('tb_pembimbingan.id_user=', $id);
		$this->db->where('tb_pembimbingan.kategori=', $kategori);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdSemesterPembimbingan($value,$id,$kategori){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.semester=', $value);
		$this->db->where('tb_pembimbingan.id_user=', $id);
		$this->db->where('tb_pembimbingan.kategori=', $kategori);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdAllPembimbingan($tahun,$semester,$id,$kategori){
		$this->db->select('*');
		$this->db->from('tb_pembimbingan');
		$this->db->join('tb_user', 'tb_pembimbingan.id_user = tb_user.id_user');
		$this->db->where('tb_pembimbingan.tahun=', $tahun);
		$this->db->where('tb_pembimbingan.semester=', $semester);
		$this->db->where('tb_pembimbingan.id_user=', $id);
		$this->db->where('tb_pembimbingan.kategori=', $kategori);

		$query = $this->db->get('');

		return $query;
	}

	//pengajaran
	function readPengajaran(){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readBkdPengajaran($id){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.status_ebkd as status_ebkd, tb_matkul.jenjang as jenjang, tb_matkul.kelas as kelas, tb_matkul.mhs as mhs, tb_matkul.dosen as dosen, tb_pengajaran.jml_tatapmuka as jml_tatapmuka, tb_pengajaran.total_tatapmuka as total_tatapmuka, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.id_user=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readPengajaranByID($id){
		$this->db->select('*');
		$this->db->from('tb_pengajaran');
		$this->db->where('tb_pengajaran.id_pengajaran=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readKerjasamaByID($id){
		$this->db->select('*');
		$this->db->from('tb_kerjasama');
		$this->db->where('tb_kerjasama.id_kerjasama=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readPersonalByID($id){
		$this->db->select('*');
		$this->db->from('tb_personal');
		$this->db->where('tb_personal.id_personal=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readDatadiriByID($id){
		$this->db->select('*');
		$this->db->from('tb_data_diri');
		$this->db->where('tb_data_diri.id_data_diri=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readRiwayatByID($id){
		$this->db->select('*');
		$this->db->from('tb_riwayat');
		$this->db->where('tb_riwayat.id_riwayat=',$id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readRiwayatnfByID($id){
		$this->db->select('*');
		$this->db->from('tb_riwayatnf');
		$this->db->where('tb_riwayatnf.id_riwayatnf=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePengajaran($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_matkul');
		$this->db->where('file_matkul.id_matkul=', $id);
		$this->db->where('file_matkul.kategori_file=', $jenis);

		$query = $this->db->get('');

		return $query;
	}
	
	function readFileKerjasama($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_kerjasama');
		$this->db->where('file_kerjasama.id_kerjasama=', $id);
		$this->db->where('file_kerjasama.kategori_file=', $jenis);

		$query = $this->db->get('');

		return $query;
	}
	
	function readFilePersonal($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_personal');
		$this->db->where('file_personal.id_personal=', $id);
		$this->db->where('file_personal.kategori_file=', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePengajaranID($id){
		$this->db->select('*');
		$this->db->from('file_matkul');
		$this->db->where('file_matkul.id_file_matkul=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readFileKerjasamaID($id){
		$this->db->select('*');
		$this->db->from('file_kerjasama');
		$this->db->where('file_kerjasama.id_file_kerjasama=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readFilePersonalID($id){
		$this->db->select('*');
		$this->db->from('file_personal');
		$this->db->where('file_personal.id_file_personal=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePengajaranALL($id){
		$this->db->select('*');
		$this->db->from('file_pengajaran');
		$this->db->where('file_pengajaran.id_pengajaran=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readFilePersonalALL($id){
		$this->db->select('*');
		$this->db->from('file_personal');
		$this->db->where('file_personal.id_personal=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunPengajaran($value){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.id_matkul as idmatkul,tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPengajaran($value){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllPengajaran($tahun,$semester){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.tahun=', $tahun);
		$this->db->where('tb_pengajaran.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdTahunPengajaran($value, $id){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.status_ebkd as status_ebkd, tb_matkul.jenjang as jenjang, tb_matkul.kelas as kelas, tb_matkul.mhs as mhs, tb_matkul.dosen as dosen, tb_pengajaran.jml_tatapmuka as jml_tatapmuka, tb_pengajaran.total_tatapmuka as total_tatapmuka, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.tahun=', $value);
		$this->db->where('tb_pengajaran.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdSemesterPengajaran($value, $id){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.status_ebkd as status_ebkd, tb_matkul.jenjang as jenjang, tb_matkul.kelas as kelas, tb_matkul.mhs as mhs, tb_matkul.dosen as dosen, tb_pengajaran.jml_tatapmuka as jml_tatapmuka, tb_pengajaran.total_tatapmuka as total_tatapmuka, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.semester=', $value);
		$this->db->where('tb_pengajaran.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdAllPengajaran($tahun,$semester,$id){
		$this->db->select('tb_pengajaran.tahun as datatahun, tb_pengajaran.semester as datasemester, tb_matkul.nama_matkul as matkul, tb_matkul.sks_matkul as sks, tb_matkul.kode_matkul as kode, tb_pengajaran.id_pengajaran as id, tb_user.nama as namauser, tb_user.level as level, tb_pengajaran.id_matkul as idmatkul, tb_pengajaran.status_ebkd as status_ebkd, tb_matkul.jenjang as jenjang, tb_matkul.kelas as kelas, tb_matkul.mhs as mhs, tb_matkul.dosen as dosen, tb_pengajaran.jml_tatapmuka as jml_tatapmuka, tb_pengajaran.total_tatapmuka as total_tatapmuka, tb_pengajaran.masa_penugasan as masa_penugasan, tb_pengajaran.bukti_penugasan as bukti_penugasan, tb_pengajaran.bukti_dokumen as bukti_dokumen');
		$this->db->from('tb_pengajaran');
		$this->db->join('tb_matkul', 'tb_pengajaran.id_matkul = tb_matkul.id_matkul');
		$this->db->join('tb_user', 'tb_pengajaran.id_user = tb_user.id_user');
		$this->db->where('tb_pengajaran.tahun=', $tahun);
		$this->db->where('tb_pengajaran.semester=', $semester);
		$this->db->where('tb_pengajaran.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	//kegiatan
	function readKegiatan(){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg, 
		tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser,tb_user.level as level');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$query = $this->db->get('');
		return $query;
	}
	
	function readBkdKegiatan($id){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg, 
		tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser,tb_user.level as level, tb_kegiatan.status_ebkd as status_ebkd');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.id_user=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFileKegiatan($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_kegiatan=', $id);
		$this->db->where('file_kegiatan.kategori_file=', $jenis);
		$query = $this->db->get('');
		return $query;
	}
	function readFileKegiatanALL($id){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_kegiatan=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function readKegiatanByID($id){
		$this->db->select('*');
		$this->db->from('tb_kegiatan');
		$this->db->where('tb_kegiatan.id_kegiatan=',$id);
		$query = $this->db->get('');
		return $query;
	}
	function readFileKegiatanID($id){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_file_kegiatan=', $id);
		$query = $this->db->get('');
		return $query;
	}

	function sortTahunKegiatan($value){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.namkeg as namkeg, tb_kegiatan.semester as datasemester, tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.tahun=', $value);

		$query = $this->db->get('');
		return $query;
	}

	function sortSemesterKegiatan($value){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg,tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.semester=', $value);

		$query = $this->db->get('');
		return $query;
	}

	function sortAllKegiatan($tahun,$semester){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg, tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.tahun=', $tahun);
		$this->db->where('tb_kegiatan.semester=', $semester);

		$query = $this->db->get('');
		return $query;
	}

	function sortBkdTahunKegiatan($value,$id){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.namkeg as namkeg, tb_kegiatan.semester as datasemester, tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level, tb_kegiatan.status_ebkd as status_ebkd');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.tahun=', $value);
		$this->db->where('tb_kegiatan.id_user=', $id);

		$query = $this->db->get('');
		return $query;
	}

	function sortBkdSemesterKegiatan($value,$id){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg,tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level, tb_kegiatan.status_ebkd as status_ebkd');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.semester=', $value);
		$this->db->where('tb_kegiatan.id_user=', $id);

		$query = $this->db->get('');
		return $query;
	}

	function sortBkdAllKegiatan($tahun,$semester,$id){
		$this->db->select('tb_kegiatan.tahun as datatahun, tb_kegiatan.semester as datasemester, tb_kegiatan.namkeg as namkeg, tb_kegiatan.id_kegiatan as id, tb_user.nama as namauser, tb_user.level as level, tb_kegiatan.status_ebkd as status_ebkd');
		$this->db->from('tb_kegiatan');
		$this->db->join('tb_user', 'tb_kegiatan.id_user = tb_user.id_user');
		$this->db->where('tb_kegiatan.tahun=', $tahun);
		$this->db->where('tb_kegiatan.semester=', $semester);
		$this->db->where('tb_kegiatan.id_user=', $id);
		
		$query = $this->db->get('');
		return $query;
	}

	//penunjang
	function readPenunjang(){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$query = $this->db->get('');
		return $query;
	}

	function readBkdPenunjang($id){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.id_user=',$id);

		$query = $this->db->get('');
		return $query;
	}

	function readFilePenunjang($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_penunjang');
		$this->db->where('file_penunjang.id_penunjang=', $id);
		$this->db->where('file_penunjang.kategori_file=', $jenis);
		$query = $this->db->get('');
		return $query;
	}
	function readFilePenunjangALL($id){
		$this->db->select('*');
		$this->db->from('file_penunjang');
		$this->db->where('file_penunjang.id_penunjang=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function readPenunjangByID($id){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->where('tb_penunjang.id_penunjang=',$id);
		$query = $this->db->get('');
		return $query;
	}
	function readFilePenunjangID($id){
		$this->db->select('*');
		$this->db->from('file_penunjang');
		$this->db->where('file_penunjang.id_file_penunjang=', $id);
		$query = $this->db->get('');
		return $query;
	}

	function sortTahunPenunjang($value){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.tahun=', $value);
		$query = $this->db->get('');
		return $query;
	}

	function sortSemesterPenunjang($value){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.semester=', $value);
		$query = $this->db->get('');
		return $query;
	}

	function sortAllPenunjang($tahun,$semester){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.tahun=', $tahun);
		$this->db->where('tb_penunjang.semester=', $semester);
		$query = $this->db->get('');
		return $query;
	}

	function sortBkdTahunPenunjang($value,$id){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.tahun=', $value);
		$this->db->where('tb_penunjang.id_user=', $id);
		$query = $this->db->get('');
		return $query;
	}

	function sortBkdSemesterPenunjang($value,$id){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.semester=', $value);
		$this->db->where('tb_penunjang.id_user=', $id);
		$query = $this->db->get('');
		return $query;
	}

	function sortBkdAllPenunjang($tahun,$semester,$id){
		$this->db->select('*');
		$this->db->from('tb_penunjang');
		$this->db->join('tb_user', 'tb_penunjang.id_user = tb_user.id_user');
		$this->db->where('tb_penunjang.tahun=', $tahun);
		$this->db->where('tb_penunjang.semester=', $semester);
		$this->db->where('tb_penunjang.id_user=', $id);
		$query = $this->db->get('');
		return $query;
	}


	//pkm
	function readPkm(){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readBkdPkm($id){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPkmByID($id){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->where('tb_pkm.id_pkm=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePkm($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_pkm');
		$this->db->where('file_pkm.id_pkm =', $id);
		$this->db->where('file_pkm.kategori_file =', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePkmID($id){
		$this->db->select('*');
		$this->db->from('file_pkm');
		$this->db->where('file_pkm.id_file_pkm=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePkmALL($id){
		$this->db->select('*');
		$this->db->from('file_pkm');
		$this->db->where('file_pkm.id_pkm=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunPkm($value){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPkm($value){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllPkm($tahun,$semester){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.tahun=', $tahun);
		$this->db->where('tb_pkm.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdTahunPkm($value, $id){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.tahun=', $value);
		$this->db->where('tb_pkm.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdSemesterPkm($value, $id){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.semester=', $value);
		$this->db->where('tb_pkm.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdAllPkm($tahun,$semester, $id){
		$this->db->select('*');
		$this->db->from('tb_pkm');
		$this->db->join('tb_user', 'tb_pkm.id_user = tb_user.id_user');
		$this->db->where('tb_pkm.tahun=', $tahun);
		$this->db->where('tb_pkm.semester=', $semester);
		$this->db->where('tb_pkm.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	//Publikasi
	function readPublikasi(){
		$this->db->select('tb_publikasi.tahun as datatahun,
						   tb_publikasi.semester as datasemester,
						   tb_publikasi.nama_publikasi as datanama,
						   tb_publikasi.link as datalink,
						   tb_publikasi.id_publikasi as id,
						   tb_user.nama as namauser,
						   tb_user.level as level');
		$this->db->from('tb_publikasi');
		$this->db->join('tb_user', 'tb_publikasi.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readPublikasiByID($id){
		$this->db->select('*');
		$this->db->from('tb_publikasi');
		$this->db->where('tb_publikasi.id_publikasi=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePublikasi($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_publikasi');
		$this->db->where('file_publikasi.id_publikasi =', $id);
		$this->db->where('file_publikasi.kategori_file =', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePublikasiID($id){
		$this->db->select('*');
		$this->db->from('file_publikasi');
		$this->db->where('file_publikasi.id_file_publikasi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePublikasiALL($id){
		$this->db->select('*');
		$this->db->from('file_publikasi');
		$this->db->where('file_publikasi.id_publikasi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunPublikasi($value){
		$this->db->select('tb_publikasi.tahun as datatahun,
						   tb_publikasi.semester as datasemester,
						   tb_publikasi.nama_publikasi as datanama,
						   tb_publikasi.link as datalink,
						   tb_publikasi.id_publikasi as id,
						   tb_user.nama as namauser,
						   tb_user.level as level');
		$this->db->from('tb_publikasi');
		$this->db->join('tb_user', 'tb_publikasi.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPublikasi($value){
		$this->db->select('tb_publikasi.tahun as datatahun,
						   tb_publikasi.semester as datasemester,
						   tb_publikasi.nama_publikasi as datanama,
						   tb_publikasi.link as datalink,
						   tb_publikasi.id_publikasi as id,
						   tb_user.nama as namauser,
						   tb_user.level as level');
		$this->db->from('tb_publikasi');
		$this->db->join('tb_user', 'tb_publikasi.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllPublikasi($tahun,$semester){
		$this->db->select('tb_publikasi.tahun as datatahun,
						   tb_publikasi.semester as datasemester,
						   tb_publikasi.nama_publikasi as datanama,
						   tb_publikasi.link as datalink,
						   tb_publikasi.id_publikasi as id,
						   tb_user.nama as namauser,
						   tb_user.level as level');
		$this->db->from('tb_publikasi');
		$this->db->join('tb_user', 'tb_publikasi.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi.tahun=', $tahun);
		$this->db->where('tb_publikasi.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	//penelitian
	function readPenelitian(){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readBkdPenelitian($id){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPenelitianByID($id){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->where('tb_penelitian.id_penelitian=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePenelitian($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_penelitian');
		$this->db->where('file_penelitian.id_penelitian =', $id);
		$this->db->where('file_penelitian.kategori_file =', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePenelitianID($id){
		$this->db->select('*');
		$this->db->from('file_penelitian');
		$this->db->where('file_penelitian.id_file_penelitian=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortTahunKerjasama($value){
		$this->db->select('tb_kerjasama.tahun as datatahun, tb_kerjasama.nama_kerjasama as nama_kerjasama, tb_kerjasama.semester as datasemester, tb_kerjasama.id_kerjasama as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kerjasama');
		$this->db->join('tb_user', 'tb_kerjasama.id_user = tb_user.id_user');
		$this->db->where('tb_kerjasama.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortTahunPersonal($value){
		$this->db->select('tb_personal.nama_personal as nama_personal, tb_personal.nip as nip, tb_personal.alamat as alamat, tb_personal.email as email, tb_personal.nohp as nohp, tb_personal.tahun as datatahun, tb_personal.semester as datasemester, tb_personal.id_personal as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_personal');
		$this->db->join('tb_user', 'tb_personal.id_user = tb_user.id_user');
		$this->db->where('tb_personal.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortTahunDatadiri($value){
		$this->db->select('tb_data_diri.nama as nama, tb_data_diri.jenkel as jenkel, tb_data_diri.jabatan_fungsional as jabatan_fungsional, tb_data_diri.nip as nip, tb_data_diri.nidn as nidn, tb_data_diri.tempat as tempat, tb_data_diri.tanggal as tanggal, tb_data_diri.email as email, tb_data_diri.no_hp as no_hp, tb_data_diri.alamat_kantor as alamat_kantor, tb_data_diri.no_telp_kantor as no_telp_kantor, tb_data_diri.s1 as s1, tb_data_diri.s2 as s2, tb_data_diri.s3 as s3, tb_data_diri.matkul_diampu as matkul_diampu, tb_data_diri.bidang_keahlian as bidang_keahlian, tb_data_diri.id_data_diri as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_data_diri');
		$this->db->join('tb_user', 'tb_data_diri.id_user = tb_user.id_user');
		$this->db->where('tb_data_diri.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function readFilePenelitianALL($id){
		$this->db->select('*');
		$this->db->from('file_penelitian');
		$this->db->where('file_penelitian.id_penelitian=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortSemesterKerjasama($value){
		$this->db->select('tb_kerjasama.tahun as datatahun, tb_kerjasama.nama_kerjasama as nama_kerjasama, tb_kerjasama.semester as datasemester, tb_kerjasama.id_kerjasama as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kerjasama');
		$this->db->join('tb_user', 'tb_kerjasama.id_user = tb_user.id_user');
		$this->db->where('tb_kerjasama.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortSemesterPersonal($value){
		$this->db->select('tb_personal.nama_personal as nama_personal, tb_personal.nip as nip, tb_personal.alamat as alamat, tb_personal.email as email, tb_personal.nohp as nohp, tb_personal.tahun as datatahun, tb_personal.semester as datasemester, tb_personal.id_personal as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_personal');
		$this->db->join('tb_user', 'tb_personal.id_user = tb_user.id_user');
		$this->db->where('tb_personal.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortSemesterDatadiri($value){
		$this->db->select('tb_data_diri.nama as nama, tb_data_diri.jenkel as jenkel, tb_data_diri.jabatan_fungsional as jabatan_fungsional, tb_data_diri.nip as nip, tb_data_diri.nidn as nidn, tb_data_diri.tempat as tempat, tb_data_diri.tanggal as tanggal, tb_data_diri.email as email, tb_data_diri.no_hp as no_hp, tb_data_diri.alamat_kantor as alamat_kantor, tb_data_diri.no_telp_kantor as no_telp_kantor, tb_data_diri.s1 as s1, tb_data_diri.s2 as s2, tb_data_diri.s3 as s3, tb_data_diri.matkul_diampu as matkul_diampu, tb_data_diri.bidang_keahlian as bidang_keahlian, tb_data_diri.id_data_diri as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_data_diri');
		$this->db->join('tb_user', 'tb_data_diri.id_user = tb_user.id_user');
		$this->db->where('tb_data_diri.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunPenelitian($value){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPenelitian($value){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllKerjasama($tahun,$semester){
		$this->db->select('tb_kerjasama.tahun as datatahun, tb_kerjasama.nama_kerjasama as nama_kerjasama, tb_kerjasama.semester as datasemester, tb_kerjasama.id_kerjasama as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_kerjasama');
		$this->db->join('tb_user', 'tb_kerjasama.id_user = tb_user.id_user');
		$this->db->where('tb_kerjasama.tahun=', $tahun);
		$this->db->where('tb_kerjasama.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllPersonal($tahun,$semester){
		$this->db->select('tb_personal.nama_personal as nama_personal, tb_personal.nip as nip, tb_personal.alamat as alamat, tb_personal.email as email, tb_personal.nohp as nohp, tb_personal.tahun as datatahun, tb_personal.semester as datasemester, tb_personal.id_personal as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_personal');
		$this->db->join('tb_user', 'tb_personal.id_user = tb_user.id_user');
		$this->db->where('tb_personal.tahun=', $tahun);
		$this->db->where('tb_personal.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllDatadiri($tahun,$semester){
		$this->db->select('tb_data_diri.nama as nama, tb_data_diri.jenkel as jenkel, tb_data_diri.jabatan_fungsional as jabatan_fungsional, tb_data_diri.nip as nip, tb_data_diri.nidn as nidn, tb_data_diri.tempat as tempat, tb_data_diri.tanggal as tanggal, tb_data_diri.email as email, tb_data_diri.no_hp as no_hp, tb_data_diri.alamat_kantor as alamat_kantor, tb_data_diri.no_telp_kantor as no_telp_kantor, tb_data_diri.s1 as s1, tb_data_diri.s2 as s2, tb_data_diri.s3 as s3, tb_data_diri.matkul_diampu as matkul_diampu, tb_data_diri.bidang_keahlian as bidang_keahlian, tb_data_diri.id_data_diri as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_data_diri');
		$this->db->join('tb_user', 'tb_data_diri.id_user = tb_user.id_user');
		$this->db->where('tb_data_diri.tahun=', $tahun);
		$this->db->where('tb_data_diri.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllPenelitian($tahun,$semester){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.tahun=', $tahun);
		$this->db->where('tb_penelitian.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdTahunPenelitian($value, $id){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.tahun=', $value);
		$this->db->where('tb_penelitian.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdSemesterPenelitian($value, $id){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.semester=', $value);
		$this->db->where('tb_penelitian.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortBkdAllPenelitian($tahun,$semester, $id){
		$this->db->select('*');
		$this->db->from('tb_penelitian');
		$this->db->join('tb_user', 'tb_penelitian.id_user = tb_user.id_user');
		$this->db->where('tb_penelitian.tahun=', $tahun);
		$this->db->where('tb_penelitian.semester=', $semester);
		$this->db->where('tb_penelitian.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	//CV
	function readCv(){
		$this->db->select('tb_cv.tahun as datatahun, tb_cv.semester as datasemester, tb_cv.id_cv as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_cv');
		$this->db->join('tb_user', 'tb_cv.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readCvByID($id){
		$this->db->select('*');
		$this->db->from('tb_cv');
		$this->db->where('tb_cv.id_cv=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readFileCv($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_cv');
		$this->db->where('file_cv.id_cv =', $id);
		$this->db->where('file_cv.kategori_file =', $jenis);

		$query = $this->db->get('');

		return $query;
	}

	function readFileCvID($id){
		$this->db->select('*');
		$this->db->from('file_cv');
		$this->db->where('file_cv.id_file_cv=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readFileCvALL($id){
		$this->db->select('*');
		$this->db->from('file_cv');
		$this->db->where('file_cv.id_cv=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahunCv($value){
		$this->db->select('tb_cv.tahun as datatahun, tb_cv.semester as datasemester, tb_cv.id_cv as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_cv');
		$this->db->join('tb_user', 'tb_cv.id_user = tb_user.id_user');
		$this->db->where('tb_cv.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterCv($value){
		$this->db->select('tb_cv.tahun as datatahun, tb_cv.semester as datasemester, tb_cv.id_cv as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_cv');
		$this->db->join('tb_user', 'tb_cv.id_user = tb_user.id_user');
		$this->db->where('tb_cv.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAllCv($tahun,$semester){
		$this->db->select('tb_cv.tahun as datatahun, tb_cv.semester as datasemester, tb_cv.id_cv as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_cv');
		$this->db->join('tb_user', 'tb_cv.id_user = tb_user.id_user');
		$this->db->where('tb_cv.tahun=', $tahun);
		$this->db->where('tb_cv.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	//Publikasi Lainnya
	function readPublikasiLain(){
		$this->db->select('tb_publikasi_lain.waktu as datawaktu,
						   tb_publikasi_lain.media as datamedia,
						   tb_publikasi_lain.judul as datajudul,
						   tb_publikasi_lain.id_publikasi_lain as id,
						   tb_user.nama as namauser');
		$this->db->from('tb_publikasi_lain');
		$this->db->join('tb_user', 'tb_publikasi_lain.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readPublikasiLainByID($id){
		$this->db->select('*');
		$this->db->from('tb_publikasi_lain');
		$this->db->where('tb_publikasi_lain.id_publikasi_lain=',$id);

		$query = $this->db->get('');

		return $query;
	}

	//Narasumber
	function readNarasumber(){
		$this->db->select('tb_narasumber.kegiatan as datakegiatan,
						   tb_narasumber.status as datastatus,
						   tb_narasumber.penyelenggara as datapenyelenggara,
						   tb_narasumber.tempat_waktu as datatempatwaktu,
						   tb_narasumber.id_narasumber as id,
						   tb_user.nama as namauser');
		$this->db->from('tb_narasumber');
		$this->db->join('tb_user', 'tb_narasumber.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readNarasumberByID($id){
		$this->db->select('*');
		$this->db->from('tb_narasumber');
		$this->db->where('tb_narasumber.id_narasumber=',$id);

		$query = $this->db->get('');

		return $query;
	}

	//Kegiatan Profesional Lainnya
	function readKegiatanLain(){
		$this->db->select('tb_kegiatan_lain.kegiatan as datakegiatan,
						   tb_kegiatan_lain.institusi as datainstitusi,
						   tb_kegiatan_lain.tempat_waktu as datatempatwaktu,
						   tb_kegiatan_lain.id_kegiatan_lain as id,
						   tb_user.nama as namauser');
		$this->db->from('tb_kegiatan_lain');
		$this->db->join('tb_user', 'tb_kegiatan_lain.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readKegiatanLainByID($id){
		$this->db->select('*');
		$this->db->from('tb_kegiatan_lain');
		$this->db->where('tb_kegiatan_lain.id_kegiatan_lain=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function sortTahun($tabel,$value){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where('tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemester($tabel,$value){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where('semester=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortAll($tabel,$tahun,$semester){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where('tahun=', $tahun);
		$this->db->where('semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	function countPanitia($table, $id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_user=', $id);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function countNetto($table, $id){
		$this->db->select('*');
		$this->db->from($table);
		$this->db->where('id_user=', $id);

		$query = $this->db->get('');

		$netto = 0;

		foreach ($query->result() as $key) {
			$netto = $netto + $key->jml_netto;
		}

		return $netto;
	}
	
	function readKegiatanId($id){
		$this->db->select('*');
		$this->db->from('kegiatan');
		$this->db->join('kategori_kegiatan', 'kegiatan.id_katkeg = kategori_kegiatan.id_katkeg');
		$this->db->where('kegiatan.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readHonor($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->join('golongan', 'honor_panitia.id_golongan = golongan.id_golongan');
		$this->db->where('honor_panitia.id_user=',$id);

		$query = $this->db->get('');

		return $query;
	}

	function readHonorId($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->join('kegiatan', 'honor_panitia.id_kegiatan = kegiatan.id_kegiatan');
		$this->db->where('honor_panitia.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}
	
	function readHonorById($id){
		$this->db->select('*');
		$this->db->from('honor_panitia');
		$this->db->join('kategori_kepanitiaan', 'honor_panitia.id_kepanitiaan = kategori_kepanitiaan.id_kepanitiaan');
		$this->db->where('honor_panitia.id_honor=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readIsianId($id){
		$this->db->select('*');
		$this->db->from('isian_kegiatan');
		$this->db->join('kategori_jabatan', 'isian_kegiatan.id_jabatan = kategori_jabatan.id_jabatan');
		$this->db->join('kegiatan', 'isian_kegiatan.id_kegiatan = kegiatan.id_kegiatan');
		$this->db->join('golongan', 'isian_kegiatan.id_golongan = golongan.id_golongan');
		$this->db->where('isian_kegiatan.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readIsianById($id){
		$this->db->select('*');
		$this->db->from('isian_kegiatan');
		$this->db->join('kategori_jabatan', 'isian_kegiatan.id_jabatan = kategori_jabatan.id_jabatan');
		$this->db->where('isian_kegiatan.id_kegiatan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readAtkId($id){
		$this->db->select('*');
		$this->db->from('isian_atk');
		$this->db->where('isian_atk.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readAtkById($id){
		$this->db->select('*');
		$this->db->from('isian_atk');
		$this->db->where('isian_atk.id_atk=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPrlngkpnId($id){
		$this->db->select('*');
		$this->db->from('isian_perlengkapan');
		$this->db->where('isian_perlengkapan.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readPrlngkpnById($id){
		$this->db->select('*');
		$this->db->from('isian_perlengkapan');
		$this->db->where('isian_perlengkapan.id_perlengkapan=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKonsumsiId($id){
		$this->db->select('*');
		$this->db->from('isian_konsumsi');
		$this->db->where('isian_konsumsi.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKonsumsiById($id){
		$this->db->select('*');
		$this->db->from('isian_konsumsi');
		$this->db->where('isian_konsumsi.id_konsumsi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readTransportasiId($id){
		$this->db->select('*');
		$this->db->from('isian_transportasi');
		$this->db->join('kota', 'isian_transportasi.id_kota = kota.id_kota');
		$this->db->where('isian_transportasi.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readTransportasiById($id){
		$this->db->select('*');
		$this->db->from('isian_transportasi');
		$this->db->join('kota', 'isian_transportasi.id_kota = kota.id_kota');
		$this->db->where('isian_transportasi.id_transportasi=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readUserId($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.id_user!=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readUserLvl($lvl){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.level=', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readUserById($id){
		$this->db->select('*');
		$this->db->from('user');
		$this->db->where('user.id_user=', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readStockRe($date1, $date2){
		$this->db->select('tb_pengembalian.no_faktur, tb_faktur_kembali.no_faktur_kembali, tb_faktur_kembali.tanggal');
		$this->db->from('tb_pengembalian');
		$this->db->join('tb_faktur_kembali', 'tb_pengembalian.no_faktur = tb_faktur_kembali.no_faktur_kembali');
		$this->db->where('tb_faktur_kembali.tanggal <=', $date2);
		$this->db->where('tb_faktur_kembali.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function readKadaluarsa(){
		$this->db->select('(CURDATE()) AS tgl, tb_penerimaan.tgl_kedaluwarsa AS tgl2 ');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_farmasi', 'tb_penerimaan.kode_obat = tb_farmasi.kode');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_obat');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasama(){
		$this->db->select('tb_kerjasama.nama_kerjasama as nama_kerjasama, tb_kerjasama.tahun as datatahun, tb_kerjasama.semester as datasemester, tb_kerjasama.id_kerjasama as id');
		$this->db->from('tb_kerjasama');

		$query = $this->db->get('');

		return $query;
	}
	
	function readPersonal(){
		$this->db->select('tb_personal.nama as nama, tb_personal.nosertif as nosertif, tb_personal.perting as perting, tb_personal.status as status, tb_personal.alamat as alamat, tb_personal.jurusan as jurusan, tb_personal.prodi as prodi, tb_personal.jabatan as jabatan, tb_personal.tempat as tempat, tb_personal.tanggal as tanggal, tb_personal.s1 as s1, tb_personal.s2 as s2, tb_personal.s3 as s3, tb_personal.ilmu as ilmu, tb_personal.nohape as nohape, tb_personal.tahun as datatahun, tb_personal.semester as datasemester, tb_personal.id_personal as id, tb_user.nama as namauser, tb_user.level as level');
		$this->db->from('tb_personal');
		$this->db->join('tb_user', 'tb_personal.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}
	
	function readDatadiri(){
		$this->db->select('tb_data_diri.nama as nama, tb_data_diri.jenkel as jenkel, tb_data_diri.jabatan_fungsional as jabatan_fungsional, tb_data_diri.nip as nip, tb_data_diri.nidn as nidn, tb_data_diri.tempat as tempat, tb_data_diri.tanggal as tanggal, tb_data_diri.email as email, tb_data_diri.no_hp as no_hp, tb_data_diri.alamat_kantor as alamat_kantor, tb_data_diri.no_telp_kantor as no_telp_kantor, tb_data_diri.s1 as s1, tb_data_diri.s2 as s2, tb_data_diri.s3 as s3, tb_data_diri.matkul_diampu as matkul_diampu, tb_data_diri.bidang_keahlian as bidang_keahlian, tb_data_diri.tahun as datatahun, tb_data_diri.semester as datasemester,tb_data_diri.id_data_diri as id');
		$this->db->from('tb_data_diri');

		$query = $this->db->get('');

		return $query;
	}
	
	function readRiwayat(){
		$this->db->select('*');
		$this->db->from('tb_riwayat');
		$this->db->join('tb_user', 'tb_riwayat.id_user=tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}
	
	function readRiwayatnf(){
		$this->db->select('tb_riwayatnf.nama_kegiatan as nama_kegiatan, tb_riwayatnf.penyelenggara as penyelenggara, tb_riwayatnf.tempat as tempat, tb_riwayatnf.waktu as waktu, tb_riwayatnf.id_riwayatnf as id_riwayatnf, tb_riwayatnf.id_user as id_user');
		$this->db->from('tb_riwayatnf');

		$query = $this->db->get('');

		return $query;
	}

	function readInisiasiAll(){
		$this->db->select('*');
		$this->db->from('tb_inisiasi');
		$this->db->join('tb_user', 'tb_inisiasi.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readMou($id){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->where('tb_mou.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKerja(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.status = 1');
		$this->db->order_by('tb_mou.tahun', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readKerja1(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer1($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjaTer2($date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.status = 1');
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query;
	}

	function readKategori($kat,$sub,$thn,$ngr){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		if($kat!=null){
			$this->db->where('tb_kategori.id_sub =', $kat);
		}
		if($sub!=null){
			$this->db->where('tb_mou.id_kategori =', $sub);
		}
		if($thn!=null){
			$this->db->where('tb_mou.tahun =', $thn);
		}
		if($ngr!=null){
			$this->db->where('tb_mou.negara =', $ngr);
		}
		$this->db->order_by('tb_mou.tahun', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaKat($kat){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.tahun =', $thn);


		$query = $this->db->get('');

		return $query;
	}


	function readKategoriThn($thn){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.tahun =', $thn);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaId($id){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readProses($id){
		$this->db->select('*');
		$this->db->from('tb_detail');
		$this->db->join('tb_kegiatan', 'tb_detail.id_kegiatan = tb_kegiatan.id_kegiatan');
		$this->db->join('tb_proses', 'tb_detail.id_proses = tb_proses.id_proses');
		$this->db->join('tb_user', 'tb_detail.id_user = tb_user.id_user');
		$this->db->where('tb_detail.id_mou =', $id);

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaAll(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->order_by('tb_mou.id_mou', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	function readKerjasamaAll1(){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_user', 'tb_mou.id_user = tb_user.id_user');
		$this->db->join('tb_kategori', 'tb_kategori.id_kategori = tb_mou.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->order_by('tb_mou.tahun', 'DESC');

		$query = $this->db->get('');

		return $query;
	}

	//CV Penelitian Artikel Ilmiah
	function readArtikelIlmiah(){
		$this->db->select('tb_artikel_ilmiah.tahun as datatahun, tb_artikel_ilmiah.semester as datasemester, tb_artikel_ilmiah.tahun as tahun, tb_artikel_ilmiah.judul_artikel as judul_artikel, tb_artikel_ilmiah.nama_jurnal as nama_jurnal, tb_artikel_ilmiah.volume_nomor_tahun as volume_nomor_tahun, tb_artikel_ilmiah.id_artikel_ilmiah as id, tb_user.nama as nama_user');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->join('tb_user', 'tb_artikel_ilmiah.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readArtikelIlmiahByID($id){
		$this->db->select('*');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->where('tb_artikel_ilmiah.id_artikel_ilmiah=',$id);

		$query = $this->db->get('');

		return $query;
	}


	function sortTahunArtikelIlmiah($value){
		$this->db->select('tb_artikel_ilmiah.tahun as datatahun, tb_artikel_ilmiah.semester as datasemester, tb_artikel_ilmiah.tahun as tahun, tb_artikel_ilmiah.judul_artikel as judul_artikel, tb_artikel_ilmiah.nama_jurnal as nama_jurnal, tb_artikel_ilmiah.volume_nomor_tahun as volume_nomor_tahun, tb_artikel_ilmiah.id_artikel_ilmiah as id, tb_user.nama as nama_user');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->join('tb_user', 'tb_artikel_ilmiah.id_user = tb_user.id_user');
		$this->db->where('tb_artikel_ilmiah.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterArtikelIlmiah($value){
		$this->db->select('tb_artikel_ilmiah.tahun as datatahun, tb_artikel_ilmiah.semester as datasemester, tb_artikel_ilmiah.tahun as tahun, tb_artikel_ilmiah.judul_artikel as judul_artikel, tb_artikel_ilmiah.nama_jurnal as nama_jurnal, tb_artikel_ilmiah.volume_nomor_tahun as volume_nomor_tahun, tb_artikel_ilmiah.id_artikel_ilmiah as id, tb_user.nama as nama_user');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->join('tb_user', 'tb_artikel_ilmiah.id_user = tb_user.id_user');
		$this->db->where('tb_artikel_ilmiah.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllArtikelIlmiahh($tahun,$semester){
		$this->db->select('tb_artikel_ilmiah.tahun as datatahun, tb_artikel_ilmiah.semester as datasemester, tb_artikel_ilmiah.tahun as tahun, tb_artikel_ilmiah.judul_artikel as judul_artikel, tb_artikel_ilmiah.nama_jurnal as nama_jurnal, tb_artikel_ilmiah.volume_nomor_tahun as volume_nomor_tahun, tb_artikel_ilmiah.id_artikel_ilmiah as id, tb_user.nama as nama_user');
		$this->db->from('tb_artikel_ilmiah');
		$this->db->join('tb_user', 'tb_artikel_ilmiah.id_user = tb_user.id_user');
		$this->db->where('tb_artikel_ilmiah.tahun=', $tahun);
		$this->db->where('tb_artikel_ilmiah.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	//CV Publikasi Seminar
	function readPublikasiSeminar(){
		$this->db->select('tb_publikasi_seminar.nama_pertemuan as nama_pertemuan, tb_publikasi_seminar.judul_artikel as judul_artikel, tb_publikasi_seminar.waktu as waktu, tb_publikasi_seminar.tempat as tempat, tb_publikasi_seminar.id_publikasi_seminar as id, tb_user.nama as nama_user');
		$this->db->from('tb_publikasi_seminar');
		$this->db->join('tb_user', 'tb_publikasi_seminar.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readPublikasiSeminarByID($id){
		$this->db->select('*');
		$this->db->from('tb_publikasi_seminar');
		$this->db->where('tb_publikasi_seminar.id_publikasi_seminar=',$id);

		$query = $this->db->get('');

		return $query;
	}


	function sortTahunPublikasiSeminar($value){
		$this->db->select('tb_publikasi_seminar.nama_pertemuan as nama_pertemuan, tb_publikasi_seminar.judul_artikel as judul_artikel, tb_publikasi_seminar.waktu as waktu, tb_publikasi_seminar.tempat as tempat, tb_publikasi_seminar.id_publikasi_seminar as id, tb_user.nama as nama_user');
		$this->db->from('tb_publikasi_seminar');
		$this->db->join('tb_user', 'tb_publikasi_seminar.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi_seminar.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterPublikasiSeminar($value){
		$this->db->select('tb_publikasi_seminar.nama_pertemuan as nama_pertemuan, tb_publikasi_seminar.judul_artikel as judul_artikel, tb_publikasi_seminar.waktu as waktu, tb_publikasi_seminar.tempat as tempat, tb_publikasi_seminar.id_publikasi_seminar as id, tb_user.nama as nama_user');
		$this->db->from('tb_publikasi_seminar');
		$this->db->join('tb_user', 'tb_publikasi_seminar.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi_seminar.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllPublikasiSeminar($tahun,$semester){
		$this->db->select('tb_publikasi_seminar.nama_pertemuan as nama_pertemuan, tb_publikasi_seminar.judul_artikel as judul_artikel, tb_publikasi_seminar.waktu as waktu, tb_publikasi_seminar.tempat as tempat, tb_publikasi_seminar.id_publikasi_seminar as id, tb_user.nama as nama_user');
		$this->db->from('tb_publikasi_seminar');
		$this->db->join('tb_user', 'tb_publikasi_seminar.id_user = tb_user.id_user');
		$this->db->where('tb_publikasi_seminar.tahun=', $tahun);
		$this->db->where('tb_publikasi_seminar.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}

	//CV Karya Buku
	function readKaryaBuku(){
		$this->db->select('tb_karya_buku.judul_buku as judul_buku, tb_karya_buku.tahun as tahun, tb_karya_buku.jumlah_halaman as jumlah_halaman, tb_karya_buku.penerbit as penerbit, tb_karya_buku.id_karya_buku as id, tb_user.nama as nama_user');
		$this->db->from('tb_karya_buku');
		$this->db->join('tb_user', 'tb_karya_buku.id_user = tb_user.id_user');

		$query = $this->db->get('');

		return $query;
	}

	function readKaryaBukuByID($id){
		$this->db->select('*');
		$this->db->from('tb_karya_buku');
		$this->db->where('tb_karya_buku.id_karya_buku=',$id);

		$query = $this->db->get('');

		return $query;
	}


	function sortTahunKaryaBuku($value){
		$this->db->select('tb_karya_buku.judul_buku as judul_buku, tb_karya_buku.tahun as tahun, tb_karya_buku.jumlah_halaman as jumlah_halaman, tb_karya_buku.penerbit as penerbit, tb_karya_buku.id_karya_buku as id, tb_user.nama as nama_user');
		$this->db->from('tb_karya_buku');
		$this->db->join('tb_user', 'tb_karya_buku.id_user = tb_user.id_user');
		$this->db->where('tb_karya_buku.tahun=', $value);

		$query = $this->db->get('');

		return $query;
	}

	function sortSemesterKaryaBuku($value){
		$this->db->select('tb_karya_buku.judul_buku as judul_buku, tb_karya_buku.tahun as tahun, tb_karya_buku.jumlah_halaman as jumlah_halaman, tb_karya_buku.penerbit as penerbit, tb_karya_buku.id_karya_buku as id, tb_user.nama as nama_user');
		$this->db->from('tb_karya_buku');
		$this->db->join('tb_user', 'tb_karya_buku.id_user = tb_user.id_user');
		$this->db->where('tb_karya_buku.semester=', $value);

		$query = $this->db->get('');

		return $query;
	}
	
	function sortAllKayaBuku($tahun,$semester){
		$this->db->select('datasemester, tb_karya_buku.judul_buku as judul_buku, tb_karya_buku.tahun as tahun, tb_karya_buku.jumlah_halaman as jumlah_halaman, tb_karya_buku.penerbit as penerbit, tb_karya_buku.id_karya_buku as id, tb_user.nama as nama_user');
		$this->db->from('tb_karya_buku');
		$this->db->join('tb_user', 'tb_karya_buku.id_user = tb_user.id_user');
		$this->db->where('tb_karya_buku.tahun=', $tahun);
		$this->db->where('tb_karya_buku.semester=', $semester);

		$query = $this->db->get('');

		return $query;
	}


	function readDash($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv1($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur1 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv2($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur2 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv3($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur3 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv4($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur4 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv5($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur5 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv6($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur6 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv7($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur7 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readSurv8($nilai,$lvl){
		$this->db->select('*');
		$this->db->from('tb_survei');
		$this->db->join('tb_user','tb_user.id_user = tb_survei.id_user');
		$this->db->where('tb_survei.sur8 =', $nilai);
		$this->db->where('tb_user.level =', $lvl);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readDash1($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_mou.id_user =', $this->session->userdata('iduser'));
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);

		$query = $this->db->get('');

		return $query->num_rows();
	}

	function readDash2($kat,$date1,$date2){
		$this->db->select('*');
		$this->db->from('tb_mou');
		$this->db->join('tb_kategori', 'tb_mou.id_kategori = tb_kategori.id_kategori');
		$this->db->where('tb_kategori.kategori =', $kat);
		$this->db->where('tb_mou.tgl_usul >=', $date1);
		$this->db->where('tb_mou.tgl_usul <=', $date2);
		$this->db->where('tb_mou.status = 1');

		$query = $this->db->get('');

		return $query->num_rows();
	}


	function readMasukTot($date1, $date2){
		$this->db->select('SUM(tb_penerimaan.jumlah * tb_harga.harga) AS total');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$query = $this->db->get('');

		foreach ($query->result() as $result){
			$total = $result->total;
		}
		return $total;
	}

	function readTabMasuk($date1, $date2){
		$this->db->select('tb_penerimaan.kode_obat AS kode, tb_farmasi.nama AS obat, tb_penerimaan.jumlah AS jumlah, tb_harga.harga AS harga, tb_faktur.tgl_faktur AS tgl, tb_faktur.sumber_dana AS dana');
		$this->db->from('tb_penerimaan');
		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->join('tb_farmasi', 'tb_penerimaan.kode_obat = tb_harga.kode_harga AND tb_harga.kode_obat=tb_farmasi.kode');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$query = $this->db->get();

		return $query;
	}

	function readMasuk($date1, $date2, $dana){
		$this->db->select('SUM(tb_penerimaan.jumlah * tb_harga.harga) AS total');
		$this->db->from('tb_penerimaan');

		$this->db->join('tb_harga', 'tb_penerimaan.kode_obat = tb_harga.kode_harga');
		$this->db->join('tb_faktur', 'tb_penerimaan.no_faktur = tb_faktur.no_faktur');
		$this->db->where('tb_faktur.tgl_faktur <=', $date2);
		$this->db->where('tb_faktur.tgl_faktur >=', $date1);
		$this->db->where('tb_faktur.sumber_dana =', $dana);
		$query = $this->db->get();

		foreach ($query->result() as $result){
			$harga = $result->total;
		}
		return $harga;
	}

	function readKembali($date1, $date2){
		$this->db->select('tb_pengembalian.no_faktur, tb_faktur_kembali.no_faktur_kembali, tb_faktur_kembali.tanggal');
		$this->db->from('tb_pengembalian');
		$this->db->join('tb_faktur_kembali', 'tb_pengembalian.no_faktur = tb_faktur_kembali.no_faktur_kembali');
		$this->db->where('tb_faktur_kembali.tanggal <=', $date2);
		$this->db->where('tb_faktur_kembali.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function readKeluar($date1, $date2){
		$this->db->select('tb_pengeluaran.no_faktur_keluar, tb_faktur_keluar.no_sbbk, tb_faktur_keluar.tanggal');
		$this->db->from('tb_pengeluaran');
		$this->db->join('tb_faktur_keluar', 'tb_pengeluaran.no_faktur_keluar = tb_faktur_keluar.no_sbbk');
		$this->db->where('tb_faktur_keluar.tanggal <=', $date2);
		$this->db->where('tb_faktur_keluar.tanggal >=', $date1);
		$query = $this->db->get();

		return $query->num_rows();
	}

	function read($table, $cond, $ordField, $ordType){
		if($cond!=null){
			$this->db->where($cond);
		}
		if($ordField!=null){
			$this->db->order_by($ordField, $ordType);
		}
		$query = $this->db->get($table);
		return $query;
	}

	function readPaging($table, $cond, $ordField, $ordType, $limit, $start){
		if($cond!=null){
			$this->db->where($cond);
		}
		if($ordField!=null){
			$this->db->order_by($ordField, $ordType);
		}
		$query = $this->db->get($table, $limit, $start);
		return $query;
	}

	function totalData($table){
		$query = $this->db->get($table);
		return $query->num_rows();
	}

	function update($cond, $table, $data){
		$this->db->where($cond);
		$query = $this->db->update($table, $data);
		return $query;
	}

	function delete($cond, $table){
		$this->db->where($cond);
		$query = $this->db->delete($table);
		return $query;
	}

	//profile
	function readProfile(){
		$this->db->select('tb_profile.nama_web as nama_web, tb_profile.email as email, 
			tb_profile.no_hp as no_hp,
			tb_profile.alamat as alamat,
			tb_profile.link_fb as link_fb,
			tb_profile.link_twitter as link_twitter,
			tb_profile.link_ig as link_ig,
			tb_profile.link_linkedin as link_linkedin,
			tb_profile.desc_laradem as desc_laradem
			');
		$this->db->from('tb_profile');
		$query = $this->db->get('');
		return $query;
	}
	function readProfileByID($id){
		$this->db->select('*');
		$this->db->from('tb_profile');
		$this->db->where('tb_profile.id_profile=',$id);
		$query = $this->db->get('');
		return $query;
	}
	function getProfil(){
		return $this->db->get('tb_profile');
	}
	//publish
	function getPublish(){
		return $this->db->get('tb_publish');
	}
	function getPublishLimit(){
		return $this->db->get('tb_publish', 4);
	}
	function readPublish(){
		$this->db->select('
			tb_publish.id_publish as id,
			tb_publish.judul as judul, 
			tb_publish.jenis as jenis, 
			tb_publish.tanggal as tanggal, 
			tb_publish.publisher as publisher, 
			tb_publish.link as link');
		$this->db->from('tb_publish');
		$query = $this->db->get('');
		return $query;
	}
	function readFilePublish($id){
		$this->db->select('*');
		$this->db->from('tb_publish');
		$this->db->where('tb_publish.id_publish=', $id);
		$query = $this->db->get('');
		return $query;
	}

	function readPublishByID($id){
		$this->db->select('*');
		$this->db->from('tb_publish');
		$this->db->where('tb_publish.id_publish=',$id);
		$query = $this->db->get('');
		return $query;
	}
	//On Going penelitian
	function readOngoing(){
		$this->db->select('
			tb_ongoing.id_ongoing as id,
			tb_ongoing.nama_penelitian as nama_penelitian,
			tb_ongoing.desc_ongoing as desc_ongoing, ');
		$this->db->from('tb_ongoing');
		$query = $this->db->get('');
		return $query;
	}
	function readFileOngoing($id){
		$this->db->select('*');
		$this->db->from('tb_ongoing');
		$this->db->where('tb_ongoing.id_ongoing=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function readOngoingByID($id){
		$this->db->select('*');
		$this->db->from('tb_ongoing');
		$this->db->where('tb_ongoing.id_ongoing=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function getOngoing(){
		return $this->db->get('tb_ongoing');
	}
	//ourteam
	function readTeam(){
		$this->db->select('tb_ourteam.nama as nama, tb_ourteam.status as status, tb_ourteam.nip as nip, 
		tb_ourteam.id_ourteam as id');
		$this->db->from('tb_ourteam');
		$query = $this->db->get('');
		return $query;
	}
	
	function readFileTeam($id,$jenis){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_ourteam=', $id);
		$this->db->where('file_kegiatan.kategori_file=', $jenis);
		$query = $this->db->get('');
		return $query;
	}
	function readFileTeamALL($id){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_ourteam=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function readTeamByID($id){
		$this->db->select('*');
		$this->db->from('tb_ourteam');
		$this->db->where('tb_ourteam.id_ourteam=',$id);
		$query = $this->db->get('');
		return $query;
	}
	function readFileTeamID($id){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->where('file_kegiatan.id_file_kegiatan=', $id);
		$query = $this->db->get('');
		return $query;
	}
	function getAboutUs(){
		$this->db->select('*');
		$this->db->from('file_kegiatan');
		$this->db->join('tb_ourteam','file_kegiatan.id_ourteam=tb_ourteam.id_ourteam');
		$query = $this->db->get('');
		return $query;
	}
	//galery
	function readGalery(){
		$this->db->select('*');
		$this->db->from('file_galery');
		$query = $this->db->get('');
		return $query;
	}

	function readFileGalery($file1, $file2){
		$this->db->select('*');
		$this->db->from('file_galery');
		$this->db->where('nama_file=', $file1);
		$this->db->or_where('nama_file=', $file2);

		$query = $this->db->get('');

		return $query;
	}

	function readDataGalery(){
		$this->db->select('*');
		$this->db->from('file_galery');

		$query = $this->db->get('');

		return $query;
	}
	function getGalery(){
		return $this->db->get('file_galery');
	}

	function readDataGaleryId($id){
		$this->db->select('file_galery.nama_file');
		$this->db->from('file_galery');
		$this->db->where('file_galery.id_galery=', $id);
		$query = $this->db->get('');

		return $query;
	}

	function readGaleryID($id){
		$this->db->select('*');
		$this->db->from('file_galery');
		$this->db->where('id_galery=', $id);

		$query = $this->db->get('');

		return $query;
	}


	function readGaleryByID($tabel,$idtabel,$id){
		$this->db->select('*');
		$this->db->from($tabel);
		$this->db->where($idtabel.'=', $id);
		$query = $this->db->get('');

		return $query;
	}

	

}