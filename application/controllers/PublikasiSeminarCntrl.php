<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PublikasiSeminarCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'PENDATAAN CV - PUBLIKASI SEMINAR',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('publikasi-seminar', $data);
	}

	public function ebkd()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'E-BKD - Penelitian',
			'date' => date('l, d-m-Y', strtotime("now")),
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('bkdpenelitian', $data);
	}

	public function getBkd()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPenelitian($iddosen)
		];

		return $this->load->view('tabel-bkd-penelitian', $data);
	}

	public function sortingBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readBkdPenelitian($iddosen);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortBkdSemesterPenelitian($semester,$iddosen);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortBkdTahunPenelitian($tahun,$iddosen);
		}else{
			$sorting = $this->Crud->sortBkdAllPenelitian($tahun,$semester,$iddosen);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-bkd-penelitian', $data);
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array('id_penelitian'=>$id), 'tb_penelitian', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPublikasiSeminar()
		];
		return $this->load->view('tabel-publikasi-seminar', $data);
	}

	public function getFileTable()
	{	
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($id,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function addData()
	{
		$data = [
			'nama_pertemuan' => $this->input->post('nama_pertemuan'),
			'judul_artikel' => $this->input->post('judul_artikel'),
			'waktu' => $this->input->post('waktu'),
			'tempat' => $this->input->post('tempat'),
			'id_user' => $this->input->post('dosen'),
		];

		$this->Crud->create('tb_publikasi_seminar',$data);

		$id = $this->Crud->readLast('tb_penelitian','id_penelitian');

		$this->upload_files($id,'skpenugasan','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['skpenugasan']);
		$this->upload_files($id,'laporan','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['laporan']);
		$this->upload_files($id,'dokumentasi','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['dokumentasi']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($id,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpenelitian = $this->input->get('idpenelitian');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePenelitianID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_penelitian'=>$id), 'file_penelitian');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($idpenelitian,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPublikasiSeminarByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama_pertemuan'=>$result->nama_pertemuan,
				'judul_artikel'=>$result->judul_artikel,
				'waktu'=>$result->waktu,
				'tempat'=>$result->tempat,
				'id_user'=>$result->id_user,
				'id_publikasi_seminar'=>$result->id_publikasi_seminar,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_pertemuan' => $this->input->post('edit_nama_pertemuan'),
			'judul_artikel' => $this->input->post('edit_judul_artikel'),
			'waktu' => $this->input->post('edit_waktu'),
			'tempat' => $this->input->post('edit_tempat'),
			'id_user' => $this->input->post('edit_dosen'),
		];

		$update = $this->Crud->update(array('id_publikasi_seminar'=>$id), 'tb_publikasi_seminar', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePenelitianALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_publikasi_seminar'=>$id), 'tb_publikasi_seminar');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPublikasiSeminar();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPublikasiSeminar($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPublikasiSeminar($tahun);
		}else{
			$sorting = $this->Crud->sortAllPublikasiSeminar($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-publikasi-seminar', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}