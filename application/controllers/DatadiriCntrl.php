<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DatadiriCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'SIDIA - DATA PRIBADI',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('datadiri', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readDatadiri()
		];
		return $this->load->view('tabel-datadiri', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePersonal($id,$kategori)
		];
		return $this->load->view('file-personal', $data);
	}

	public function addData()
	{
		$data = [
			'jenkel' => $this->input->post('jenkel'),
			'jabatan_fungsional' => $this->input->post('jabatan_fungsional'),
			'nip' => $this->input->post('nip'),
			'nidn' => $this->input->post('nidn'),
			'tempat' => $this->input->post('tempat'),
			'tanggal' => $this->input->post('tanggal'),
			'email' => $this->input->post('email'),
			'no_hp' => $this->input->post('no_hp'),
			'alamat_kantor' => $this->input->post('alamat'),
			'no_telp_kantor' => $this->input->post('no_telp'),
			's1' => $this->input->post('s1'),
			's2' => $this->input->post('s2'),
			's3' => $this->input->post('s3'),
			'matkul_diampu' => $this->input->post('matkul'),
			'bidang_keahlian' => $this->input->post('bidang'),
			'nosertif' => $this->input->post('nosertif'),
			'perguruan_tinggi' => $this->input->post('perguruan_tinggi'),
			'status' => $this->input->post('status'),
			'jurusan' => $this->input->post('jurusan'),
			'prodi' => $this->input->post('prodi'),
			'jabatan' => $this->input->post('jabatan'),
			'ilmu' => $this->input->post('ilmu'),
			'id_user' => $this->input->post('dosen'),
			
		];

		$this->Crud->create('tb_data_diri',$data);

		$id = $this->Crud->readLast('tb_data_diri','id_data_diri');
	}

	public function getData()
	{
		$id = $this->input->get('id');
		$query = $this->Crud->readDatadiriByID($id);
		foreach($query->result() as $result){
			$data = [
			'nama' => $result->nama,
			'jenkel' => $result->jenkel,
			'jabatan_fungsional' => $result->jabatan_fungsional,
			'nip' => $result->nip,
			'nidn' => $result->nidn,
			'tempat' => $result->tempat,
			'tanggal' => $result->tanggal,
			'email' => $result->email,
			'no_hp' => $result->no_hp,
			'alamat' => $result->alamat_kantor,
			'no_telp' => $result->no_telp_kantor,
			's1' => $result->s1,
			's2' => $result->s2,
			's3' => $result->s3,
			'matkul_diampu' => $result->matkul_diampu,
			'bidang' => $result->bidang_keahlian,
			'nosertif' => $result->nosertif,
			'perguruan_tinggi' => $result->perguruan_tinggi,
			'status' => $result->status,
			'jurusan' => $result->jurusan,
			'prodi' => $result->prodi,
			'jabatan' => $result->jabatan,
			'ilmu' => $result->ilmu,
			'id_user' => $result->id_user,
			'id_data_diri' => $id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'jenkel' => $this->input->post('edit_jenkel'),
			'jabatan_fungsional' => $this->input->post('edit_jabatan_fungsional'),
			'nip' => $this->input->post('edit_nip'),
			'nidn' => $this->input->post('edit_nidn'),
			'tempat' => $this->input->post('edit_tempat'),
			'tanggal' => $this->input->post('edit_tanggal'),
			'email' => $this->input->post('edit_email'),
			'no_hp' => $this->input->post('edit_no_hp'),
			'alamat_kantor' => $this->input->post('edit_alamat'),
			'no_telp_kantor' => $this->input->post('edit_no_telp'),
			's1' => $this->input->post('edit_s1'),
			's2' => $this->input->post('edit_s2'),
			's3' => $this->input->post('edit_s3'),
			'matkul_diampu' => $this->input->post('edit_matkul'),
			'nosertif' => $this->input->post('edit_nosertif'),
			'perguruan_tinggi' => $this->input->post('edit_perguruan_tinggi'),
			'status' => $this->input->post('edit_status'),
			'jurusan' => $this->input->post('edit_jurusan'),
			'prodi' => $this->input->post('edit_prodi'),
			'jabatan' => $this->input->post('edit_jabatan'),
			'ilmu' => $this->input->post('edit_ilmu'),
			'waktus' => $this->input->post('edit_waktus'),
			'id_user' => $this->input->post('editdosen'),
		];

		$update = $this->Crud->update(array('id_data_diri'=>$id), 'tb_data_diri', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_data_diri'=>$id), 'tb_data_diri');
	}
	
	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readDatadiri();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterDatadiri($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunDatadiri($tahun);
		}else{
			$sorting = $this->Crud->sortAllDatadiri($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-datadiri', $data);
	}
}