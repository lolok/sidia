<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EbkdCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'E-BKD - Cetak Laporan E-BKD',
			'date' => date('l, d-m-Y', strtotime("now")),
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null),
		];
		return $this->load->view('printbkd', $data);
	}

	public function getPrint()
	{
		$iddosen = $this->input->get('id');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$pengajaran = $this->Crud->readBkdPengajaran($iddosen);
			$pembimbingan = $this->Crud->readBkdPembimbingan($iddosen,1);
			$pengujian = $this->Crud->readBkdPembimbingan($iddosen,2);
			$penelitian = $this->Crud->readBkdPenelitian($iddosen);
			$pkm = $this->Crud->readBkdPkm($iddosen);
			$penunjang = $this->Crud->readBkdPenunjang($iddosen);
		}else if ($tahun == 'all') {
			$pengajaran = $this->Crud->sortBkdSemesterPengajaran($semester,$iddosen);
			$pembimbingan = $this->Crud->sortBkdSemesterPembimbingan($semester,$iddosen,1);
			$pengujian = $this->Crud->sortBkdSemesterPembimbingan($semester,$iddosen,2);
			$penelitian = $this->Crud->sortBkdSemesterPenelitian($semester,$iddosen);
			$pkm = $this->Crud->sortBkdSemesterPkm($semester,$iddosen);
			$penunjang = $this->Crud->sortBkdSemesterPenunjang($semester,$iddosen);
		}else if($semester == 'all'){
			$pengajaran = $this->Crud->sortBkdTahunPengajaran($tahun,$iddosen);
			$pembimbingan = $this->Crud->sortBkdTahunPembimbingan($tahun,$iddosen,1);
			$pengujian = $this->Crud->sortBkdTahunPembimbingan($tahun,$iddosen,2);
			$penelitian = $this->Crud->sortBkdTahunPenelitian($tahun,$iddosen);
			$pkm = $this->Crud->sortBkdTahunPkm($tahun,$iddosen);
			$penunjang = $this->Crud->sortBkdTahunPenunjang($tahun,$iddosen);
		}else{
			$pengajaran = $this->Crud->sortBkdAllPengajaran($tahun,$semester,$iddosen);
			$pembimbingan = $this->Crud->sortBkdAllPembimbingan($tahun,$semester,$iddosen,1);
			$pengujian = $this->Crud->sortBkdAllPembimbingan($tahun,$semester,$iddosen,2);
			$penelitian = $this->Crud->sortBkdAllPenelitian($tahun,$semester,$iddosen);
			$pkm = $this->Crud->sortBkdAllPkm($tahun,$semester,$iddosen);
			$penunjang = $this->Crud->sortBkdAllPenunjang($tahun,$semester,$iddosen);
		}

		$data = [
			'pengajaran' => $pengajaran,
			'pembimbingan' => $pembimbingan,
			'pengujian' => $pengujian,
			'penelitian' => $penelitian,
			'pkm' => $pkm,
			'penunjang' => $penunjang,
		];

		$this->load->view('printebkd', $data);
	}

	public function getPendidikan()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'pengajaran' => $this->Crud->readBkdPengajaran($iddosen),
			'pembimbingan' => $this->Crud->readBkdPembimbingan($iddosen,1),
			'pengujian' => $this->Crud->readBkdPembimbingan($iddosen,2),
		];

		return $this->load->view('tabel-bkd-print-pendidikan', $data);
	}

	public function getPenelitian()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPenelitian($iddosen)
		];

		return $this->load->view('tabel-bkd-print-penelitian', $data);
	}

	public function getPkm()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPkm($iddosen)
		];

		return $this->load->view('tabel-bkd-print-pengabdian', $data);
	}

	public function getPenunjang()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPenunjang($iddosen)
		];

		return $this->load->view('tabel-bkd-print-penunjang', $data);
	}

	public function sortPendidikan()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$pengajaran = $this->Crud->readBkdPengajaran($iddosen);
			$pembimbingan = $this->Crud->readBkdPembimbingan($iddosen,1);
			$pengujian = $this->Crud->readBkdPembimbingan($iddosen,2);
		}else if ($tahun == 'all') {
			$pengajaran = $this->Crud->sortBkdSemesterPengajaran($semester,$iddosen);
			$pembimbingan = $this->Crud->sortBkdSemesterPembimbingan($semester,$iddosen,1);
			$pengujian = $this->Crud->sortBkdSemesterPembimbingan($semester,$iddosen,2);
		}else if($semester == 'all'){
			$pengajaran = $this->Crud->sortBkdTahunPengajaran($tahun,$iddosen);
			$pembimbingan = $this->Crud->sortBkdTahunPembimbingan($tahun,$iddosen,1);
			$pengujian = $this->Crud->sortBkdTahunPembimbingan($tahun,$iddosen,2);
		}else{
			$pengajaran = $this->Crud->sortBkdAllPengajaran($tahun,$semester,$iddosen);
			$pembimbingan = $this->Crud->sortBkdAllPembimbingan($tahun,$semester,$iddosen,1);
			$pengujian = $this->Crud->sortBkdAllPembimbingan($tahun,$semester,$iddosen,2);
		}

		$data = [
			'pengajaran' => $pengajaran,
			'pembimbingan' => $pembimbingan,
			'pengujian' => $pengujian,
		];

		return $this->load->view('tabel-bkd-print-pendidikan', $data);
	}

	public function sortPenelitian()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$datas = $this->Crud->readBkdPenelitian($iddosen);
		}else if ($tahun == 'all') {
			$datas = $this->Crud->sortBkdSemesterPenelitian($semester,$iddosen);
		}else if($semester == 'all'){
			$datas = $this->Crud->sortBkdTahunPenelitian($tahun,$iddosen);
		}else{
			$datas = $this->Crud->sortBkdAllPenelitian($tahun,$semester,$iddosen);
		}

		$data = [
			'tabel' => $datas,
		];

		return $this->load->view('tabel-bkd-print-penelitian', $data);
	}

	public function sortPkm()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$datas = $this->Crud->readBkdPkm($iddosen);
		}else if ($tahun == 'all') {
			$datas = $this->Crud->sortBkdSemesterPkm($semester,$iddosen);
		}else if($semester == 'all'){
			$datas = $this->Crud->sortBkdTahunPkm($tahun,$iddosen);
		}else{
			$datas = $this->Crud->sortBkdAllPkm($tahun,$semester,$iddosen);
		}

		$data = [
			'tabel' => $datas,
		];

		return $this->load->view('tabel-bkd-print-pengabdian', $data);
	}

	public function sortPenunjang()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$datas = $this->Crud->readBkdPenunjang($iddosen);
		}else if ($tahun == 'all') {
			$datas = $this->Crud->sortBkdSemesterPenunjang($semester,$iddosen);
		}else if($semester == 'all'){
			$datas = $this->Crud->sortBkdTahunPenunjang($tahun,$iddosen);
		}else{
			$datas = $this->Crud->sortBkdAllPenunjang($tahun,$semester,$iddosen);
		}

		$data = [
			'tabel' => $datas,
		];

		return $this->load->view('tabel-bkd-print-penunjang', $data);
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$tabel = $this->input->get('tabel');
		$idname = $this->input->get('idname');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array($idname=>$id), $tabel, $data);
	}

	// public function print(){
	// 	$yearnow = (int)date('Y', strtotime('now'));
	// 	$yearstart = 2016;
	// 	$diff = $yearnow-$yearstart;
	// 	for ($i=0; $i <= $diff; $i++) { 
	// 		$tahun = $yearnow-$i;
	// 		$year[] = (object)[
	// 			'year' => $tahun,
	// 		];
	// 	}
	// 	$data = [
	// 		'title' => 'E-BKD - Cetak Laporan E-BKD',
	// 		'date' => date('l, d-m-Y', strtotime("now")),
	// 		'matkul' => $this->Crud->read('tb_matkul',null,null,null),
	// 		'tahun' => $year,
	// 		'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null),
	// 	];
	// 	return $this->load->view('printbkd', $data);
	// }
	//public function print(){
		//$yearnow = (int)date('Y', strtotime('now'));
		//$yearstart = 2016;
		//$diff = $yearnow-$yearstart;
		//for ($i=0; $i <= $diff; $i++) { 
			//$tahun = $yearnow-$i;
			//$year[] = (object)[
				//'year' => $tahun,
			//];
		//}
		//$data = [
			//'title' => 'E-BKD - Cetak Laporan E-BKD',
			//'date' => date('l, d-m-Y', strtotime("now")),
			//'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			//'tahun' => $year,
			//'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null),
		//];
		//return $this->load->view('printbkd', $data);
	//}

}