<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TeamCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'Additional Info - Our Team',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('ourteam', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readTeam()
		];
		return $this->load->view('tabel-ourteam', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFileTeam($id,$kategori)
		];
		return $this->load->view('file-kegiatan', $data);
	}

	public function addData()
	{
		$data = [
			'nama' => $this->input->post('nama'),
			'status' => $this->input->post('status'),
			'nip' => $this->input->post('nip'),
			
		];

		$this->Crud->create('tb_ourteam',$data);
		$id = $this->Crud->readLast('tb_ourteam','id_ourteam');
		$this->upload_files($id,'photo','id_ourteam','file_kegiatan','assets/file/photo/','profile', $_FILES['filekontrak']);
		
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_ourteam','file_kegiatan','assets/file/tridarma/kegiatan/','kegiatan', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFileTeam($id,$kategori)
		];
		return $this->load->view('file-kegiatan', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$id_ourteam = $this->input->get('id_ourteam');
		$kategori = $this->input->get('kategori');
		$data = $this->Crud->readFileTeamID($id);
		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_kegiatan'=>$id), 'file_kegiatan');
		

		$data = [
			'tabel_file' => $this->Crud->readFileTeam($id_ourteam,$kategori)
		];
		return $this->load->view('file-kegiatan', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readTeamByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama'=>$result->nama,
				'status'=>$result->status,
				'nip'=>$result->nip,
				'id_ourteam'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama' => $this->input->post('editnama'),
			'status' => $this->input->post('editstatus'),
			'nip' => $this->input->post('editnip'),
		];

		$update = $this->Crud->update(array('id_ourteam'=>$id), 'tb_ourteam', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFileTeamALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_ourteam'=>$id), 'tb_ourteam');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readTeam();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterTeam($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunTeam($tahun);
		}else{
			$sorting = $this->Crud->sortAllTeam($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-ourteam', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => 'profile',
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}