<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PembimbingCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - MEMBIMBING DAN MENGUJI',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('pembimbingan', $data);
	}

	public function getTabel()
	{
		$level = $this->session->userdata('level');
		$id = $this->session->userdata('iduser');

		if ($level == 1) {
			$data = [
				'tabel' => $this->Crud->readPembimbingan()
			];
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPembimbingan1($id)
			];
		}

		return $this->load->view('tabel-pembimbingan', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePembimbingan($id,$kategori)
		];
		return $this->load->view('file-pembimbingan', $data);
	}

	public function getFileBerkasTable()
	{
		$id = $this->input->post('id');
		$file1 = $this->input->post('file1');
		$file2 = $this->input->post('file2');

		$data = [
			'berkas' => $this->Crud->readFileBerkas($file1,$file2),
		];

		return $this->load->view('file-berkas-data', $data);
	}

	public function getMhs()
	{
		$id = $this->input->get('id');

		$data = [
			'data_dosen' => $this->Crud->readMhsPembimbingan($id)
		];

		return $this->load->view('tabel-mhs-pembimbingan', $data);
	}

	public function addMhs()
	{
		$id = $this->input->post('id_data');

		$param = [
			'nim' => $this->input->post('nim'),
			'nama' => $this->input->post('nama'),
			'id_pembimbingan' => $id,
		];

		$this->Crud->create('tb_mhs_pembimbingan',$param);

		$data = [
			'data_dosen' => $this->Crud->readMhsPembimbingan($id)
		];

		return $this->load->view('tabel-mhs-pembimbingan', $data);
	}

	public function hapusMhs()
	{
		$id = $this->input->get('id');
		$iddata = $this->input->get('iddata');

		// $matkul = $this->Crud->read('tb_matkul', ['id_matkul' => $idmatkul], null, null);
		// foreach ($matkul->result() as $key) {
		// 	$dosen = $key->dosen;
		// }
		// $minusdosen = $dosen-1;

		// $update = $this->Crud->update(array('id_matkul'=>$idmatkul), 'tb_matkul', ['dosen' => $minusdosen]);

		$delete = $this->Crud->delete(array('id_mhs_pembimbingan'=>$id), 'tb_mhs_pembimbingan');

		$data = [
			'data_dosen' => $this->Crud->readMhsPembimbingan($iddata)
		];

		return $this->load->view('tabel-mhs-pembimbingan', $data);
	}

	public function getBerkas()
	{
		$data = $this->Crud->readDataBerkas(1,3);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkasId()
	{
		$val = $this->input->post('val');
		$data = $this->Crud->readDataBerkasId(1,3,$val);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}

		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function addData()
	{
		$data = [
			'kategori' => $this->input->post('kategori'),
			'jenis' => $this->input->post('jenis'),
			'masa_penugasan' => $this->input->post('masa_penugasan'),
			'id_user' => $this->input->post('dosen'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
		];

		$this->Crud->create('tb_pembimbingan',$data);

		$id = $this->Crud->readLast('tb_pembimbingan','id_pembimbingan');

		$this->upload_files($id,'membimbing','id_pembimbingan','file_pembimbingan','assets/file/tridarma/bimbingan/','pembimbingan', $_FILES['filemembimbing']);
		$this->upload_files($id,'menguji','id_pembimbingan','file_pembimbingan','assets/file/tridarma/bimbingan/','pembimbingan', $_FILES['filemenguji']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_pembimbingan','file_pembimbingan','assets/file/tridarma/bimbingan/','pembimbingan', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePembimbingan($id,$kategori)
		];
		return $this->load->view('file-pembimbingan', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpengajaran = $this->input->get('idpengajaran');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePembimbinganID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_pembimbingan'=>$id), 'file_pembimbingan');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePembimbingan($idpengajaran,$kategori)
		];
		return $this->load->view('file-pembimbingan', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPembimbinganByID($id);
		foreach($query->result() as $result){
			$data = [
				'kategori'=>$result->kategori,
				'jenis'=>$result->jenis,
				'masa_penugasan'=>$result->masa_penugasan,
				'id_user'=>$result->id_user,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
				'id_pembimbingan'=>$id
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'kategori' => $this->input->post('editkategori'),
			'jenis' => $this->input->post('editjenis'),
			'masa_penugasan' => $this->input->post('editmasa_penugasan'),
			'id_user' => $this->input->post('editdosen'),
			'semester' => $this->input->post('editsemester'),
			'tahun' => $this->input->post('edittahun'),
			'bukti_penugasan' => $this->input->post('editbukti_penugasan'),
			'bukti_dokumen' => $this->input->post('editbukti_dokumen'),
		];

		$update = $this->Crud->update(array('id_pembimbingan'=>$id), 'tb_pembimbingan', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePembimbinganALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_pembimbingan'=>$id), 'tb_pembimbingan');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPembimbingan();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPembimbingan($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPembimbingan($tahun);
		}else{
			$sorting = $this->Crud->sortAllPembimbingan($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-pembimbingan', $data);
	}

	public function getBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$kategori = $this->input->get('kategori');

		if ($kategori == 1) {
			$data = [
				'tabel' => $this->Crud->readBkdPembimbingan($iddosen,$kategori)
			];

			return $this->load->view('tabel-bkd-bimbingan', $data);
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPembimbingan($iddosen,$kategori)
			];

			return $this->load->view('tabel-bkd-menguji', $data);
		}
		
	}

	public function sortingBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');
		$kategori = $this->input->get('kategori');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readBkdPembimbingan($iddosen,$kategori);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortBkdSemesterPembimbingan($semester,$iddosen,$kategori);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortBkdTahunPembimbingan($tahun,$iddosen,$kategori);
		}else{
			$sorting = $this->Crud->sortBkdAllPembimbingan($tahun,$semester,$iddosen,$kategori);
		}

		if ($kategori == 1) {
			$data = [
				'tabel' => $this->Crud->readBkdPembimbingan($iddosen,$kategori)
			];

			return $this->load->view('tabel-bkd-bimbingan', $data);
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPembimbingan($iddosen,$kategori)
			];

			return $this->load->view('tabel-bkd-menguji', $data);
		}
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array('id_pembimbingan'=>$id), 'tb_pembimbingan', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}