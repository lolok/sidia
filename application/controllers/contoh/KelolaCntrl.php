<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['tabuser']=$this->Crud->read('tb_user',null,'id_user','DESC');

		$this->load->view('hal_kelola', $data);

	}

	public function tambahPengguna(){

		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$nama = $this->input->post('nama');
		mkdir('./assets/file/'.$username);
		mkdir('./assets/file/'.$username.'/mou');
		mkdir('./assets/file/'.$username.'/berkas');
		mkdir('./assets/file/'.$username.'/naskah');
		$kategori = 3;
		$data  = array('username'=>$username, 'password'=>$password, 'nama'=>$nama, 'level'=>$kategori); 
		$insert = $this->Crud->create('tb_user', $data);
		redirect(site_url('KelolaCntrl'), 'refresh');
	}

	public function getData($id){
		$query = $this->Crud->read('tb_user', array('id_user'=>$id), null, null);
		foreach($query->result() as $result){
			$data = array('username'=>$result->username,
				'nama'=>$result->nama,
				'email'=>$result->email,
				'nohp'=>$result->nohp,
				'namaper'=>$result->namaper,
				'emailper'=>$result->emailper,
				'provinsi'=>$result->provinsi,
				'kota'=>$result->kota,
				'alamat'=>$result->alamat,
				'telp'=>$result->telp
				);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function cekUser(){
		$username   = $this->input->post('username');
		$match = $this->Crud->read('tb_user', array('username'=>$username), null, null);
		if($match->num_rows() > 0){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function cekData($table, $field, $data){
		$match = $this->MProfil->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$kategori   = $this->input->post('kategori');
		$data = array('kategori'=>$kategori);

		$update = $this->Crud->update(array('id_pengguna'=>$id), 'tb_pengguna', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $query = $this->Crud->read('tb_user', array('id_user'=>$id), null, null);
		foreach($query->result() as $result){
	    	delete_files('./assets/file/'.$result->username.'/mou/');
			rmdir('./assets/file/'.$result->username.'/mou/');
			delete_files('./assets/file/'.$result->username.'/berkas/');
			rmdir('./assets/file/'.$result->username.'/berkas/');
			rmdir('./assets/file/'.$result->username.'/');
		}
     	$delete = $this->Crud->delete(array('id_user'=>$id), 'tb_user');
    }

}
