<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarKerjasama extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabmou']=$this->Crud->readKerja();
		$data['tabkat1']=$this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']=$this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']=$this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']=$this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['negara']=$this->Crud->read('tb_negara',null,null,null);

		$this->load->view('hal_kerjasama', $data);
	}

	public function pilihKat(){
		$kategori = $this->input->post('kategori');
		$subkategori1 = $this->input->post('subkategori1');
		$subkategori2 = $this->input->post('subkategori2');
		$subkategori3 = $this->input->post('subkategori3');
		$subkategori4 = $this->input->post('subkategori4');
		$tahun = $this->input->post('tahun');
		$negara = $this->input->post('negara');
		

		if($kategori==1){
			$data['tabmou']=$this->Crud->readKategori($kategori,$subkategori1,$tahun,$negara);
		}else if($kategori==2){
			$data['tabmou']=$this->Crud->readKategori($kategori,$subkategori2,$tahun,$negara);
		}else if($kategori==3){
			$data['tabmou']=$this->Crud->readKategori($kategori,$subkategori3,$tahun,$negara);
		}else if($kategori==4){
			$data['tabmou']=$this->Crud->readKategori($kategori,$subkategori4,$tahun,$negara);
		}else if($kategori==0){
			$data['tabmou']=$this->Crud->readKategori($kategori,$subkategori1,$tahun,$negara);
		}
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabkat1']=$this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']=$this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']=$this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']=$this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['negara']=$this->Crud->read('tb_negara',null,null,null);

		$this->load->view('hal_kerjasama', $data);
	}	

}