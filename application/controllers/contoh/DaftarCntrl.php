<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DaftarCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		
		$this->sessionIn(); //cek session
		if($this->input->get('balasan')!=null){
			$data['report'] = 1;
		}else{
			$data['report'] = 0;
		}
		
		$this->load->view('hal_daftar', $data);
	}

	public function daftar(){
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$nama = $this->input->post('nama');
		$email = $this->input->post('email');
		$hape = $this->input->post('hape');
		$namaper = $this->input->post('namaper');
		$emailper = $this->input->post('emailper');
		$provinsi = $this->input->post('provinsi');
		$kota = $this->input->post('kota');
		$alamat = $this->input->post('alamat');
		$telp = $this->input->post('telp');
		mkdir('./assets/file/'.$username);
		mkdir('./assets/file/'.$username.'/mou');
		mkdir('./assets/file/'.$username.'/berkas');
		$kategori = 2;
		$match = $this->Crud->read('tb_user', array('username'=>$username), null, null);
		if($match->num_rows() > 0){
			$data['report']=1;
			$this->load->view('hal_daftar', $data);
		}else{
			$data  = array('username'=>$username, 'password'=>$password, 'nama'=>$nama, 'email'=>$email, 'nohp'=>$hape, 'namaper'=>$namaper, 'emailper'=>$emailper, 'provinsi'=>$provinsi, 'kota'=>$kota, 'alamat'=>$alamat, 'telp'=>$telp, 'level'=>$kategori); 
			$insert = $this->Crud->create('tb_user', $data);
			$this->load->view('hal_berhasil');
		}
	}

	public function cekUser(){
		$username   = $this->input->post('username');
		$match = $this->Crud->read('tb_user', array('username'=>$username), null, null);
		if($match->num_rows() > 0){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function cekEmail(){
		$email   = $this->input->post('email');
		$match = $this->Crud->read('tb_user', array('email'=>$email), null, null);
		if($match->num_rows() > 0){
			echo 'false';
		}else{
			echo 'true';
		}
	}

	public function berhasil(){
		$this->load->view('hal_berhasil');
	}

}