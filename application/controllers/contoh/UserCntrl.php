<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UserCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data = [
			'title' => 'LPJ SM | User',
			'date' => date("l, d-m-Y", strtotime("now")),
		];
		$this->load->view('user', $data);
	}

	public function getCount()
	{
		$data = [
			'tot_pusat' => $this->Crud->readUserLvl(2),
			'tot_admin' => $this->Crud->readUserLvl(1),
			'tot_dpd' => $this->Crud->readUserLvl(3),
		];

		return $this->load->view('countuser', $data);
	}

	public function getUser()
	{
		$id = '1';
		$data = [
			'datauser' => $this->Crud->readUserId($id),
		];
		return $this->load->view('tabeluser', $data);
	}

	public function tambahUser()
	{
		$data = [
			'username' => $this->input->post('username_user'),
			'nama' => $this->input->post('nama_user'),
			'email' => $this->input->post('email_user'),
			'password' => md5($this->input->post('password_user')),
			'level' => $this->input->post('level_user'),
		];

		$this->Crud->create('user', $data);
	}

	public function getDataUser(){
		$id = $this->input->get('id');

		$query = $this->Crud->readUserById($id);
		foreach($query->result() as $result){
			$data = [
				'username'=>$result->username,
				'nama'=>$result->nama,
				'email'=>$result->email,
				'password'=>$result->password,
				'level'=>$result->level,
				'id_user'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editUser()
	{
		$id = $this->input->post('iduser');
		$old = $this->input->post('editpassword_userlama');
		$new = $this->input->post('editpassword_userbaru');

		if ($old == $new) {
			$data =[
				'username' => $this->input->post('editusername_user'),
				'nama' => $this->input->post('editnama_user'),
				'email' => $this->input->post('editemail_user'),
				'level' => $this->input->post('editlevel_user'),
			];
		}else{
			$data =[
				'username' => $this->input->post('editusername_user'),
				'nama' => $this->input->post('editnama_user'),
				'email' => $this->input->post('editemail_user'),
				'password' => md5($new),
				'level' => $this->input->post('editlevel_user'),
			];
		}

		$update = $this->Crud->update(array('id_user'=>$id), 'user', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function deleteUser()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_user'=>$id), 'user');
	}
}