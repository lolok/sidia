<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DashAdmin extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$kat1 = "PERGURUAN TINGGI";
		$kat2 = "PEMERINTAH";
		$kat3 = "LEMBAGA SWASTA";
		$kat4 = "DUNIA INDUSTRI";
		$data['tahun'] = date("Y", strtotime("now"));
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabmou']=$this->Crud->read('tb_mou',null,'id_mou',null);
		$data['tabmou1']=$this->Crud->readKerja();
		$data['tabter']=$this->Crud->readKerjaTer($data['tahun1'].'-01-01', $data['tahun1'].'-12-31');
		$data['tabter2']=$this->Crud->readKerjaTer($data['tahun2'].'-01-01', $data['tahun2'].'-12-31');
		$data['tabter3']=$this->Crud->readKerjaTer($data['tahun3'].'-01-01', $data['tahun3'].'-12-31');
		$data['tabter4']=$this->Crud->readKerjaTer($data['tahun4'].'-01-01', $data['tahun4'].'-12-31');
		$data['tabter5']=$this->Crud->readKerjaTer($data['tahun5'].'-01-01', $data['tahun5'].'-12-31');
		$data['tabkerjasama']=$this->Crud->readKerjasamaAll();
		//survei
		$data['sur11']=$this->Crud->readSurv1(1,2);
		$data['sur12']=$this->Crud->readSurv2(1,2);
		$data['sur13']=$this->Crud->readSurv3(1,2);
		$data['sur14']=$this->Crud->readSurv4(1,2);
		$data['sur15']=$this->Crud->readSurv5(1,2);
		$data['sur16']=$this->Crud->readSurv6(1,2);
		$data['sur17']=$this->Crud->readSurv7(1,2);
		$data['sur18']=$this->Crud->readSurv8(1,2);
		//survei
		$data['sur21']=$this->Crud->readSurv1(2,2);
		$data['sur22']=$this->Crud->readSurv2(2,2);
		$data['sur23']=$this->Crud->readSurv3(2,2);
		$data['sur24']=$this->Crud->readSurv4(2,2);
		$data['sur25']=$this->Crud->readSurv5(2,2);
		$data['sur26']=$this->Crud->readSurv6(2,2);
		$data['sur27']=$this->Crud->readSurv7(2,2);
		$data['sur28']=$this->Crud->readSurv8(2,2);
		//survei
		$data['sur31']=$this->Crud->readSurv1(3,2);
		$data['sur32']=$this->Crud->readSurv2(3,2);
		$data['sur33']=$this->Crud->readSurv3(3,2);
		$data['sur34']=$this->Crud->readSurv4(3,2);
		$data['sur35']=$this->Crud->readSurv5(3,2);
		$data['sur36']=$this->Crud->readSurv6(3,2);
		$data['sur37']=$this->Crud->readSurv7(3,2);
		$data['sur38']=$this->Crud->readSurv8(3,2);
		//survei
		$data['sur41']=$this->Crud->readSurv1(4,2);
		$data['sur42']=$this->Crud->readSurv2(4,2);
		$data['sur43']=$this->Crud->readSurv3(4,2);
		$data['sur44']=$this->Crud->readSurv4(4,2);
		$data['sur45']=$this->Crud->readSurv5(4,2);
		$data['sur46']=$this->Crud->readSurv6(4,2);
		$data['sur47']=$this->Crud->readSurv7(4,2);
		$data['sur48']=$this->Crud->readSurv8(4,2);
		//survei
		$data['asur11']=$this->Crud->readSurv1(1,3);
		$data['asur12']=$this->Crud->readSurv2(1,3);
		$data['asur13']=$this->Crud->readSurv3(1,3);
		$data['asur14']=$this->Crud->readSurv4(1,3);
		$data['asur15']=$this->Crud->readSurv5(1,3);
		$data['asur16']=$this->Crud->readSurv6(1,3);
		$data['asur17']=$this->Crud->readSurv7(1,3);
		$data['asur18']=$this->Crud->readSurv8(1,3);
		//survei
		$data['asur21']=$this->Crud->readSurv1(2,3);
		$data['asur22']=$this->Crud->readSurv2(2,3);
		$data['asur23']=$this->Crud->readSurv3(2,3);
		$data['asur24']=$this->Crud->readSurv4(2,3);
		$data['asur25']=$this->Crud->readSurv5(2,3);
		$data['asur26']=$this->Crud->readSurv6(2,3);
		$data['asur27']=$this->Crud->readSurv7(2,3);
		$data['asur28']=$this->Crud->readSurv8(2,3);
		//survei
		$data['asur31']=$this->Crud->readSurv1(3,3);
		$data['asur32']=$this->Crud->readSurv2(3,3);
		$data['asur33']=$this->Crud->readSurv3(3,3);
		$data['asur34']=$this->Crud->readSurv4(3,3);
		$data['asur35']=$this->Crud->readSurv5(3,3);
		$data['asur36']=$this->Crud->readSurv6(3,3);
		$data['asur37']=$this->Crud->readSurv7(3,3);
		$data['asur38']=$this->Crud->readSurv8(3,3);
		//survei
		$data['asur41']=$this->Crud->readSurv1(4,3);
		$data['asur42']=$this->Crud->readSurv2(4,3);
		$data['asur43']=$this->Crud->readSurv3(4,3);
		$data['asur44']=$this->Crud->readSurv4(4,3);
		$data['asur45']=$this->Crud->readSurv5(4,3);
		$data['asur46']=$this->Crud->readSurv6(4,3);
		$data['asur47']=$this->Crud->readSurv7(4,3);
		$data['asur48']=$this->Crud->readSurv8(4,3);


		//perguruan tinggi tahun1
		$data['per11']=$this->Crud->readDash($kat1, $data['tahun1'].'-01-01', $data['tahun1'].'-01-31');
		$data['per12']=$this->Crud->readDash($kat1, $data['tahun1'].'-02-01', $data['tahun1'].'-02-31');
		$data['per13']=$this->Crud->readDash($kat1, $data['tahun1'].'-03-01', $data['tahun1'].'-03-31');
		$data['per14']=$this->Crud->readDash($kat1, $data['tahun1'].'-04-01', $data['tahun1'].'-04-31');
		$data['per15']=$this->Crud->readDash($kat1, $data['tahun1'].'-05-01', $data['tahun1'].'-05-31');
		$data['per16']=$this->Crud->readDash($kat1, $data['tahun1'].'-06-01', $data['tahun1'].'-06-31');
		$data['per17']=$this->Crud->readDash($kat1, $data['tahun1'].'-07-01', $data['tahun1'].'-07-31');
		$data['per18']=$this->Crud->readDash($kat1, $data['tahun1'].'-08-01', $data['tahun1'].'-08-31');
		$data['per19']=$this->Crud->readDash($kat1, $data['tahun1'].'-09-01', $data['tahun1'].'-09-31');
		$data['per110']=$this->Crud->readDash($kat1, $data['tahun1'].'-10-01', $data['tahun1'].'-10-31');
		$data['per111']=$this->Crud->readDash($kat1, $data['tahun1'].'-11-01', $data['tahun1'].'-11-31');
		$data['per112']=$this->Crud->readDash($kat1, $data['tahun1'].'-12-01', $data['tahun1'].'-12-31');
		//perguruan tinggi tahun2
		$data['per21']=$this->Crud->readDash($kat1, $data['tahun2'].'-01-01', $data['tahun2'].'-01-31');
		$data['per22']=$this->Crud->readDash($kat1, $data['tahun2'].'-02-01', $data['tahun2'].'-02-31');
		$data['per23']=$this->Crud->readDash($kat1, $data['tahun2'].'-03-01', $data['tahun2'].'-03-31');
		$data['per24']=$this->Crud->readDash($kat1, $data['tahun2'].'-04-01', $data['tahun2'].'-04-31');
		$data['per25']=$this->Crud->readDash($kat1, $data['tahun2'].'-05-01', $data['tahun2'].'-05-31');
		$data['per26']=$this->Crud->readDash($kat1, $data['tahun2'].'-06-01', $data['tahun2'].'-06-31');
		$data['per27']=$this->Crud->readDash($kat1, $data['tahun2'].'-07-01', $data['tahun2'].'-07-31');
		$data['per28']=$this->Crud->readDash($kat1, $data['tahun2'].'-08-01', $data['tahun2'].'-08-31');
		$data['per29']=$this->Crud->readDash($kat1, $data['tahun2'].'-09-01', $data['tahun2'].'-09-31');
		$data['per210']=$this->Crud->readDash($kat1, $data['tahun2'].'-10-01', $data['tahun2'].'-10-31');
		$data['per211']=$this->Crud->readDash($kat1, $data['tahun2'].'-11-01', $data['tahun2'].'-11-31');
		$data['per212']=$this->Crud->readDash($kat1, $data['tahun2'].'-12-01', $data['tahun2'].'-12-31');
		//perguruan tinggi tahun3
		$data['per31']=$this->Crud->readDash($kat1, $data['tahun3'].'-01-01', $data['tahun3'].'-01-31');
		$data['per32']=$this->Crud->readDash($kat1, $data['tahun3'].'-02-01', $data['tahun3'].'-02-31');
		$data['per33']=$this->Crud->readDash($kat1, $data['tahun3'].'-03-01', $data['tahun3'].'-03-31');
		$data['per34']=$this->Crud->readDash($kat1, $data['tahun3'].'-04-01', $data['tahun3'].'-04-31');
		$data['per35']=$this->Crud->readDash($kat1, $data['tahun3'].'-05-01', $data['tahun3'].'-05-31');
		$data['per36']=$this->Crud->readDash($kat1, $data['tahun3'].'-06-01', $data['tahun3'].'-06-31');
		$data['per37']=$this->Crud->readDash($kat1, $data['tahun3'].'-07-01', $data['tahun3'].'-07-31');
		$data['per38']=$this->Crud->readDash($kat1, $data['tahun3'].'-08-01', $data['tahun3'].'-08-31');
		$data['per39']=$this->Crud->readDash($kat1, $data['tahun3'].'-09-01', $data['tahun3'].'-09-31');
		$data['per310']=$this->Crud->readDash($kat1, $data['tahun3'].'-10-01', $data['tahun3'].'-10-31');
		$data['per311']=$this->Crud->readDash($kat1, $data['tahun3'].'-11-01', $data['tahun3'].'-11-31');
		$data['per312']=$this->Crud->readDash($kat1, $data['tahun3'].'-12-01', $data['tahun3'].'-12-31');
		//pergurun tinggi tahun4
		$data['per41']=$this->Crud->readDash($kat1, $data['tahun4'].'-01-01', $data['tahun4'].'-01-31');
		$data['per42']=$this->Crud->readDash($kat1, $data['tahun4'].'-02-01', $data['tahun4'].'-02-31');
		$data['per43']=$this->Crud->readDash($kat1, $data['tahun4'].'-03-01', $data['tahun4'].'-03-31');
		$data['per44']=$this->Crud->readDash($kat1, $data['tahun4'].'-04-01', $data['tahun4'].'-04-31');
		$data['per45']=$this->Crud->readDash($kat1, $data['tahun4'].'-05-01', $data['tahun4'].'-05-31');
		$data['per46']=$this->Crud->readDash($kat1, $data['tahun4'].'-06-01', $data['tahun4'].'-06-31');
		$data['per47']=$this->Crud->readDash($kat1, $data['tahun4'].'-07-01', $data['tahun4'].'-07-31');
		$data['per48']=$this->Crud->readDash($kat1, $data['tahun4'].'-08-01', $data['tahun4'].'-08-31');
		$data['per49']=$this->Crud->readDash($kat1, $data['tahun4'].'-09-01', $data['tahun4'].'-09-31');
		$data['per410']=$this->Crud->readDash($kat1, $data['tahun4'].'-10-01', $data['tahun4'].'-10-31');
		$data['per411']=$this->Crud->readDash($kat1, $data['tahun4'].'-11-01', $data['tahun4'].'-11-31');
		$data['per412']=$this->Crud->readDash($kat1, $data['tahun4'].'-12-01', $data['tahun4'].'-12-31');
		//perguruan tinggi tahun5
		$data['per51']=$this->Crud->readDash($kat1, $data['tahun5'].'-01-01', $data['tahun5'].'-01-31');
		$data['per52']=$this->Crud->readDash($kat1, $data['tahun5'].'-02-01', $data['tahun5'].'-02-31');
		$data['per53']=$this->Crud->readDash($kat1, $data['tahun5'].'-03-01', $data['tahun5'].'-03-31');
		$data['per54']=$this->Crud->readDash($kat1, $data['tahun5'].'-04-01', $data['tahun5'].'-04-31');
		$data['per55']=$this->Crud->readDash($kat1, $data['tahun5'].'-05-01', $data['tahun5'].'-05-31');
		$data['per56']=$this->Crud->readDash($kat1, $data['tahun5'].'-06-01', $data['tahun5'].'-06-31');
		$data['per57']=$this->Crud->readDash($kat1, $data['tahun5'].'-07-01', $data['tahun5'].'-07-31');
		$data['per58']=$this->Crud->readDash($kat1, $data['tahun5'].'-08-01', $data['tahun5'].'-08-31');
		$data['per59']=$this->Crud->readDash($kat1, $data['tahun5'].'-09-01', $data['tahun5'].'-09-31');
		$data['per510']=$this->Crud->readDash($kat1, $data['tahun5'].'-10-01', $data['tahun5'].'-10-31');
		$data['per511']=$this->Crud->readDash($kat1, $data['tahun5'].'-11-01', $data['tahun5'].'-11-31');
		$data['per512']=$this->Crud->readDash($kat1, $data['tahun5'].'-12-01', $data['tahun5'].'-12-31');
		//pemerintah tahun1
		$data['pem11']=$this->Crud->readDash($kat2, $data['tahun1'].'-01-01', $data['tahun1'].'-01-31');
		$data['pem12']=$this->Crud->readDash($kat2, $data['tahun1'].'-02-01', $data['tahun1'].'-02-31');
		$data['pem13']=$this->Crud->readDash($kat2, $data['tahun1'].'-03-01', $data['tahun1'].'-03-31');
		$data['pem14']=$this->Crud->readDash($kat2, $data['tahun1'].'-04-01', $data['tahun1'].'-04-31');
		$data['pem15']=$this->Crud->readDash($kat2, $data['tahun1'].'-05-01', $data['tahun1'].'-05-31');
		$data['pem16']=$this->Crud->readDash($kat2, $data['tahun1'].'-06-01', $data['tahun1'].'-06-31');
		$data['pem17']=$this->Crud->readDash($kat2, $data['tahun1'].'-07-01', $data['tahun1'].'-07-31');
		$data['pem18']=$this->Crud->readDash($kat2, $data['tahun1'].'-08-01', $data['tahun1'].'-08-31');
		$data['pem19']=$this->Crud->readDash($kat2, $data['tahun1'].'-09-01', $data['tahun1'].'-09-31');
		$data['pem110']=$this->Crud->readDash($kat2, $data['tahun1'].'-10-01', $data['tahun1'].'-10-31');
		$data['pem111']=$this->Crud->readDash($kat2, $data['tahun1'].'-11-01', $data['tahun1'].'-11-31');
		$data['pem112']=$this->Crud->readDash($kat2, $data['tahun1'].'-12-01', $data['tahun1'].'-12-31');
		//perguruan tinggi tahun2
		$data['pem21']=$this->Crud->readDash($kat2, $data['tahun2'].'-01-01', $data['tahun2'].'-01-31');
		$data['pem22']=$this->Crud->readDash($kat2, $data['tahun2'].'-02-01', $data['tahun2'].'-02-31');
		$data['pem23']=$this->Crud->readDash($kat2, $data['tahun2'].'-03-01', $data['tahun2'].'-03-31');
		$data['pem24']=$this->Crud->readDash($kat2, $data['tahun2'].'-04-01', $data['tahun2'].'-04-31');
		$data['pem25']=$this->Crud->readDash($kat2, $data['tahun2'].'-05-01', $data['tahun2'].'-05-31');
		$data['pem26']=$this->Crud->readDash($kat2, $data['tahun2'].'-06-01', $data['tahun2'].'-06-31');
		$data['pem27']=$this->Crud->readDash($kat2, $data['tahun2'].'-07-01', $data['tahun2'].'-07-31');
		$data['pem28']=$this->Crud->readDash($kat2, $data['tahun2'].'-08-01', $data['tahun2'].'-08-31');
		$data['pem29']=$this->Crud->readDash($kat2, $data['tahun2'].'-09-01', $data['tahun2'].'-09-31');
		$data['pem210']=$this->Crud->readDash($kat2, $data['tahun2'].'-10-01', $data['tahun2'].'-10-31');
		$data['pem211']=$this->Crud->readDash($kat2, $data['tahun2'].'-11-01', $data['tahun2'].'-11-31');
		$data['pem212']=$this->Crud->readDash($kat2, $data['tahun2'].'-12-01', $data['tahun2'].'-12-31');
		//perguruan tinggi tahun3
		$data['pem31']=$this->Crud->readDash($kat2, $data['tahun3'].'-01-01', $data['tahun3'].'-01-31');
		$data['pem32']=$this->Crud->readDash($kat2, $data['tahun3'].'-02-01', $data['tahun3'].'-02-31');
		$data['pem33']=$this->Crud->readDash($kat2, $data['tahun3'].'-03-01', $data['tahun3'].'-03-31');
		$data['pem34']=$this->Crud->readDash($kat2, $data['tahun3'].'-04-01', $data['tahun3'].'-04-31');
		$data['pem35']=$this->Crud->readDash($kat2, $data['tahun3'].'-05-01', $data['tahun3'].'-05-31');
		$data['pem36']=$this->Crud->readDash($kat2, $data['tahun3'].'-06-01', $data['tahun3'].'-06-31');
		$data['pem37']=$this->Crud->readDash($kat2, $data['tahun3'].'-07-01', $data['tahun3'].'-07-31');
		$data['pem38']=$this->Crud->readDash($kat2, $data['tahun3'].'-08-01', $data['tahun3'].'-08-31');
		$data['pem39']=$this->Crud->readDash($kat2, $data['tahun3'].'-09-01', $data['tahun3'].'-09-31');
		$data['pem310']=$this->Crud->readDash($kat2, $data['tahun3'].'-10-01', $data['tahun3'].'-10-31');
		$data['pem311']=$this->Crud->readDash($kat2, $data['tahun3'].'-11-01', $data['tahun3'].'-11-31');
		$data['pem312']=$this->Crud->readDash($kat2, $data['tahun3'].'-12-01', $data['tahun3'].'-12-31');
		//perguruan tinggi tahun4
		$data['pem41']=$this->Crud->readDash($kat2, $data['tahun4'].'-01-01', $data['tahun4'].'-01-31');
		$data['pem42']=$this->Crud->readDash($kat2, $data['tahun4'].'-02-01', $data['tahun4'].'-02-31');
		$data['pem43']=$this->Crud->readDash($kat2, $data['tahun4'].'-03-01', $data['tahun4'].'-03-31');
		$data['pem44']=$this->Crud->readDash($kat2, $data['tahun4'].'-04-01', $data['tahun4'].'-04-31');
		$data['pem45']=$this->Crud->readDash($kat2, $data['tahun4'].'-05-01', $data['tahun4'].'-05-31');
		$data['pem46']=$this->Crud->readDash($kat2, $data['tahun4'].'-06-01', $data['tahun4'].'-06-31');
		$data['pem47']=$this->Crud->readDash($kat2, $data['tahun4'].'-07-01', $data['tahun4'].'-07-31');
		$data['pem48']=$this->Crud->readDash($kat2, $data['tahun4'].'-08-01', $data['tahun4'].'-08-31');
		$data['pem49']=$this->Crud->readDash($kat2, $data['tahun4'].'-09-01', $data['tahun4'].'-09-31');
		$data['pem410']=$this->Crud->readDash($kat2, $data['tahun4'].'-10-01', $data['tahun4'].'-10-31');
		$data['pem411']=$this->Crud->readDash($kat2, $data['tahun4'].'-11-01', $data['tahun4'].'-11-31');
		$data['pem412']=$this->Crud->readDash($kat2, $data['tahun4'].'-12-01', $data['tahun4'].'-12-31');
		//perguruan tinggi tahun5
		$data['pem51']=$this->Crud->readDash($kat2, $data['tahun5'].'-01-01', $data['tahun5'].'-01-31');
		$data['pem52']=$this->Crud->readDash($kat2, $data['tahun5'].'-02-01', $data['tahun5'].'-02-31');
		$data['pem53']=$this->Crud->readDash($kat2, $data['tahun5'].'-03-01', $data['tahun5'].'-03-31');
		$data['pem54']=$this->Crud->readDash($kat2, $data['tahun5'].'-04-01', $data['tahun5'].'-04-31');
		$data['pem55']=$this->Crud->readDash($kat2, $data['tahun5'].'-05-01', $data['tahun5'].'-05-31');
		$data['pem56']=$this->Crud->readDash($kat2, $data['tahun5'].'-06-01', $data['tahun5'].'-06-31');
		$data['pem57']=$this->Crud->readDash($kat2, $data['tahun5'].'-07-01', $data['tahun5'].'-07-31');
		$data['pem58']=$this->Crud->readDash($kat2, $data['tahun5'].'-08-01', $data['tahun5'].'-08-31');
		$data['pem59']=$this->Crud->readDash($kat2, $data['tahun5'].'-09-01', $data['tahun5'].'-09-31');
		$data['pem510']=$this->Crud->readDash($kat2, $data['tahun5'].'-10-01', $data['tahun5'].'-10-31');
		$data['pem511']=$this->Crud->readDash($kat2, $data['tahun5'].'-11-01', $data['tahun5'].'-11-31');
		$data['pem512']=$this->Crud->readDash($kat2, $data['tahun5'].'-12-01', $data['tahun5'].'-12-31');
		//lembaga swasta tahun1
		$data['lem11']=$this->Crud->readDash($kat3, $data['tahun1'].'-01-01', $data['tahun1'].'-01-31');
		$data['lem12']=$this->Crud->readDash($kat3, $data['tahun1'].'-02-01', $data['tahun1'].'-02-31');
		$data['lem13']=$this->Crud->readDash($kat3, $data['tahun1'].'-03-01', $data['tahun1'].'-03-31');
		$data['lem14']=$this->Crud->readDash($kat3, $data['tahun1'].'-04-01', $data['tahun1'].'-04-31');
		$data['lem15']=$this->Crud->readDash($kat3, $data['tahun1'].'-05-01', $data['tahun1'].'-05-31');
		$data['lem16']=$this->Crud->readDash($kat3, $data['tahun1'].'-06-01', $data['tahun1'].'-06-31');
		$data['lem17']=$this->Crud->readDash($kat3, $data['tahun1'].'-07-01', $data['tahun1'].'-07-31');
		$data['lem18']=$this->Crud->readDash($kat3, $data['tahun1'].'-08-01', $data['tahun1'].'-08-31');
		$data['lem19']=$this->Crud->readDash($kat3, $data['tahun1'].'-09-01', $data['tahun1'].'-09-31');
		$data['lem110']=$this->Crud->readDash($kat3, $data['tahun1'].'-10-01', $data['tahun1'].'-10-31');
		$data['lem111']=$this->Crud->readDash($kat3, $data['tahun1'].'-11-01', $data['tahun1'].'-11-31');
		$data['lem112']=$this->Crud->readDash($kat3, $data['tahun1'].'-12-01', $data['tahun1'].'-12-31');
		//perguruan tinggi tahun2
		$data['lem21']=$this->Crud->readDash($kat3, $data['tahun2'].'-01-01', $data['tahun2'].'-01-31');
		$data['lem22']=$this->Crud->readDash($kat3, $data['tahun2'].'-02-01', $data['tahun2'].'-02-31');
		$data['lem23']=$this->Crud->readDash($kat3, $data['tahun2'].'-03-01', $data['tahun2'].'-03-31');
		$data['lem24']=$this->Crud->readDash($kat3, $data['tahun2'].'-04-01', $data['tahun2'].'-04-31');
		$data['lem25']=$this->Crud->readDash($kat3, $data['tahun2'].'-05-01', $data['tahun2'].'-05-31');
		$data['lem26']=$this->Crud->readDash($kat3, $data['tahun2'].'-06-01', $data['tahun2'].'-06-31');
		$data['lem27']=$this->Crud->readDash($kat3, $data['tahun2'].'-07-01', $data['tahun2'].'-07-31');
		$data['lem28']=$this->Crud->readDash($kat3, $data['tahun2'].'-08-01', $data['tahun2'].'-08-31');
		$data['lem29']=$this->Crud->readDash($kat3, $data['tahun2'].'-09-01', $data['tahun2'].'-09-31');
		$data['lem210']=$this->Crud->readDash($kat3, $data['tahun2'].'-10-01', $data['tahun2'].'-10-31');
		$data['lem211']=$this->Crud->readDash($kat3, $data['tahun2'].'-11-01', $data['tahun2'].'-11-31');
		$data['lem212']=$this->Crud->readDash($kat3, $data['tahun2'].'-12-01', $data['tahun2'].'-12-31');
		//perguruan tinggi tahun3
		$data['lem31']=$this->Crud->readDash($kat3, $data['tahun3'].'-01-01', $data['tahun3'].'-01-31');
		$data['lem32']=$this->Crud->readDash($kat3, $data['tahun3'].'-02-01', $data['tahun3'].'-02-31');
		$data['lem33']=$this->Crud->readDash($kat3, $data['tahun3'].'-03-01', $data['tahun3'].'-03-31');
		$data['lem34']=$this->Crud->readDash($kat3, $data['tahun3'].'-04-01', $data['tahun3'].'-04-31');
		$data['lem35']=$this->Crud->readDash($kat3, $data['tahun3'].'-05-01', $data['tahun3'].'-05-31');
		$data['lem36']=$this->Crud->readDash($kat3, $data['tahun3'].'-06-01', $data['tahun3'].'-06-31');
		$data['lem37']=$this->Crud->readDash($kat3, $data['tahun3'].'-07-01', $data['tahun3'].'-07-31');
		$data['lem38']=$this->Crud->readDash($kat3, $data['tahun3'].'-08-01', $data['tahun3'].'-08-31');
		$data['lem39']=$this->Crud->readDash($kat3, $data['tahun3'].'-09-01', $data['tahun3'].'-09-31');
		$data['lem310']=$this->Crud->readDash($kat3, $data['tahun3'].'-10-01', $data['tahun3'].'-10-31');
		$data['lem311']=$this->Crud->readDash($kat3, $data['tahun3'].'-11-01', $data['tahun3'].'-11-31');
		$data['lem312']=$this->Crud->readDash($kat3, $data['tahun3'].'-12-01', $data['tahun3'].'-12-31');
		//perguruan tinggi tahun4
		$data['lem41']=$this->Crud->readDash($kat3, $data['tahun4'].'-01-01', $data['tahun4'].'-01-31');
		$data['lem42']=$this->Crud->readDash($kat3, $data['tahun4'].'-02-01', $data['tahun4'].'-02-31');
		$data['lem43']=$this->Crud->readDash($kat3, $data['tahun4'].'-03-01', $data['tahun4'].'-03-31');
		$data['lem44']=$this->Crud->readDash($kat3, $data['tahun4'].'-04-01', $data['tahun4'].'-04-31');
		$data['lem45']=$this->Crud->readDash($kat3, $data['tahun4'].'-05-01', $data['tahun4'].'-05-31');
		$data['lem46']=$this->Crud->readDash($kat3, $data['tahun4'].'-06-01', $data['tahun4'].'-06-31');
		$data['lem47']=$this->Crud->readDash($kat3, $data['tahun4'].'-07-01', $data['tahun4'].'-07-31');
		$data['lem48']=$this->Crud->readDash($kat3, $data['tahun4'].'-08-01', $data['tahun4'].'-08-31');
		$data['lem49']=$this->Crud->readDash($kat3, $data['tahun4'].'-09-01', $data['tahun4'].'-09-31');
		$data['lem410']=$this->Crud->readDash($kat3, $data['tahun4'].'-10-01', $data['tahun4'].'-10-31');
		$data['lem411']=$this->Crud->readDash($kat3, $data['tahun4'].'-11-01', $data['tahun4'].'-11-31');
		$data['lem412']=$this->Crud->readDash($kat3, $data['tahun4'].'-12-01', $data['tahun4'].'-12-31');
		//perguruan tinggi tahun5
		$data['lem51']=$this->Crud->readDash($kat3, $data['tahun5'].'-01-01', $data['tahun5'].'-01-31');
		$data['lem52']=$this->Crud->readDash($kat3, $data['tahun5'].'-02-01', $data['tahun5'].'-02-31');
		$data['lem53']=$this->Crud->readDash($kat3, $data['tahun5'].'-03-01', $data['tahun5'].'-03-31');
		$data['lem54']=$this->Crud->readDash($kat3, $data['tahun5'].'-04-01', $data['tahun5'].'-04-31');
		$data['lem55']=$this->Crud->readDash($kat3, $data['tahun5'].'-05-01', $data['tahun5'].'-05-31');
		$data['lem56']=$this->Crud->readDash($kat3, $data['tahun5'].'-06-01', $data['tahun5'].'-06-31');
		$data['lem57']=$this->Crud->readDash($kat3, $data['tahun5'].'-07-01', $data['tahun5'].'-07-31');
		$data['lem58']=$this->Crud->readDash($kat3, $data['tahun5'].'-08-01', $data['tahun5'].'-08-31');
		$data['lem59']=$this->Crud->readDash($kat3, $data['tahun5'].'-09-01', $data['tahun5'].'-09-31');
		$data['lem510']=$this->Crud->readDash($kat3, $data['tahun5'].'-10-01', $data['tahun5'].'-10-31');
		$data['lem511']=$this->Crud->readDash($kat3, $data['tahun5'].'-11-01', $data['tahun5'].'-11-31');
		$data['lem512']=$this->Crud->readDash($kat3, $data['tahun5'].'-12-01', $data['tahun5'].'-12-31');
		//dunia industri tahun1
		$data['iem11']=$this->Crud->readDash($kat4, $data['tahun1'].'-01-01', $data['tahun1'].'-01-31');
		$data['iem12']=$this->Crud->readDash($kat4, $data['tahun1'].'-02-01', $data['tahun1'].'-02-31');
		$data['iem13']=$this->Crud->readDash($kat4, $data['tahun1'].'-03-01', $data['tahun1'].'-03-31');
		$data['iem14']=$this->Crud->readDash($kat4, $data['tahun1'].'-04-01', $data['tahun1'].'-04-31');
		$data['iem15']=$this->Crud->readDash($kat4, $data['tahun1'].'-05-01', $data['tahun1'].'-05-31');
		$data['iem16']=$this->Crud->readDash($kat4, $data['tahun1'].'-06-01', $data['tahun1'].'-06-31');
		$data['iem17']=$this->Crud->readDash($kat4, $data['tahun1'].'-07-01', $data['tahun1'].'-07-31');
		$data['iem18']=$this->Crud->readDash($kat4, $data['tahun1'].'-08-01', $data['tahun1'].'-08-31');
		$data['iem19']=$this->Crud->readDash($kat4, $data['tahun1'].'-09-01', $data['tahun1'].'-09-31');
		$data['iem110']=$this->Crud->readDash($kat4, $data['tahun1'].'-10-01', $data['tahun1'].'-10-31');
		$data['iem111']=$this->Crud->readDash($kat4, $data['tahun1'].'-11-01', $data['tahun1'].'-11-31');
		$data['iem112']=$this->Crud->readDash($kat4, $data['tahun1'].'-12-01', $data['tahun1'].'-12-31');
		//perguruan tinggi tahun2
		$data['iem21']=$this->Crud->readDash($kat4, $data['tahun2'].'-01-01', $data['tahun2'].'-01-31');
		$data['iem22']=$this->Crud->readDash($kat4, $data['tahun2'].'-02-01', $data['tahun2'].'-02-31');
		$data['iem23']=$this->Crud->readDash($kat4, $data['tahun2'].'-03-01', $data['tahun2'].'-03-31');
		$data['iem24']=$this->Crud->readDash($kat4, $data['tahun2'].'-04-01', $data['tahun2'].'-04-31');
		$data['iem25']=$this->Crud->readDash($kat4, $data['tahun2'].'-05-01', $data['tahun2'].'-05-31');
		$data['iem26']=$this->Crud->readDash($kat4, $data['tahun2'].'-06-01', $data['tahun2'].'-06-31');
		$data['iem27']=$this->Crud->readDash($kat4, $data['tahun2'].'-07-01', $data['tahun2'].'-07-31');
		$data['iem28']=$this->Crud->readDash($kat4, $data['tahun2'].'-08-01', $data['tahun2'].'-08-31');
		$data['iem29']=$this->Crud->readDash($kat4, $data['tahun2'].'-09-01', $data['tahun2'].'-09-31');
		$data['iem210']=$this->Crud->readDash($kat4, $data['tahun2'].'-10-01', $data['tahun2'].'-10-31');
		$data['iem211']=$this->Crud->readDash($kat4, $data['tahun2'].'-11-01', $data['tahun2'].'-11-31');
		$data['iem212']=$this->Crud->readDash($kat4, $data['tahun2'].'-12-01', $data['tahun2'].'-12-31');
		//perguruan tinggi tahun3
		$data['iem31']=$this->Crud->readDash($kat4, $data['tahun3'].'-01-01', $data['tahun3'].'-01-31');
		$data['iem32']=$this->Crud->readDash($kat4, $data['tahun3'].'-02-01', $data['tahun3'].'-02-31');
		$data['iem33']=$this->Crud->readDash($kat4, $data['tahun3'].'-03-01', $data['tahun3'].'-03-31');
		$data['iem34']=$this->Crud->readDash($kat4, $data['tahun3'].'-04-01', $data['tahun3'].'-04-31');
		$data['iem35']=$this->Crud->readDash($kat4, $data['tahun3'].'-05-01', $data['tahun3'].'-05-31');
		$data['iem36']=$this->Crud->readDash($kat4, $data['tahun3'].'-06-01', $data['tahun3'].'-06-31');
		$data['iem37']=$this->Crud->readDash($kat4, $data['tahun3'].'-07-01', $data['tahun3'].'-07-31');
		$data['iem38']=$this->Crud->readDash($kat4, $data['tahun3'].'-08-01', $data['tahun3'].'-08-31');
		$data['iem39']=$this->Crud->readDash($kat4, $data['tahun3'].'-09-01', $data['tahun3'].'-09-31');
		$data['iem310']=$this->Crud->readDash($kat4, $data['tahun3'].'-10-01', $data['tahun3'].'-10-31');
		$data['iem311']=$this->Crud->readDash($kat4, $data['tahun3'].'-11-01', $data['tahun3'].'-11-31');
		$data['iem312']=$this->Crud->readDash($kat4, $data['tahun3'].'-12-01', $data['tahun3'].'-12-31');
		//perguruan tinggi tahun4
		$data['iem41']=$this->Crud->readDash($kat4, $data['tahun4'].'-01-01', $data['tahun4'].'-01-31');
		$data['iem42']=$this->Crud->readDash($kat4, $data['tahun4'].'-02-01', $data['tahun4'].'-02-31');
		$data['iem43']=$this->Crud->readDash($kat4, $data['tahun4'].'-03-01', $data['tahun4'].'-03-31');
		$data['iem44']=$this->Crud->readDash($kat4, $data['tahun4'].'-04-01', $data['tahun4'].'-04-31');
		$data['iem45']=$this->Crud->readDash($kat4, $data['tahun4'].'-05-01', $data['tahun4'].'-05-31');
		$data['iem46']=$this->Crud->readDash($kat4, $data['tahun4'].'-06-01', $data['tahun4'].'-06-31');
		$data['iem47']=$this->Crud->readDash($kat4, $data['tahun4'].'-07-01', $data['tahun4'].'-07-31');
		$data['iem48']=$this->Crud->readDash($kat4, $data['tahun4'].'-08-01', $data['tahun4'].'-08-31');
		$data['iem49']=$this->Crud->readDash($kat4, $data['tahun4'].'-09-01', $data['tahun4'].'-09-31');
		$data['iem410']=$this->Crud->readDash($kat4, $data['tahun4'].'-10-01', $data['tahun4'].'-10-31');
		$data['iem411']=$this->Crud->readDash($kat4, $data['tahun4'].'-11-01', $data['tahun4'].'-11-31');
		$data['iem412']=$this->Crud->readDash($kat4, $data['tahun4'].'-12-01', $data['tahun4'].'-12-31');
		//perguruan tinggi tahun5
		$data['iem51']=$this->Crud->readDash($kat4, $data['tahun5'].'-01-01', $data['tahun5'].'-01-31');
		$data['iem52']=$this->Crud->readDash($kat4, $data['tahun5'].'-02-01', $data['tahun5'].'-02-31');
		$data['iem53']=$this->Crud->readDash($kat4, $data['tahun5'].'-03-01', $data['tahun5'].'-03-31');
		$data['iem54']=$this->Crud->readDash($kat4, $data['tahun5'].'-04-01', $data['tahun5'].'-04-31');
		$data['iem55']=$this->Crud->readDash($kat4, $data['tahun5'].'-05-01', $data['tahun5'].'-05-31');
		$data['iem56']=$this->Crud->readDash($kat4, $data['tahun5'].'-06-01', $data['tahun5'].'-06-31');
		$data['iem57']=$this->Crud->readDash($kat4, $data['tahun5'].'-07-01', $data['tahun5'].'-07-31');
		$data['iem58']=$this->Crud->readDash($kat4, $data['tahun5'].'-08-01', $data['tahun5'].'-08-31');
		$data['iem59']=$this->Crud->readDash($kat4, $data['tahun5'].'-09-01', $data['tahun5'].'-09-31');
		$data['iem510']=$this->Crud->readDash($kat4, $data['tahun5'].'-10-01', $data['tahun5'].'-10-31');
		$data['iem511']=$this->Crud->readDash($kat4, $data['tahun5'].'-11-01', $data['tahun5'].'-11-31');
		$data['iem512']=$this->Crud->readDash($kat4, $data['tahun5'].'-12-01', $data['tahun5'].'-12-31');

		$this->load->view('hal_admin', $data);
	}

	public function lihatFile($usr,$bid, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid;
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file/'.$usr.'/'.$bid; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

	public function cekFile1(){
		$path  = $this->input->post('file1');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile2(){
		$path  = $this->input->post('file2');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile3(){
		$path  = $this->input->post('file3');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile4(){
		$path  = $this->input->post('file4');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function getData($id){
		$query = $this->Crud->read('tb_mou', array('id_mou'=>$id), null, null);
		foreach($query->result() as $result){
			$data = array('bidang'=>$result->bidang,
				'kategori'=>$result->kategori,
				'judul'=>$result->judul,
				'manfaat_m'=>$result->manfaat_m,
				'manfaat_u'=>$result->manfaat_u,
				'negara'=>$result->negara,
				'periode'=>$result->periode,
				'tgl_usul'=>$result->tgl_usul,
				'tgl_lama'=>$result->tgl_lama,
				'berkas1'=>$result->file1,
				'berkas2'=>$result->file2,
				'berkas3'=>$result->file3,
				'berkas4'=>$result->file4
				);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function cekData($table, $field, $data){
		$match = $this->Crud->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$bidang   = $this->input->post('bidang');
		$kategori  = $this->input->post('kategori');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');

		if($_FILES['file1']['name']=="" && $_FILES['file2']['name']=="" && $_FILES['file3']['name']=="" && $_FILES['file4']['name']==""){
				$data = array('bidang'=>$bidang, 'kategori'=>$kategori, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m, 'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode);
		}else{
			$photoName = gmdate("d-m-y-H-i-s", time()+3600*7).".jpg";
			$config['upload_path'] = './assets/img/barang';
			$config['allowed_types'] = 'gif||jpg||png';
			$config['max_size'] = '2048000';
			$config['file_name'] = $photoName;
			$this->load->library('upload',$config);
			if($this->upload->do_upload('ubahfoto')){			
				$upload = 1;
				$data = array('nama'=>$nama, 'stock'=>$stock, 'harga'=>$harga,'foto'=>$photoName, 'idkategori'=>$idkategori, 'deskripsi'=>$deskripsi, 'idpetugas'=>$this->session->userdata('iduser'));
			}
			else{
				$upload = 2;
			}
				$query = $this->Crud->read('barang', array('idbarang'=>$id), null, null);
				foreach($query->result() as $result){
				unlink('assets/img/barang/'.$result->foto.'');
			}
		}
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $query = $this->Crud->read('tb_mou', array('id_mou'=>$id), null, null);
		foreach($query->result() as $result){
	    	delete_files('./assets/file/'.$this->session->userdata("nama").'/'.$result->bidang, true);
			rmdir('./assets/file/'.$this->session->userdata("nama").'/'.$result->bidang);
		}
     	$delete = $this->Crud->delete(array('id_mou'=>$id), 'tb_mou');
    }

}