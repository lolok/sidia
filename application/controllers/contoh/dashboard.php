<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class dashboard extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	public function index(){
		$data['tahun'] = date("Y", strtotime("now"));
		$data['tabmou']=$this->Crud->read('tb_mou',array('id_user'=>$this->session->userdata("iduser")),'id_mou',null);
		$this->load->view('hal_user', $data);
	}

	public function addKerjasama(){
		$nama	= $this->session->userdata("username");
		$bidang   = $this->input->post('bidang');
		$kategori  = $this->input->post('kategori');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');
		$status = 2;
		$tgl = date("Y-m-d", strtotime("now"));
		mkdir('./assets/file/'.$nama.'/'.$bidang);
		//file1
		$fileName = $nama."-".$bidang."-1.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/'.$bidang;
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		$this->upload->do_upload('file1');	
		//file2
		$fileName2 = $nama."-".$bidang."-2.pdf";
		$config2['upload_path'] = './assets/file/'.$nama.'/'.$bidang;
		$config2['allowed_types'] = 'pdf';
		$config2['max_size'] = '2048000';
		$config2['file_name'] = $fileName2;
		$this->load->library('upload',$config2);
		$this->upload->initialize($config2);
		$this->upload->do_upload('file2');
		//file3
		$fileName3 = $nama."-".$bidang."-3.pdf";
		$config3['upload_path'] = './assets/file/'.$nama.'/'.$bidang;
		$config3['allowed_types'] = 'pdf';
		$config3['max_size'] = '2048000';
		$config3['file_name'] = $fileName3;
		$this->load->library('upload',$config3);
		$this->upload->initialize($config3);
		$this->upload->do_upload('file3');
		//file4
		$fileName4 = $nama."-".$bidang."-4.pdf";
		$config4['upload_path'] = './assets/file/'.$nama.'/'.$bidang;
		$config4['allowed_types'] = 'pdf';
		$config4['max_size'] = '2048000';
		$config4['file_name'] = $fileName4;
		$this->load->library('upload',$config4);
		$this->upload->initialize($config4);
		$this->upload->do_upload('file4');

		$data   = array('bidang'=>$bidang, 'id_user'=>$this->session->userdata("iduser"), 'status'=>$status, 'tgl_usul'=>$tgl, 'kategori'=>$kategori, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4);
		$insert = $this->Crud->create('tb_mou', $data);
		redirect(site_url('DashUser'), 'refresh');

	}

	public function lihatFile($usr,$bid, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid;
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file'; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

	public function cekFile1(){
		$path  = $this->input->post('file1');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile2(){
		$path  = $this->input->post('file2');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile3(){
		$path  = $this->input->post('file3');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile4(){
		$path  = $this->input->post('file4');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function getData($id){
		$query = $this->Crud->read('tb_mou', array('id_mou'=>$id), null, null);
		foreach($query->result() as $result){
			$data = array('bidang'=>$result->bidang,
				'kategori'=>$result->kategori,
				'judul'=>$result->judul,
				'manfaat_m'=>$result->manfaat_m,
				'manfaat_u'=>$result->manfaat_u,
				'negara'=>$result->negara,
				'periode'=>$result->periode,
				'berkas1'=>$result->file1,
				'berkas2'=>$result->file2,
				'berkas3'=>$result->file3,
				'berkas4'=>$result->file4
				);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function cekData($table, $field, $data){
		$match = $this->Crud->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$kategori   = $this->input->post('kategori');
		$data = array('kategori'=>$kategori);

		$update = $this->Crud->update(array('id_pengguna'=>$id), 'tb_pengguna', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $delete = $this->Crud->delete(array('id_pengguna'=>$id), 'tb_pengguna');
    
     
    }

}