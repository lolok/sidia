<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KerjaFakCtrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	public function index(){
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabkat1']= $this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']= $this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']= $this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']= $this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['tabmou']=$this->Crud->read('tb_mou',array('id_user'=>$this->session->userdata("iduser")),'id_mou',null);
		$data['tabkerjasama']=$this->Crud->readKerjasama($this->session->userdata("iduser"));
		$data['negara']=$this->Crud->read('tb_negara',null,null,null);
		
		$this->load->view('hal_kerjasamaFak', $data);
	}

	public function pilihKat(){
		$kategori = $this->input->post('kategori');
		$subkategori1 = $this->input->post('subkategori1');
		$subkategori2 = $this->input->post('subkategori2');
		$subkategori3 = $this->input->post('subkategori3');
		$subkategori4 = $this->input->post('subkategori4');
		$tahun = $this->input->post('tahun');
		$negara = $this->input->post('negara');

		if($kategori==1){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori1,$tahun,$negara);
		}else if($kategori==2){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori2,$tahun,$negara);
		}else if($kategori==3){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori3,$tahun,$negara);
		}else if($kategori==4){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori4,$tahun,$negara);
		}else if($kategori==0){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori1,$tahun,$negara);
		}
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabkat1']=$this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']=$this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']=$this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']=$this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['tabmou']=$this->Crud->read('tb_mou',array('id_user'=>$this->session->userdata("iduser")),'id_mou',null);
		$data['tabkerjasama']=$this->Crud->readKerjasama($this->session->userdata("iduser"));
		$data['negara']=$this->Crud->read('tb_negara',null,null,null);

		$this->load->view('hal_kerjasamaFak', $data);
	}

	public function getData($id){
		$query = $this->Crud->readKerjasamaId($id);
		foreach($query->result() as $result){
			$data = array('mitra'=>$result->mitra,
				'judul'=>$result->judul,
				'tahun'=>$result->tahun,
				'tgl_usul'=>$result->tgl_usul,
				'tgl_lama'=>$result->tgl_lama,
				'kategori'=>$result->kategori,
				'subkategori'=>$result->subkategori,
				'pengusul'=>$result->nama,
				'bidang1'=>$result->bidang1,
				'bidang2'=>$result->bidang2,
				'bidang3'=>$result->bidang3,
				'bidang4'=>$result->bidang4,
				'bidang5'=>$result->bidang5,
				'negara'=>$result->negara,
				'periode'=>$result->periode,
				'manfaat_m'=>$result->manfaat_m,
				'manfaat_u'=>$result->manfaat_u,
				'berkas1'=>$result->file1,
				'berkas2'=>$result->file2,
				'berkas3'=>$result->file3,
				'berkas4'=>$result->file4,
				'folder'=>$result->folder
				);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function addKerjasama(){
		$nama = $this->session->userdata("username");
		$iduser	= $this->session->userdata("iduser");
		$mitra   = $this->input->post('mitra');
		$jenis   = $this->input->post('jenis');
		$kategori  = $this->input->post('kategori1');
		$subkategori1 = $this->input->post('asubkategori1');
		$subkategori2 = $this->input->post('asubkategori2');
		$subkategori3 = $this->input->post('asubkategori3');
		$subkategori4 = $this->input->post('asubkategori4');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');
		$tglusul  = $this->input->post('tglusul');
		$tglsls  = $this->input->post('tglsls');
		$bidang1  = $this->input->post('bidang1');
		$bidang2  = $this->input->post('bidang2');
		$bidang3  = $this->input->post('bidang3');
		$bidang4  = $this->input->post('bidang4');
		$bidang5  = $this->input->post('bidang5');
		$status = 2;
		$tahun = date("Y", strtotime("now"));
		$folder = gmdate("d-m-y-H-i-s", time()+3600*7);
		mkdir('./assets/file/'.$nama.'/berkas/'.$folder);
		//file1
		$fileName = gmdate("d-m-y-H-i-s", time()+3600*7)."-1.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		$this->upload->do_upload('file1');	
		//file2
		$fileName2 = gmdate("d-m-y-H-i-s", time()+3600*7)."-2.pdf";
		$config2['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config2['allowed_types'] = 'pdf';
		$config2['max_size'] = '2048000';
		$config2['file_name'] = $fileName2;
		$this->load->library('upload',$config2);
		$this->upload->initialize($config2);
		$this->upload->do_upload('file2');
		//file3
		$fileName3 = gmdate("d-m-y-H-i-s", time()+3600*7)."-3.pdf";
		$config3['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config3['allowed_types'] = 'pdf';
		$config3['max_size'] = '2048000';
		$config3['file_name'] = $fileName3;
		$this->load->library('upload',$config3);
		$this->upload->initialize($config3);
		$this->upload->do_upload('file3');
		//file4
		$fileName4 = gmdate("d-m-y-H-i-s", time()+3600*7)."-4.pdf";
		$config4['upload_path'] = './assets/file/'.$nama.'/berkas/'.$folder;
		$config4['allowed_types'] = 'pdf';
		$config4['max_size'] = '2048000';
		$config4['file_name'] = $fileName4;
		$this->load->library('upload',$config4);
		$this->upload->initialize($config4);
		$this->upload->do_upload('file4');

		if($kategori==1){
			$data   = array('id_user'=>$iduser,'jenis'=>$jenis, 'mitra'=>$mitra, 'id_kategori'=>$subkategori1, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==2){
			$data   = array('id_user'=>$iduser, 'jenis'=>$jenis, 'mitra'=>$mitra, 'id_kategori'=>$subkategori2, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==3){
			$data   = array('id_user'=>$iduser, 'jenis'=>$jenis, 'mitra'=>$mitra, 'id_kategori'=>$subkategori3, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}else if($kategori==4){
			$data   = array('id_user'=>$iduser, 'jenis'=>$jenis, 'mitra'=>$mitra, 'id_kategori'=>$subkategori4, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m,'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode, 'tgl_usul'=>$tglusul, 'tgl_lama'=>$tglsls, 'status'=>$status, 'tahun'=>$tahun, 'bidang1'=>$bidang1, 'bidang2'=>$bidang2, 'bidang3'=>$bidang3, 'bidang4'=>$bidang4, 'bidang5'=>$bidang5, 'file1'=>$fileName, 'file2'=>$fileName2, 'file3'=>$fileName3, 'file4'=>$fileName4, 'folder'=>$folder);
		}
		
		$insert = $this->Crud->create('tb_mou', $data);
		redirect(site_url('KerjaFakCtrl'), 'refresh');

	}

	public function detStat($id){
        $data['detstat'] = $this->Crud->readProses($id);
        $this->load->view('tabel_detail',$data);
    }

	public function lihatFile($usr,$bid, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid.'/';
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file/'.$usr.'/'.$bid.'/'; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

    public function lihatFile1($usr,$bid,$fol, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid.'/'.$fol;
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file/'.$usr.'/'.$bid.'/'.$fol; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

    public function cekFileMOU(){
		$path  = $this->input->post('fileMOU');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFileNaskah(){
		$path  = $this->input->post('fileNaskah');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile1(){
		$path  = $this->input->post('file1');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile2(){
		$path  = $this->input->post('file2');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile3(){
		$path  = $this->input->post('file3');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekFile4(){
		$path  = $this->input->post('file4');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function cekData($table, $field, $data){
		$match = $this->Crud->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$bidang   = $this->input->post('bidang');
		$kategori  = $this->input->post('kategori');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');

		if($_FILES['file1']['name']=="" && $_FILES['file2']['name']=="" && $_FILES['file3']['name']=="" && $_FILES['file4']['name']==""){
				$data = array('bidang'=>$bidang, 'kategori'=>$kategori, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m, 'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode);
		}else{
			$photoName = gmdate("d-m-y-H-i-s", time()+3600*7).".jpg";
			$config['upload_path'] = './assets/img/barang';
			$config['allowed_types'] = 'gif||jpg||png';
			$config['max_size'] = '2048000';
			$config['file_name'] = $photoName;
			$this->load->library('upload',$config);
			if($this->upload->do_upload('ubahfoto')){			
				$upload = 1;
				$data = array('nama'=>$nama, 'stock'=>$stock, 'harga'=>$harga,'foto'=>$photoName, 'idkategori'=>$idkategori, 'deskripsi'=>$deskripsi, 'idpetugas'=>$this->session->userdata('iduser'));
			}
			else{
				$upload = 2;
			}
				$query = $this->Crud->read('barang', array('idbarang'=>$id), null, null);
				foreach($query->result() as $result){
				unlink('assets/img/barang/'.$result->foto.'');
			}
		}
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $query = $this->Crud->read('tb_mou', array('id_mou'=>$id), null, null);
		foreach($query->result() as $result){
	    	delete_files('./assets/file/'.$this->session->userdata("nama").'/berkas/'.$result->folder, true);
			rmdir('./assets/file/'.$this->session->userdata("nama").'/berkas/'.$result->folder);
			unlink('assets/file/'.$this->session->userdata("nama").'/mou/'.$result->filemou.'');
		}
     	$delete = $this->Crud->delete(array('id_mou'=>$id), 'tb_mou');
    }

    public function upload($id){
		$query = $this->Crud->readMou($id);
		foreach($query->result() as $result){
			$nama = $result->username;
			unlink('assets/file/'.$nama.'/mou/'.$result->filemou.'');
		}
		
		$fileName = $nama."-".$id."-MOU.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/mou';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);	
		$this->upload->do_upload('fileMOU');
		$data = array('filemou'=>$fileName);
		
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
	}

	public function upload1($id){
		$query = $this->Crud->readMou($id);
		foreach($query->result() as $result){
			$nama = $result->username;
			unlink('assets/file/'.$nama.'/naskah/'.$result->filenaskah.'');
		}
		
		$fileName = $nama."-".$id."-naskah.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/naskah';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);	
		$this->upload->do_upload('fileNaskah');
		$data = array('filenaskah'=>$fileName);
		
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
	}

}