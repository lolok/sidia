<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KelolaInisiasi extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('form');
		$this->load->helper('url');
		$this->load->helper('file');
		$this->load->model('Crud');
	}

	public function index(){
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabkat1']= $this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']= $this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']= $this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']= $this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['tabkerjasama']=$this->Crud->readInisiasiAll();

		$this->load->view('hal_kelolaInisiasi', $data);

	}

	public function pilihKat(){
		$kategori = $this->input->post('kategori');
		$subkategori1 = $this->input->post('subkategori1');
		$subkategori2 = $this->input->post('subkategori2');
		$subkategori3 = $this->input->post('subkategori3');
		$subkategori4 = $this->input->post('subkategori4');
		$tahun = $this->input->post('tahun');

		if($kategori==1){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori1,$tahun);
		}else if($kategori==2){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori2,$tahun);
		}else if($kategori==3){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori3,$tahun);
		}else if($kategori==4){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori4,$tahun);
		}else if($kategori==0){
			$data['tabkerjasama']=$this->Crud->readKategori($kategori,$subkategori1,$tahun);
		}
		$data['tahun1'] = date("Y", strtotime("now"));
		$data['tahun2'] = $data['tahun1']-1;
		$data['tahun3'] = $data['tahun1']-2;
		$data['tahun4'] = $data['tahun1']-3;
		$data['tahun5'] = $data['tahun1']-4;
		$data['tabkat1']=$this->Crud->read('tb_kategori',array('id_sub'=>1),'id_kategori',null);
		$data['tabkat2']=$this->Crud->read('tb_kategori',array('id_sub'=>2),'id_kategori',null);
		$data['tabkat3']=$this->Crud->read('tb_kategori',array('id_sub'=>3),'id_kategori',null);
		$data['tabkat4']=$this->Crud->read('tb_kategori',array('id_sub'=>4),'id_kategori',null);
		$data['date'] = date("l, d F Y", strtotime("now"));

		$this->load->view('hal_MOUadmin', $data);
	}	

	public function lihatFile($usr,$bid, $id, $ext){
       $file_location = 'assets/file/'.$usr.'/'.$bid;
       switch($ext){
           case 'pdf':
             $file_location = 'assets/file/'.$usr.'/'.$bid; // store as constant maybe inside index.php - PDF = 'uploads/pdf/';

             //must have PDF viewer installed in browser !
          $this->output
           ->set_content_type('application/pdf')
           ->set_output(file_get_contents($file_location . '/' . $id));

           break;
           //jpg gif etc here...
       }

    }

	public function cekFile(){
		$path  = $this->input->post('fileMOU');
		$file = pathinfo($path, PATHINFO_EXTENSION);
		if($file == 'pdf'){
			echo 'true';
		}else{
			echo 'false';
		}
	}

	public function getData($id){
		$query = $this->Crud->readKerjasamaId($id);
		foreach($query->result() as $result){
			$data = array('mitra'=>$result->mitra,
				'judul'=>$result->judul,
				'tahun'=>$result->tahun,
				'thn_lama'=>$result->thn_lama,
				'tgl_usul'=>$result->tgl_usul,
				'tgl_lama'=>$result->tgl_lama,
				'kategori'=>$result->kategori,
				'subkategori'=>$result->subkategori,
				'pengusul'=>$result->nama,
				'bidang1'=>$result->bidang1,
				'bidang2'=>$result->bidang2,
				'bidang3'=>$result->bidang3,
				'bidang4'=>$result->bidang4,
				'bidang5'=>$result->bidang5,
				'negara'=>$result->negara,
				'periode'=>$result->periode,
				'manfaat_m'=>$result->manfaat_m,
				'manfaat_u'=>$result->manfaat_u,
				'berkas1'=>$result->file1,
				'berkas2'=>$result->file2,
				'berkas3'=>$result->file3,
				'berkas4'=>$result->file4
				);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function cekData($table, $field, $data){
		$match = $this->Crud->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$bidang   = $this->input->post('bidang');
		$kategori  = $this->input->post('kategori');
		$judul  = $this->input->post('judul');
		$manfaat_m  = $this->input->post('manfaat_m');
		$manfaat_u  = $this->input->post('manfaat_u');
		$negara  = $this->input->post('negara');	
		$periode  = $this->input->post('periode');

		if($_FILES['file1']['name']=="" && $_FILES['file2']['name']=="" && $_FILES['file3']['name']=="" && $_FILES['file4']['name']==""){
				$data = array('bidang'=>$bidang, 'kategori'=>$kategori, 'judul'=>$judul, 'manfaat_m'=>$manfaat_m, 'manfaat_u'=>$manfaat_u, 'negara'=>$negara, 'periode'=>$periode);
		}else{
			$photoName = gmdate("d-m-y-H-i-s", time()+3600*7).".pdf";
			$config['upload_path'] = './assets/img/barang';
			$config['allowed_types'] = 'gif||jpg||png';
			$config['max_size'] = '2048000';
			$config['file_name'] = $photoName;
			$this->load->library('upload',$config);
			if($this->upload->do_upload('ubahfoto')){			
				$upload = 1;
				$data = array('nama'=>$nama, 'stock'=>$stock, 'harga'=>$harga,'foto'=>$photoName, 'idkategori'=>$idkategori, 'deskripsi'=>$deskripsi, 'idpetugas'=>$this->session->userdata('iduser'));
			}
			else{
				$upload = 2;
			}
				$query = $this->Crud->read('barang', array('idbarang'=>$id), null, null);
				foreach($query->result() as $result){
				unlink('assets/img/barang/'.$result->foto.'');
			}
		}
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function upload($id){
		$query = $this->Crud->readMou($id);
		foreach($query->result() as $result){
			$nama = $result->username;
			unlink('assets/file/'.$nama.'/mou/'.$result->filemou.'');
		}
		
		$fileName = $nama."-".$id."-MOU.pdf";
		$config['upload_path'] = './assets/file/'.$nama.'/mou';
		$config['allowed_types'] = 'pdf';
		$config['max_size'] = '2048000';
		$config['file_name'] = $fileName;
		$this->load->library('upload',$config);
		$this->upload->initialize($config);	
		$this->upload->do_upload('fileMOU');
		$data = array('filemou'=>$fileName);
		
		$update = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
	}

	public function delete($id)
    {
        $query = $this->Crud->read('tb_mou', array('id_mou'=>$id), null, null);
		foreach($query->result() as $result){
	    	delete_files('./assets/file/'.$this->session->userdata("nama").'/'.$result->bidang, true);
			rmdir('./assets/file/'.$this->session->userdata("nama").'/'.$result->bidang);
		}
     	$delete = $this->Crud->delete(array('id_mou'=>$id), 'tb_mou');
    }

    public function accept($id)
    {
    	$con = 1;
		$data = array('status'=>$con);
        $confirm = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
    
     
    }

    public function cancel($id)
    {
       $con = 3;
		$data = array('status'=>$con);
        $cancel = $this->Crud->update(array('id_mou'=>$id), 'tb_mou', $data);
    
     
    }

}
