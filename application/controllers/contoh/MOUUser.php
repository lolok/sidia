<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MOUUser extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['tabmou']=$this->Crud->read('tb_mou',null,'id_mou',null);

		$this->load->view('hal_MOUuser', $data);

	}

	public function tambahPengguna(){

		$nip = $this->input->post('nip');
		$nama = $this->input->post('nama');
		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$kategori = $this->input->post('kategori');
		$data  = array('nama'=>$nama, 'nip'=>$nip, 'username'=>$username, 'password'=>$password, 'email'=>$email, 'telephon'=>$phone, 'kategori'=>$kategori); 
		$insert = $this->Crud->create('tb_pengguna', $data);
		$data['tabuser']=$this->Crud->read('tb_pengguna',null,'id_pengguna',null);
		redirect(site_url('kepala/UserCntrl'));
	}

	public function getData($id){
		$query = $this->Crud->read('tb_pengguna', array('id_pengguna'=>$id), null, null);
		foreach($query->result() as $result){
			$data = array('nip'=>$result->nip,
				'nama'=>$result->nama,
				'kategori'=>$result->kategori);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function cekData($table, $field, $data){
		$match = $this->MProfil->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function update($id){
		$kategori   = $this->input->post('kategori');
		$data = array('kategori'=>$kategori);

		$update = $this->Crud->update(array('id_pengguna'=>$id), 'tb_pengguna', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $delete = $this->Crud->delete(array('id_pengguna'=>$id), 'tb_pengguna');
    
     
    }

}
