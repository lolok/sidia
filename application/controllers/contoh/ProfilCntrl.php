<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProfilCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('MProfil');
	}

	public function index(){
		$id = $this->input->get('iduser');
		$data['date'] = date("l, d F Y", strtotime("now"));
		$data['profil']=$this->MProfil->read('tb_pengguna',array('id_pengguna'=>$id),null,null);
		$this->load->view('hal_profil', $data);

	}

	public function cekData($table, $field, $data){
		$match = $this->MProfil->read($table, array($field=>$data), null, null);
		if($match->num_rows() > 0){
			$report = 2;
		}else{
			$report = 1;
		}
		echo $report;
	}

	public function getData($id){
		$query = $this->MProfil->read('tb_pengguna', array('id_pengguna'=>$id), null, null);
		foreach($query->result() as $result){
			$data = array('nip'=>$result->nip,'nama'=>$result->nama, 'username'=>$result->username, 'password'=>$result->password, 'telephone'=>$result->telephon, 'email'=>$result->email);
		}
		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function update($id){
		$nama = $this->input->post('nama');
		$nip = $this->input->post('nip');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$email = $this->input->post('email');
		$telephone = $this->input->post('telephone');

		$query = $this->MProfil->read('tb_pengguna', array('id_pengguna'=>$id), null, null);
		foreach($query->result() as $result){
			if($password != $result->password){
				$data = array('nip'=>$nip,'nama'=>$nama, 'username'=>$username, 'password'=>md5($password), 'telephon'=>$telephone, 'email'=>$email);
			}else{
				$data = array('nip'=>$nip,'nama'=>$nama, 'username'=>$username,  'telephon'=>$telephone, 'email'=>$email);
			}
		}

		$update = $this->MProfil->update(array('id_pengguna'=>$id), 'tb_pengguna', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function delete($id)
    {
        $delete = $this->MProfil->delete(array('id_pengguna'=>$id), 'tb_pengguna');
    
     
    }

}
