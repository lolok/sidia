<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InisiasiCtrl extends MY_Controller{
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->load->helper('url');
		$this->load->model('Crud');
		$this->load->helper('form');
	}
		public function index(){
			$data['date'] = date("l, d F Y", strtotime("now"));
			$data['tabmou']=$this->Crud->read('tb_mou',array('id_user'=>$this->session->userdata("id_user")),'id_mou',null);
			$data['jenis'] = $this->Crud->read('tb_jenis', null, null, null);

			$this->load->view('hal_inisiasi', $data);

		}

		public function addInisiasi(){
			$nama	= $this->session->userdata("username");
			$fakultas   = $this->input->post('fakultas');
			$tglpengusulan   = $this->input->post('tglpengusulan');
			$namapengusul   = $this->input->post('namapengusul');
			$nip   = $this->input->post('nip');
			$telp   = $this->input->post('telp');
			$hp   = $this->input->post('hp');
			$email   = $this->input->post('email');
			$caljurusan   = $this->input->post('caljurusan');
			$calfakultas   = $this->input->post('calfakultas');
			$calinstitusi   = $this->input->post('calinstitusi');
			$website   = $this->input->post('website');
			$kontak   = $this->input->post('kontak');
			$jabatan   = $this->input->post('jabatan');
			$alamatsurat   = $this->input->post('alamatsurat');
			$caltelp   = $this->input->post('caltelp');
			$calhp   = $this->input->post('calhp');
			$calemail   = $this->input->post('calemail');
			$profilcalon   = $this->input->post('profilcalon');
			$bulanusulan   = $this->input->post('bulanusulan');
			$tahunusulan   = $this->input->post('tahunusulan');
			$usulanseremoni   = $this->input->post('usulanseremoni');
			$tingkatusulan   = $this->input->post('tingkatusulan');
			$izindikti   = $this->input->post('izindikti');

			$fileName = $nama."-izindikti.pdf";
			$config['upload_path'] = './assets/file/'.$nama;
			$config['allowed_types'] = 'pdf';
			$config['max_size'] = '2048000';
			$config['file_name'] = $fileName;
			$this->load->library('upload',$config);
			$this->upload->initialize($config);
			$this->upload->do_upload('fileizindikti');

			$kegiatanmitra   = $this->input->post('kegiatanmitra');
			$partisipasi   = $this->input->post('partisipasi');
			$namapartisipasi   = $this->input->post('namapartisipasi');
			$bulanrencana   = $this->input->post('bulanrencana');
			$tahunrencana   = $this->input->post('tahunrencana');
			$aktivitas   = $this->input->post('aktivitas');
			$konsistensirensa   = $this->input->post('konsistensirensa');
			$proseskelola   = $this->input->post('proseskelola');
			$keunggulan   = $this->input->post('keunggulan');
			$faktor   = $this->input->post('faktor');
			$tantangan   = $this->input->post('tantangan');
			$solusitantangan   = $this->input->post('solusitantangan');
			$dana   = $this->input->post('dana');

			$fileName2 = $nama."-dana.pdf";
			$config2['upload_path'] = './assets/file/'.$nama;
			$config2['allowed_types'] = 'pdf';
			$config2['max_size'] = '2048000';
			$config2['file_name'] = $fileName2;
			$this->load->library('upload',$config2);
			$this->upload->initialize($config2);
			$this->upload->do_upload('filedana');

			$mou   = $this->input->post('mou');

			$fileName3 = $nama."-mou.pdf";
			$config3['upload_path'] = './assets/file/'.$nama;
			$config3['allowed_types'] = 'pdf';
			$config3['max_size'] = '2048000';
			$config3['file_name'] = $fileName3;
			$this->load->library('upload',$config3);
			$this->upload->initialize($config3);
			$this->upload->do_upload('filemou');

			$moa   = $this->input->post('moa');

			$fileName4 = $nama."-moa.pdf";
			$config4['upload_path'] = './assets/file/'.$nama;
			$config4['allowed_types'] = 'pdf';
			$config4['max_size'] = '2048000';
			$config4['file_name'] = $fileName4;
			$this->load->library('upload',$config4);
			$this->upload->initialize($config4);
			$this->upload->do_upload('filemoa');

			$tgl = date("Y", strtotime("now"));

			$data   = array('fakultas'=>$fakultas, 'id_user'=>$this->session->userdata("id_user"), 'tglpengusulan'=>$tglpengusulan, 'namapengusul'=>$namapengusul, 'nip'=>$nip, 'telp'=>$telp, 'hp'=>$hp,'email'=>$email, 'bulanusulan'=>$bulanusulan, 'tahunusulan'=>$tahunusulan, 'usulanseremoni'=>$usulanseremoni, 'tingkatusulan'=>$tingkatusulan, 'izindikti'=>$izindikti, 'fileizindikti'=>$fileName, 'kegiatanmitra'=>$kegiatanmitra, 'partisipasi'=>$partisipasi,'namapartisipasi'=>$namapartisipasi,'bulanrencana'=>$bulanrencana,'tahunrencana'=>$tahunrencana,'aktivitas'=>$aktivitas,'konsistensirensa'=>$konsistensirensa,'proseskelola'=>$proseskelola,'keunggulan'=>$keunggulan,'faktor'=>$faktor,'tantangan'=>$tantangan,'solusitantangan'=>$solusitantangan,'dana'=>$dana,'filedana'=>$fileName2,'mou'=>$mou,'filemou'=>$fileName3,'moa'=>$moa,'filemoa'=>$fileName4);
			$data2   = array('caljurusan'=>$caljurusan, 'id_user'=>$this->session->userdata("id_user"), 'calfakultas'=>$calfakultas, 'calinstitusi'=>$calinstitusi, 'website'=>$website, 'kontak'=>$kontak, 'jabatan'=>$jabatan,'alamatsurat'=>$alamatsurat, 'caltelp'=>$caltelp, 'calhp'=>$calhp, 'calemail'=>$calemail, 'profilcalon'=>$profilcalon);

			$insert = $this->Crud->create('tb_inisiasi', $data);
			$insert2 = $this->Crud->create('tb_calon', $data2);

			redirect(site_url('InisiasiCtrl'), 'refresh');




}
			public function get_subjenis(){
			$id = $this->input->post('jenis');
			$data['subjenis'] = $this->Crud->get_subjenis_byjenis($id);
			$this->load->view('subjenis',$data);
		}
	}