<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SidiaCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$this->sessionIn(); //cek session
		if($this->input->get('balasan')!=null){
			$data['report'] = 1;
		}else{
			$data['report'] = 0;
		}
		$this->load->view('login', $data);
	}

	public function forget(){
		$this->load->view('forgetpass');
	}

	public function loginProcess(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$match = $this->Crud->read('tb_user', array('username'=>$username, 'password'=>md5($password)), null, null);
		if($match->num_rows() > 0){
			foreach($match->result() as $result){
				$iduser = $result->id_user;
				$level = $result->level;
				$username = $result->username;
				$nama = $result->nama;
				$email = $result->email;
			}
			//set session
			$this->session->set_userdata('iduser', $iduser);
			$this->session->set_userdata('level', $level);
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('nama', $nama);
			$this->session->set_userdata('email', $email);
			/*switch($level){
				case "1" :redirect(site_url('DashAdmin'));
				case "2" :redirect(site_url('DashUser'));
				case "3" :redirect(site_url('DashFakultas'));
			}*/
			redirect(site_url('dashboard'));
		}else{
			redirect(site_url('sidia?balasan=1'));
		}
	}

	public function logoutProcess(){
		$this->session->unset_userdata('iduser');
		$this->session->unset_userdata('level');
		$this->session->sess_destroy();
		redirect(site_url('sidia'));
	}

}