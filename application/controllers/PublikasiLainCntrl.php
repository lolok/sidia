<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PublikasiLainCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'PENDATAAN CV - Publikasi Lainnya',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		$this->load->view('publikasi_lain', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPublikasiLain()
		];
		return $this->load->view('tabel-publikasi-lain', $data);
	}

	public function addData()
	{
		$data = [
			'id_publikasi_lain' => $this->input->post('publikasi_lain'),
			'id_user'        	=> $this->input->post('dosen'),
			'judul' 			=> $this->input->post('judul'),
			'media'           	=> $this->input->post('media'),
			'waktu' 		 	=> $this->input->post('waktu'),
		];

		$this->Crud->create('tb_publikasi_lain',$data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPublikasiLainByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user' 	   		=>$result->id_user,
				'judul'		   		=>$result->judul,
				'media'    	   		=>$result->media,
				'waktu'	  	   		=>$result->waktu,
				'id_publikasi_lain' =>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' => $this->input->post('editdosen'),
			'judul'   => $this->input->post('editjudul'),
			'media'   => $this->input->post('editmedia'),
			'waktu'   => $this->input->post('editwaktu'),
		];

		$update = $this->Crud->update(array('id_publikasi_lain'=>$id), 'tb_publikasi_lain', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_publikasi_lain'=>$id), 'tb_publikasi_lain');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->read('tb_matkul', null, null, null);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemester('tb_matkul',$semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahun('tb_matkul', $tahun);
		}else{
			$sorting = $this->Crud->sortAll('tb_matkul',$tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-matakuliah', $data);
	}

}