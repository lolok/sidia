<?php
defined('BASEPATH') OR exit('No direct script access allowed');
////web
class InfoCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'Additional Info',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('additionalinfo', $data);
	}

	public function getOngoing()
	{
		$data = [
			'tabel' => $this->Crud->readOngoing()
		];
		return $this->load->view('tabel-ongoing', $data);
	}

	public function addOngoing()
	{
		$data = [
			'nama_penelitian' => $this->input->post('nama_penelitian'),
			'desc_ongoing' => $this->input->post('desc_ongoing'),
		];
		$this->Crud->create('tb_ongoing',$data);
	}

		public function editOngoing()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_penelitian' => $this->input->post('nama_penelitian'),
			'desc_ongoing' => $this->input->post('desc_ongoing'),
		];

		$update = $this->Crud->update(array('id_ongoing'=>$id), 'tb_ongoing', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}
	public function getDataInfo()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readOngoingByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama_penelitian'=>$result->nama_penelitian,
				'desc_ongoing'=>$result->desc_ongoing,
				
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function hapusOngoing()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFileOngoing($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_ongoing'=>$id), 'tb_ongoing');
	}
	
}