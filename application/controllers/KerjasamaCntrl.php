<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KerjasamaCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'SIDIA - KERJASAMA',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('kerjasama', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readKerjasama()
		];
		return $this->load->view('tabel-kerjasama', $data);
	}

	public function getDosen()
	{
		$id = $this->input->get('id');

		$data = [
			'data_dosen' => $this->Crud->readDosenKerjasama($id)
		];

		return $this->load->view('tabel-kerjasama-dosen', $data);
	}

	public function addDosen()
	{
		$id = $this->input->post('id_data');

		$param = [
			'id_user' => $this->input->post('dosen'),
			'id_kerjasama' => $id,
		];

		$this->Crud->create('tb_dosen_kerjasama',$param);

		$data = [
			'data_dosen' => $this->Crud->readDosenKerjasama($id)
		];

		return $this->load->view('tabel-kerjasama-dosen', $data);
	}

	public function hapusDosen()
	{
		$id = $this->input->get('id');
		$iddata = $this->input->get('iddata');

		// $matkul = $this->Crud->read('tb_matkul', ['id_matkul' => $idmatkul], null, null);
		// foreach ($matkul->result() as $key) {
		// 	$dosen = $key->dosen;
		// }
		// $minusdosen = $dosen-1;

		// $update = $this->Crud->update(array('id_matkul'=>$idmatkul), 'tb_matkul', ['dosen' => $minusdosen]);

		$delete = $this->Crud->delete(array('id_dosen_kerjasama'=>$id), 'tb_dosen_kerjasama');

		$data = [
			'data_dosen' => $this->Crud->readDosenKerjasama($iddata)
		];

		return $this->load->view('tabel-kerjasama-dosen', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFileKerjasama($id,$kategori)
		];
		return $this->load->view('file-kerjasama', $data);
	}

	public function addData()
	{
		$data = [
			'nama_kerjasama' => $this->input->post('nm_kerjasama'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_kerjasama',$data);

		$id = $this->Crud->readLast('tb_kerjasama','id_kerjasama');
		
		$this->upload_files($id,'FileAdministrasi','id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['fileadministrasi']);
		$this->upload_files($id,'laporan','id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['laporan']);
		$this->upload_files($id,'ppt','id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['ppt']);
		$this->upload_files($id,'dokumentasi','id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['dokumentasi']);
		$this->upload_files($id,'data','id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['data']);
	}
	
	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_kerjasama','file_kerjasama','assets/file/kerjasama/','kerjasama', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFileKerjasama($id,$kategori)
		];
		return $this->load->view('file-kerjasama', $data);
	}
	
	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idkerjasama = $this->input->get('idkerjasama');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFileKerjasamaID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		($path);

		$delete = $this->Crud->delete(array('id_file_kerjasama'=>$id), 'file_kerjasama');
		

		$data = [
			'tabel_file' => $this->Crud->readFileKerjasama($idkerjasama,$kategori)
		];
		return $this->load->view('file-kerjasama', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readKerjasamaByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama_kerjasama'=>$result->nama_kerjasama,
				'tahun'=>$result->tahun,
				'semester'=>$result->semester,
				'id_kerjasama'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_kerjasama' => $this->input->post('editnama_kerjasama'),
			'semester' => $this->input->post('editsemester_kerjasama'),
			'tahun' => $this->input->post('edittahun_kerjasama'),
		];

		$update = $this->Crud->update(array('id_kerjasama'=>$id), 'tb_kerjasama', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_kerjasama'=>$id), 'tb_kerjasama');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readKerjasama();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterKerjasama($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunKerjasama($tahun);
		}else{
			$sorting = $this->Crud->sortAllKerjasama($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-kerjasama', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.$valueid.'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);
            
            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}