<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PendidikanCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'E-BKD - Pendidikan',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('pendidikan', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPengajaran()
		];
		return $this->load->view('tabel-pengajaran', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function addData()
	{
		$data = [
			'id_matkul' => $this->input->post('matkul'),
			'id_user' => $this->input->post('dosen'),
			'jml_tatapmuka' => $this->input->post('jml_tatapmuka'),
			'total_tatapmuka' => $this->input->post('total_tatapmuka'),
			'masa_penugasan1' => $this->input->post('masa_penugasan1'),
			'masa_penugasan2' => $this->input->post('masa_penugasan2'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_pengajaran',$data);

		$id = $this->Crud->readLast('tb_pengajaran','id_pengajaran');

		$this->upload_files($id,'kontrak','id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['filekontrak']);
		$this->upload_files($id,'absendosen','id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['absendosen']);
		$this->upload_files($id,'absenmhs','id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['absenmhs']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpengajaran = $this->input->get('idpengajaran');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePengajaranID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_pengajaran'=>$id), 'file_pengajaran');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($idpengajaran,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPengajaranByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_matkul'=>$result->id_matkul,
				'id_user'=>$result->id_user,
				'jml_tatapmuka'=>$result->jml_tatapmuka,
				'total_tatapmuka'=>$result->total_tatapmuka,
				'masa_penugasan1'=>$result->masa_penugasan1,
				'masa_penugasan2'=>$result->masa_penugasan2,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'id_pengajaran'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_matkul' => $this->input->post('editmatkul'),
			'id_user' => $this->input->post('editdosen'),
			'jml_tatapmuka' => $this->input->post('editjml_tatapmuka'),
			'total_tatapmuka' => $this->input->post('edittotal_tatapmuka'),
			'masa_penugasan1' => $this->input->post('editmasa_penugasan1'),
			'masa_penugasan2' => $this->input->post('editmasa_penugasan2'),
			'semester' => $this->input->post('editsemester'),
			'tahun' => $this->input->post('edittahun'),
		];

		$update = $this->Crud->update(array('id_pengajaran'=>$id), 'tb_pengajaran', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePengajaranALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_pengajaran'=>$id), 'tb_pengajaran');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPengajaran();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPengajaran($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPengajaran($tahun);
		}else{
			$sorting = $this->Crud->sortAllPengajaran($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-pengajaran', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}