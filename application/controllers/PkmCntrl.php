<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PkmCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - PKM',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('pkm', $data);
	}

	public function getTabel()
	{
		$level = $this->session->userdata('level');
		$id = $this->session->userdata('iduser');

		if ($level == 1) {
			$data = [
				'tabel' => $this->Crud->readPkm()
			];
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPkm($id)
			];
		}

		return $this->load->view('tabel-pkm', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePkm($id,$kategori)
		];
		return $this->load->view('file-pkm', $data);
	}

	public function addData()
	{
		$data = [
			'id_pkm'   => $this->input->post('pkm'),
			'id_user'  => $this->input->post('dosen'),
			'nama_pkm' => $this->input->post('nama_pkm'),
			'kategori_id' => $this->input->post('kategori_id'),
			'kategori' => $this->input->post('kategori'),
			'sks' => $this->input->post('sks'),
			'anggota_id' => $this->input->post('anggota_id'),
			'status_anggota' => $this->input->post('status_anggota'),
			'nilai_status_anggota' => $this->input->post('nilai_status_anggota'),
			'pencapaian_id' => $this->input->post('pencapaian_id'),
			'status_pencapaian' => $this->input->post('status_pencapaian'),
			'nilai_status_pencapaian' => $this->input->post('nilai_status_pencapaian'),
			'masa_penugasan' => $this->input->post('masa_penugasan'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
			'semester' => $this->input->post('semester'),
			'tahun'    => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_pkm',$data);

		$id = $this->Crud->readLast('tb_pkm','id_pkm');

		$this->upload_files($id,'penugasan','id_pkm','file_pkm','assets/file/tridarma/pkm/','pkm', $_FILES['penugasan']);
		$this->upload_files($id,'laporan','id_pkm','file_pkm','assets/file/tridarma/pkm/','pkm', $_FILES['laporan']);
		$this->upload_files($id,'dokumentasi','id_pkm','file_pkm','assets/file/tridarma/pkm/','pkm', $_FILES['dokumentasi']);
	}

	public function getFileBerkasTable()
	{
		$id = $this->input->post('id');
		$file1 = $this->input->post('file1');
		$file2 = $this->input->post('file2');

		$data = [
			'berkas' => $this->Crud->readFileBerkas($file1,$file2),
		];

		return $this->load->view('file-berkas-data', $data);
	}

	public function getBerkas()
	{
		$data = $this->Crud->readDataBerkas(1,5);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkasId()
	{
		$val = $this->input->post('val');
		$data = $this->Crud->readDataBerkasId(1,5,$val);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_pkm','file_pkm','assets/file/tridarma/pkm/','pkm', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePkm($id,$kategori)
		];
		return $this->load->view('file-pkm', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpkm = $this->input->get('idpkm');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePkmID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_pkm'=>$id), 'file_pkm');
		
		$data = [
			'tabel_file' => $this->Crud->readFilePkm($idpkm,$kategori)
		];
		return $this->load->view('file-pkm', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPkmByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user'=>$result->id_user,
				'nama_pkm'=>$result->nama_pkm,
				'kategori_id'=>$result->kategori_id,
				'anggota_id'=>$result->anggota_id,
				'pencapaian_id'=>$result->pencapaian_id,
				'masa_penugasan'=>$result->masa_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'tahun'=>$result->tahun,
				'semester'=>$result->semester,
				'id_pkm'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' => $this->input->post('editdosen'),
			'nama_pkm' => $this->input->post('editnama'),
			'kategori_id' => $this->input->post('kategori_id'),
			'kategori' => $this->input->post('kategori'),
			'sks' => $this->input->post('sks'),
			'anggota_id' => $this->input->post('anggota_id'),
			'status_anggota' => $this->input->post('status_anggota'),
			'nilai_status_anggota' => $this->input->post('nilai_status_anggota'),
			'pencapaian_id' => $this->input->post('pencapaian_id'),
			'status_pencapaian' => $this->input->post('status_pencapaian'),
			'nilai_status_pencapaian' => $this->input->post('nilai_status_pencapaian'),
			'masa_penugasan' => $this->input->post('editmasa_penugasan'),
			'bukti_penugasan' => $this->input->post('editbukti_penugasan'),
			'bukti_dokumen' => $this->input->post('editbukti_dokumen'),
			'semester' => $this->input->post('editsemester'),
			'tahun' => $this->input->post('edittahun'),
		];

		$update = $this->Crud->update(array('id_pkm'=>$id), 'tb_pkm', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePkmALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_pkm'=>$id), 'tb_pkm');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPkm();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPkm($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPkm($tahun);
		}else{
			$sorting = $this->Crud->sortAllPkm($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-pkm', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }

}