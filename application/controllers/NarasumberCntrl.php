<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NarasumberCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'PENDATAAN CV - Kegiatan Ilmiah ',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		$this->load->view('narasumber', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readNarasumber()
		];
		return $this->load->view('tabel-narasumber', $data);
	}

	public function addData()
	{
		$data = [
			'id_narasumber' => $this->input->post('narasumber'),
			'id_user'       => $this->input->post('dosen'),
			'kegiatan' 		=> $this->input->post('kegiatan'),
			'status' 		=> $this->input->post('status'),
			'penyelenggara' => $this->input->post('penyelenggara'),
			'tempat_waktu' 	=> $this->input->post('tempat_waktu'),
		];

		$this->Crud->create('tb_narasumber',$data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readNarasumberByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user' 	   	=>$result->id_user,
				'kegiatan'		=>$result->kegiatan,
				'status'    	=>$result->status,
				'penyelenggara' =>$result->penyelenggara,
				'tempat_waktu'	=>$result->tempat_waktu,
				'id_narasumber' =>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' 		=> $this->input->post('editdosen'),
			'kegiatan'   	=> $this->input->post('editkegiatan'),
			'status'   		=> $this->input->post('editstatus'),
			'penyelenggara' => $this->input->post('editpenyelenggara'),
			'tempat_waktu'  => $this->input->post('edittempatwaktu'),
		];

		$update = $this->Crud->update(array('id_narasumber'=>$id), 'tb_narasumber', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_narasumber'=>$id), 'tb_narasumber');
	}
}