<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LarademCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data = [
			'title' => ' | Home',
			'navbar' => 1
		];
		$data['profile'] = $this->Crud->getProfil()->result();
		$data['publish'] = $this->Crud->getPublishLimit()->result();
		$data['ongoing'] = $this->Crud->getOngoing()->result();

		$this->load->view('laradem/laradem', $data);
	}

	public function aboutus(){
		$data = [
			'title' => ' | About',
			'navbar' => 2
		];
		$data['profile'] = $this->Crud->getProfil()->result();
		$data['team'] = $this->Crud->getAboutUs()->result();
		$data['galery'] = $this->Crud->getGalery()->result();

		$this->load->view('laradem/aboutus', $data);
	}

	public function news(){
		$data = [
			'title' => ' | News',
			'navbar' => 3
		];
		$data['profile'] = $this->Crud->getProfil()->result();
		$this->load->view('laradem/news', $data);
	}

	public function ourwork(){
		$data = [
			'title' => ' | Our Work',
			'navbar' => 4
		];
		$data['profile'] = $this->Crud->getProfil()->result();
		$this->load->view('laradem/ourwork', $data);
	}

	public function publication(){
		$data = [
			'title' => ' | Publication',
			'navbar' => 5
		];
		$data['publish'] = $this->Crud->getPublish()->result();
		$data['profile'] = $this->Crud->getProfil()->result();
		$this->load->view('laradem/publication', $data);
	}

	public function contactus(){
		$data = [
			'title' => ' | Hubungi Kami',
			'navbar' => 6
		];
		$data['profile'] = $this->Crud->getProfil()->result();
		$this->load->view('laradem/contactus', $data);
	}

	public function forget(){
		$this->load->view('forgetpass');
	}

	public function loginProcess(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$match = $this->Crud->read('user', array('username'=>$username, 'password'=>md5($password)), null, null);
		if($match->num_rows() > 0){
			foreach($match->result() as $result){
				$iduser = $result->id_user;
				$level = $result->level;
				$username = $result->username;
				$nama = $result->nama;
				$email = $result->email;
			}
			//set session
			$this->session->set_userdata('iduser', $iduser);
			$this->session->set_userdata('level', $level);
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('nama', $nama);
			$this->session->set_userdata('email', $email);
			/*switch($level){
				case "1" :redirect(site_url('DashAdmin'));
				case "2" :redirect(site_url('DashUser'));
				case "3" :redirect(site_url('DashFakultas'));
			}*/
			redirect(site_url('dashboard'));
		}else{
			redirect(site_url('login?balasan=1'));
		}
	}

	public function logoutProcess(){
		$this->session->unset_userdata('iduser');
		$this->session->unset_userdata('level');
		$this->session->sess_destroy();
		redirect(site_url(''));
	}

}