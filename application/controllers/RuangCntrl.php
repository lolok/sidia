<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RuangCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'DAFTAR RUANG',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year
		];
		$this->load->view('ruang', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->read('tb_user', ['level' => 2], null, null)
		];
		return $this->load->view('tabel-ruang', $data);
	}

	public function addData()
	{
		$data = [
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'level' => 2,
		];

		$this->Crud->create('tb_user',$data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readDataByID('tb_user','id_user',$id);
		foreach($query->result() as $result){
			$data = [
				'nip'=>$result->nip,
				'nama'=>$result->nama,
				'id_user'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama' => $this->input->post('editnama'),
			'nip' => $this->input->post('editnip'),
		];

		$update = $this->Crud->update(array('id_user'=>$id), 'tb_user', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_user'=>$id), 'tb_user');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->read('tb_matkul', null, null, null);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemester('tb_matkul',$semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahun('tb_matkul', $tahun);
		}else{
			$sorting = $this->Crud->sortAll('tb_matkul',$tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-matakuliah', $data);
	}

}