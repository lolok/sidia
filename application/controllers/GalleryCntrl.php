<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GalleryCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'GALLERY',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('gallery', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readGalery()
		];
		return $this->load->view('tabel-galery', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function addData()
	{
		$nama = $this->input->post('nama');
		$this->upload_files($nama,$kategori,$semester,$tahun,'assets/file/berkas/', $_FILES['fileberkas']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readBerkasID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}

		unlink($path);

		$delete = $this->Crud->delete(array('id_galery'=>$id), 'file_galery');
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPengajaranByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_matkul'=>$result->id_matkul,
				'id_user'=>$result->id_user,
				'jml_tatapmuka'=>$result->jml_tatapmuka,
				'total_tatapmuka'=>$result->total_tatapmuka,
				'masa_penugasan'=>$result->masa_penugasan,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'id_pengajaran'=>$id,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}


	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPengajaran();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPengajaran($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPengajaran($tahun);
		}else{
			$sorting = $this->Crud->sortAllPengajaran($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-pengajaran', $data);
	}


	private function upload_files($nama, $kategori, $semester, $tahun, $path, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $name = $kategori.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $nama,
            	'path_file' => $path.$name.'.'.$ext
            	
            ];

            $this->Crud->create('file_galery',$data);
        }

        return true;
    }
}