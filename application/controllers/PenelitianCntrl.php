<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenelitianCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - PENELITIAN',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('penelitian', $data);
	}

	public function ebkd()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'E-BKD - Penelitian',
			'date' => date('l, d-m-Y', strtotime("now")),
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('bkdpenelitian', $data);
	}

	public function getFileBerkasTable()
	{
		$id = $this->input->post('id');
		$file1 = $this->input->post('file1');
		$file2 = $this->input->post('file2');

		$data = [
			'berkas' => $this->Crud->readFileBerkas($file1,$file2),
		];

		return $this->load->view('file-berkas-data', $data);
	}

	public function getBerkas()
	{
		$data = $this->Crud->readDataBerkas(1,4);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkasId()
	{
		$val = $this->input->post('val');
		$data = $this->Crud->readDataBerkasId(1,4,$val);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBkd()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPenelitian($iddosen)
		];

		return $this->load->view('tabel-bkd-penelitian', $data);
	}

	public function sortingBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readBkdPenelitian($iddosen);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortBkdSemesterPenelitian($semester,$iddosen);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortBkdTahunPenelitian($tahun,$iddosen);
		}else{
			$sorting = $this->Crud->sortBkdAllPenelitian($tahun,$semester,$iddosen);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-bkd-penelitian', $data);
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array('id_penelitian'=>$id), 'tb_penelitian', $data);
	}

	public function getTabel()
	{
		$level = $this->session->userdata('level');
		$id = $this->session->userdata('iduser');

		if ($level == 1) {
			$data = [
				'tabel' => $this->Crud->readPenelitian()
			];
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPenelitian($id)
			];
		}
		return $this->load->view('tabel-penelitian', $data);
	}

	public function getFileTable()
	{	
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($id,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function addData()
	{
		$data = [
			'nama_penelitian' => $this->input->post('nama_penelitian'),
			'status_anggota' => $this->input->post('status_anggota'),
			'nilai_status_anggota' => $this->input->post('nilai_status_anggota'),
			'sks_status_anggota' => $this->input->post('sks_status_anggota'),
			'kategori' => $this->input->post('kategori'),
			'kategori_id' => $this->input->post('kategori_id'),
			'jenis_id' => $this->input->post('jenis_id'),
			'anggota_id' => $this->input->post('anggota_id'),
			'pencapaian_id' => $this->input->post('pencapaian_id'),
			'jenis' => $this->input->post('jenis'),
			'sks' => $this->input->post('sks'),
			'status_pencapaian' => $this->input->post('status_pencapaian'),
			'nilai_status_pencapaian' => $this->input->post('nilai_status_pencapaian'),
			'masa_penugasan ' => $this->input->post('masa_penugasan'),
			'id_user' => $this->input->post('dosen'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
		];

		$this->Crud->create('tb_penelitian',$data);

		$id = $this->Crud->readLast('tb_penelitian','id_penelitian');

		$this->upload_files($id,'skpenugasan','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['skpenugasan']);
		$this->upload_files($id,'laporan','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['laporan']);
		$this->upload_files($id,'dokumentasi','id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['dokumentasi']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_penelitian','file_penelitian','assets/file/tridarma/penelitian/','penelitian', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($id,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpenelitian = $this->input->get('idpenelitian');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePenelitianID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_penelitian'=>$id), 'file_penelitian');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePenelitian($idpenelitian,$kategori)
		];
		return $this->load->view('file-penelitian', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPenelitianByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama_penelitian'=>$result->nama_penelitian,
				'id_user'=>$result->id_user,
				'masa_penugasan'=>$result->masa_penugasan,
				'status_anggota'=>$result->status_anggota,
				'kategori_id'=>$result->kategori_id,
				'jenis_id'=>$result->jenis_id,
				'anggota_id'=>$result->anggota_id,
				'pencapaian_id'=>$result->pencapaian_id,
				'nilai_status_anggota'=>$result->nilai_status_anggota,
				'nilai_status_pencapaian'=>$result->nilai_status_pencapaian,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
				'id_penelitian'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_penelitian' => $this->input->post('edit_nama_penelitian'),
			'id_user' => $this->input->post('edit_dosen'),
			'status_anggota' => $this->input->post('status_anggota'),
			'nilai_status_anggota' => $this->input->post('nilai_status_anggota'),
			'sks_status_anggota' => $this->input->post('sks_status_anggota'),
			'kategori_id' => $this->input->post('kategori_id'),
			'jenis_id' => $this->input->post('jenis_id'),
			'pencapaian_id' => $this->input->post('pencapaian_id'),
			'anggota_id' => $this->input->post('anggota_id'),
			'kategori' => $this->input->post('kategori'),
			'jenis' => $this->input->post('jenis'),
			'sks' => $this->input->post('sks'),
			'status_pencapaian' => $this->input->post('status_pencapaian'),
			'nilai_status_pencapaian' => $this->input->post('nilai_status_pencapaian'),
			'masa_penugasan ' => $this->input->post('masa_penugasan'),
			'semester' => $this->input->post('edit_semester'),
			'tahun' => $this->input->post('edit_tahun'),
			'bukti_penugasan' => $this->input->post('editbukti_penugasan'),
			'bukti_dokumen' => $this->input->post('editbukti_dokumen'),
		];

		$update = $this->Crud->update(array('id_penelitian'=>$id), 'tb_penelitian', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePenelitianALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_penelitian'=>$id), 'tb_penelitian');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPenelitian();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPenelitian($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPenelitian($tahun);
		}else{
			$sorting = $this->Crud->sortAllPenelitian($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-penelitian', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}