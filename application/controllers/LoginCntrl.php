<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		
		$this->sessionIn(); //cek session
		if($this->input->get('balasan')!=null){
			$data['report'] = 1;
		}else{
			$data['report'] = 0;
		}
		
		$this->load->view('hal_login', $data);
	}

	public function loginProcess(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$match = $this->Crud->read('tb_user', array('username'=>$username, 'password'=>md5($password)), null, null);
		if($match->num_rows() > 0){
			foreach($match->result() as $result){
				$iduser = $result->id_user;
				$level = $result->level;
				$username = $result->username;
				$nama = $result->nama;
				$namaper = $result->namaper;
				$email = $result->email;
			}
			//set session
			$this->session->set_userdata('iduser', $iduser);
			$this->session->set_userdata('level', $level);
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('nama', $nama);
			$this->session->set_userdata('namaper', $namaper);
			$this->session->set_userdata('email', $email);
			switch($level){
				case "1" :redirect(site_url('DashAdmin'));
				case "2" :redirect(site_url('DashUser'));
				case "3" :redirect(site_url('DashFakultas'));
			}
		}else{
			redirect(site_url('LoginCntrl?balasan=1'));
		}
	}

	public function logoutProcess(){
		$this->session->unset_userdata('iduser');
		$this->session->unset_userdata('level');
		$this->session->sess_destroy();
		redirect(site_url(''));
	}

	public function lupaPassword(){
		$this->load->view('hal_lupa');
	}

	public function getNotification(){
		$notif = $this->pullNotification();
		echo $notif;
	}

	public function ubahPassword(){
		$id_pengguna = $this->session->userdata('idpetta');
		$oldpass = sha1($this->input->post('oldpass'));
		$newpass = $this->input->post('newpass');
		$confirmnewpass = $this->input->post('confirmnewpass');
		//cek old pass
		$query_cek = $this->MPengguna->read(array('id_pengguna'=>$id_pengguna, 'password'=>$oldpass), null, null);
		if($query_cek->num_rows() > 0){
			if($newpass==$confirmnewpass){
				$data = array('password'=>sha1($newpass));
				$reset = $this->MPengguna->update(array('id_pengguna'=>$id_pengguna), $data);
				if($reset){
					echo 1;
				}else{
					echo 4; //gagal
				}
			}else{
				echo 3; //konfirm tidak sama
			}
		}else{
			echo 2; //password lama salah
		}
	}
}