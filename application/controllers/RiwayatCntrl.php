<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RiwayatCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'SIDIA - RIWAYAT FORMAL',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('riwayat', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readRiwayat()
		];

		return $this->load->view('tabel-riwayat', $data);
	}
	
	public function getTabelnf()
	{
		
		$data = [
			'tabel' => $this->Crud->readRiwayatnf()
		];
		return $this->load->view('tabel-riwayatnf', $data);
	}

	public function addData()
	{
		$data = [
			'perguruan_tinggi' => $this->input->post('nama_perguruan_tinggi'),
			'bidang_ilmu' => $this->input->post('bidang'),
			'tahun_masuk_lulus' => $this->input->post('tahun'),
			'judul_skripsi' => $this->input->post('judul'),
			'kategori' => $this->input->post('kategori'),
			'nama_pembimbing' => $this->input->post('nama_pembimbing'),
			'id_user' => $this->input->post('dosen'),
			
		];

		$this->Crud->create('tb_riwayat',$data);

		$id = $this->Crud->readLast('tb_riwayat','id_riwayat');
	}
	
	public function addDatanf()
	{
		$data = [
			'nama_kegiatan' => $this->input->post('kegiatan'),
			'penyelenggara' => $this->input->post('penyelenggara'),
			'tempat' => $this->input->post('tempat'),
			'waktu' => $this->input->post('waktu'),
			'id_user' => $this->input->post('dosen'),
			
		];

		$this->Crud->create('tb_riwayatnf',$data);

		$id = $this->Crud->readLast('tb_riwayatnf','id_riwayatnf');
	}

	public function getData()
	{
		$id = $this->input->get('id');
		$query = $this->Crud->readRiwayatByID($id);
		foreach($query->result() as $result){
			$data = [
			'perguruan_tinggi' => $result->perguruan_tinggi,
			'bidang_ilmu' => $result->bidang_ilmu,
			'tahun' => $result->tahun_masuk_lulus,
			'judul' => $result->judul_skripsi,
			'nama_pembimbing' => $result->nama_pembimbing,
			'kategori' => $result->kategori,
			'id_user' => $result->id_user,
			'id_riwayat' => $id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}
	
	public function getDatanf()
	{
		$id = $this->input->get('id');
		$query = $this->Crud->readRiwayatnfByID($id);
		foreach($query->result() as $result){
			$data = [
			'nama_kegiatan' => $result->nama_kegiatan,
			'penyelenggara' => $result->penyelenggara,
			'tempat' => $result->tempat,
			'waktu' => $result->waktu,
			'id_user' => $result->id_user,
			'id_riwayatnf' => $id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'perguruan_tinggi' => $this->input->post('edit_perguruan_tinggi'),
			'bidang_ilmu' => $this->input->post('edit_bidang'),
			'tahun_masuk_lulus' => $this->input->post('edit_tahun'),
			'judul_skripsi' => $this->input->post('edit_judul'),
			'kategori' => $this->input->post('edit_kategori'),
			'nama_pembimbing' => $this->input->post('edit_nama_pembimbing'),
		];

		$update = $this->Crud->update(array('id_riwayat'=>$id), 'tb_riwayat', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}
	
	public function editDatanf()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_kegiatan' => $this->input->post('edit_kegiatan'),
			'penyelenggara' => $this->input->post('edit_penyelenggara'),
			'tempat' => $this->input->post('edit_tempat'),
			'waktu' => $this->input->post('edit_waktu'),
			'id_user' => $this->input->post('edit_dosen'),
		];

		$update = $this->Crud->update(array('id_riwayatnf'=>$id), 'tb_riwayatnf', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_riwayat'=>$id), 'tb_riwayat');
	}
	
	public function hapusDatanf()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_riwayatnf'=>$id), 'tb_riwayatnf');
	}
	
	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readRiwayat();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterRiwayat($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunRiwayat($tahun);
		}else{
			$sorting = $this->Crud->sortAllRiwayat($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-riwayat', $data);
	}
}