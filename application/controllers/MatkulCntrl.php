<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MatkulCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'DAFTAR MATAKULIAH',
			'date' => date('l, d-m-Y', strtotime("now")),
			'dosen' => $this->Crud->read('tb_user', ['level' => 2], null, null),
			'tahun' => $year
		];
		return $this->load->view('matakuliah', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->read('tb_matkul', null, null, null)
		];
		return $this->load->view('tabel-matakuliah', $data);
	}

	public function addData()
	{
		$data = [
			'kode_matkul' => $this->input->post('kode_matkul'),
			'nama_matkul' => $this->input->post('nama_matkul'),
			'sks_matkul' => $this->input->post('sks_matkul'),
			'jenjang' => $this->input->post('jenjang'),
			'kelas' => $this->input->post('kelas'),
			'mhs' => $this->input->post('mhs'),
			'semester' => $this->input->post('semester_matkul'),
			'tahun' => $this->input->post('tahun_matkul'),
		];

		$this->Crud->create('tb_matkul',$data);

		$id = $this->Crud->readLast('tb_matkul','id_matkul');

		$this->upload_files($id,'kontrak','id_matkul','file_matkul','assets/file/tridarma/matkul/','matkul', $_FILES['filekontrak']);
		$this->upload_files($id,'absendosen','id_matkul','file_matkul','assets/file/tridarma/matkul/','matkul', $_FILES['absendosen']);
		$this->upload_files($id,'absenmhs','id_matkul','file_matkul','assets/file/tridarma/matkul/','matkul', $_FILES['absenmhs']);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readDataByID('tb_matkul','id_matkul',$id);
		foreach($query->result() as $result){
			$data = [
				'kode_matkul'=>$result->kode_matkul,
				'nama_matkul'=>$result->nama_matkul,
				'sks_matkul'=>$result->sks_matkul,
				'jenjang'=>$result->jenjang,
				'kelas'=>$result->kelas,
				'tahun_matkul'=>$result->tahun,
				'mhs'=>$result->mhs,
				'semester_matkul'=>$result->semester,
				'id_matkul'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function getDosen()
	{
		$id = $this->input->get('id');

		$data = [
			'data_dosen' => $this->Crud->readDosenMatkul($id)
		];

		return $this->load->view('tabel-matkul-dosen', $data);
	}

	public function addDosen()
	{
		$id = $this->input->post('id_matkul');

		$param = [
			'id_user' => $this->input->post('dosen'),
			'id_matkul' => $id,
			'status' => $this->input->post('status_dosen'),
		];

		$this->Crud->create('tb_dosen_matkul',$param);

		$matkul = $this->Crud->read('tb_matkul', ['id_matkul' => $id], null, null);
		foreach ($matkul->result() as $key) {
			$dosen = $key->dosen;
		}

		$update = $this->Crud->update(array('id_matkul'=>$id), 'tb_matkul', ['dosen' => $dosen+1]);

		$data = [
			'data_dosen' => $this->Crud->readDosenMatkul($id)
		];

		return $this->load->view('tabel-matkul-dosen', $data);
	}

	public function hapusDosen()
	{
		$id = $this->input->get('id');
		$idmatkul = $this->input->get('idmatkul');

		$matkul = $this->Crud->read('tb_matkul', ['id_matkul' => $idmatkul], null, null);
		foreach ($matkul->result() as $key) {
			$dosen = $key->dosen;
		}
		$minusdosen = $dosen-1;

		$update = $this->Crud->update(array('id_matkul'=>$idmatkul), 'tb_matkul', ['dosen' => $minusdosen]);

		$delete = $this->Crud->delete(array('id_dosen_matkul'=>$id), 'tb_dosen_matkul');

		$data = [
			'data_dosen' => $this->Crud->readDosenMatkul($idmatkul)
		];

		return $this->load->view('tabel-matkul-dosen', $data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'kode_matkul' => $this->input->post('editkode_matkul'),
			'nama_matkul' => $this->input->post('nama_matkul'),
			'sks_matkul' => $this->input->post('editsks'),
			'jenjang' => $this->input->post('editjenjang'),
			'kelas' => $this->input->post('editkelas'),
			'mhs' => $this->input->post('editmhs'),
			'semester' => $this->input->post('editsemester_matkul'),
			'tahun' => $this->input->post('edittahun_matkul'),
		];

		$update = $this->Crud->update(array('id_matkul'=>$id), 'tb_matkul', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_matkul'=>$id), 'tb_matkul');
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_matkul','file_matkul','assets/file/tridarma/matkul/','matkul', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpengajaran = $this->input->get('idpengajaran');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePengajaranID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_matkul'=>$id), 'file_matkul');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($idpengajaran,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->read('tb_matkul', null, null, null);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemester('tb_matkul',$semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahun('tb_matkul', $tahun);
		}else{
			$sorting = $this->Crud->sortAll('tb_matkul',$tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-matakuliah', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }

}