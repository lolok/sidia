<?php
defined('BASEPATH') OR exit('No direct script access allowed');
////web
class WebCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function profile()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'Web Profile',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('web/profile', $data);
	}
	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readProfile()
		];
		return $this->load->view('tabel-profile', $data);
	}
	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readProfileByID($id);
		foreach($query->result() as $result){
			$data = [
				'nama_web'=>$result->nama_web,
				'email'=>$result->email,
				'no_hp'=>$result->no_hp,
				'alamat'=>$result->alamat,
				'link_fb'=>$result->link_fb,
				'link_twitter'=>$result->link_twitter,
				'link_ig'=>$result->link_ig,
				'link_linkedin'=>$result->link_linkedin,
				'desc_laradem'=>$result->desc_laradem,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama_web' => $this->input->post('editnamaweb'),
			'email' => $this->input->post('editemail'),
			'no_hp' => $this->input->post('editnohp'),
			'alamat' => $this->input->post('editalamat'),
			'link_fb' => $this->input->post('editlink_fb'),
			'link_twitter' => $this->input->post('editlink_twitter'),
			'link_ig' => $this->input->post('editlink_ig'),
			'link_linkedin' => $this->input->post('editlink_linkedin'),
			'desc_laradem' => $this->input->post('editdesc'),
		];

		$update = $this->Crud->update(array('id_profile'=>'1'), 'tb_profile', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}
////publikasi
		public function publikasi()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'Publikasi Judul',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('web/publikasi', $data);
	}


	public function getPublish()
	{
		$data = [
			'tabel' => $this->Crud->readPublish()
		];
		return $this->load->view('tabel-publish', $data);
	}

	public function addPublish()
	{
		$data = [
			'judul' => $this->input->post('judul'),
			'jenis' => $this->input->post('jenis'),
			'tanggal' => $this->input->post('tanggal'),
			'publisher' => $this->input->post('publisher'),
			'link' => $this->input->post('link'),

		];

		$this->Crud->create('tb_publish',$data);
	}

		public function editPublish()
	{
		$id = $this->input->post('id');

		$data = [
			'judul' => $this->input->post('judul'),
			'jenis' => $this->input->post('jenis'),
			'tanggal' => $this->input->post('tanggal'),
			'publisher' => $this->input->post('publisher'),
			'link' => $this->input->post('link'),
		];

		$update = $this->Crud->update(array('id_publish'=>$id), 'tb_publish', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}
	public function getDataPublish()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPublishByID($id);
		foreach($query->result() as $result){
			$data = [
				'judul'=>$result->judul,
				'jenis'=>$result->jenis,
				'tanggal'=>$result->tanggal,
				'publisher'=>$result->publisher,
				'link'=>$result->link,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function hapusPublish()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePublish($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_publish'=>$id), 'tb_publish');
	}
////info
		public function info()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'Additional Info',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('web/info', $data);
	}






	
}