<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CvCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - CV',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('cv', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readCv()
		];
		return $this->load->view('tabel-cv', $data);
	}

	public function getFileTable()
	{	
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFileCv($id,$kategori)
		];
		return $this->load->view('file-cv', $data);
	}

	public function addData()
	{
		$data = [
			'id_user' => $this->input->post('dosen'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_cv',$data);

		$id = $this->Crud->readLast('tb_cv','id_cv');

		$this->upload_files($id,'cv_simlitabmas','id_cv','file_cv','assets/file/cv/','cv', $_FILES['cv_simlitabmas']);
		$this->upload_files($id,'cv_web','id_cv','file_cv','assets/file/cv/','cv', $_FILES['cv_web']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_cv','file_cv','assets/file/cv/','cv', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFileCv($id,$kategori)
		];
		return $this->load->view('file-cv', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idcv = $this->input->get('idcv');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFileCvID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_cv'=>$id), 'file_cv');
		

		$data = [
			'tabel_file' => $this->Crud->readFileCv($idcv,$kategori)
		];
		return $this->load->view('file-cv', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readCvByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user'=>$result->id_user,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'id_cv'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' => $this->input->post('edit_dosen'),
			'semester' => $this->input->post('edit_semester'),
			'tahun' => $this->input->post('edit_tahun'),
		];

		$update = $this->Crud->update(array('id_cv'=>$id), 'tb_cv', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFileCvALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_cv'=>$id), 'tb_cv');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readCv();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterCv($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunCv($tahun);
		}else{
			$sorting = $this->Crud->sortAllCv($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-cv', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}