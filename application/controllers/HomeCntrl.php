<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data = [
			'title' => 'SIDIA | HOME',
			'date' => date('l, d-m-Y', strtotime("now"))
		];
		$this->load->view('home', $data);
	}

}