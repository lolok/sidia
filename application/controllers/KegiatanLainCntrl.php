<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KegiatanLainCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'PENDATAAN CV - Kegiatan Profesional Lainnya ',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		$this->load->view('kegiatan_lain', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readKegiatanLain()
		];
		return $this->load->view('tabel-kegiatan-lain', $data);
	}

	public function addData()
	{
		$data = [
			'id_kegiatan_lain' => $this->input->post('kegiatan_lain'),
			'id_user'          => $this->input->post('dosen'),
			'kegiatan' 		   => $this->input->post('kegiatan'),
			'institusi' 	   => $this->input->post('institusi'),
			'tempat_waktu' 	   => $this->input->post('tempat_waktu'),
		];

		$this->Crud->create('tb_kegiatan_lain',$data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readKegiatanLainByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user' 	   	   =>$result->id_user,
				'kegiatan'		   =>$result->kegiatan,
				'institusi'    	   =>$result->institusi,
				'tempat_waktu'	   =>$result->tempat_waktu,
				'id_kegiatan_lain' =>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' 		=> $this->input->post('editdosen'),
			'kegiatan'   	=> $this->input->post('editkegiatan'),
			'institusi'     => $this->input->post('editinstitusi'),
			'tempat_waktu'  => $this->input->post('edittempatwaktu'),
		];

		$update = $this->Crud->update(array('id_kegiatan_lain'=>$id), 'tb_kegiatan_lain', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_kegiatan_lain'=>$id), 'tb_kegiatan_lain');
	}
}