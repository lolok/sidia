<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PengajaranCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - PENGAJARAN',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('pengajaran', $data);
	}

	public function getTabel()
	{
		$level = $this->session->userdata('level');
		$id = $this->session->userdata('iduser');

		if ($level == 1) {
			$data = [
				'tabel' => $this->Crud->readPengajaran()
			];
		}else{
			$data = [
				'tabel' => $this->Crud->readBkdPengajaran($id)
			];
		}
		
		return $this->load->view('tabel-pengajaran', $data);
	}

	public function getDosen()
	{
		$data = $this->Crud->readDosen();

		foreach ($data->result() as $key) {
			$arr['id'] = $key->id_user;
            $arr['text'] = $key->nama;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getDosenId()
	{
		$id = $this->input->get('id');
		$data = $this->Crud->readDosenId($id);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->id_user;
            $arr['text'] = $key->nama;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getMatkul()
	{
		$data = $this->Crud->readMatkul();

		foreach ($data->result() as $key) {
			$arr['id'] = $key->id_matkul;
            $arr['text'] = $key->nama_matkul;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getMatkulId()
	{
		$id = $this->input->get('id');
		$data = $this->Crud->readMatkulId($id);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->id_matkul;
            $arr['text'] = $key->nama_matkul;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkas()
	{
		$data = $this->Crud->readDataBerkas(1,2);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		$arr1['id'] = 'Absensi Kehadiran';
		$arr1['text'] = 'Absensi Kehadiran';
		$arr1['kategori_file'] = 2;
		$return[] = array_merge($return,$arr1);

		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkasId()
	{
		$val = $this->input->post('val');
		$data = $this->Crud->readDataBerkasId(1,2,$val);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		$arr1['id'] = 'Absensi Kehadiran';
		$arr1['text'] = 'Absensi Kehadiran';
		$arr1['kategori_file'] = 2;
		$return[] = array_merge($return,$arr1);

		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getMatkulDosen()
	{
		$id = $this->input->get('id');
		$data = $this->Crud->readMatkulDosen($id);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->id_matkul;
            $arr['text'] = $key->nama_matkul;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getFileTable()
	{
		$id = $this->input->post('id');
		$file1 = $this->input->post('file1');
		$file2 = $this->input->post('file2');

		if ($file1 == 'Absensi Kehadiran') {
			$data = [
				'kontrak' => $this->Crud->readFilePengajaran($id,'kontrak'),
				'absendosen' => $this->Crud->readFilePengajaran($id,'absendosen'),
				'absenmhs' => $this->Crud->readFilePengajaran($id,'absenmhs'),
				'berkas' => $this->Crud->readFileBerkas1($file2),
			];
		}else if ($file2 == 'Absensi Kehadiran'){
			$data = [
				'kontrak' => $this->Crud->readFilePengajaran($id,'kontrak'),
				'absendosen' => $this->Crud->readFilePengajaran($id,'absendosen'),
				'absenmhs' => $this->Crud->readFilePengajaran($id,'absenmhs'),
				'berkas' => $this->Crud->readFileBerkas1($file1),
			];
		}else{
			$data = [
				'kontrak' => $this->Crud->readFilePengajaran($id,'kontrak'),
				'absendosen' => $this->Crud->readFilePengajaran($id,'absendosen'),
				'absenmhs' => $this->Crud->readFilePengajaran($id,'absenmhs'),
				'berkas' => $this->Crud->readFileBerkas($file1,$file2),
			];
		}
		
		return $this->load->view('file-berkas-pengajaran', $data);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_pengajaran','file_pengajaran','assets/file/tridarma/pengajaran/','pengajaran', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($id,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpengajaran = $this->input->get('idpengajaran');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePengajaranID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_pengajaran'=>$id), 'file_pengajaran');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePengajaran($idpengajaran,$kategori)
		];
		return $this->load->view('file-pengajaran', $data);
	}

	public function addData()
	{
		$data = [
			'id_matkul' => $this->input->post('matkul'),
			'id_user' => $this->input->post('dosen'),
			'jml_tatapmuka' => $this->input->post('jml_tatapmuka'),
			'total_tatapmuka' => $this->input->post('total_tatapmuka'),
			'masa_penugasan' => $this->input->post('masa_penugasan'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
			'id_berkas_penugasan' => $this->input->post('id_berkas_penugasan'),
			'id_berkas_dokumen' => $this->input->post('id_berkas_dokumen'),
		];

		$this->Crud->create('tb_pengajaran',$data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPengajaranByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_matkul'=>$result->id_matkul,
				'id_user'=>$result->id_user,
				'jml_tatapmuka'=>$result->jml_tatapmuka,
				'total_tatapmuka'=>$result->total_tatapmuka,
				'masa_penugasan'=>$result->masa_penugasan,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'id_pengajaran'=>$id,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_matkul' => $this->input->post('matkul'),
			'id_user' => $this->input->post('dosen'),
			'jml_tatapmuka' => $this->input->post('editjml_tatapmuka'),
			'total_tatapmuka' => $this->input->post('edittotal_tatapmuka'),
			'masa_penugasan' => $this->input->post('editmasa_penugasan'),
			'semester' => $this->input->post('editsemester'),
			'tahun' => $this->input->post('edittahun'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
			'id_berkas_penugasan' => $this->input->post('id_berkas_penugasan'),
			'id_berkas_dokumen' => $this->input->post('id_berkas_dokumen'),
		];

		$update = $this->Crud->update(array('id_pengajaran'=>$id), 'tb_pengajaran', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$delete = $this->Crud->delete(array('id_pengajaran'=>$id), 'tb_pengajaran');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPengajaran();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPengajaran($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPengajaran($tahun);
		}else{
			$sorting = $this->Crud->sortAllPengajaran($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-pengajaran', $data);
	}

	public function getBkd()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPengajaran($iddosen)
		];

		return $this->load->view('tabel-bkd-pengajaran', $data);
	}

	public function getPrintBkd()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'pengajaran' => $this->Crud->readBkdPengajaran($iddosen),
			'pembimbingan' => $this->Crud->readBkdPembimbingan($iddosen,1),
			'pengujian' => $this->Crud->readBkdPembimbingan($iddosen,2),
		];

		return $this->load->view('tabel-bkd-print-pendidikan', $data);
	}

	public function sortingBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readBkdPengajaran($iddosen);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortBkdSemesterPengajaran($semester,$iddosen);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortBkdTahunPengajaran($tahun,$iddosen);
		}else{
			$sorting = $this->Crud->sortBkdAllPengajaran($tahun,$semester,$iddosen);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-bkd-pengajaran', $data);
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array('id_pengajaran'=>$id), 'tb_pengajaran', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}