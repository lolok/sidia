<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('Crud');
	}

	public function index(){
		$data = [
			'title' => 'SIDIA | DATA SIDIA',
			'date' => date('l, d-m-Y', strtotime("now"))
		];
		$this->load->view('data', $data);
	}

	public function tridarma(){
		$data = [
			'title' => 'SIDIA | TRIDARMA PT',
			'date' => date('l, d-m-Y', strtotime("now"))
		];
		$this->load->view('tridarma', $data);
	}

}