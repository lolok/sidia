<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PublikasiCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - Publikasi',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('publikasi', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPublikasi()
		];
		return $this->load->view('tabel-publikasi', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePublikasi($id,$kategori)
		];
		return $this->load->view('file-publikasi', $data);
	}

	public function addData()
	{
		$data = [
			'id_publikasi'   => $this->input->post('publikasi'),
			'id_user'        => $this->input->post('dosen'),
			'nama_publikasi' => $this->input->post('nama_publikasi'),
			'link'           => $this->input->post('link'),
			'semester' 		 => $this->input->post('semester'),
			'tahun'    		 => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_publikasi',$data);

		$id = $this->Crud->readLast('tb_publikasi','id_publikasi');

		$this->upload_files($id,'penugasan','id_publikasi','file_publikasi','assets/file/tridarma/publikasi/','publikasi', $_FILES['penugasan']);
		$this->upload_files($id,'paper','id_publikasi','file_publikasi','assets/file/tridarma/publikasi/','publikasi', $_FILES['paper']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_publikasi','file_publikasi','assets/file/tridarma/publikasi/','publikasi', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePublikasi($id,$kategori)
		];
		return $this->load->view('file-publikasi', $data);
	}

	public function hapusFile()
	{
		$id 		 = $this->input->get('id');
		$idpublikasi = $this->input->get('idpublikasi');
		$kategori 	 = $this->input->get('kategori');

		$data = $this->Crud->readFilePublikasiID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_publikasi'=>$id), 'file_publikasi');
		
		$data = [
			'tabel_file' => $this->Crud->readFilePublikasi($idpublikasi,$kategori)
		];
		return $this->load->view('file-publikasi', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPublikasiByID($id);
		foreach($query->result() as $result){
			$data = [
				'id_user' 		=>$result->id_user,
				'nama_publikasi'=>$result->nama_publikasi,
				'link'    		=>$result->link,
				'tahun'	  		=>$result->tahun,
				'semester'		=>$result->semester,
				'id_publikasi'  =>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'id_user' 		 => $this->input->post('editdosen'),
			'nama_publikasi' => $this->input->post('editnama'),
			'link' 			 => $this->input->post('editlink'),
			'semester' 		 => $this->input->post('editsemester'),
			'tahun' 		 => $this->input->post('edittahun'),
		];

		$update = $this->Crud->update(array('id_publikasi'=>$id), 'tb_publikasi', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePublikasiALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_publikasi'=>$id), 'tb_publikasi');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPublikasi();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPublikasi($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPublikasi($tahun);
		}else{
			$sorting = $this->Crud->sortAllPublikasi($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-publikasi', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }

}