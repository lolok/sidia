<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PenunjangCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 1990;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'TRI DHARMA PT - Penunjang',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('penunjang', $data);
	}

	public function ebkd()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'E-BKD - Penunjang',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'matkul' => $this->Crud->read('tb_matkul',null,null,null),
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('bkdpenunjang', $data);
	}

	public function getBkd()
	{
		$iddosen = $this->input->get('iddosen');

		$data = [
			'tabel' => $this->Crud->readBkdPenunjang($iddosen)
		];

		return $this->load->view('tabel-bkd-penunjang', $data);
	}

	public function sortingBkd()
	{
		$iddosen = $this->input->get('iddosen');
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readBkdPenunjang($iddosen);
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortBkdSemesterPenunjang($semester,$iddosen);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortBkdTahunPenunjang($tahun,$iddosen);
		}else{
			$sorting = $this->Crud->sortBkdAllPenunjang($tahun,$semester,$iddosen);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-bkd-penunjang', $data);
	}

	public function getStatusBkd()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = [
			'status_ebkd' => $status,
		];

		$update = $this->Crud->update(array('id_penunjang'=>$id), 'tb_penunjang', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPenunjang()
		];
		return $this->load->view('tabel-penunjang', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePenunjang($id,$kategori)
		];
		return $this->load->view('file-penunjang', $data);
	}

	public function getFileBerkasTable()
	{
		$id = $this->input->post('id');
		$file1 = $this->input->post('file1');
		$file2 = $this->input->post('file2');

		$data = [
			'berkas' => $this->Crud->readFileBerkas($file1,$file2),
		];

		return $this->load->view('file-berkas-data', $data);
	}

	public function getBerkas()
	{
		$data = $this->Crud->readDataBerkas(1,6);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function getBerkasId()
	{
		$val = $this->input->post('val');
		$data = $this->Crud->readDataBerkasId(1,6,$val);

		foreach ($data->result() as $key) {
			$arr['id'] = $key->nama_file;;
            $arr['text'] = $key->nama_file;
            $arr['kategori'] = $key->kategori_file;
            $return[] = $arr;
		}
		
		header('Content-Type: application/json');
		echo json_encode($return);
	}

	public function addData()
	{
		$data = [
			'kategori_id' => $this->input->post('kategori_id'),
			'jenis_id' => $this->input->post('jenis_id'),
			'anggota_id' => $this->input->post('anggota_id'),
			'kategori' => $this->input->post('kategori'),
			'jenis' => $this->input->post('jenis'),
			'sks' => $this->input->post('sks'),
			'status_anggota' => $this->input->post('status_anggota'),
			'masa_penugasan' => $this->input->post('masa_penugasan'),
			'bukti_penugasan' => $this->input->post('bukti_penugasan'),
			'bukti_dokumen' => $this->input->post('bukti_dokumen'),
			'jml_mhs' => $this->input->post('jml_mhs'),
			'jml_dosen' => $this->input->post('jml_dosen'),
			'nampen' => $this->input->post('nampen'),
			'id_user' => $this->input->post('dosen'),
			'semester' => $this->input->post('semester'),
			'tahun' => $this->input->post('tahun'),
		];

		$this->Crud->create('tb_penunjang',$data);

		$id = $this->Crud->readLast('tb_penunjang','id_penunjang');

		$this->upload_files($id,'skpen','id_penunjang','file_penunjang','assets/file/tridarma/penunjang/','penunjang', $_FILES['filekontrak']);
		$this->upload_files($id,'skpen','id_penunjang','file_penunjang','assets/file/tridarma/penunjang/','penunjang', $_FILES['absendosen']);
		$this->upload_files($id,'skpen','id_penunjang','file_penunjang','assets/file/tridarma/penunjang/','penunjang', $_FILES['absenmhs']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_penunjang','file_penunjang','assets/file/tridarma/penunjang/','penunjang', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePenunjang($id,$kategori)
		];
		return $this->load->view('file-penunjang', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$id_penunjang = $this->input->get('id_penunjang');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePenunjangID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_penunjang'=>$id), 'file_penunjang');
		

		$data = [
			'tabel_file' => $this->Crud->readFilepenunjang($id_penunjang,$kategori)
		];
		return $this->load->view('file-penunjang', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');

		$query = $this->Crud->readPenunjangByID($id);
		foreach($query->result() as $result){
			$data = [
				'kategori'=>$result->kategori_id,
				'jenis'=>$result->jenis_id,
				'status_anggota'=>$result->anggota_id,
				'masa_penugasan'=>$result->masa_penugasan,
				'bukti_dokumen'=>$result->bukti_dokumen,
				'bukti_penugasan'=>$result->bukti_penugasan,
				'jml_mhs'=>$result->jml_mhs,
				'jml_dosen'=>$result->jml_dosen,
				'nampen'=>$result->nampen,
				'id_user'=>$result->id_user,
				'semester'=>$result->semester,
				'tahun'=>$result->tahun,
				'id_penunjang'=>$id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'kategori_id' => $this->input->post('kategori_id'),
			'jenis_id' => $this->input->post('jenis_id'),
			'anggota_id' => $this->input->post('anggota_id'),
			'kategori' => $this->input->post('kategori'),
			'jenis' => $this->input->post('jenis'),
			'sks' => $this->input->post('sks'),
			'status_anggota' => $this->input->post('status_anggota'),
			'masa_penugasan' => $this->input->post('editmasa_penugasan'),
			'bukti_penugasan' => $this->input->post('editbukti_penugasan'),
			'bukti_dokumen' => $this->input->post('editbukti_dokumen'),
			'jml_mhs' => $this->input->post('editjml_mhs'),
			'jml_dosen' => $this->input->post('editjml_dosen'),
			'nampen' => $this->input->post('editnampen'),
			'id_user' => $this->input->post('editdosen'),
			'semester' => $this->input->post('editsemester'),
			'tahun' => $this->input->post('edittahun'),
		];

		$update = $this->Crud->update(array('id_penunjang'=>$id), 'tb_penunjang', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilepenunjangALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_penunjang'=>$id), 'tb_penunjang');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPenunjang();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPenunjang($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPenunjang($tahun);
		}else{
			$sorting = $this->Crud->sortAllPenunjang($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-penunjang', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$title.'-'.gmdate("dmyHis", time()+3600*7).'-'.$i;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}