<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PersonalCntrl extends MY_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->helper('file');
		$this->load->model('Crud');
		$this->load->library('upload');
	}

	public function index()
	{
		$yearnow = (int)date('Y', strtotime('now'));
		$yearstart = 2016;
		$diff = $yearnow-$yearstart;
		for ($i=0; $i <= $diff; $i++) { 
			$tahun = $yearnow-$i;
			$year[] = (object)[
				'year' => $tahun,
			];
		}
		$data = [
			'title' => 'SIDIA - PERSONAL',
			'date' => date('l, d-m-Y', strtotime("now")),
			'tahun' => $year,
			'dosen' => $this->Crud->read('tb_user',['level' => 2],null,null)
		];
		return $this->load->view('personal', $data);
	}

	public function getTabel()
	{
		$data = [
			'tabel' => $this->Crud->readPersonal()
		];
		return $this->load->view('tabel-personal', $data);
	}

	public function getFileTable()
	{
		$id = $this->input->get('id');
		$kategori = $this->input->get('kategori');

		$data = [
			'tabel_file' => $this->Crud->readFilePersonal($id,$kategori)
		];
		return $this->load->view('file-personal', $data);
	}

	public function addData()
	{
		$data = [
			'nama' => $this->input->post('nama'),
			'nosertif' => $this->input->post('nosertif'),
			'perting' => $this->input->post('perting'),
			'status' => $this->input->post('status'),
			'alamat' => $this->input->post('alamat'),
			'jurusan' => $this->input->post('jurusan'),
			'prodi' => $this->input->post('prodi'),
			'jabatan' => $this->input->post('jabatan'),
			'tempat' => $this->input->post('tempat'),
			's1' => $this->input->post('s1'),
			's2' => $this->input->post('s2'),
			's3' => $this->input->post('s3'),
			'ilmu' => $this->input->post('ilmu'),
			'nohape' => $this->input->post('nohape'),
			'tanggal' => $this->input->post('tanggal'),
			'tahun' => $this->input->post('tahun'),
			'semester' => $this->input->post('semester'),
			'id_user' => $this->input->post('dosen'),
		];

		$this->Crud->create('tb_personal',$data);

		$id = $this->Crud->readLast('tb_personal','id_personal');
		$idu = $this->input->post('dosen');

		$this->upload_files($id,'berkas','id_personal','file_personal','assets/file/personal/','personal', $_FILES['berkasdosen']);
	}

	public function addFile()
	{
		$id = $this->input->post('id');
		$kategori = $this->input->post('kategori');

		$this->upload_files($id,$kategori,'id_personal','file_personal','assets/file/personal/','personal', $_FILES['fileberkas']);

		$data = [
			'tabel_file' => $this->Crud->readFilePersonal($id,$kategori)
		];
		return $this->load->view('file-personal', $data);
	}

	public function hapusFile()
	{
		$id = $this->input->get('id');
		$idpersonal = $this->input->get('idpersonal');
		$kategori = $this->input->get('kategori');

		$data = $this->Crud->readFilePersonalID($id);

		foreach ($data->result() as $key) {
			$path = $key->path_file;
		}
		unlink($path);

		$delete = $this->Crud->delete(array('id_file_personal'=>$id), 'file_personal');
		

		$data = [
			'tabel_file' => $this->Crud->readFilePersonal($idpersonal,$kategori)
		];
		return $this->load->view('file-personal', $data);
	}

	public function getData()
	{
		$id = $this->input->get('id');
		$query = $this->Crud->readPersonalByID($id);
		foreach($query->result() as $result){
			$data = [
			'nama' => $result->nama,
			'nosertif' => $result->nosertif,
			'perting' => $result->perting,
			'status' => $result->status,
			'alamat' => $result->alamat,
			'jurusan' => $result->jurusan,
			'prodi' => $result->prodi,
			'jabatan' => $result->jabatan,
			'tempat' => $result->tempat,
			's1' => $result->s1,
			's2' => $result->s2,
			's3' => $result->s3,
			'ilmu' => $result->ilmu,
			'nohape' => $result->nohape,
			'tanggal' => $result->tanggal,
			'tahun' => $result->tahun,
			'semester' => $result->semester,
			'id_user' => $result->id_user,
			'id_personal' => $id,
			];
		}

		header('Content-Type: application/json');
		echo json_encode($data);
	}

	public function editData()
	{
		$id = $this->input->post('id');

		$data = [
			'nama' => $this->input->post('editnama_personal'),
			'nosertif' => $this->input->post('editnosertif'),
			'perting' => $this->input->post('editperting'),
			'status' => $this->input->post('editstatus'),
			'alamat' => $this->input->post('editalamat'),
			'jurusan' => $this->input->post('editjurusan'),
			'prodi' => $this->input->post('editprodi'),
			'jabatan' => $this->input->post('editjabatan'),
			'tempat' => $this->input->post('edittempat'),
			's1' => $this->input->post('edits1'),
			's2' => $this->input->post('edits2'),
			's3' => $this->input->post('edits3'),
			'ilmu' => $this->input->post('editilmu'),
			'nohape' => $this->input->post('editnohape'),
			'tanggal' => $this->input->post('edittanggal'),
			'tahun' => $this->input->post('edittahun'),
			'semester' => $this->input->post('editsemester'),
			'id_user' => $this->input->post('editdosen'),
		];

		$update = $this->Crud->update(array('id_personal'=>$id), 'tb_personal', $data);
		if($update){
			echo 1;
		}else{
			echo 2;
		}
	}

	public function hapusData()
	{
		$id = $this->input->get('id');

		$data = $this->Crud->readFilePersonalALL($id);
		if(!empty($data)){
			foreach ($data->result() as $key) {
				unlink($key->path_file);
			}
		}

		$delete = $this->Crud->delete(array('id_personal'=>$id), 'tb_personal');
	}

	public function sorting()
	{
		$tahun = $this->input->get('tahun');
		$semester = $this->input->get('semester');

		if($semester == 'all' && $tahun == 'all'){
			$sorting = $this->Crud->readPersonal();
		}else if ($tahun == 'all') {
			$sorting = $this->Crud->sortSemesterPersonal($semester);
		}else if($semester == 'all'){
			$sorting = $this->Crud->sortTahunPersonal($tahun);
		}else{
			$sorting = $this->Crud->sortAllPersonal($tahun,$semester);
		}
		$data = [
			'tabel' => $sorting
		];

		return $this->load->view('tabel-personal', $data);
	}

	private function upload_files($valueid,$kategori,$id,$table, $path, $title, $files)
    {
        $config = array(
            'upload_path'   => './'.$path,
            'allowed_types' => 'pdf|gif|jpeg|png|jpg|docx|doc|images',
            'overwrite'     => 1,    
            'max_size'		=> '5048000'               
        );

        $this->load->library('upload', $config);

        $images = array();
        $i=0;
        foreach ($files['name'] as $key => $image) {
        	$i++;
            $_FILES['images[]']['name']= $files['name'][$key];
            $_FILES['images[]']['type']= $files['type'][$key];
            $_FILES['images[]']['tmp_name']= $files['tmp_name'][$key];
            $_FILES['images[]']['error']= $files['error'][$key];
            $_FILES['images[]']['size']= $files['size'][$key];

            $fileName = gmdate("d-m-y-H-i-s", time()+3600*7).'-'.$title;
            $name = $kategori.'-'.$valueid;

            $images[] = $name;

            $config['file_name'] = $name;

            $ext = pathinfo($files['name'][$key], PATHINFO_EXTENSION);

            $this->upload->initialize($config);

            if ($this->upload->do_upload('images[]')) {
                $this->upload->data();
            } else {
                return false;
            }
            $data = [
            	'nama_file' => $name,
            	'path_file' => $path.$name.'.'.$ext,
            	$id => $valueid,
            	'kategori_file' => $kategori
            ];

            $this->Crud->create($table,$data);
        }

        return true;
    }
}