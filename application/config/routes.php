<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'LarademCntrl';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//routing
//laradem
$route['home'] = 'LarademCntrl/index';
$route['about'] = 'LarademCntrl/aboutus';
$route['news'] = 'LarademCntrl/news';
$route['ourwork'] = 'LarademCntrl/ourwork';
$route['publication'] = 'LarademCntrl/publication';
$route['contactus'] = 'LarademCntrl/contactus';
//sidia
$route['sidia'] = 'SidiaCntrl/index';
//auth
$route['logout'] = 'SidiaCntrl/logoutProcess';
$route['login'] = 'SidiaCntrl/loginProcess';
//menu
$route['dashboard'] = 'HomeCntrl/index';
//administrasi
$route['administrasi'] = 'AdminCntrl/index';
$route['administrasi/matakuliah'] = 'MatkulCntrl/index';
$route['administrasi/dosen'] = 'DosenCntrl/index';
$route['administrasi/ruang'] = 'RuangCntrl/index';
$route['administrasi/kelas'] = 'KelasCntrl/index';
//sidia
$route['data'] = 'DataCntrl/index';
$route['data/kerjasama'] = 'KerjasamaCntrl/index';
$route['data/personal'] = 'PersonalCntrl/index';
$route['data/datadiri'] = 'DatadiriCntrl/index';
$route['data/riwayat'] = 'RiwayatCntrl/index';
$route['data/riwayat/dosen'] = 'RiwayatCntrl/getTabel';
$route['data/berkas'] = 'BerkasCntrl/index';
$route['data/cv'] = 'CvCntrl/index';
$route['data/tridarma'] = 'DataCntrl/tridarma';
$route['data/tridarma/pengajaran'] = 'PengajaranCntrl/index';
$route['data/tridarma/pembimbingan'] = 'PembimbingCntrl/index';
$route['data/tridarma/penelitian'] = 'PenelitianCntrl/index';
$route['data/tridarma/pkm'] = 'PkmCntrl/index';
$route['data/tridarma/publikasi'] = 'PublikasiCntrl/index';
$route['data/tridarma/penunjang'] = 'PenunjangCntrl/index';
$route['data/tridarma/kegiatan'] = 'Team/index';
//ebkd
$route['ebkd'] = 'EbkdCntrl/index';
$route['ebkd/pendidikan'] = 'PendidikanCntrl/index';
$route['ebkd/penelitian'] = 'PenelitianCntrl/ebkd';
$route['ebkd/pengabdian'] = 'TeamCntrl/ebkd';
$route['ebkd/penunjang'] = 'PenunjangCntrl/ebkd';
$route['ebkd/print'] = 'EbkdCntrl/print';
//web
$route['web'] = 'WebCntrl/index';
$route['web/profile'] = 'WebCntrl/profile';
$route['web/publikasi'] = 'WebCntrl/publikasi';
//Info
$route['info'] = 'InfoCntrl/index';
$route['info/ongoing'] = 'InfoCntrl/index';
$route['gallery'] = 'GalleryCntrl/index';
$route['info/team'] = 'TeamCntrl/index';
