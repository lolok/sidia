$(document).ready(function(){

	var total_member = 1;

	function tambahMember(){
		var n = total_member + 1;

		var isi = '<div class="member" id="tambahmember'+total_member+'" style="display:none">';

		isi += '<hr>';

		isi += '<div class="form-group"><input type="text" class="form-control" name="matkul'+n+'" required ></div>';

		isi += '</div>';

		$('a.tambahmember').before(isi);
		$('#tambahmember'+total_member).slideDown('medium');

		total_member++;
	}

	function hapusMember(){
		total_member--;
		if(total_member<=1){
			total_member = 1;
		}
		$('#tambahmember'+total_member).slideUp('medium', function(){
			$(this).remove();
		});
	}

	$('a.tambahmember').click(function(){
		tambahMember();
	});

	$('a.hapusmember').click(function(){
		hapusMember();
	});

});